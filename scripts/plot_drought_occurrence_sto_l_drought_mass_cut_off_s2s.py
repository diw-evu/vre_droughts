# -*- coding: utf-8 -*-
"""
Created on Sat Jul 27 18:41:21 2024

@author: mkittel
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Jul 20 14:23:44 2024

@author: mkittel
"""

import os
import multiprocess as mp
import pandas as pd
import numpy as np
import pickle
import psutil
import gzip
from pathlib import Path
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.io as pio
import datetime
import webbrowser


def load_object(path, file):
    '''
    For debugging: Loads object from pickle.
    '''
    cwd = os.getcwd()
    os.chdir(path)
    with gzip.open(file, 'rb') as file:
        obj = pickle.load(file)
    os.chdir(cwd)
    return obj


# settings
os_plotting = 'local'
cut_off = 0.75  # 0.65, 0.7
dm_filter = 1 #, 12, 18, 24, 48]
dm_filter_ts = 12
corrections = ['s2s-yearly', 's2s-winter']
path_vreda_output_win = 'T:\\mk\\vreda\\output\\2024-08-02_08-42-30_mbt_event\\'
path_vreda_output_linux = '/home/mkittel/tsla-tde/mk/vreda/output/2024-08-02_08-42-30_mbt_event/'
sel = ['AT', 'DE', 'ES', 'FR', 'NL', 'PL', 'RO', 'UK']
flat_dem_switch = False
demand_switch = 2009  # 'yearly'

if os_plotting == 'local':
    firefox_path = 'C:\\Program Files\\Mozilla Firefox\\firefox.exe'
else:
    firefox_path = '/bin/firefox'
webbrowser.register('firefox', None, webbrowser.BackgroundBrowser(firefox_path))
pio.renderers.default = 'firefox'  # 'browser'

# import storage level results
print('Importing storage level data.')
if os_plotting == 'local':
    if demand_switch == 2009:
        path = 'S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_data\\2024-10-31_h2_sto_l_all.csv'
    elif demand_switch == 'yearly':
        path = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-10-31_h2_sto_l_all.csv"
else:
    path = '/home/mkittel/tsla-tde/mk/vreda/input/h2_sto_l.csv'
sto_l = pd.read_csv(path)

# prepare copper plate data, accumulate all sto levels for copperplate, normalize to 100% for each year
sto_l_cp = sto_l.query('custom_flow == "copperplate-eh"')
sto_l_cp = sto_l_cp[['n', 'h', 'value', 'custom_year']]
sto_l_cp = sto_l_cp.groupby(['h', 'custom_year'])['value'].sum().reset_index()
# sto_l_cp['value'] = sto_l_cp.groupby('custom_year')['value'].transform(lambda x: x / x.max()) * 100

# prepare country data, normalize to 100% for each combo custom_year-country
sto_l = sto_l.query('n in @sel and custom_flow != "copperplate-eh"')
# sto_l['value'] = sto_l.groupby(['custom_year', 'n'])['value'].transform(lambda x: x / x.max()) * 100

if flat_dem_switch:
    
    # import storage level results for flat demand data
    print('Importing storage level data.')
    if os_plotting == 'local':
        path = 'S:\\projekte_paper\\vre-drought-mass\\result_data\\h2_sto_l_flat_dem.csv'
    else:
        path = '/home/mkittel/tsla-tde/mk/vreda/input/h2_sto_l_flat_dem.csv'
    sto_l_flat = pd.read_csv(path)
    scenarios = {'island-e': 'island', 'TYNDP-e': 'TYNDP', 'copperplate-e': 'copperplate'}
    sto_l_flat = sto_l_flat.replace(scenarios)

    # prepare copper plate data, accumulate all sto levels for copperplate, normalize to 100% for each year
    sto_l_flat_cp = sto_l_flat.query('custom_flow == "copperplate"')
    sto_l_flat_cp = sto_l_flat_cp[['n', 'h', 'value', 'custom_year']]
    sto_l_flat_cp = sto_l_flat_cp.groupby(['h', 'custom_year'])['value'].sum().reset_index()
    sto_l_flat_cp['value'] = sto_l_flat_cp.groupby('custom_year')['value'].transform(lambda x: x / x.max()) * 100

    # prepare country data, normalize to 100% for each combo custom_year-country
    sto_l_flat = sto_l_flat.query('n in @sel and custom_flow != "copperplate"')
    sto_l_flat['value'] = sto_l_flat.groupby(['custom_year', 'n'])['value'].transform(lambda x: x / x.max()) * 100    

# import demand time series
print('Importing demand data.')
if os_plotting == 'local':
    if demand_switch == 2009:
        path = "S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_data\\demand_all_2024-10-31.csv"
    elif demand_switch == 'yearly':
        path = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-10-31_demand_all.csv"
else:
    path = '/home/mkittel/tsla-tde/mk/vreda/input/demand.csv'
dem = pd.read_csv(path)

# retrieving peak demand and computing moving average over one week
dem_max = dem.groupby(['n', 'custom_flow', 'custom_year'])['value'].max().reset_index()
dem = dem.sort_values(by=['custom_year', 'h'])
grouped_dem = dem.groupby(['custom_flow', 'n'])
dem['value'] = grouped_dem['value'].transform(lambda x: x.rolling(window=168, min_periods=1).mean())
dem = dem.sort_values(by=['n', 'custom_flow', 'custom_year', 'h'])

# prepare copper plate data, accumulate all demands for copperplate, normalize to 100% for each year
dem_cp = dem.query('custom_flow == "copperplate-eh"')
dem_cp = dem_cp[['n', 'h', 'value', 'custom_year']]
dem_cp = dem_cp.groupby(['h', 'custom_year'])['value'].sum().reset_index()
dem_cp_max = dem_cp.groupby(['custom_year'])['value'].max().reset_index()
# dem_cp['value'] = dem_cp.groupby('custom_year')['value'].transform(lambda x: x / x.max()) * 100

# prepare country data, normalize to 100% for each combo custom_year-country
dem = dem.query('n in @sel and custom_flow != "copperplate-eh"')
# dem['value'] = dem.groupby(['custom_year', 'n'])['value'].transform(lambda x: x / x.max()) * 100

if flat_dem_switch:
    
    # import flat demand time series
    print('Importing flat demand data.')
    if os_plotting == 'local':
        path = 'S:\\projekte_paper\\vre-drought-mass\\result_data\\demand_flat_dem.csv'
    else:
        path = '/home/mkittel/tsla-tde/mk/vreda/input/demand_flat_dem.csv'
    dem_flat = pd.read_csv(path)
    scenarios = {'island-e': 'island', 'TYNDP-e': 'TYNDP', 'copperplate-e': 'copperplate'}
    dem_flat = dem_flat.replace(scenarios)
    
    # normalize flat demand by dividing by max demand value from variable demand
    dem_flat = pd.merge(dem_flat, dem_max, on=['n', 'custom_flow', 'custom_year'], suffixes=('_flat', '_max'))
    dem_flat['normalized_dem'] = (dem_flat['value_flat'] / dem_flat['value_max']) * 100
    
    # prepare copper plate data, accumulate all sto levels for copperplate, normalize to 100% for each year
    dem_flat_cp = dem_flat.query('custom_flow == "copperplate"')
    dem_flat_cp = dem_flat_cp[['n', 'h', 'value_flat', 'custom_year']]
    dem_flat_cp = dem_flat_cp.groupby(['h', 'custom_year'])['value_flat'].sum().reset_index()
    dem_flat_cp = pd.merge(dem_flat_cp, dem_cp_max, on=['custom_year'])
    dem_flat_cp = dem_flat_cp.rename(columns={'value': 'value_max'})
    dem_flat_cp['normalized_dem'] = (dem_flat_cp['value_flat'] / dem_flat_cp['value_max']) * 100

sel.insert(0, 'CP')

# create plots
print(f'Start plotting of time series filtered for event longer than {dm_filter} hours.')

# import drought occurrences
print('Importing drought occurrence data.')
if os_plotting == 'local':
    path = path_vreda_output_win
else:
    path = path_vreda_output_linux
drought_occurence_portfolio = load_object(path, f'drought_occurence_portfolio_{dm_filter_ts}.gz')

drought_occurence_portfolio = drought_occurence_portfolio.query('region in @sel')
drought_occurence_portfolio['drought_indicator'] = drought_occurence_portfolio['drought_indicator'].replace(0, np.nan)
drought_occurence_portfolio['threshold'] = drought_occurence_portfolio['threshold'].apply(lambda x: "{:.2f}".format(float(x.split('=')[1])))
excluded = [] #'1.00'
drought_occurence_portfolio = drought_occurence_portfolio.query('threshold not in @excluded')

# import drought mass results
print('Importing drought mass data.')
if os_plotting == 'local':
    path = os.path.join(path_vreda_output_win, f'drought_mass_filter_{dm_filter}',
                        'drought_mass_s2s')
else:
    path = os.path.join(path_vreda_output_linux, f'drought_mass_filter_{dm_filter}',
                        'drought_mass_s2s')
mass_single_event_max_all_years_corrections = {}
for correction in corrections:
    path_import = os.path.join(path, correction)
    file = 'mass_single_event_max_all_years.gz'
    mass_single_event_max_all_years = load_object(path_import, file)
    mass_single_event_max_all_years = mass_single_event_max_all_years.query('region in @sel')
    mass_single_event_max_all_years_corrections[correction] = mass_single_event_max_all_years

# define years and colors
year_pairs = [[i, i+1] for i in range(1996, 1997)]

# customize colors
custom_colors_extended = [
    '#4a006e',  # Dark Violet
    '#611785',  # Between Dark Violet and Violet
    '#78209b',  # Violet
    # '#7f104c',  # Transition between Violet and Dark Red
    '#840826',  # New Color (Enhanced Violet to Dark Red Transition)
    '#880000',  # Dark Red
    '#a41100',  # Between Dark Red and Light Red
    '#c62200',  # Light Red
    '#e84013',  # Between Light Red and Dark Orange
    '#f95927',  # Dark Orange
    '#fc6f20',  # New Color (Enhanced Red to Orange Transition)
    '#ff851a',  # Between Dark Orange and Light Orange
    '#ffa72d',  # Light Orange
    '#ffcc2a',  # Between Light Orange and Yellow
    '#ffdf2d',  # Yellow
    '#d0d830',  # New Color (Enhanced Yellow to Green Transition)
    '#a0d034',  # Between Yellow and Light Green
    '#7cb342',  # Light Green
    '#05723c',  # Dark Green
    '#022915',  # New Color (Enhanced Light Green to Dark Green Transition)
]

for yy in year_pairs:

    print(f'Plotting started for {yy[0]}-{yy[1]}.')

    drought_occurrence_sel = drought_occurence_portfolio.query('year in @yy')

    # get number of thresholds
    thresholds = sorted(list(drought_occurrence_sel.threshold.unique()))

    no_col = len(custom_colors_extended) - len(thresholds)
    custom_colors_extended_sel = custom_colors_extended[no_col:]

    row_heights = [
        # 0.04, 0.07,  # for 9 regions
        # 0.04, 0.06,  # for 10 regions
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07
        ]

    row_no = len(row_heights)
    spec_sec_y = [[{"secondary_y": (i % 3 == 1)}] for i in range(row_no)]
    fig = make_subplots(rows=row_no, cols=1, row_heights=row_heights, shared_xaxes=True,
                        vertical_spacing=0.01, specs=spec_sec_y)
    # fig.print_grid()

    # plot drought ts
    regions = []
    row_counter = 1

    for region in sel:

        group_drought_region = drought_occurrence_sel.query('region == @region')

        regions.append(region)
        regions.append(region)
        regions.append(region)

        for idx, (threshold, group_drought_threshold) in enumerate(group_drought_region.groupby('threshold')):
            
            # reduce to s2s
            group_drought_threshold['hour_total'] = range(len(group_drought_threshold))
            group_drought_threshold = group_drought_threshold.query('hour_total >= 4344 and hour_total < 13104')

            # with legend
            if row_counter == 1:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_drought_threshold.drought_indicator,
                        marker=dict(size=1.5, color=custom_colors_extended[idx]),
                        name=threshold,
                        mode='markers',
                        legend='legend1',
                        # legendgrouptitle=dict(text='relative thresholds')
                    ),
                    row=row_counter, col=1,
                )

            # without legend
            else:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_drought_threshold.drought_indicator,
                        marker=dict(size=1.5, color=custom_colors_extended[idx]),
                        name=threshold,
                        mode='markers',
                        showlegend=False
                    ),
                    row=row_counter, col=1,
                )
        row_counter += 3

    # plot identified max period
    time_frame = f'{yy[0]}-{yy[1]}'
    row_counter = 1

    for region in sel:

        # identified events across all year
        start_all_year = mass_single_event_max_all_years_corrections[corrections[0]].query(
            'region == @region and time_frame == @time_frame')['start'].iloc[0][0] - pd.Timedelta(hours=30)
        end_all_year = mass_single_event_max_all_years_corrections[corrections[0]].query(
            'region == @region and time_frame == @time_frame')['end'].iloc[0][0] + pd.Timedelta(hours=30)

        # identified winter events
        start_winter = mass_single_event_max_all_years_corrections[corrections[1]].query(
            'region == @region and time_frame == @time_frame')['start'].iloc[0][0] - pd.Timedelta(hours=30)
        end_winter = mass_single_event_max_all_years_corrections[corrections[1]].query(
            'region == @region and time_frame == @time_frame')['end'].iloc[0][0] + pd.Timedelta(hours=30)

        # draw one event box if identified events match
        if start_all_year == start_winter:
            if end_all_year == end_winter:
                fig.add_vrect(x0=start_all_year, x1=end_all_year, row=row_counter, col=1,
                              # annotation_text="decline", annotation_position="top left",
                              fillcolor='#326776', opacity=0.38, line_width=0)

        # draw two event boxes if identified event do not match
        else:
            fig.add_vrect(x0=start_winter, x1=end_winter, row=row_counter, col=1,
                          #annotation_text="winter", annotation_position="bottom",
                          fillcolor='#326776', opacity=0.38, line_width=0)
            fig.add_vrect(x0=start_all_year, x1=end_all_year, row=row_counter, col=1,
                          #annotation_text="all year", annotation_position="bottom",
                          fillcolor='gray', opacity=0.38, line_width=0)

        row_counter += 3

    # add storage SOC CP
    if flat_dem_switch:
        sto_l_sel = sto_l_flat_cp.query('custom_year == @time_frame')
    else:
        sto_l_sel = sto_l_cp.query('custom_year == @time_frame')

    fig.add_trace(
        go.Scatter(
            x=group_drought_threshold.index,
            y=sto_l_sel.value,
            line=dict(color='teal', width=1.5),
            mode='lines',
            name='copperplate-eh',
            legend='legend2',
            # legendgrouptitle=dict(text='storage level scenarios')
        ),
        row=2, col=1, secondary_y=False
    )

    # add demand CP
    if flat_dem_switch:
        dem_sel = dem_flat_cp.query('custom_year == @time_frame')
        dem_sel = dem_sel.rename(columns={'normalized_dem': 'value'})
    else:
        dem_sel = dem_cp.query('custom_year == @time_frame')

    fig.add_trace(
        go.Scatter(
            x=group_drought_threshold.index,
            y=dem_sel.value,
            line=dict(color='gray', width=1),
            mode='lines',
            name='all scenarios',
            legend='legend3',
            # legendgrouptitle=dict(text='storage level scenarios')
        ),
        row=2, col=1, secondary_y=True
    )

    # add storage SOC countries
    scens = ['island-eh', 'TYNDP-e/island-h', 'TYNDP-eh']
    sto_colors = ['black', 'rosybrown', 'darkorange']

    sel.remove('CP')

    for scen_idx, scen in enumerate(scens):

        row_counter = 5

        if flat_dem_switch:
            sto_l_sel = sto_l_flat.query('custom_year == @time_frame and custom_flow == @scen')
        else:
            sto_l_sel = sto_l.query('custom_year == @time_frame and custom_flow == @scen')

        for idx, region in enumerate(sel):

            group_sto = sto_l_sel.query('n == @region')

            # group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
            # group_sto = group_sto.iloc[::ts_len]

            # with legend
            if idx == 0:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_sto.value,
                        line=dict(color=sto_colors[scen_idx], width=1.5),
                        mode='lines',
                        name=f'{scen}',
                        legend='legend2',
                        # legendgrouptitle=dict(text='storage level scenarios')
                    ),
                    row=row_counter, col=1, secondary_y=False
                )
                row_counter += 3

            # without legend
            else:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_sto.value,
                        line=dict(color=sto_colors[scen_idx], width=1.5),
                        mode='lines',
                        # name='',
                        showlegend=False
                    ),
                    row=row_counter, col=1, secondary_y=False
                )
                row_counter += 3

    sel.insert(0, 'CP')

    # add demand countries
    scens = ['island-eh', 'TYNDP-e/island-h', 'TYNDP-eh']
    colors_scen = ['black', 'gray', 'green']

    sel.remove('CP')

    for scen_idx, scen in enumerate(scens):

        row_counter = 5

        if flat_dem_switch:
            dem_sel = dem_flat.query('custom_year == @time_frame and custom_flow == @scen')
            dem_sel = dem_sel.rename(columns={'normalized_dem': 'value'})
        else:
            dem_sel = dem.query('custom_year == @time_frame and custom_flow == @scen')

        for idx, region in enumerate(sel):

            group_dem = dem_sel.query('n == @region')

            # group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
            # group_sto = group_sto.iloc[::ts_len]

            # with legend
            if idx == 0:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_dem.value,
                        line=dict(color='gray', width=1),
                        mode='lines',
                        # name=f'demand ({scen})',
                        showlegend=False
                        # legend='legend',
                        # legendgrouptitle=dict(text='storage level scenarios')
                    ),
                    row=row_counter, col=1, secondary_y=True
                )
                row_counter += 3

            # without legend
            else:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_dem.value,
                        line=dict(color='gray', width=1),
                        mode='lines',
                        # name='',
                        showlegend=False
                    ),
                    row=row_counter, col=1, secondary_y=True
                )
                row_counter += 3

    sel.insert(0, 'CP')

    # Update xaxis properties
    start_year = yy[0]
    end_year = yy[-1]

    for r in range(row_no):
        if r < row_no-1:
            fig.update_xaxes(
                title_text='',
                range=[datetime.datetime(start_year, 6, 30), datetime.datetime(end_year, 7, 1)],
                mirror=True,
                dtick="M1",
                row=r+1, col=1)

    fig.update_xaxes(
        title_text='',
        range=[datetime.datetime(start_year, 6, 30), datetime.datetime(end_year, 7, 1)],
        mirror=True,
        row=row_no, col=1,
        dtick="M1",
        tickformat="%b<br>'%y",
        tickfont=dict(size=10)
        )

    # update yaxis properties of occurrence subplots
    for r in range(1, row_no+1, 3):
        fig.update_yaxes(title=dict(text=regions[r], font=dict(size=20), standoff=35), range=[0, 1],
                         ticks='', showticklabels=False, mirror=True, row=r, col=1)

    # update yaxis properties of storage and demand level subplots
    fig.update_layout(
        yaxis2=dict(title=dict(text='storage [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis3=dict(title=dict(text='demand [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis6=dict(title=dict(text='storage [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis7=dict(title=dict(text='demand [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis10=dict(title=dict(text='storage [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis11=dict(title=dict(text='demand [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis14=dict(title=dict(text='storage [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis15=dict(title=dict(text='demand [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis18=dict(title=dict(text='storage [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis19=dict(title=dict(text='demand [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis22=dict(title=dict(text='storage [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis23=dict(title=dict(text='demand [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis26=dict(title=dict(text='storage [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis27=dict(title=dict(text='demand [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis30=dict(title=dict(text='storage [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis31=dict(title=dict(text='demand [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis34=dict(title=dict(text='storage [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True),
        yaxis35=dict(title=dict(text='demand [GWh]', font=dict(size=9), standoff=5), mirror=True, tickfont=dict(size=8), automargin=True)
        )

# =============================================================================
#     for r in range(2, row_no+1, 3):
#         fig.update_yaxes(range=[35, 105], row=r, col=1, secondary_y=True) 
# =============================================================================

    fig.update_layout(template='simple_white',
                      margin=dict(l=0, r=0, t=0, b=30),
                      height=1200, width=1100
                      )

    fig.update_layout(
        legend1=dict(
            title='Identified events according to relative thresholds:',
            font=dict(size=10),
            title_side='top',
            orientation='h',  # Horizontal orientation
            x=0.48,  # Center the legend horizontally
            y=-0.04,  # Move the legend further down
            xanchor='center',  # Anchor the legend's horizontal center to x position
            yanchor='top',  # Anchor the legend's top to the y position
            itemsizing='constant',
            traceorder='normal',
            entrywidthmode='fraction',
            entrywidth=0.09,
            #itemwidth=80,
            )
        )
    
# =============================================================================
#     fig.update_layout(
#         legend2=dict(
#             title='Storage level in modeled scenarios (left y-axis):',
#             font=dict(size=10),
#             title_side='top',
#             orientation='h',  # Horizontal orientation
#             x=0.1,  # Center the legend horizontally
#             y=-0.11,  # Move the legend further down
#             xanchor='center',  # Anchor the legend's horizontal center to x position
#             yanchor='top',  # Anchor the legend's top to the y position
#             itemsizing='constant',
#             # traceorder='normal',
#             # entrywidthmode='fraction',
#             # entrywidth=0.09,
#             #itemwidth=80,
#             )
#         )
# =============================================================================
    
    fig.update_layout(
        legend2=dict(
            title='Storage level in modeled scenarios (left y-axis):',
            font=dict(size=10),
            title_side='top',
            orientation='h',  # Horizontal orientation
            x=0.41,  # Center the legend horizontally
            y=-0.11,  # Move the legend further down
            xref="paper", yref="paper",
            xanchor='right',  # Anchor the legend's horizontal center to x position
            yanchor='top',  # Anchor the legend's top to the y position
            itemsizing='constant',
            #traceorder='normal',
            #entrywidthmode='fraction',
            #entrywidth=0.11,
            #itemwidth=80,
            )
        )
    if flat_dem_switch:
        fig.update_layout(
            legend3=dict(
                title='Normalized flat electricity demand in modeled scenarios (right y-axis):',
                font=dict(size=10),
                title_side='top',
                orientation='h',  # Horizontal orientation
                x=0.95,  # Center the legend horizontally
                y=-0.11,  # Move the legend further down
                xref="paper", yref="paper",
                xanchor='right',  # Anchor the legend's horizontal center to x position
                yanchor='top',  # Anchor the legend's top to the y position
                itemsizing='constant',
                #traceorder='normal',
                #entrywidthmode='fraction',
                #entrywidth=0.11,
                #itemwidth=80,
                )
            )
        
    else:
        fig.update_layout(
            legend3=dict(
                title='Moving average of electricity demand in modeled scenarios (right y-axis):',
                font=dict(size=10),
                title_side='top',
                orientation='h',  # Horizontal orientation
                x=0.95,  # Center the legend horizontally
                y=-0.11,  # Move the legend further down
                xref="paper", yref="paper",
                xanchor='right',  # Anchor the legend's horizontal center to x position
                yanchor='top',  # Anchor the legend's top to the y position
                itemsizing='constant',
                #traceorder='normal',
                #entrywidthmode='fraction',
                #entrywidth=0.11,
                #itemwidth=80,
                )
            )

    #fig.show()

    if os_plotting == 'local':
        path = 'T:\\mk\\vreda\\result_plots\\'
    else:
        path = '/home/mkittel/tsla-tde/mk/vreda/result_plots/'
    path_output = os.path.join(path, 'drought_time_series_STO_L', f'year_pair_all_regions_sto_l_s2s_{demand_switch}')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    if flat_dem_switch:
        name = f'drought_ts_sto_l_flat_{start_year}_{end_year}_filter_{dm_filter}'
    else:
        name = f'drought_ts_sto_l_{start_year}_{end_year}_filter_{dm_filter}'
    path_output = os.path.join(path_output, name)

    # fig.write_html(f'{path_output}.html')
    fig.write_image(f'{path_output}.pdf')
    fig.write_image(f'{path_output}.png', scale=3, engine="kaleido")

    del fig
    # print(f'Png written.')

        # =============================================================================
        #     fig.update_layout(
        #         xaxis_range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
        #         yaxis_range=[0.05, 0.95],
        #         template='simple_white',
        #         margin=dict(l=0, r=0, t=30, b=0),
        #         legend={'itemsizing': 'constant'},
        #         height=1400, width=1240
        #     )
        #     fig.update_xaxes(dtick="M1", tickformat="%b%y")
        #     fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))
        #     fig.update_yaxes(visible=False, showticklabels=False)
        #     fig.update_xaxes(title='')
        # =============================================================================