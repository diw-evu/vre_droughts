# -*- coding: utf-8 -*-
"""
This script generates a time series reflecting the generation potential of
multuiple VRE technologies. First, it reads single VRE capacity and
availability assumptions. Next, it combines these assumptions to one time series
reflecting the generation potential of all VRE technologies considered.

The script uses the package "multiprocessing" to distribute the tasks among all
CPU cores.
"""

import pandas as pd
import os
import yaml
import time
from pathlib import Path
# import ipyparallel as ipp
import multiprocessing as mp


def get_config(path):

    # initial directory
    os.chdir(path)
    # import settings
    with open('config.yaml', 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    return config


def get_capacity_data(config):
    '''
    Reads capacity data. Returns dataframe(index=technology, columns=region codes).
    '''
    path_cap = os.path.join(config['paths']['input'], config['paths']['capacity'])
    cap = pd.read_csv(path_cap, index_col=0)
    cap = cap.loc[list(config['technologies_portfolio'].values())]
    cap = cap.loc[:, list(config['regions'])]
    cap.loc['cap_total'] = cap.sum(axis=0)
    return cap


class Portfolio():

    def __init__(self, region, config, cap):
        self.region = region
        self.config = config
        self.cap = cap

    def get_vre_availability(self):
        energy = {}
        for tech, tech_code in self.config['technologies_portfolio'].items():
            if tech in self.config['excluded'] and self.region in self.config['excluded'][tech]:
                continue
            try:
                path = os.path.join(self.config['paths']['input'], self.config['paths'][tech], f'{self.region}.csv')
                ts_wide = pd.read_csv(path)
            except:
                print(self.region, tech)
            vre_energy = ts_wide.iloc[:, 2:] * self.cap.loc[tech_code, self.region]
            energy[tech] = vre_energy
        self.energy = energy
        self.cols = ts_wide.iloc[:, :2]

    def portfolio_vre_availability(self):
        # combine to one df
        df = sum(list(self.energy.values())) / self.cap.loc['cap_total', self.region]
        df.insert(0, self.cols.columns[1], self.cols.hour)
        df.insert(0, self.cols.columns[0], self.cols.date)
        self.portfolio_vre = df

    def export(self):
        path_new_folder = os.path.join(self.config['paths']['input'], self.config['paths']['portfolio'])
        Path(path_new_folder).mkdir(parents=True, exist_ok=True)
        path_file = os.path.join(path_new_folder, f'{self.region}.csv')
        self.portfolio_vre.to_csv(path_file, index=False, header=True)

    def execute(self):
        self.get_vre_availability()
        self.portfolio_vre_availability()
        self.export()
        return f'{self.region} done.'


def initiate(region, config, cap):
    vre_portfolio = Portfolio(region, config, cap)
    return vre_portfolio.execute()


def main():

    # time the execution
    start_time_total = time.perf_counter()

    config = get_config('T:\\mk\\vreda\\config')
    cap = get_capacity_data(config)

    # setup multiprocessing
    values = tuple([(region, config, cap) for region in config['regions']])

    with mp.Pool() as pool:
        result = pool.starmap(initiate, values)
        print(result)

    duration = time.perf_counter() - start_time_total
    print(f"Duration computation: {round(duration, 2)} seconds")


if __name__ == '__main__':
    main()


# This is an alternative implementation, yet not as fast as multipprocessing
# =============================================================================
# def execute(r_code, config, cap):
# 
#     import pandas as pd
#     import os
#     from pathlib import Path
# 
#     def combined_vre_availability(energy, r_code, cols, cap):
#         # combine to one df
#         df = sum(list(energy.values())) / cap.loc['cap_total', r_code]
#         df.insert(0, cols.columns[1], cols.Hour)
#         df.insert(0, cols.columns[0], cols.Date)
#         return df
# 
#     def get_vre_availability(config, r_code, cap):
#         energy = {}
#         for tech, tech_code in config['technologies']['combined'].items():
#             path = os.path.join(config['paths']['input'], config['paths'][tech], f'{r_code}.csv')
#             ts_wide = pd.read_csv(path)
#             vre_energy = ts_wide.iloc[:, 2:] * cap.loc[tech_code, r_code]
#             energy[tech] = vre_energy
#         cols = ts_wide.iloc[:, :2]
# 
#         df = combined_vre_availability(energy, r_code, cols, cap)
# 
#         return df
# 
#     def export(r_code, df, config):
#         path_new_folder = os.path.join(config['paths']['input'], config['paths']['combined'])
#         Path(path_new_folder).mkdir(parents=True, exist_ok=True)
#         path_file = os.path.join(path_new_folder, f'{r_code}.csv')
#         df.to_csv(path_file, index=True, header=True)
# 
#     df = get_vre_availability(config, r_code, cap)
#     export(r_code, df, config)
# 
#     return f'{r_code} done.'
# 
# 
# # time the execution
# start_time_total = time.perf_counter()
# 
# config = get_config('C:\\git\\vre_droughts\\scripts')
# cap = get_capacity_data(config)
# 
# # setup parallalization
# cluster = ipp.Cluster()
# cluster.start_cluster_sync()
# rc = cluster.connect_client_sync()
# # rc.wait_for_engines(8)
# rc.ids
# view = rc.load_balanced_view()
# async_results = []
# 
# for r_name, r_code in config['regions'].items():
#     ar = view.apply_async(execute, r_code, config, cap)
#     async_results.append(ar)
# 
# rc.wait(async_results)
# 
# results = [ar.get() for ar in async_results]
# =============================================================================

# =============================================================================
# duration = time.perf_counter() - start_time_total
# print(f"Duration computation: {round(duration, 2)} seconds")
# =============================================================================
