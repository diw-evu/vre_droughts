# -*- coding: utf-8 -*-
"""
This module contains functionality for plotting.
"""
import matplotlib.pyplot as plt
import os
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.io as pio
# import plotly.graph_objects as go
# from plotly.subplots import make_subplots
import webbrowser
import seaborn as sns
from matplotlib import rcParams, style

plt.rcdefaults()
style.use('seaborn-paper')
rcParams['font.family'] = 'sans-serif'
firefox_path = 'C:\\Program Files\\Mozilla Firefox\\firefox.exe'
webbrowser.register('firefox', None, webbrowser.BackgroundBrowser(firefox_path))
pio.renderers.default = 'firefox'  # 'browser'


class Plotter():

    def __init__(self,
                 drought_type,
                 counting_algorithm,
                 start,
                 end,
                 tech_labels,
                 config_plots
                 ):
        self.drought_type = drought_type
        self.counting_algorithm = counting_algorithm
        if self.counting_algorithm == 'window':
            self.lower_end = start
        elif self.counting_algorithm == 'event':
            self.lower_end = end
        self.tech_labels = tech_labels
        self.region = None
        self.threshold = None
        self.path_output_region = None
        self.config_plots = config_plots
        plt.ioff()

    def plot_thresholds_capacity_factor_duration_curve(
            self,
            ts_orig,
            absolute_drought_thresholds,
            drought_thresholds_fractions_mean_capacity_factor
            ):
        '''
        Plot determination of threshold for a given fraction of cumultive energy amount.
        '''
        ts = ts_orig.copy()
        
        # generate duration curve
        ts = ts.sort_values('capacity_factor', ascending=False)
        ts = ts.assign(Index=range(len(ts))).set_index('Index').reset_index()
        ts.rename(columns={'Index': 'hour'}, inplace=True)

        # plot main lines
        fig, ax = plt.subplots(figsize=(10, 5))
        ts.plot(x='hour', y='capacity_factor', ax=ax, label='duration curve', c='k', lw=1.2)
        ax.axhline(lw=0.3, c='k')

        # plot absolute thresholds
        if absolute_drought_thresholds:
            for thres in absolute_drought_thresholds:
                ts[f"thres_abs_{thres}"] = thres
                ts.plot(x='hour', y=f"thres_abs_{thres}", ax=ax, lw=0.9, label=f"{round(thres, 3)}$^{{abs}}$")

        # plot thresholds acc. to fraction of mean capacity factor
        if drought_thresholds_fractions_mean_capacity_factor:
            for thres in drought_thresholds_fractions_mean_capacity_factor:
                ts[f"thres_mean_{thres}"] = thres
                fraction = drought_thresholds_fractions_mean_capacity_factor[thres]['fraction_mean_cf']
                ts.plot(x='hour', y=f"thres_mean_{thres}", ax=ax, lw=0.9,
                        label=f"{round(thres, 3)}$^{{mean cf={fraction}}}$")

        # further settings
        ax.set_ylabel('capacity factor')
        ax.set_xlim(0, len(ts))
        ax.set_ylim(-0.1, 1)
        plt.tight_layout()

        for f in self.config_plots['thresholds_capacity_factor_duration_curve']:
            name = f'threshold_determination{suffix}.{f}'
            plt.savefig(os.path.join(self.path_output_region, f, name))
        plt.close(fig)

    @staticmethod
    def min_max_correlation_matrix(correlation_dict, yearly=False):
        '''
        Gets highest and lowest correlation of entire analysis.

        Parameters
        ----------
        correlation_dict : dictionary
            Holds correlation data as nested dictionary.
        yearly : boolean, optional
            True for another layer in the nested dictionary correlation_dict for yearly
            correlation matrices. The default is False.

        Returns
        -------
        min_corr : float
            Lowest correlation of entire analysis.
        max_corr : float
            Highest correlation of entire analysis.
        '''
        max_corr = {}
        min_corr = {}

        for corr_thres, corr_data in correlation_dict.items():

            if not yearly:
                max_corr[corr_thres] = []
                min_corr[corr_thres] = []
            else:
                max_corr[corr_thres] = {}
                min_corr[corr_thres] = {}

            for drought_thres, data in corr_data.items():

                if not yearly:
                    # get min and max of each df
                    max_corr[corr_thres].append(np.nanmax([data[col].nlargest(2).min() for col in data]))
                    min_corr[corr_thres].append(np.nanmin([data[col].min() for col in data]))
                else:
                    max_corr[corr_thres][drought_thres] = []
                    min_corr[corr_thres][drought_thres] = []

                    for y, df in data.items():
                        max_corr[corr_thres][drought_thres].append(np.nanmax([df[col].nlargest(2).min() for col in df]))
                        min_corr[corr_thres][drought_thres].append(np.nanmin([df[col].min() for col in df]))

            if yearly:
                max_corr[corr_thres] = np.nanmax([np.nanmax(max_list) for max_list in max_corr[corr_thres].values()])
                min_corr[corr_thres] = np.nanmin([np.nanmin(min_list) for min_list in min_corr[corr_thres].values()])

        # get min and max across all correlation thresholds
        max_corr = np.nanmax([(np.nanmax(data) if np.nanmax(data) != 1 else 0) for data in max_corr.values() if data])
        min_corr = np.nanmin([(np.nanmin(data) if np.nanmin(data) != 1 else 0) for data in min_corr.values() if data])

        return min_corr, max_corr

    def plot_correlation_tech(
            self,
            correlation_dict_orig,
            technology,
            path_run,
            cbar_label='Pearson correlation',
            font_scale=0.9,
            figsize=(9, 7),
            cmap='flare'):
        '''
        Plots lower triangle of drought correlation matrix (one technology, all regions).
        All plots corresponding to one technology have same color scale.
        '''
        correlation_dict = correlation_dict_orig.copy()
        
        # highest and lowest correlation of entire run for uniform scale of correlation matrix
        min_corr, max_corr = self.min_max_correlation_matrix(correlation_dict)

        for corr_thres, corr_data in correlation_dict.items():
            for drought_thres, df in corr_data.items():
                
                # create path
                path_correlation = os.path.join(
                    path_run, technology, 'pearson_correlation_per_technology_all_regions',
                    f"correlation_threshold_{corr_thres}", drought_thres
                    )
# =============================================================================
#                 # split in text and number part
#                 kind, number = drought_thres.split('=')
# 
#                 drought_thres_i = int(float(number)*100)
# 
#                 # assign name
#                 if kind == 'a':
#                     thres_kind = f"absolute drought threshold of {drought_thres_i}%"
#                 elif kind == 'f-mean-cf':
#                     thres_kind = (f"drought threshold corresponding \nto the {drought_thres_i}% fraction of the "
#                                   f"mean capacity factor")
# =============================================================================
                # upper triagle to be hidden in heatmap
                upper = np.triu(df.to_numpy())

                # adjust mask and df
                upper_trunc = upper[1:, :-1]
                corr_trunc = df.iloc[1:, :-1].copy()

                # plot heat map
                sns.set(font_scale=font_scale, font='serif')
                fig = plt.figure(figsize=figsize)
                sns.heatmap(corr_trunc, cmap=cmap, cbar_kws={'label': cbar_label}, \
                            mask=upper_trunc, vmax=max_corr, vmin=min_corr)

# =============================================================================
#                 # add label
#                 tech_label = self.tech_labels[technology]
#                 if technology == 'portfolio':
#                     label = (
#                         f"Pearson correlation of {tech_label} droughts \n lasting longer than {corr_thres} hours"
#                         f" under a {thres_kind}"
#                         )
#                 else:
#                     label = (
#                         f"Pearson correlation of {tech_label} droughts lasting longer \nthan {corr_thres} hours"
#                         f" under a {thres_kind}"
#                         )
#                 # fig.text(0.84, 0.92, label, ha='right', va='center', fontsize=12)
# =============================================================================
                plt.subplots_adjust(top=0.98, bottom=0.13, left=0.06, right=0.99, hspace=0.0, wspace=0.1)

                # save figure
                for f in self.config_plots['correlation_technologies']:
                    name_file = f"{technology}_drought_thres_{drought_thres}_all_years.{f}"
                    plt.savefig(os.path.join(path_correlation, f, name_file))
                plt.close(fig)

    def plot_correlation_tech_yearly(
            self,
            correlation_dict_orig,
            technology,
            path_run,
            cbar_label='Pearson correlation',
            font_scale=0.9,
            figsize=(9, 7),
            cmap='flare'):
        '''
        Plots lower triangle of drought correlation matrix (one technology, all regions).
        All plots corresponding to one technology have same color scale.
        '''
        correlation_dict = correlation_dict_orig.copy()
        
        # highest and lowest correlation of entire run for uniform scale of correlation matrix
        min_corr, max_corr = self.min_max_correlation_matrix(correlation_dict, yearly=True)

        for corr_thres, corr_data in correlation_dict.items():
            for drought_thres, yearly_dict in corr_data.items():
                
                # create path
                path_correlation = os.path.join(
                    path_run, technology, 'pearson_correlation_per_technology_all_regions',
                    f"correlation_threshold_{corr_thres}", drought_thres
                    )

                for y, df in yearly_dict.items():

                    # upper triagle to be hidden in heatmap
                    upper = np.triu(df.to_numpy())
    
                    # adjust mask and df
                    upper_trunc = upper[1:, :-1]
                    corr_trunc = df.iloc[1:, :-1].copy()
    
                    # plot heat map
                    sns.set(font_scale=font_scale, font='serif')
                    fig = plt.figure(figsize=figsize)
                    sns.heatmap(corr_trunc, cmap=cmap, cbar_kws={'label': cbar_label}, \
                                mask=upper_trunc, vmax=max_corr, vmin=min_corr)
    
                    plt.subplots_adjust(top=0.98, bottom=0.13, left=0.06, right=0.99, hspace=0.0, wspace=0.1)
    
                    # save figure
                    for f in self.config_plots['correlation_technologies_yearly']:
                        name_file = f"{technology}_drought_thres_{drought_thres}_{y}.{f}"
                        plt.savefig(os.path.join(path_correlation, f, name_file))
                    plt.close(fig)

    def plot_correlation_region(
            self,
            correlation_dict,
            region,
            path_run,
            cbar_label='Pearson correlation',
            font_scale=0.9,
            figsize=(9, 7),
            cmap='flare'):
        '''
        Plots lower triangle of drought correlation matrix (one region, all technologies).
        All plots corresponding to one region have same color scale.
        '''
        # highest and lowest correlation of entire run for uniform scale of correlation matrix
        min_corr, max_corr = self.min_max_correlation_matrix(correlation_dict)

        for corr_thres, corr_data in correlation_dict.items():
            for drought_thres, df in corr_data.items():

                # create path          
                path_correlation = os.path.join(
                    path_run, 'pearson_correlation_per_region_all_technologies', region,
                    f"correlation_threshold_{corr_thres}"
                    )

                # upper triagle to be hidden in heatmap
                upper = np.triu(df.to_numpy())

                # truncate mask and df
                upper_trunc = upper[1:, :-1]
                corr_trunc = df.iloc[1:, :-1].copy()

                # plot heat map
                sns.set(font_scale=font_scale, font='serif')
                fig = plt.figure(figsize=figsize)
                sns.heatmap(corr_trunc, cmap=cmap, cbar_kws={'label': cbar_label}, \
                            mask=upper_trunc, vmax=max_corr, vmin=min_corr)

                plt.subplots_adjust(top=0.98, bottom=0.13, left=0.06, right=1.05, hspace=0.0, wspace=0.1)

                # save figure
                for f in self.config_plots['correlation_regions']:
                    name_file = f"{region}_drought_thres_{drought_thres}.{f}"
                    plt.savefig(os.path.join(path_correlation, drought_thres, f, name_file))
                plt.close(fig)

    def plot_correlation_region_yearly(
            self,
            correlation_dict,
            region,
            path_run,
            cbar_label='Pearson correlation',
            font_scale=0.9,
            figsize=(9, 7),
            cmap='flare'):
        '''
        Plots lower triangle of drought correlation matrix (one region, all technologies).
        All plots corresponding to one region have same color scale.
        '''
        # highest and lowest correlation of entire run for uniform scale of correlation matrix
        min_corr, max_corr = self.min_max_correlation_matrix(correlation_dict, yearly=True)

        for corr_thres, corr_data in correlation_dict.items():
            for drought_thres, yearly_dict in corr_data.items():
                
                # create path          
                path_correlation = os.path.join(
                    path_run, 'pearson_correlation_per_region_all_technologies', region,
                    f"correlation_threshold_{corr_thres}", drought_thres
                    )

                for y, df in yearly_dict.items():

                    # upper triagle to be hidden in heatmap
                    upper = np.triu(df.to_numpy())

                    # truncate mask and df
                    upper_trunc = upper[1:, :-1]
                    corr_trunc = df.iloc[1:, :-1].copy()

                    # plot heat map
                    sns.set(font_scale=font_scale, font='serif')
                    fig = plt.figure(figsize=figsize)
                    sns.heatmap(corr_trunc, cmap=cmap, cbar_kws={'label': cbar_label}, \
                                mask=upper_trunc, vmax=max_corr, vmin=min_corr)

                    plt.subplots_adjust(top=0.98, bottom=0.13, left=0.06, right=1.05, hspace=0.0, wspace=0.1)

                    # save figure
                    for f in self.config_plots['correlation_regions_yearly']:
                        name_file = f"{region}_drought_thres_{drought_thres}_{y}.{f}"
                        plt.savefig(os.path.join(path_correlation, f, name_file))
                    plt.close(fig)

    def lines_single(
            self,
            df_orig,
            ylabel,
            xlabel='time in hours',
            legend=True,
            truncate=None,
            fontsize=10,
            file_types=['png']
            ):
        '''
        Plots average frequency as line plot with period duration on x-axis and frequency
        on y-axis.
        '''
        df = df_orig.copy()
        
        fig = plt.figure(figsize=(10, 3))
        ax = plt.axes()
        x = df.columns.values
        for r in df.iterrows():
            if r[0] == 'Average':
                ax.plot(x, r[1], 'k', label=r[0])
            else:
                if type(r[0]) is float:
                    label = round(r[0], 3)
                else:
                    label = r[0]
                ax.plot(x, r[1], label=label)

        # plot truncated plot
        if truncate:
            plt.xlim((self.lower_end, 168))

        # settings
        ax.margins(x=0.001)
        plt.xlabel(xlabel, fontsize=fontsize)
        plt.ylabel(ylabel, fontsize=fontsize)

        # configure file name and save as pdf and png and legend title
        if ylabel == 'average frequency per quarter':
            str1 = 'frequency'
            str2 = f'_{round(self.threshold, 2)}'
            plt.legend(title=f'threshold {round(self.threshold, 2)}')
        elif ylabel == 'average frequency across all years':
            str1 = 'frequency'
            str2 = ''
            plt.legend(title='threshold')
        elif 'across' in ylabel:
            str1 = 'return_period_any'
            str2 = ''
            plt.legend(title='threshold')
        elif 'Q1 [' in ylabel:
            str1 = 'return_period_q1'
            str2 = ''
            plt.legend(title='threshold')
        elif 'Q2 [' in ylabel:
            str1 = 'return_period_q2'
            str2 = ''
            plt.legend(title='threshold')
        elif 'Q3 [' in ylabel:
            str1 = 'return_period_q3'
            str2 = ''
            plt.legend(title='threshold')
        elif 'Q4 [' in ylabel:
            str1 = 'return_period_q4'
            str2 = ''
            plt.legend(title='threshold')
        elif 'Q1 and Q4' in ylabel:
            str1 = 'return_period_q14'
            str2 = ''
        elif 'Q2 and Q3' in ylabel:
            str1 = 'return_period_q23'
            str2 = ''

        name_file = f"{str1}_{self.region}_{self.counting_algorithm}_{self.drought_type}{str2}"
        if truncate:
            name_file = f"{name_file}_trunc"

        # layout settings
        plt.tight_layout()
        # plt.subplots_adjust(top=0.99, bottom=0.15, left=0.05, right=0.99, hspace=0.0, wspace=0.1)

        for f in file_types:
            name = f"{name_file}.{f}"
            plt.savefig(os.path.join(self.path_output_region, f, name))
        plt.close(fig)

    def bars_sec_y_single(
            self,
            df_left_orig,
            df_right_orig,
            name,
            run_labels,
            y_label_left='days',
            y_label_right='full load hours [h/a]',
            drop_maximum=True
            ):
        '''
        Takes dataframe with temporal scale as index and results in columns,
        e.g., threshold-sepcific results. Last row of dataframe is discarded.
        '''
        df_left = df_left_orig.copy()
        df_right = df_right_orig.copy()

        if drop_maximum:
            df_left = df_left.drop('Maximum')

        # sort columns subject to threshold in ascending order
        df_left = df_left.reindex(sorted(df_left.columns), axis=1)

        # exclude years from flh that cannot be plotted
        df_right = df_right.loc[df_left.index, :]

        # create label suffix
        run_labels = dict(run_labels)

        # create figure
        fig, ax1 = plt.subplots(figsize=(15, 3))
        ax2 = ax1.twinx()

        # axes data
        x_labels = df_left.index.tolist()
        x = np.arange(len(x_labels))
        bars = list(df_left)
        bars_no = len(bars)
        if bars_no <= 2:
            offset = 1
        elif bars_no == 3:
            offset = 1.5
        elif bars_no == 4:
            offset = 2
        else:
            offset = 2.5

        # axes settings
        ax1.yaxis.grid(color='gray', linewidth=.4, linestyle='dashed')
        ax1.set_axisbelow(True)
        ax1.set_ylabel(y_label_left, fontsize=11)
        ax1.tick_params(axis='y', labelsize=10)
        ax2.set_ylabel(y_label_right, fontsize=11)
        ax2.tick_params(axis='y', labelsize=10)

        colors = ['#a4a5cd', '#727397', '#434566', '#212233', '#1a1b28', '#101119']
        width = 1 / (bars_no+1) - 0.1

        # plot the data
        plot_list = []
        for no, bar in enumerate(bars):
            label = run_labels[bar]
            if label[0] == 'a':
                label = label[0]
            p = ax1.bar(
                x=x-(width*(bars_no-no-offset)),
                height=df_left[bar],
                width=width,
                # color=colors[no],
                align='center',
                label=f"${round(bar, 3)}^{{{label}}}$",
                edgecolor='white',
                linewidth=0.3
                )
            plot_list.append(p)
        position_flh = x-(width*(bars_no-no-offset)) + 0.01
        p2 = ax2.bar(
            x=position_flh+width,
            height=df_right['flh'],
            width=width,
            color='#c42d4a',
            align='center',
            label='flh',
            edgecolor='white',
            linewidth=0.3
            )

        ax1.set_xticks(x)
        ax1.set_xticklabels(x_labels, size=11, rotation=90)
        ax1.margins(x=0.01)

        # legend settings
        box = ax1.get_position()
        ax1.set_position([box.x0, box.y0, box.width * 0.9, box.height])
        plt.subplots_adjust(top=0.99, bottom=0.18, left=0.05, right=0.85, hspace=0.0, wspace=0.1)
        ax1.legend(handles=[*plot_list, p2], loc='center left', prop={'size': 10},
                   bbox_to_anchor=(1.05, 0.5), title='threshold', frameon=False)

        # save as pdf and png
        name_prefix = f"{name}_{self.region}_{self.counting_algorithm}_{self.drought_type}"
        for f in self.config_plots['max_period_yearly_flh']:
            name = f"{name_prefix}.{f}"
            plt.savefig(os.path.join(self.path_output_region, f, name))
        plt.close(fig)

    def bars_single(
            self,
            df_orig,
            name,
            path,
            level,
            y_label='days',
            discard_last_row=True,
            sort_columns=True,
            file_types=['png']
            ):
        '''
        Takes dataframe with temporal scale as index and results in columns,
        e.g., threshold-sepcific results. Last row of dataframe is discarded.
        '''
        df = df_orig.copy()
        # default setting
        plt.rcdefaults()
        style.use('seaborn-paper')

        if discard_last_row:
            df = df[:-1]

        # sort columns based on the sum of each column
        if sort_columns:
            col_sums = df.sum().sort_values(ascending=True)
            col_sums = col_sums.index
            df = df.loc[:, col_sums]

        # set width of bars
        bar_width = 1 / len(df.columns) - 0.1

        # set position of bar on X axis
        r = []
        r1 = np.arange(len(df))
        r.append(r1)
        if len(df.columns) > 1:
            for idx in range(len(df.columns)-1):
                r.append([x + bar_width for x in r[idx]])

        # create figure
        fig = plt.figure(figsize=(10, 3))
        ax = plt.subplot(111)
        ax.yaxis.grid(color='gray', linewidth=.4, linestyle='dashed')

        # make the plot
        for idx, col in enumerate(df.columns):
            if type(col) is float:
                label = round(col, 3)
            else:
                label = col
            ax.bar(r[idx], df.iloc[:, idx], width=bar_width, edgecolor='white', label=label)

        # add xticks on the middle of the group bars
        plt.xticks([r + bar_width for r in range(len(df))], df.index, rotation=90)

        # further settings
        plt.ylabel(y_label, fontsize=9)
        ax.margins(x=0.01)

        # legend settings
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
        right = 0.908
        if (name == 'extremes_yearly') or (name == 'weighted_extremes_yearly'):
            bottom = 0.16
            right = 0.88
        elif name == 'extremes_monthly':
            bottom = 0.3
        plt.subplots_adjust(top=0.99, bottom=bottom, left=0.06, right=right, hspace=0.0, wspace=0.1)
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), title='threshold', frameon=False)

        # save as pdf and png
        for f in file_types:
            if level == 'region':
                name = f"{name}_{self.region}_{self.counting_algorithm}_{self.drought_type}"
            elif level == 'across_regions':
                name = f"{name}_{self.counting_algorithm}_{self.drought_type}"
            name_file = f"{name}.{f}"
            if level == 'region':
                plt.savefig(os.path.join(path, f, name_file))
            elif level == 'across_regions':
                plt.savefig(os.path.join(path, 'aggregated', f, name_file))
        plt.close(fig)

    # TODO:
    def boxplot_single():
        pass

    def bars_all(
            self,
            path,
            df_orig,
            x,
            y,
            color='threshold',
            facet_row='region',
            barmode='group',
            days=True,
            rotate=None,
            run_labels=None,
            ):
        '''
        Plots most extreme events for all regions.
        '''
        df = df_orig.copy()
        
        # aggregate to daily values
        if days:
            df[y] /= 24

        # drop row "Maximum"
        df = df[df.iloc[:, 0] != 'Maximum']

        # create correct labels
        df_regions = []
        for region, df_region in df.groupby('region'):
            df_regions.append(df_region.replace({'threshold': run_labels[region]}))
        df = pd.concat(df_regions)

        # plot most extreme periods for all regions
        fig = px.bar(df,
                     x=x,
                     y=y,
                     color=color,  # 'region'
                     barmode=barmode,
                     facet_row=facet_row,  # 'color'
                     template='plotly_white',
                     title='Most extreme MBT events',
                     hover_name='region',  # 'color'
                     labels={y: 'days'},
                     facet_row_spacing=0.01)
        if rotate:
            fig.update_xaxes(type='category', tickangle=90)
        fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))

        # save file
        name = f"{y}_all_regions"
        for f in self.config_plots['aggregated']:
            path_final = os.path.join(path, 'aggregated', f, name)
            if f == 'html':
                fig.write_html(f"{path_final}.{f}")
            else:
                fig.write_image(f"{path_final}.{f}", width=1980, height=1080)

    def lines_all(
            self,
            path,
            df_orig,
            x,
            y,
            color,
            facet_row,
            suffix='',
            labels=None,
            title=None,
            days=True,
            rotate=None,
            run_labels=None,
            file_types=['html']
            ):
        """
        Returns plotly line plot to show results either differentiated by country
        or threshold.
        """
        df = df_orig.copy()
        
        if df.empty:
            return

        # aggregate to daily values
        if days:
            df[y] /= 24

        # drop row "Maximum"
        df = df[df.iloc[:, 0] != 'Maximum']

        # create correct labels
        df_regions = []
        for region, df_region in df.groupby('region'):
            df_regions.append(df_region.replace({'threshold': run_labels[region]}))
        df = pd.concat(df_regions)

        # plot
        fig = px.line(df,
                      x=x,
                      y=y,
                      color=color,
                      facet_row=facet_row,
                      template='plotly_white',
                      title=title,
                      hover_name=color,
                      labels=labels,
                      facet_row_spacing=0.01)
        if rotate:
            fig.update_xaxes(type='category', tickangle=90)
        if color == 'region':
            fig.for_each_annotation(lambda x: x.update(text=f"{x.text.split('=')[-2]}={x.text.split('=')[-1]}"))
        else:
            fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))

        # save file
        name = f"{y}_all_regions_{color}_{suffix}"
        for f in self.config_plots['aggregated']:
            path_final = os.path.join(path, 'aggregated', f, name)
            if f == 'html':
                fig.write_html(f"{path_final}.{f}")
            else:
                fig.write_image(f"{path_final}.{f}", width=1980, height=1080)

    def plot_drought_thresholds(self, drought_thresholds_all, min_thres, max_thres, path_run):
        '''
        Plots actuall drought thresholds for all fractions as heatmap. Color bar is
        scaled to min and max of all thresholds for comparibility.
        '''
        for tech, df in drought_thresholds_all.items():

            # move CP row to very top
            if 'CP' in df.index:
                cp_row = df.loc['CP']
                rest_of_df = df.drop('CP')
                df = pd.concat([cp_row.to_frame().T, rest_of_df])

            # plot heat map
            sns.set(font_scale=1.3, font='serif')
            fig = plt.figure(figsize=(10, 10))
            label = f'drought thresholds in absolute terms'
            heatmap = sns.heatmap(df, cmap='flare', cbar_kws={'label': label}, vmax=max_thres, vmin=min_thres)
            heatmap.set_yticklabels(heatmap.get_yticklabels(), ha='left')
            yticklabels = [item.get_text() for item in heatmap.get_yticklabels()]
            heatmap.set_yticklabels(yticklabels, rotation=0, va='center', position=(-0.03, 0))
            plt.ylabel('')
            plt.xlabel('fraction of mean availability factor', labelpad=15)
            plt.tight_layout()
            plt.subplots_adjust(right=1.0)

            # save figure
            for f in self.config_plots['mean_capacity_factors']:
                name_file = f"heatmap_drought_thresholds_{tech}.{f}"
                path = os.path.join(os.path.join(path_run, 'mean_availability_factor_and_drought_thresholds', name_file))
                fig.savefig(path)
            plt.close(fig)
