# -*- coding: utf-8 -*-
"""
This module organizes the analysis at the top level using an instance of the class Analyzer.
"""
import os
from datetime import datetime
from pathlib import Path
import yaml
import pandas as pd
import numpy as np
from functools import reduce
import pickle
import gzip
from collections import defaultdict
import multiprocess as mp
import psutil

from plotter import Plotter
from region_analyzer import RegionAnalyzer


class Analyzer():

    def __init__(self, config, cwd):

        self.config = config
        self.path_project = cwd
        self.regions = self.config['regions']
        self.corr_thresholds = self.config['correlation_thresholds'].copy()
        # list in descending order -> otherwise capturing of drought_ts_regions fails
        self.corr_thresholds.sort(reverse=True)
        self.absolute_drought_thresholds = self.config['drought_thresholds']['absolute']
        self.fractions_mean_capacity_factor = self.config['drought_thresholds']['fractions_mean_capacity_factor']
        self.fractions_cumulative_energy = self.config['drought_thresholds']['fractions_cumulative_energy']
        self.drought_type = self.config['drought_type']
        self.counting_algorithm = self.config['counting_algorithm']
        self.start = self.config['start']
        self.end = self.config['end']
        self.correlation_type = self.config['correlation_type']

        # result container
        self.drought_thresholds_run_labels = {technology: {} for technology in self.config['technologies']}
        self.mean_frequency_yearly = {technology: {} for technology in self.config['technologies']}
        self.mean_frequency_q1_long = {technology: {} for technology in self.config['technologies']}
        self.mean_frequency_q2_long = {technology: {} for technology in self.config['technologies']}
        self.mean_frequency_q3_long = {technology: {} for technology in self.config['technologies']}
        self.mean_frequency_q4_long = {technology: {} for technology in self.config['technologies']}
        self.mean_frequency_q14_long = {technology: {} for technology in self.config['technologies']}
        self.mean_frequency_q23_long = {technology: {} for technology in self.config['technologies']}
        self.extremes_yearly_long = {technology: {} for technology in self.config['technologies']}
        self.extremes_yearly = {technology: {} for technology in self.config['technologies']}
        self.extremes_yearly_weighted = {technology: {} for technology in self.config['technologies']}
        self.extremes_yearly_weighted_max = {technology: {} for technology in self.config['technologies']}
        self.extremes_yearly_weighted_max_region = {technology: {} for technology in self.config['technologies']}
        self.extremes_monthly_long = {technology: {} for technology in self.config['technologies']}
        self.extremes_monthly_mid_long = {technology: {} for technology in self.config['technologies']}
        self.weights = {technology: {} for technology in self.config['technologies']}
        self.return_period_all_long = {technology: {} for technology in self.config['technologies']}
        self.return_period_q1_long = {technology: {} for technology in self.config['technologies']}
        self.return_period_q2_long = {technology: {} for technology in self.config['technologies']}
        self.return_period_q3_long = {technology: {} for technology in self.config['technologies']}
        self.return_period_q4_long = {technology: {} for technology in self.config['technologies']}
        self.return_period_q14_long = {technology: {} for technology in self.config['technologies']}
        self.return_period_q23_long = {technology: {} for technology in self.config['technologies']}
        self.correlation_pearson_tech = {technology: {} for technology in self.config['technologies']}
        self.correlation_pearson_tech_yearly = {technology: {} for technology in self.config['technologies']}
        self.energy_deficit_cum = {technology: {} for technology in self.config['technologies']}
        self.energy_deficit_cum_max = {technology: {} for technology in self.config['technologies']}
        self.drought_ts_tech = {technology: {} for technology in self.config['technologies']}
        self.drought_ts_region = {technology: {} for technology in self.config['technologies']}
        self.drought_ts_e_region = {technology: {} for technology in self.config['technologies']}
        self.path_output = None
        self.mean_capacity_factor_regions = {technology: {} for technology in self.config['technologies']}
        self.correlation_pearson_region = None
        self.correlation_pearson_region_yearly = None

        # create result directory
        self.create_run_directory()

        # store config of current run
        self.safe_config()

    def create_run_directory(self):
        '''
        Creates result directory, either for previous run or for new run.
        '''
        # TODO: adjust to new pickling mode
        time_stamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

        # create output folder based on timestamp
        suffix = f"{time_stamp}_{self.drought_type}_{self.counting_algorithm}"
        path_new_folder = os.path.join(self.path_project, 'output', suffix)
        Path(path_new_folder).mkdir(parents=True, exist_ok=True)
        self.path_run = path_new_folder

        # create folder for mean capacity factor and drought thresholds
        path_mean_cf = os.path.join(self.path_project, 'output', suffix, 'mean_availability_factor_and_drought_thresholds')
        Path(path_mean_cf).mkdir(parents=True, exist_ok=True)

    def safe_config(self):
        '''
        Safes config of current run to project directory.
        '''
        path = os.path.join(self.path_run, 'config.yaml')
        with open(path, 'w') as file:
            yaml.dump(self.config, file)

    @staticmethod
    def dict_to_df(dictionary):
        '''
        Returns one dataframe that contains all single dataframes from dictionary values.
        '''
        if not all([val is None for val in dictionary.values()]):
            return pd.concat(dictionary.values(), ignore_index=True)
        else:
            return pd.DataFrame()

    @staticmethod
    def dict_to_df_merge(dictionary):
        '''
        Returns one dataframe that merges all single dataframes in dict on index.
        '''
        dictionary = {i: j.reset_index() for i, j in dictionary.items()}
        df = reduce(lambda left, right: pd.merge(left, right, on=['index'], how='outer'), dictionary.values())
        df.set_index('index', inplace=True)
        return df

    @staticmethod
    def series_in_dict_to_dataframe(dictionary):
        '''
        Returns a dataframe with columns that are stored as values in dictionary. Column
        names equal dictionary keys.
        '''
        return pd.DataFrame(list(dictionary.values()), index=list(dictionary.keys())).T

    def region_weights(self, technology):
        '''
        Generates region weights according to installed capacities per region.
        '''
        path_cap = os.path.join(self.path_project, 'input', self.config['paths']['capacity'])
        cap = pd.read_csv(path_cap, index_col=0)
        cap = cap.loc[self.config['technologies'][technology], list(self.regions)]
        self.weights[technology] = cap / cap.sum()

    def weight_extremes(self, technology, region_results):
        '''
        Generates weighted average yearly maximum duration across all regions.
        '''
        extremes_yearly_weighted_all_regions = pd.DataFrame(
            data=0,
            index=region_results[0].input_extractor.years,
            columns=list(self.drought_thresholds_run_labels[technology].values())[0].values()
            )

        # harmonize column names according to labels of each threshold run & weight extremes
        for region, df in self.extremes_yearly[technology].items():
            df.rename(columns=self.drought_thresholds_run_labels[technology][region], inplace=True)
            df = df * self.weights[technology][region]
            extremes_yearly_weighted_all_regions += df.iloc[:-1, :]
        self.extremes_yearly_weighted[technology] = extremes_yearly_weighted_all_regions

        # get weighted maximum value and year per threshold run
        max_years = extremes_yearly_weighted_all_regions.idxmax()
        max_years.name = 'year*_all'
        max_values = extremes_yearly_weighted_all_regions.max()
        max_values.name = 'hours*_all'
        self.extremes_yearly_weighted_max[technology] = pd.concat([max_years, max_values], axis=1)

    def get_weighted_yearly_extremes_max_region(self, technology):
        '''
        Retrieves for each region yearly maximum duration and related year
        as well as maximum duration in weighted cross-regional maximum year.
        '''
        extremes_yearly_weighted_max_all_regions = {}

        for region, df in self.extremes_yearly[technology].items():

            # get weighted maximum value and year per threshold run
            max_years = df.idxmax()
            max_years.name = 'year_max'
            max_values = df.max()
            max_values.name = 'hours_max'
            max_years_values = pd.concat([max_years, max_values], axis=1)
            max_years_values['region'] = region
            max_years_values['latitude'] = self.config['regions'][region]['lat']
            max_years_values['longitude'] = self.config['regions'][region]['long']
            max_years_values['weight'] = self.weights[technology][region]

            # merge with most extreme year and duration across all regions and other regions
            df_merge = pd.merge(
                self.extremes_yearly_weighted_max[technology],
                max_years_values,
                left_index=True,
                right_index=True
                )

            # drop thresholds if there are no droughts at all
            df_merge = df_merge[df_merge['year*_all'].notna()]

            # get duration of weighted max year per region
            df_merge['hours*_region'] = np.diag(df.loc[df_merge['year*_all'], df_merge.index])
            extremes_yearly_weighted_max_all_regions[region] = df_merge.reset_index(names='threshold')

        # concat to one df
        df = pd.concat(extremes_yearly_weighted_max_all_regions.values())
        self.extremes_yearly_weighted_max_region[technology] = df

    def get_max_duration(self, technology, region_results):
        '''
        Retrieves maximum drought duration of across all countries for each threshold.
        Returns result in DataFrame.
        '''
        # get max duration across all countries for each threshold
        dur_max_pre = {}
        for ra in region_results:
            dur_max_pre[ra.region] = {}
            for thres, dc in ra.drought_counters.items():
                dur_max_pre[ra.region][thres] = max(dc.count_yearly)

        # flip dict hierarchy
        duration_max = defaultdict(dict)
        for key, val in dur_max_pre.items():
            for subkey, subval in val.items():
                duration_max[subkey][key] = subval

        # convert to df
        duration_max = pd.DataFrame.from_dict(duration_max, orient='index')
        duration_max['max'] = duration_max.max(axis=1)

        return duration_max

    def create_droughts_time_series_duration_blocks_binary(self, technology, corr_threshold, region_results):
        '''
        Constructs one time series in long format holding binary data (1: drougt,
        0: no drought) for all investigated time steps for all regions across
        all years, one block for each drought duration. Returns dictionary
        with dataframes for each investigated threshold.
        '''
        # retrieve maximum drought duration across all countries for each threshold
        duration_max = self.get_max_duration(technology, region_results)

        # create time series encompassing all droughts for all years, hours, durations, countries
        ts_thres_tech = {}
        ts_thres_region = {}

        for thres in duration_max.index:

            # sanity check if there are droughts longer than corr_threshold
            if corr_threshold > duration_max.loc[thres, 'max']:
                continue

            ts_thres_tech[thres] = {}
            ts_thres_region[thres] = {}

            for ra in region_results:

                ts_thres_tech[thres][ra.region] = {}
                ts_thres_region[thres][technology] = {}

                # retrieve lower end of drought duration range
                start = min(ra.drought_counters[thres].start, ra.drought_counters[thres].end)

                # construct time series for durations above user-defined threshold
                start = max(start, corr_threshold)

                for dur in np.arange(start, duration_max.loc[thres, 'max']+1):

                    ts_thres_tech[thres][ra.region][dur] = {}

                    # construct raw time series
                    for y, data in ra.drought_counters[thres].idx.items():

                        # number of time steps in leap and ordinary years
                        if y in ra.drought_counters[thres].leap_years:
                            steps = 8784
                        else:
                            steps = 8760

                        # construct raw time series
                        ts = pd.DataFrame(index=np.arange(steps))
                        ts.index.name = 'hour'
                        ts['duration'] = dur
                        ts['year'] = y
                        ts[ra.region] = np.zeros(steps)

                        # assign value 1 to drought hours
                        if dur in data:
                            for idx in data[dur]:
                                # TODO: overlap with previous years (see solution in subsequent function)
                                end = idx+dur if idx+dur <= steps else steps
                                ts.loc[np.arange(idx, end), ra.region] = 1

                        ts_thres_tech[thres][ra.region][dur][y] = ts

                    # concate yearly time series to one series for one duration
                    ts_thres_tech[thres][ra.region][dur] = pd.concat(
                        list(ts_thres_tech[thres][ra.region][dur].values()))

                # concate all duration time series per country to one
                ts_thres_tech[thres][ra.region] = pd.concat(list(ts_thres_tech[thres][ra.region].values()))
                ts_thres_region[thres][technology] = ts_thres_tech[thres][ra.region]

            # merge country time series together
            dfs = list(ts_thres_tech[thres].values())
            ts_thres_tech[thres] = reduce(
                lambda left, right: pd.merge(
                    left, right, on=['duration', 'year', 'hour'], how='outer'), dfs).fillna(0)

        self.drought_ts_tech[technology][corr_threshold] = ts_thres_tech
        self.drought_ts_region[technology][corr_threshold] = ts_thres_region

    def create_droughts_time_series_integrated_binary(self, technology, corr_threshold, region_results):
        '''
        TODO
        Constructs one time series in long format holding binary data (1: drougt,
        0: no drought) for all investigated time steps for all regions across all years.
        Works only for mean below threshold droughts as droughts of all duration are
        integrated to one time series (as oppossed to the block structure of
        the method create_droughts_time_series_duration_blocks. Returns dictionary
        with dataframes for each investigated threshold.
        '''
        # retrieve maximum drought duration across all countries for each threshold
        duration_max = self.get_max_duration(technology, region_results)

        # create time series encompassing all droughts for all years, hours, durations, countries
        ts_thres_tech = {}
        ts_thres_region = {}

        for thres in duration_max.index:

            # sanity check if there are droughts longer than corr_threshold
            if corr_threshold > duration_max.loc[thres, 'max']:
                print(
                    f"No droughts lasting at least as long as the user-defined "
                    f"correlation threshold of {corr_threshold} time steps "
                    f"identified for the drought threshold {thres}. Consequently, "
                    f"no correlation computed for this drought threshold."
                    )
                continue

            ts_thres_tech[thres] = {}
            ts_thres_region[thres] = {}

            for ra in region_results:

                ts_thres_tech[thres][ra.region] = {}
                ts_thres_region[thres][ra.region] = {}

                # retrieve lower end of drought duration range
                start = min(ra.drought_counters[thres].start, ra.drought_counters[thres].end)

                # construct time series for durations above user-defined threshold
                start = max(start, corr_threshold)

                # construct raw time series
                for y in ra.drought_counters[thres].years:

                    # number of time steps in leap and ordinary years
                    if y in ra.drought_counters[thres].leap_years:
                        steps = 8784
                    else:
                        steps = 8760

                    # construct raw time series
                    ts = pd.DataFrame(index=np.arange(steps))
                    ts.index.name = 'hour'
                    ts['year'] = y
                    ts[ra.region] = np.zeros(steps)

                    # assign value 1 to drought hours
                    for dur in np.arange(start, duration_max.loc[thres, 'max']+1):
                        if dur in ra.drought_counters[thres].idx[y]:
                            for idx in ra.drought_counters[thres].idx[y][dur]:
                                # TODO: overlap with previous years (see solution in subsequent function)
                                end = idx+dur if idx+dur <= steps else steps
                                ts.loc[np.arange(idx, end), ra.region] = 1

                    ts_thres_tech[thres][ra.region][y] = ts

                # concate yearly time series to one series per region
                ts_thres_tech[thres][ra.region] = pd.concat(list(ts_thres_tech[thres][ra.region].values()))
                ts_thres_region[thres][ra.region] = ts_thres_tech[thres][ra.region]

            # merge country time series together
            dfs = list(ts_thres_tech[thres].values())
            ts_thres_tech[thres] = reduce(
                lambda left, right: pd.merge(
                    left, right, on=['year', 'hour'], how='outer'), dfs).fillna(0)

        self.drought_ts_tech[technology][corr_threshold] = ts_thres_tech
        self.drought_ts_region[technology][corr_threshold] = ts_thres_region

    def create_droughts_time_series_integrated(self, technology, corr_threshold, region_results):
        '''
        TODO
        Constructs one time series in long format holding binary data for all
        investigated time steps for all regions across all years. Works only for
        mean below threshold droughts as droughts of all duration are integrated to
        one time series (as opposed to the block structure of
        the method create_droughts_time_series_duration_blocks. Returns dictionary
        with dataframes for each investigated threshold.
        '''
        # retrieve maximum drought duration across all countries for each threshold
        duration_max = self.get_max_duration(technology, region_results)

        # create time series encompassing all droughts for all years, hours, durations, countries
        ts_thres_tech = {}
        ts_thres_region = {}

        for thres in duration_max.index:

            # sanity check if there are droughts longer than corr_threshold
            if corr_threshold > duration_max.loc[thres, 'max']:
                print(
                    f"No droughts lasting at least as long as the user-defined "
                    f"correlation threshold of {corr_threshold} time steps "
                    f"identified for the drought threshold {thres}. Consequently, "
                    f"no correlation computed for this drought threshold."
                    )
                continue

            ts_thres_tech[thres] = {}
            ts_thres_region[thres] = {}

            for ra in region_results:

                ts_thres_tech[thres][ra.region] = {}
                ts_thres_region[thres][ra.region] = {}

                # retrieve lower end of drought duration range
                start = min(ra.drought_counters[thres].start, ra.drought_counters[thres].end)

                # construct time series for durations above user-defined threshold
                start = max(start, corr_threshold)

                for y in ra.drought_counters[thres].years:

                    # number of time steps in leap and ordinary years
                    if y in ra.drought_counters[thres].leap_years:
                        steps = 8784
                    else:
                        steps = 8760

                    # construct raw time series
                    ts = pd.DataFrame(index=np.arange(steps))
                    ts.index.name = 'hour'
                    ts['year'] = y
                    ts[ra.region] = np.zeros(steps)

                    # assign capacity factors to drought hours
                    for dur in np.arange(start, duration_max.loc[thres, 'max']+1):
                        if dur in ra.drought_counters[thres].cf_idx[y]:
                            for _, cf_idx_series in ra.drought_counters[thres].cf_idx[y][dur].items():

                                # if drought period starts in previous year
                                if any(idx < 0 for idx in cf_idx_series.index.to_list()):

                                    # split data
                                    prev_yr = cf_idx_series.loc[:-1]
                                    this_yr = cf_idx_series[cf_idx_series.index >= 0]

                                    # assign data to previous year if previous year == first year
                                    if y == ra.drought_counters[thres].years[0]:

                                        # if drought occurs at beginning of first year, add an extra year
                                        ts_prev_yr = ts.copy()
                                        ts_prev_yr['year'] -= 1
                                        ts_prev_yr[ra.region] = 0

                                        # assign data and save extra year to collection of years
                                        idx_series = np.arange(steps+prev_yr.index[0], steps+prev_yr.index[-1]+1)
                                        ts_prev_yr.loc[idx_series, ra.region] = prev_yr.values
                                        ts_thres_tech[thres][ra.region][y-1] = ts_prev_yr

                                    # assign data to previous year if previous year != first year
                                    else:
                                        idx_series = np.arange(steps+prev_yr.index[0], steps+prev_yr.index[-1]+1)
                                        ts_thres_tech[thres][ra.region][y-1].loc[idx_series, ra.region] = prev_yr.values

                                    # assign data this year
                                    ts.loc[this_yr.index, ra.region] = this_yr.values

                                # if no overlap with previous year, assign data
                                else:
                                    ts.loc[cf_idx_series.index, ra.region] = cf_idx_series.values

                    ts_thres_tech[thres][ra.region][y] = ts

                # concate yearly time series to one series per region
                ts_thres_tech[thres][ra.region] = pd.concat(list(ts_thres_tech[thres][ra.region].values()))
                ts_thres_region[thres][ra.region] = ts_thres_tech[thres][ra.region]

            # merge country time series together
            dfs = list(ts_thres_tech[thres].values())
            ts_thres_tech[thres] = reduce(
                lambda left, right: pd.merge(
                    left, right, on=['year', 'hour'], how='outer'), dfs).fillna(0).sort_values(['year', 'hour'])

        self.drought_ts_tech[technology][corr_threshold] = ts_thres_tech
        self.drought_ts_region[technology][corr_threshold] = ts_thres_region

    @staticmethod
    def corr_matrix_pearson(df):
        '''
        Return Pearson correlation of input time series for all countries.
        Deactivated: If all columns in df hold only zero values, correlation
        matrix also holds zero.
        '''
        if 'duration' in df.columns:
            df = df.drop(['duration', 'year'], axis=1)
        else:
            df = df.drop(['year'], axis=1)
        return df.corr()

    def get_corr_matrix_pearson_tech(self, technology, region_results):
        '''
        Computes Pearson correlation of either integrated drought time series
        (drought events) or block-wise droght time series (drought windows) for
        each given technology across all regions.

        a) Integrated, i.e., each time step exists only once either in binary
        format for drought occurrence correlation (0 -> no drought time step,
        1 -> drought time step) or with actual capacity factor data for drought
        shape correlation (0 -> no drought time step, capacity factor -> drought
        time step);
        b) block-wise for drought windows (integrated not possible, creates overlap),
        each block corresponds to one specific drought duration.
        '''
        correlation_pearson_tech = {}
        correlation_pearson_tech_yearly = {}

        for corr_thres in self.corr_thresholds:

            correlation_pearson_tech[corr_thres] = {}
            correlation_pearson_tech_yearly[corr_thres] = {}

            # extract drought time series
            if self.counting_algorithm == 'event':

                # get binary drought time series
                if self.correlation_type[0] == 'occurrence':
                    self.create_droughts_time_series_integrated_binary(technology, corr_thres, region_results)

                # get drought time series with capacity factor info
                elif self.correlation_type[0] == 'shape':
                    self.create_droughts_time_series_integrated(technology, corr_thres, region_results)

            # get time series block-wise, one block for each duration (deprecated)
            elif self.counting_algorithm == 'window':
                self.create_droughts_time_series_duration_blocks_binary(technology, corr_thres, region_results)

            # yearly and total correlation matrix of all droughts: each technology for all regions
            for thres, df_all_years in self.drought_ts_tech[technology][corr_thres].items():

                # erase countries with zero values only
                df_nonzero = df_all_years.loc[:, (df_all_years != 0).any(axis=0)].copy()
                df_corr = self.corr_matrix_pearson(df_nonzero)

                # correlation across all years
                if df_corr.size > 1:
                    correlation_pearson_tech[corr_thres][thres] = df_corr

                correlation_pearson_tech_yearly[corr_thres][thres] = {}

                for y, df_single_year in df_all_years.groupby('year'):

                    # erase countries with zero values only
                    df_single_year_nozero = df_single_year.loc[:, (df_single_year != 0).any(axis=0)].copy()
                    df_single_year_corr = self.corr_matrix_pearson(df_single_year_nozero)

                    # yearly correlation
                    if df_single_year_corr.size > 1:
                        correlation_pearson_tech_yearly[corr_thres][thres][y] = df_single_year_corr

            # drop empty dict entries
            correlation_pearson_tech_yearly[corr_thres] = {thres: dict_drought_thres for (thres, dict_drought_thres)
                                                           in correlation_pearson_tech_yearly[corr_thres].items()
                                                           if bool(dict_drought_thres)}

        self.correlation_pearson_tech[technology] = correlation_pearson_tech
        self.correlation_pearson_tech_yearly[technology] = correlation_pearson_tech_yearly

        # export correlation to csv
        self.export_correlation_tech_to_csv(technology)

    def export_correlation_tech_to_csv(self, technology):
        '''
        Exports pearson correlation (one technology, all regions)
        to csv in running result folder.
        '''
        # export correlation of single technology for all regions across all years
        for corr_thres, corr_data in self.correlation_pearson_tech[technology].items():
            for drought_thres, df in corr_data.items():

                # create directory
                formats = ['png', 'pdf']
                for f in formats:
                    path_correlation = os.path.join(
                        self.path_run, technology, 'pearson_correlation_per_technology_all_regions',
                        f"correlation_threshold_{corr_thres}", drought_thres, f
                        )
                    Path(path_correlation).mkdir(parents=True, exist_ok=True)
                path_correlation = Path(path_correlation).parent

                # print correlation
                file_name = f"pearson_correlation_drought_threshold_{drought_thres}_all_years.csv"
                path = os.path.join(path_correlation, file_name)
                df.to_csv(path)

        # export correlation of single technology for all regions for single years
        for corr_thres, corr_data in self.correlation_pearson_tech_yearly[technology].items():
            for drought_thres, dict_yearly_df in corr_data.items():
                for y, df in dict_yearly_df.items():
                    path_correlation = os.path.join(
                        self.path_run, technology, 'pearson_correlation_per_technology_all_regions',
                        f"correlation_threshold_{corr_thres}", drought_thres)
                    file_name = f"pearson_correlation_drought_threshold_{drought_thres}_{y}.csv"
                    path = os.path.join(path_correlation, file_name)
                    df.to_csv(path)

    def save_analyzer_object(self):
        # TODO: https://datascience.stackexchange.com/questions/74172/picklingerror-in-pyspark-picklingerror-cant-pickle-class-main-person
        '''
        Stores objects as pickle in current working directory.
        '''
        os.chdir(os.path.join(self.path_run))
        file_name = 'analyzer.gz'
        with gzip.open(file_name, 'wb') as file:
            pickle.dump(self, file, protocol=pickle.HIGHEST_PROTOCOL)

    def analyze_all_regions(self, technology, region_results):
        '''
        Transforms region-specific results in one dataframe that has long format. Uses
        instance of Plotter for aggregated figures. Saves figures and pickles results
        for later use.
        '''
        # generate region weights according to capacities
        self.region_weights(technology)

        # collect results
        for ra in region_results:
            if not (ra.technology in ra.excluded and ra.region in ra.excluded[ra.technology]):
                self.mean_frequency_yearly[ra.technology][ra.region] = ra.mean_yearly_cum_all_t_long
                self.mean_frequency_q1_long[ra.technology][ra.region] = ra.mean_quarterly_cum_q1_long
                self.mean_frequency_q2_long[ra.technology][ra.region] = ra.mean_quarterly_cum_q2_long
                self.mean_frequency_q3_long[ra.technology][ra.region] = ra.mean_quarterly_cum_q3_long
                self.mean_frequency_q4_long[ra.technology][ra.region] = ra.mean_quarterly_cum_q4_long
                self.mean_frequency_q14_long[ra.technology][ra.region] = ra.mean_quarterly_cum_q14_long
                self.mean_frequency_q23_long[ra.technology][ra.region] = ra.mean_quarterly_cum_q23_long
                self.extremes_yearly_long[ra.technology][ra.region] = ra.extremes_yearly_long
                self.extremes_yearly[ra.technology][ra.region] = ra.extremes_yearly
                self.extremes_monthly_long[ra.technology][ra.region] = ra.extremes_monthly_long
                self.extremes_monthly_mid_long[ra.technology][ra.region] = ra.extremes_monthly_mid_long
                self.return_period_all_long[ra.technology][ra.region] = ra.return_period_all_long
                self.return_period_q1_long[ra.technology][ra.region] = ra.return_period_q1_long
                self.return_period_q2_long[ra.technology][ra.region] = ra.return_period_q2_long
                self.return_period_q3_long[ra.technology][ra.region] = ra.return_period_q3_long
                self.return_period_q4_long[ra.technology][ra.region] = ra.return_period_q4_long
                self.return_period_q14_long[ra.technology][ra.region] = ra.return_period_q14_long
                self.return_period_q23_long[ra.technology][ra.region] = ra.return_period_q23_long
                self.energy_deficit_cum[ra.technology][ra.region] = ra.energy_deficit_cum
                self.energy_deficit_cum_max[ra.technology][ra.region] = ra.energy_deficit_cum_max

        # transform data
        self.mean_frequency_yearly[technology] = self.dict_to_df(self.mean_frequency_yearly[technology])
        self.mean_frequency_q1_long[technology] = self.dict_to_df(self.mean_frequency_q1_long[technology])
        self.mean_frequency_q2_long[technology] = self.dict_to_df(self.mean_frequency_q2_long[technology])
        self.mean_frequency_q3_long[technology] = self.dict_to_df(self.mean_frequency_q3_long[technology])
        self.mean_frequency_q4_long[technology] = self.dict_to_df(self.mean_frequency_q4_long[technology])
        self.mean_frequency_q14_long[technology] = self.dict_to_df(self.mean_frequency_q14_long[technology])
        self.mean_frequency_q23_long[technology] = self.dict_to_df(self.mean_frequency_q23_long[technology])
        self.extremes_yearly_long[technology] = self.dict_to_df(self.extremes_yearly_long[technology])
        self.extremes_monthly_long[technology] = self.dict_to_df(self.extremes_monthly_long[technology])
        self.extremes_monthly_mid_long[technology] = self.dict_to_df(self.extremes_monthly_mid_long[technology])
        self.return_period_all_long[technology] = self.dict_to_df(self.return_period_all_long[technology])
        self.return_period_q1_long[technology] = self.dict_to_df(self.return_period_q1_long[technology])
        self.return_period_q2_long[technology] = self.dict_to_df(self.return_period_q2_long[technology])
        self.return_period_q3_long[technology] = self.dict_to_df(self.return_period_q3_long[technology])
        self.return_period_q4_long[technology] = self.dict_to_df(self.return_period_q4_long[technology])
        self.return_period_q14_long[technology] = self.dict_to_df(self.return_period_q14_long[technology])
        self.return_period_q23_long[technology] = self.dict_to_df(self.return_period_q23_long[technology])

        # overview of thresholds for run and labels as well mean cf
        for ra in region_results:
            self.drought_thresholds_run_labels[technology][ra.region] = dict(ra.drought_thresholds_run_labels)
            self.mean_capacity_factor_regions[technology][ra.region] = ra.time_series_mean

        self.mean_capacity_factor_regions[technology] = pd.DataFrame.from_dict(
            self.mean_capacity_factor_regions[technology],
            orient='index',
            columns=[technology]
            )

        # output path
        self.path_output = region_results[0].path_output

        # retrieve most extreme duration with yearly resolution across all regions
        self.weight_extremes(technology, region_results)

        # retrieve most extreme year and duration for each region and for the weighted most extreme year
        self.get_weighted_yearly_extremes_max_region(technology)

        # drought correlation for single technology across all regions
        self.get_corr_matrix_pearson_tech(technology, region_results)

        # correlation matrix of worst droughts
        # TODO: corr_worst = corr_matrix()

        # instantiate plotter
        self.plotter = Plotter(
            self.drought_type,
            self.counting_algorithm,
            self.start,
            self.end,
            self.config['technology_labels'],
            self.config['plots']
            )

# =============================================================================
#         # plot weighted yearly most extremes periods aggregated across all regions
#         if self.config['plots']['max_period_yearly']:
#             self.plotter.bars_single(
#                 self.extremes_yearly_weighted[technology]/24,
#                 'weighted_extremes_yearly',
#                 self.path_output,
#                 'across_regions',
#                 False,
#                 True,
#                 self.config['plots']['max_period_yearly']
#                 )
# =============================================================================
# =============================================================================
#         if self.config['plots']['aggregated']:
#             # plot most extremes periods per year for all regions
#             self.plotter.bars_all(
#                 self.path_output,
#                 self.extremes_yearly_long[technology],
#                 x='years',
#                 y='extremes_yearly',
#                 days=True,
#                 rotate=True,
#                 run_labels=self.drought_thresholds_run_labels[technology],
#                 )
#
#             # plot most extremes periods per month for all regions
#             self.plotter.bars_all(
#                 self.path_output,
#                 self.extremes_monthly_long[technology],
#                 x='months',
#                 y='extremes_monthly',
#                 days=True,
#                 rotate=True,
#                 run_labels=self.drought_thresholds_run_labels[technology],
#                 )
# =============================================================================

# =============================================================================
#             # plot frequency of all regions
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.mean_frequency_yearly[technology],
#                 'duration',
#                 'frequency',
#                 'threshold',
#                 'region',
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.mean_frequency_yearly[technology],
#                 'duration',
#                 'frequency',
#                 'region',
#                 'threshold',
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
# =============================================================================

# =============================================================================
#             # plot return period across the year for of all regions
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.return_period_all_long[technology],
#                 'return_period',
#                 'max_period',
#                 'threshold',
#                 'region',
#                 'any_quarter',
#                 {'return_period': 'return period', 'max_period': 'days'},
#                 f"Return periods of {self.drought_type.upper()} {self.counting_algorithm}s from any quarter",
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.return_period_all_long[technology],
#                 'return_period',
#                 'max_period',
#                 'region',
#                 'threshold',
#                 'any_quarter',
#                 {'return_period': 'return period', 'max_period': 'days'},
#                 f"Return periods of {self.drought_type.upper()} {self.counting_algorithm}s from any quarter",
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
# 
#             # plot return period in Q1 for of all regions
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.return_period_q1_long[technology].copy(),
#                 'return_period',
#                 'max_period',
#                 'threshold',
#                 'region',
#                 'Q1',
#                 {'return_period': 'return period', 'max_period': 'days'},
#                 f"Return periods of {self.drought_type.upper()} {self.counting_algorithm}s in Q1",
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
# 
#             # plot return period in Q2 for of all regions
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.return_period_q2_long[technology].copy(),
#                 'return_period',
#                 'max_period',
#                 'threshold',
#                 'region',
#                 'Q2',
#                 {'return_period': 'return period', 'max_period': 'days'},
#                 f"Return periods of {self.drought_type.upper()} {self.counting_algorithm}s in Q2",
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
# 
#             # plot return period in Q3 for of all regions
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.return_period_q3_long[technology].copy(),
#                 'return_period',
#                 'max_period',
#                 'threshold',
#                 'region',
#                 'Q3',
#                 {'return_period': 'return period', 'max_period': 'days'},
#                 f"Return periods of {self.drought_type.upper()} {self.counting_algorithm}s in Q3",
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
# 
#             # plot return period in Q4 for of all regions
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.return_period_q4_long[technology].copy(),
#                 'return_period',
#                 'max_period',
#                 'threshold',
#                 'region',
#                 'Q4',
#                 {'return_period': 'return period', 'max_period': 'days'},
#                 f"Return periods of {self.drought_type.upper()} {self.counting_algorithm}s in Q4",
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
# 
#             # plot return period in Q1+Q4 for of all regions
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.return_period_q14_long[technology].copy(),
#                 'return_period',
#                 'max_period',
#                 'threshold',
#                 'region',
#                 'Q1+Q4',
#                 {'return_period': 'return period', 'max_period': 'days'},
#                 f"Return periods of {self.drought_type.upper()} {self.counting_algorithm}s in Q1 and Q4 combined",
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
# 
#             # plot return period in Q2+Q3 for of all regions
#             self.plotter.lines_all(
#                 self.path_output,
#                 self.return_period_q23_long[technology].copy(),
#                 'return_period',
#                 'max_period',
#                 'threshold',
#                 'region',
#                 'Q2+Q3',
#                 {'return_period': 'return period', 'max_period': 'days'},
#                 f"Return periods of {self.drought_type.upper()} {self.counting_algorithm}s in Q2 and Q3 combined",
#                 days=True,
#                 run_labels=self.drought_thresholds_run_labels[technology]
#                 )
# =============================================================================

    def parallel_plot_corr_matrix_tech(self):
        '''
        Parallel plotting of drought correlation matrices (yearly and across all years).
        '''
        # parallelized plotting of correlation matrix across all years
        values_plot = tuple([(dict_corr_thres, technology, self.path_run)
                             for technology, dict_corr_thres in self.correlation_pearson_tech.items()])
        parallelize_plot(self.plotter.plot_correlation_tech, values_plot)

        # parallelized plotting of correlation matrix single year
        values_plot_yearly = tuple([(dict_corr_thres, technology, self.path_run)
                                    for technology, dict_corr_thres in self.correlation_pearson_tech_yearly.items()])
        parallelize_plot(self.plotter.plot_correlation_tech_yearly, values_plot_yearly)

    def prepare_drought_ts_region(self):
        '''
        For each region, gathers and merges drought time series of all technologies.
        Data comes from self.create_droughts_time_series_integrated() or related
        functions.
        
        Exports and removes attribute 'self.drought_ts_region' to reduce RAM requirements.

        Returns
        -------
        Former attribute 'self.drought_ts_region' with prepared data as local variable.

        '''
        # reorder data from correlation
        drought_ts_region_reordered = {}
        for tech, dict_corr_thres in self.drought_ts_region.items():
            for corr_thres, dict_drought_thres in dict_corr_thres.items():
                for drought_thres, dict_regions in dict_drought_thres.items():
                    for region, df in dict_regions.items():
                        if region not in drought_ts_region_reordered:
                            drought_ts_region_reordered[region] = {}
                        if corr_thres not in drought_ts_region_reordered[region]:
                            drought_ts_region_reordered[region][corr_thres] = {}
                        if drought_thres not in drought_ts_region_reordered[region][corr_thres]:
                            drought_ts_region_reordered[region][corr_thres][drought_thres] = {}
                        if tech not in drought_ts_region_reordered[region][corr_thres][drought_thres]:
                            drought_ts_region_reordered[region][corr_thres][drought_thres][tech] = df.rename(
                                columns={region: tech})

        # merge technology-specific dataframes
        merged_dfs = {}
        for region, dict_corr_thres in drought_ts_region_reordered.items():
            merged_dfs[region] = {}
            for corr_thres, dict_drought_thres in dict_corr_thres.items():
                merged_dfs[region][corr_thres] = {}
                for drought_thres, dict_technologies in dict_drought_thres.items():
                    merged_dfs[region][corr_thres][drought_thres] = {}
                    dfs = list(dict_technologies.values())
                    merged_df = reduce(
                        lambda left, right: pd.merge(left, right, on=['year', 'hour'], how='outer'), dfs).fillna(0)
                    merged_df.reset_index(inplace=True)
                    merged_df.sort_values(by=['year', 'hour'], inplace=True)
                    merged_df.set_index('hour', inplace=True)
                    merged_dfs[region][corr_thres][drought_thres] = merged_df


        # pickle region-oriented drought time series for all technologies
        cwd = os.getcwd()
        os.chdir(os.path.join(self.path_run))
        file_name = 'drought_ts_region.gz'
        with gzip.open(file_name, 'wb') as file:
            pickle.dump(merged_dfs, file, protocol=pickle.HIGHEST_PROTOCOL)
        os.chdir(cwd)
        
        # remove region-oriented drought time series for all technologies
        if hasattr(self, 'drought_ts_region'):
            del self.drought_ts_region

        return merged_dfs

    def export_correlation_region_to_csv(self):
        '''
        Exports pearson correlation (one region, all technologies) to csv
        in running result folder.
        '''
        # export correlation of single region for all technologies across all years
        for region, dict_corr_thres in self.correlation_pearson_region.items():
            for corr_thres, dict_drought_thres in dict_corr_thres.items():
                for drought_thres, df in dict_drought_thres.items():

                    # create directory
                    formats = ['png', 'pdf']
                    for f in formats:
                        path_correlation = os.path.join(
                            self.path_run, 'pearson_correlation_per_region_all_technologies', region,
                            f"correlation_threshold_{corr_thres}", drought_thres, f
                            )
                        Path(path_correlation).mkdir(parents=True, exist_ok=True)
                    path_correlation = Path(path_correlation).parent

                    # export correlation
                    name_file = f"pearson_correlation_drought_threshold_{drought_thres}_all_years.csv"
                    path = os.path.join(path_correlation, name_file)
                    df.to_csv(path)

        # export correlation of single region for all technologies for single years
        for region, dict_corr_thres in self.correlation_pearson_region_yearly.items():
            for corr_thres, dict_drought_thres in dict_corr_thres.items():
                for drought_thres, dict_yearly_df in dict_drought_thres.items():
                    for y, df in dict_yearly_df.items():
                        path_correlation = os.path.join(
                            self.path_run, 'pearson_correlation_per_region_all_technologies', region,
                            f"correlation_threshold_{corr_thres}", drought_thres)
                        name_file = f"pearson_correlation_drought_threshold_{drought_thres}_{y}.csv"
                        path = os.path.join(path_correlation, name_file)
                        df.to_csv(path)

    def corr_matrix_pearson_region(self):
        '''
        Organizes the drought correlation (for each region, all technologies) computation.
        i) prepare data based on drought correlation (for each technologies, all regions).
        ii) computes the correlation matrix.
        iii) exports correlation matrix.
        iv) plots correlation matrix.

        Returns
        -------
        None, but saves correlation matrix as attribute 'self.correlation_pearson_region'.

        '''
        # prepare drought time series -> derivate from drought_ts_tech (no matter what correlation kind)
        drought_ts_region = self.prepare_drought_ts_region()

        # correlation matrix of all droughts: each region for all technologies
        correlation_pearson_region = {}
        correlation_pearson_region_yearly = {}

        for region, dict_corr_thres in drought_ts_region.items():
            correlation_pearson_region[region] = {}
            correlation_pearson_region_yearly[region] = {}

            for corr_thres, dict_drought_thres in dict_corr_thres.items():
                correlation_pearson_region[region][corr_thres] = {}
                correlation_pearson_region_yearly[region][corr_thres] = {}

                for drought_thres, df in dict_drought_thres.items():
                    correlation_pearson_region_yearly[region][corr_thres][drought_thres] = {}

                    # erase technologies with zero values only
                    df_nozero = df.loc[:, (df != 0).any(axis=0)].copy()

                    # correlation matrix with all years
                    df_corr = self.corr_matrix_pearson(df_nozero)

                    # save only if meaningful, i.e. correlation matrix holds more than one technology
                    if df_corr.size > 1:
                        correlation_pearson_region[region][corr_thres][drought_thres] = df_corr

                    for y, df_yearly in df_nozero.groupby('year'):

                        # erase technologies with zero values only
                        df_yearly_nonzero = df_yearly.loc[:, (df_yearly != 0).any(axis=0)].copy()

                        # yearly correlation matrix
                        df_corr_yearly = self.corr_matrix_pearson(df_yearly_nonzero)

                        # save only if meaningful, i.e. correlation matrix holds more than one technology
                        if df_corr_yearly.size > 1:
                            correlation_pearson_region_yearly[region][corr_thres][drought_thres][y] = df_corr_yearly

                # drop empty dict entries (no correlation matrix exists for all years)
                correlation_pearson_region_yearly[region][corr_thres] = {
                    drought_thres: dict_drought_thres for (drought_thres, dict_drought_thres)
                    in correlation_pearson_region_yearly[region][corr_thres].items() if bool(dict_drought_thres)}

            # drop empty dict entries (no correlation matrix exists all thresholds)
            correlation_pearson_region[region] = {corr_thres: dict_corr_thres for (corr_thres, dict_corr_thres)
                                                  in correlation_pearson_region[region].items()
                                                  if bool(dict_corr_thres)}

        # save results as attribute
        self.correlation_pearson_region = correlation_pearson_region
        self.correlation_pearson_region_yearly = correlation_pearson_region_yearly

        # export both correlations to csv
        self.export_correlation_region_to_csv()

        # parallelized plotting of correlation matrix
        values_plot = tuple([(dict_corr_thres, region, self.path_run)
                             for region, dict_corr_thres in correlation_pearson_region.items()])
        parallelize_plot(self.plotter.plot_correlation_region, values_plot)

        # parallelized plotting of correlation matrix
        values_plot = tuple([(dict_corr_thres, region, self.path_run, True)
                             for region, dict_corr_thres in correlation_pearson_region_yearly.items()])
        parallelize_plot(self.plotter.plot_correlation_region_yearly, values_plot)

    def export_mean_capacity_factor(self):
        '''
        Merges and exports regional mean capacity factors.

        Returns
        -------
        None, but saves mean capacity factor in attribute 'self.mean_capacity_factor'.
        '''
        mean_capacity_factor = self.dict_to_df_merge(self.mean_capacity_factor_regions)
        mean_capacity_factor.to_csv(os.path.join(self.path_run, 'mean_availability_factor_and_drought_thresholds',
                                                 'mean_availability_factor.csv'))
        self.mean_capacity_factor = mean_capacity_factor

    def export_drought_thresholds(self):
        '''
        Computes and exports all drought thresholds.

        Returns
        -------
        min_thres : int
            Minimal threshold across all technologies and regions.
        max_thres : int
            Maximal threshold across all technologies and regions.
        '''
        drought_thresholds_all = {}
        min_thres = 0
        max_thres = 0

        for counter, (tech, df) in enumerate(self.mean_capacity_factor_regions.items()):
            
            drought_thresholds_all[tech] = {}
            
            for thres in self.config['drought_thresholds']['fractions_mean_capacity_factor']:

                # compute & export thresholds
                thres_regions = df * thres
                thres_regions.columns = [f"{thres:.2f}"]
                drought_thresholds_all[tech][thres] = thres_regions
                
            drought_thresholds_all[tech] = pd.concat(drought_thresholds_all[tech].values(), axis=1)
            
            drought_thresholds_all[tech].to_csv(
                os.path.join(self.path_run, 'mean_availability_factor_and_drought_thresholds',
                             f'drought_thresholds_{tech}_mean_availability_factor.csv'))

            # min, max for threshold heatmap
            if counter == 0:
                min_thres = np.nanmin(drought_thresholds_all[tech].to_numpy())
                max_thres = np.nanmax(drought_thresholds_all[tech].to_numpy())
            else:
                if min_thres > np.nanmin(drought_thresholds_all[tech].to_numpy()):
                    min_thres = np.nanmin(drought_thresholds_all[tech].to_numpy())
                if max_thres < np.nanmax(drought_thresholds_all[tech].to_numpy()):
                    max_thres = np.nanmax(drought_thresholds_all[tech].to_numpy())

        self.drought_thresholds_all = drought_thresholds_all

        return min_thres, max_thres


def parallelize_plot(function, values):
    '''
    The unbound function is mapped to all components of values. The mapping pairs
    are automatically parallelized.
    '''
    # TODO: harmonize with function above using *args
    # print id of parent process
    print(f'Parent process {os.getpid()} initializes parallelization of plotting.')

    # number of physical cores
    no_cpu = psutil.cpu_count(logical=False)

    # iniatialize parllelization
    with mp.Pool(no_cpu) as pool:
        results = pool.starmap(function, values)
    return results
