# -*- coding: utf-8 -*-
"""
Created on Tue May 24 17:34:11 2022

@author: mkittel
"""

import pandas as pd
import os
import yaml
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import openpyxl
import numpy as np
from matplotlib import rcParams, style

plt.rcdefaults()
style.use('seaborn-paper')
rcParams['font.family'] = 'sans-serif'


# %% input

path_in = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\timeseries_example.xlsx"
path_out = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\"
#path_out_png = os.path.join(path_out, 'png')
#path_out_pdf = os.path.join(path_out, 'pdf')

df = pd.read_excel(path_in, sheet_name='Tabelle1')

#%% correction factors

# calculate flh correction factors
mean_cf = df.mean()
flh_corr_factors_won = mean_cf / mean_cf['windon_DE2010']
flh_corr_factors_pv = mean_cf / mean_cf['pv_ES2010']
flh_corr_factors_pv_ts = 1 / flh_corr_factors_pv
corr_won = df / flh_corr_factors_won
corr_pv = df / flh_corr_factors_pv
corr_won = corr_won.add_suffix('_flh_corr')
corr_pv = corr_pv.add_suffix('_flh_corr')

#%% portfolios

#df['DE_50Wind_50PV_2010'] =  (df['windon_DE2010'] + df['pv_DE2010']) / 2
df['DE_33Wind_66PV_2010'] =  (df['windon_DE2010'] * 1/3 + df['pv_DE2010'] * 2/3)
df['DE_66Wind_33PV_2010'] =  (df['windon_DE2010'] * 2/3 + df['pv_DE2010'] * 1/3)

#df['ES_50Wind_50PV_2010'] =  (df['windon_ES2010'] + df['pv_ES2010']) / 2
df['ES_33Wind_66PV_2010'] =  (df['windon_ES2010'] * 1/3 + df['pv_ES2010'] * 2/3)
df['ES_66Wind_33PV_2010'] =  (df['windon_ES2010'] * 2/3 + df['pv_ES2010'] * 1/3)


#%% prepare data for plotting of duration curves

# prepare for plotting: duration curves 2010, pv and wind, and portfolio
dc_single = df[['windon_DE2010','windon_ES2010','pv_DE2010','pv_ES2010']]
dc_single = dc_single = {c: dc_single[c].sort_values(ascending=False).reset_index(drop=True) for c in dc_single}
dc_single = pd.DataFrame(list(dc_single.values())).T
dc_single.columns = ['onshore wind in Germany', 'onshore wind in Spain', 'solar PV in Germany', 'solar PV in Spain']

dc_portfolio = df[['DE_33Wind_66PV_2010','ES_33Wind_66PV_2010','DE_66Wind_33PV_2010','ES_66Wind_33PV_2010']]
dc_portfolio = dc_portfolio = {c: dc_portfolio[c].sort_values(ascending=False).reset_index(drop=True) for c in dc_portfolio}
dc_portfolio = pd.DataFrame(list(dc_portfolio.values())).T
dc_portfolio.columns = ['Germany (33% onshore wind, 67% solar PV)','Spain (33% onshore wind, 67% solar PV)',
                        'Germany (67% onshore wind, 33% solar PV)','Spain (67% onshore wind, 33% solar PV)']


#%% plot duration curve single

# Set font to TeX Gyre Termes
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Times New Roman'

ts = dc_single
file_name='duration_curve_single_techs.pdf'

colors = ['k', 'gray', 'k', 'gray']

fig, ax = plt.subplots(figsize=(7, 5))
for idx, c in enumerate(ts):
    if c[0] == 'o':
        plt.plot(ts[c], c=colors[idx], lw=1.4, ls='-', label=c)
    else:
        plt.plot(ts[c], c=colors[idx], lw=1.4, ls='--', label=c)
    
# further settings
ax.set_ylabel('availability factor $avail_t$', fontsize=15)
ax.set_xlabel('hour', fontsize=15)
ax.set_xlim(0, len(ts))
ax.set_ylim(0, 0.9)
plt.yticks(fontsize=12)
plt.xticks(fontsize=12)
plt.legend(loc='upper right', fontsize=13)
plt.tight_layout()
plt.savefig(os.path.join(path_out, file_name))
fig.show()


#%% plot duration curve portfolio

# Set font to TeX Gyre Termes
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Times New Roman'

ts = dc_portfolio
file_name='duration_curve_portfolios.pdf'

colors = ['k', 'gray', 'k', 'gray']
linestyle = ['-', '-', '--', '--']

fig, ax = plt.subplots(figsize=(7, 5))
for idx, c in enumerate(ts):
    plt.plot(ts[c], c=colors[idx], lw=1.4, ls=linestyle[idx], label=c)

# further settings
ax.set_ylabel('availability factor $avail_t$', fontsize=15)
ax.set_xlabel('hour', fontsize=15)
ax.set_xlim(0, len(ts))
ax.set_ylim(0, 0.9)
plt.yticks(fontsize=12)
plt.xticks(fontsize=12)
plt.legend(loc='upper right', fontsize=13)
plt.tight_layout()
plt.savefig(os.path.join(path_out, file_name))


#%%

# prepare data for plotting: won with reference won
dc_won_won = pd.concat([df.iloc[:, [1, 3]].copy(), corr_won.iloc[:, [1, 3]].copy()], axis=1)
dc_won_won.columns = [col[7:] for col in dc_won_won.columns]
dc_won_won = {c: dc_won_won[c].sort_values(ascending=False).reset_index(drop=True) for c in dc_won_won}
dc_won_won = pd.DataFrame(list(dc_won_won.values())).T

# prepare data for plotting: pv with reference won    
dc_won_pv = pd.concat([df.iloc[:, [5, 7]].copy(), corr_won.iloc[:, [5, 7]].copy()], axis=1)
dc_won_pv.columns = [col[3:] for col in dc_won_pv.columns]
dc_won_pv = {c: dc_won_pv[c].sort_values(ascending=False).reset_index(drop=True) for c in dc_won_pv}
dc_won_pv = pd.DataFrame(list(dc_won_pv.values())).T

# prepare data for plotting: won with reference pv
dc_pv_won = pd.concat([df.iloc[:, [1, 3]].copy(), corr_pv.iloc[:, [1, 3]].copy()], axis=1)
dc_pv_won.columns = [col[7:] for col in dc_pv_won.columns]
dc_pv_won = {c: dc_pv_won[c].sort_values(ascending=False).reset_index(drop=True) for c in dc_pv_won}
dc_pv_won = pd.DataFrame(list(dc_pv_won.values())).T
    
# prepare data for plotting: pv with reference pv
dc_pv_pv = pd.concat([df.iloc[:, [5, 7]].copy(), corr_pv.iloc[:, [5, 7]].copy()], axis=1)
dc_pv_pv.columns = [col[3:] for col in dc_pv_pv.columns]
dc_pv_pv = {c: dc_pv_pv[c].sort_values(ascending=False).reset_index(drop=True) for c in dc_pv_pv}
dc_pv_pv = pd.DataFrame(list(dc_pv_pv.values())).T

#%% time series data

# prepare wind data & calculate MA
wind = df.copy()
wind_w = pd.concat([wind.iloc[5112:8760], wind.iloc[0:5112, :]]).copy()

wind['windon_DE2010_MA'] = wind['windon_DE2010'].rolling(window=168).mean()
wind['windon_DE2012_MA'] = wind['windon_DE2012'].rolling(window=168).mean()
wind['windon_ES2010_MA'] = wind['windon_ES2010'].rolling(window=168).mean()
wind['windon_ES2012_MA'] = wind['windon_ES2012'].rolling(window=168).mean()

wind_w['windon_DE2010_MA'] = wind_w['windon_DE2010'].rolling(window=168).mean()
wind_w['windon_DE2012_MA'] = wind_w['windon_DE2012'].rolling(window=168).mean()
wind_w['windon_ES2010_MA'] = wind_w['windon_ES2010'].rolling(window=168).mean()
wind_w['windon_ES2012_MA'] = wind_w['windon_ES2012'].rolling(window=168).mean()

# timeseries scaling with PV ES as reference
wind['windon_DE2010_scaled'] = wind['windon_DE2010'] / flh_corr_factors_pv['windon_DE2010']
wind['windon_ES2010_scaled'] = wind['windon_ES2010'] / flh_corr_factors_pv['windon_ES2010']
wind['windon_DE2010_MA_scaled'] = wind['windon_DE2010_MA'] / flh_corr_factors_pv['windon_DE2010']
wind['windon_ES2010_MA_scaled'] = wind['windon_ES2010_MA'] / flh_corr_factors_pv['windon_ES2010']

wind_s = wind.iloc[4368:6576, :].copy()
wind_w = wind_w.iloc[4368:6576, :].copy()

won_de2010_s = wind_s.loc[:, 'windon_DE2010'].to_numpy()
won_de2010_ma_s = wind_s.loc[:, 'windon_DE2010_MA'].to_numpy()
won_de2012_s = wind_s.loc[:, 'windon_DE2012'].to_numpy()
won_de2012_ma_s = wind_s.loc[:, 'windon_DE2012_MA'].to_numpy()
won_es2010_s = wind_s.loc[:, 'windon_ES2010'].to_numpy()
won_es2010_ma_s = wind_s.loc[:, 'windon_ES2010_MA'].to_numpy()
won_es2012_s = wind_s.loc[:, 'windon_ES2012'].to_numpy()
won_es2012_ma_s = wind_s.loc[:, 'windon_ES2012_MA'].to_numpy()

won_de2010_w = wind_w.loc[:, 'windon_DE2010'].to_numpy()
won_de2010_ma_w = wind_w.loc[:, 'windon_DE2010_MA'].to_numpy()
won_de2012_w = wind_w.loc[:, 'windon_DE2012'].to_numpy()
won_de2012_ma_w = wind_w.loc[:, 'windon_DE2012_MA'].to_numpy()
won_es2010_w = wind_w.loc[:, 'windon_ES2010'].to_numpy()
won_es2010_ma_w = wind_w.loc[:, 'windon_ES2010_MA'].to_numpy()
won_es2012_w = wind_w.loc[:, 'windon_ES2012'].to_numpy()
won_es2012_ma_w = wind_w.loc[:, 'windon_ES2012_MA'].to_numpy()

won_de2010_s_scaled = wind_s.loc[:, 'windon_DE2010_scaled'].to_numpy()
won_de2010_ma_s_scaled = wind_s.loc[:, 'windon_DE2010_MA_scaled'].to_numpy()
won_es2010_s_scaled = wind_s.loc[:, 'windon_ES2010_scaled'].to_numpy()
won_es2010_ma_s_scaled = wind_s.loc[:, 'windon_ES2010_scaled'].to_numpy()

# prepare pv data & calculate MA
pv = df.copy()
pv_w = pd.concat([pv.iloc[5112:8760], pv.iloc[0:5112, :]]).copy()

pv['pv_DE2010_MA'] = pv['pv_DE2010'].rolling(window=168).mean()
pv['pv_DE2012_MA'] = pv['pv_DE2012'].rolling(window=168).mean()
pv['pv_ES2010_MA'] = pv['pv_ES2010'].rolling(window=168).mean()
pv['pv_ES2012_MA'] = pv['pv_ES2012'].rolling(window=168).mean()

pv_w['pv_DE2010_MA'] = pv_w['pv_DE2010'].rolling(window=168).mean()
pv_w['pv_DE2012_MA'] = pv_w['pv_DE2012'].rolling(window=168).mean()
pv_w['pv_ES2010_MA'] = pv_w['pv_ES2010'].rolling(window=168).mean()
pv_w['pv_ES2012_MA'] = pv_w['pv_ES2012'].rolling(window=168).mean()

# timeseries scaling with PV ES as reference
pv['pv_DE2010_scaled'] = pv['pv_DE2010'] / flh_corr_factors_pv['pv_DE2010']
pv['pv_ES2010_scaled'] = pv['pv_ES2010'] / flh_corr_factors_pv['pv_ES2010']
pv['pv_DE2010_MA_scaled'] = pv['pv_DE2010_MA'] / flh_corr_factors_pv['pv_DE2010']
pv['pv_ES2010_MA_scaled'] = pv['pv_ES2010_MA'] / flh_corr_factors_pv['pv_ES2010']

pv = pv.iloc[2904:5856].copy()
pv_w = pv_w.iloc[2904:5856].copy()

pv_de2010_s = pv.loc[:, 'pv_DE2010'].to_numpy()
pv_de2010_ma_s = pv.loc[:, 'pv_DE2010_MA'].to_numpy()
pv_de2012_s = pv.loc[:, 'pv_DE2012'].to_numpy()
pv_de2012_ma_s = pv.loc[:, 'pv_DE2012_MA'].to_numpy()
pv_es2010_s = pv.loc[:, 'pv_ES2010'].to_numpy()
pv_es2010_ma_s = pv.loc[:, 'pv_ES2010_MA'].to_numpy()
pv_es2012_s = pv.loc[:, 'pv_ES2012'].to_numpy()
pv_es2012_ma_s = pv.loc[:, 'pv_ES2012_MA'].to_numpy()

pv_de2010_w = pv_w.loc[:, 'pv_DE2010'].to_numpy()
pv_de2010_ma_w = pv_w.loc[:, 'pv_DE2010_MA'].to_numpy()
pv_de2012_w = pv_w.loc[:, 'pv_DE2012'].to_numpy()
pv_de2012_ma_w = pv_w.loc[:, 'pv_DE2012_MA'].to_numpy()
pv_es2010_w = pv_w.loc[:, 'pv_ES2010'].to_numpy()
pv_es2010_ma_w = pv_w.loc[:, 'pv_ES2010_MA'].to_numpy()
pv_es2012_w = pv_w.loc[:, 'pv_ES2012'].to_numpy()
pv_es2012_ma_w = pv_w.loc[:, 'pv_ES2012_MA'].to_numpy()

pv_de2010_s_scaled = pv.loc[:, 'pv_DE2010_scaled'].to_numpy()
pv_de2010_ma_s_scaled = pv.loc[:, 'pv_DE2010_MA_scaled'].to_numpy()
pv_es2010_s_scaled = pv.loc[:, 'pv_ES2010_scaled'].to_numpy()
pv_es2010_s_scaled = pv.loc[:, 'pv_ES2010_MA_scaled'].to_numpy()

#%% 

def plot_mbt_cbt(y, y_ma, threshold, title, path_out, file_name):
    # figure
    fig, ax = plt.subplots(figsize=(4, 4))
    x = list(range(y.size))
    ax.axhline(y=threshold, color='lightgray', linestyle='-', lw=1.5)
    ax.plot(x, y, color='k', lw=1.5)
    # ax.plot(x, y_ma, color='darkred', lw=1.3, label='MA168')
    ax.fill_between(x, y, threshold, where=y<threshold, color='darkseagreen', alpha=0.8)
    # ax.fill_between(x, y_ma, threshold, where=y_ma<threshold, color='red', alpha=0.6)
    ax.margins(x=0)
    ax.set_ylim(0, 0.35)
    plt.xticks([])
    plt.xlabel('time', fontsize=14)
    plt.ylabel('availability factor $\phi_t^{VRE}$', fontsize=14)
    # plt.title(title)
    plt.tight_layout()

    # legend settings
    # box = ax.get_position()
    # handles, labels = ax.get_legend_handles_labels()
    # red_patch = mpatches.Patch(color='red', label='MBT event', alpha=0.6)
    # handles.append(red_patch)
    # green_patch = mpatches.Patch(color='green', label='CBT event', alpha=0.6)
    # handles.append(green_patch)
    # ax.legend(handles=handles, loc='lower center', prop={'size': 10}, bbox_to_anchor=(0.5, -0.3), frameon=False, ncol=4)
    # fig.subplots_adjust(bottom=0.2)
    plt.show()
    plt.savefig(os.path.join(path_out, file_name))
    #plt.close(fig)
    
def plot_mbt_cbt_scaling(y, y_ma, threshold, title, path_out, file_name):
    # figure
    fig, ax = plt.subplots(figsize=(4, 4))
    x = list(range(y.size))
    ax.plot(x, y, color='k', lw=1.3)
    # ax.plot(x, y_ma, color='darkred', lw=1.3, label='MA168')
    ax.axhline(y=threshold, color='lightgray', linestyle='-', lw=1.5)
    ax.fill_between(x, y, threshold, where=y<threshold, color='green', alpha=0.6)
    # ax.fill_between(x, y_ma, threshold, where=y_ma<threshold, color='red', alpha=0.6)
    ax.margins(x=0)
    ax.set_ylim(0, 0.35)
    plt.xlabel('hour', fontsize=14)
    plt.ylabel('capacity factor', fontsize=14)
    # plt.title(title)
    plt.tight_layout()

    # legend settings
    # box = ax.get_position()
    # handles, labels = ax.get_legend_handles_labels()
    # red_patch = mpatches.Patch(color='red', label='MBT event', alpha=0.6)
    # handles.append(red_patch)
    # green_patch = mpatches.Patch(color='green', label='CBT event', alpha=0.6)
    # handles.append(green_patch)
    # ax.legend(handles=handles, loc='lower center', prop={'size': 10}, bbox_to_anchor=(0.5, -0.3), frameon=False, ncol=4)
    # fig.subplots_adjust(bottom=0.2)
    plt.show()
    plt.savefig(os.path.join(path_out, file_name))
    #plt.close(fig)

df = pd.DataFrame(won_de2010_s[110:308])


#%% plotting timeseries

plot_mbt_cbt(won_de2010_s[110:308], won_de2010_ma_s[110:320], 0.125, 'Wind onshore Germany - 2010 summer', path_out, 'won_DE_summer_2010.pdf')
plot_mbt_cbt(won_de2010_s, won_de2010_ma_s, 0.1, 'Wind onshore Germany - 2010 summer', path_out_png, 'won_DE_summer_2010.png')
plot_mbt_cbt(won_de2010_s, won_de2010_ma_s, 0.15, 'Wind onshore Germany - 2010 summer', path_out_png, 'won_DE_summer_2010_15.png')
plot_mbt_cbt(won_de2010_s, won_de2010_ma_s, 0.05, 'Wind onshore Germany - 2010 summer', path_out_png, 'won_DE_summer_2010_05.png')
plot_mbt_cbt(won_de2010_w, won_de2010_ma_w, 0.1, 'Wind onshore Germany - 2010 winter', path_out_png, 'won_DE_winter_2010.png')
plot_mbt_cbt(won_es2010_s, won_es2010_ma_s, 0.1, 'Wind onshore Spain - 2010 summer', path_out_png, 'won_ES_summer_2010.png')
plot_mbt_cbt(won_es2010_w, won_es2010_ma_w, 0.1, 'Wind onshore Spain - 2010 winter', path_out_png, 'won_ES_winter_2010.png')

plot_mbt_cbt(won_de2010_s_scaled[110:308], won_de2010_ma_s_scaled[140:308], 0.125, 'Wind onshore Germany - 2010 summer, scaled ts (ref PVES)', path_out, 'won_DE_summer_2010_scaled_ts.pdf')
plot_mbt_cbt(won_de2010_s_scaled, won_de2010_ma_s_scaled, 0.1, 'Wind onshore Germany - 2010 summer, scaled ts (ref PVES)', path_out_pdf, 'won_DE_summer_2010_scaled_ts.pdf')
plot_mbt_cbt(won_de2010_s[110:308], won_de2010_ma_s[140:308], 0.125*flh_corr_factors_pv['windon_DE2010'], 'Wind onshore Germany - 2010 summer, scaled thres (ref PVES)', path_out, 'won_DE_summer_2010_scaled_thres.pdf')
plot_mbt_cbt(won_de2010_s, won_de2010_ma_s, 0.1*flh_corr_factors_pv['windon_DE2010'], 'Wind onshore Germany - 2010 summer, scaled thres (ref PVES)', path_out_pdf, 'won_DE_summer_2010_scaled_thres.pdf')

plot_mbt_cbt(won_de2012_s, won_de2012_ma_s, 0.1, 'Wind onshore Germany - 2012 summer', path_out_png, 'won_DE_summer_2012.png')
plot_mbt_cbt(won_de2012_w, won_de2012_ma_w, 0.1, 'Wind onshore Germany - 2012 winter', path_out_png, 'won_DE_winter_2012.png')
plot_mbt_cbt(won_es2012_s, won_es2012_ma_s, 0.1, 'Wind onshore Spain - 2012 summer', path_out_png, 'won_ES_summer_2012.png')
plot_mbt_cbt(won_es2012_w, won_es2012_ma_w, 0.1, 'Wind onshore Spain - 2012 winter', path_out_png, 'won_ES_winter_2012.png')

plot_mbt_cbt(won_de2010_s, won_de2010_ma_s, 0.1, 'Wind onshore Germany - 2010 summer', path_out_pdf, 'won_DE_summer_2010.pdf')
plot_mbt_cbt(won_de2010_s, won_de2010_ma_s, 0.15, 'Wind onshore Germany - 2010 summer', path_out_pdf, 'won_DE_summer_2010_15.pdf')
plot_mbt_cbt(won_de2010_s, won_de2010_ma_s, 0.05, 'Wind onshore Germany - 2010 summer', path_out_pdf, 'won_DE_summer_2010_05.pdf')
plot_mbt_cbt(won_de2010_w, won_de2010_ma_w, 0.1, 'Wind onshore Germany - 2010 winter', path_out_pdf, 'won_DE_winter_2010.pdf')
plot_mbt_cbt(won_es2010_s, won_es2010_ma_s, 0.1, 'Wind onshore Spain - 2010 summer', path_out_pdf, 'won_ES_summer_2010.pdf')
plot_mbt_cbt(won_es2010_w, won_es2010_ma_w, 0.1, 'Wind onshore Spain - 2010 winter', path_out_pdf, 'won_ES_winter_2010.pdf')

plot_mbt_cbt(won_de2012_s, won_de2012_ma_s, 0.1, 'Wind onshore Germany - 2012 summer', path_out_pdf, 'won_DE_summer_2012.pdf')
plot_mbt_cbt(won_de2012_w, won_de2012_ma_w, 0.1, 'Wind onshore Germany - 2012 winter', path_out_pdf, 'won_DE_winter_2012.pdf')
plot_mbt_cbt(won_es2012_s, won_es2012_ma_s, 0.1, 'Wind onshore Spain - 2012 summer', path_out_pdf, 'won_ES_summer_2012.pdf')
plot_mbt_cbt(won_es2012_w, won_es2012_ma_w, 0.1, 'Wind onshore Spain - 2012 winter', path_out_pdf, 'won_ES_winter_2012.pdf')

plot_mbt_cbt(pv_de2010_w, pv_de2010_ma_w, 0.1, 'PV Germany - 2010 winter', path_out_png, 'pv_DE_winter_2010.png')
plot_mbt_cbt(pv_de2010_s, pv_de2010_ma_s, 0.1, 'PV Germany - 2010 summer', path_out_png, 'pv_DE_summer_2010.png')
plot_mbt_cbt(pv_es2010_w, pv_es2010_ma_w, 0.1, 'PV Spain - 2010 winter', path_out_png, 'pv_ES_winter_2010.png')
plot_mbt_cbt(pv_es2010_s, pv_es2010_ma_s, 0.1, 'PV Spain - 2010 summer', path_out_png, 'pv_ES_summer_2010.png')

plot_mbt_cbt(pv_de2012_w, pv_de2012_ma_w, 0.1, 'PV Germany - 2012 winter', path_out_png, 'pv_DE_winter_2012.png')
plot_mbt_cbt(pv_de2012_s, pv_de2012_ma_s, 0.1, 'PV Germany - 2012 summer', path_out_png, 'pv_DE_summer_2012.png')
plot_mbt_cbt(pv_es2012_w, pv_es2012_ma_w, 0.1, 'PV Spain - 2012 winter', path_out_png, 'pv_ES_winter_2012.png')
plot_mbt_cbt(pv_es2012_s, pv_es2012_ma_s, 0.1, 'PV Spain - 2012 summer', path_out_png, 'pv_ES_summer_2012.png')

plot_mbt_cbt(pv_de2010_w, pv_de2010_ma_w, 0.1, 'PV Germany - 2010 winter', path_out_pdf, 'pv_DE_winter_2010.pdf')
plot_mbt_cbt(pv_de2010_s, pv_de2010_ma_s, 0.1, 'PV Germany - 2010 summer', path_out_pdf, 'pv_DE_summer_2010.pdf')
plot_mbt_cbt(pv_es2010_w, pv_es2010_ma_w, 0.1, 'PV Spain - 2010 winter', path_out_pdf, 'pv_ES_winter_2010.pdf')
plot_mbt_cbt(pv_es2010_s, pv_es2010_ma_s, 0.1, 'PV Spain - 2010 summer', path_out_pdf, 'pv_ES_summer_2010.pdf')

plot_mbt_cbt(pv_de2012_w, pv_de2012_ma_w, 0.1, 'PV Germany - 2012 winter', path_out_pdf, 'pv_DE_winter_2012.pdf')
plot_mbt_cbt(pv_de2012_s, pv_de2012_ma_s, 0.1, 'PV Germany - 2012 summer', path_out_pdf, 'pv_DE_summer_2012.pdf')
plot_mbt_cbt(pv_es2012_w, pv_es2012_ma_w, 0.1, 'PV Spain - 2012 winter', path_out_pdf, 'pv_ES_winter_2012.pdf')
plot_mbt_cbt(pv_es2012_s, pv_es2012_ma_s, 0.1, 'PV Spain - 2012 summer', path_out_pdf, 'pv_ES_summer_2012.pdf')

#%% energy defict scaling?

won_de2010_s[113:305] 0.127


#%% duration curve

def plot_duration_curves(ts: pd.DataFrame(), title: str, path_out: str, file_name: str, cumE=False, threshold=False):
        
    # plot main lines
    colors = ['k', 'gray', 'k', 'gray']
    
    fig, ax = plt.subplots(figsize=(7, 5))
    for idx, c in enumerate(ts):
        if c[-3:] == 'ion':
            plt.plot(ts[c], c=colors[idx], lw=1.4, ls='--', label=c)
        else:
            plt.plot(ts[c], c=colors[idx], lw=1.4, label=c)
    
# =============================================================================
#     if cumE:
#         ax.axhline(lw=0.3, c='k')
#         
#         # fill cumulative energy fraction area
#         ax.fill_between(ts.hour, ts[name], where=ts[name]<threshold, color='r', alpha=0.5)
#         ax.set_ylim(-0.1, 1.6)
#     else:
#         ax.set_ylim(0, 1.6)
# =============================================================================
        
    # further settings
    ax.set_ylabel('availability factor $\phi_t^{VRE}$', fontsize=15)
    ax.set_xlabel('hour', fontsize=15)
    ax.set_xlim(0, len(ts))
    ax.set_ylim(0, 1.2)
    plt.yticks(fontsize=12)
    plt.xticks(fontsize=12)
    plt.legend(loc='upper right', fontsize=13)
    plt.tight_layout()
    plt.savefig(os.path.join(path_out, file_name))
    plt.close()

dc_pv_won.columns = ['Germany', 'Spain', 'Germany (FLH adjusted)', 'Spain (FLH adjusted)']
dc_pv_pv.columns = ['Germany', 'Spain', 'Germany (FLH adjusted)', 'Spain (FLH adjusted)']

# plot_duration_curves(dc_won_won, 'wind onshore', path_out, 'duration_curve_wind_onshore_ref_won.pdf')
# plot_duration_curves(dc_won_won, 'wind onshore', path_out_png, 'duration_curve_wind_onshore_ref_won.png')
# plot_duration_curves(dc_won_pv, 'pv', path_out_pdf, 'duration_curve_pv_ref_won.pdf')
# plot_duration_curves(dc_won_pv, 'pv', path_out_png, 'duration_curve_pv_ref_won.png')

plot_duration_curves(dc_pv_won, 'wind onshore', path_out, 'duration_curve_wind_onshore_ref_pv.pdf')
# plot_duration_curves(dc_pv_won, 'wind onshore', path_out_png, 'duration_curve_wind_onshore_ref_pv.png')
plot_duration_curves(dc_pv_pv, 'pv', path_out, 'duration_curve_pv_ref_pv.pdf')
# plot_duration_curves(dc_pv_pv, 'pv', path_out_png, 'duration_curve_pv_ref_pv.png')



#%%

df = pd.read_clipboard()
fig, ax = plt.subplots(figsize=(15, 4))
ax.axhline(y=0, color='lightgray', linestyle='-', lw=1.5)
plt.plot(df, c='black', lw=1.4)
ax.set_xlim(0, len(df))
plt.yticks(fontsize=12)
plt.xticks(fontsize=12)
plt.xlabel('hour', fontsize=15)
plt.ylabel('residual load [GW]', fontsize=15)
plt.tight_layout()

plt.show()
file_name = 'rldc.pdf'
plt.savefig(os.path.join(path_out, file_name))




#%% 

def get_threshold_of_cumulative_energy_fraction(ts, feature, fraction_cum_energy):
     '''
     Identifies the value in column 'capacity_factor' that corresponds to
     the fraction 'fraction_cum_energy' [0, 1] of the capcity factor duration curve.
     '''
     # generate duration curve
     ts = ts.copy().sort_values('capacity_factor', ascending=False)

     # compute distance between cumulative sum and desired cumultative energy amount
     ts['distance'] = np.cumsum(ts['capacity_factor']) - ts['capacity_factor'].sum() * (1-fraction_cum_energy)

     # reindex
     ts = ts.assign(Index=range(len(ts))).set_index('Index').reset_index()

     # determine first capacity factor and hour index where the distance > 0
     fraction_cum_energy_threshold = ts.where(ts['distance'] > 0).dropna().iloc[0]['capacity_factor']
     fraction_cum_energy_idx = ts.where(ts['distance'] > 0).dropna().iloc[0]['Index']

     return fraction_cum_energy_threshold, fraction_cum_energy_idx



#%% Steinbruch
#%%

# initial directory
os.chdir('C:\\git\\vre_droughts\\config')

# import settings
with open('config.yaml', 'r') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

# extract data
technology = 'wind_onshore'
path = os.path.join(config['paths']['input'], f"{config['paths'][technology]}.xlsx")

ts_wide = pd.read_excel(path, sheet_name='DE00', skiprows=10)
ts_long = pd.melt(ts_wide, id_vars=['Date', 'Hour'], var_name='year', value_name='capacity_factor')
year = 1982
#years = [1982, 1983]
threshold = 0.1
data = ts_long.loc[ts_long['year'] == year].copy()
data1 = data.iloc[np.arange(8680,8760), 3]
data2 = data.iloc[np.arange(1300,1375), 3]
data3 = data.iloc[np.arange(750,790), 3]
data4 = pd.concat([data1, data2, data3])

data4.to_excel('C:\\git\\vre_droughts\\input\\example.xlsx')
data4 = pd.read_excel('C:\\git\\vre_droughts\\input\\example.xlsx')

#%%
# plot window
path = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\timeseries_example.xlsx"
df = pd.read_excel(path, sheet_name='vre_cbt_fmbt_vmbt_spa')
df = df.iloc[7:,:]
threshold=0.101
fig, ax = plt.subplots(figsize=(15, 3))
x = np.arange(len(df))
x2 = x[6:11]
x3 = x[10:12]
x4 = x[13:23]
x5 = x[22:24]
y = df['wind'].to_numpy()
y2 = y[6:11]
y3 = y[10:12]
y4 = y[13:23]
y5 = y[22:24]
ax.plot(x, df['wind'], color='black')
ax.axhline(y=threshold, color='gray', linestyle='-', lw=2, alpha=0.7)
plt.plot((18, 18), (0.038, 0.101), c='k', linestyle='--') # #003c00
ax.fill_between(x2, y2, threshold, where=y2<=threshold, color='darkseagreen', alpha=0.8)
ax.fill_between(x3, y3, threshold, where=y3<=threshold, color='gray', alpha=0.9)
ax.fill_between(x4, y4, threshold, where=y4<=threshold, color='darkseagreen', alpha=0.8)
ax.fill_between(x5, y5, threshold, where=y5<=threshold, color='gray', alpha=0.9)
ax.margins(x=0)

arrow_cbt = dict(arrowstyle='<->', linewidth=2, color='darkseagreen')
arrow_not_counted = dict(arrowstyle='->', linewidth=2, color='gray')
ax.annotate('', xy=(6, 0.12), xytext=(10, 0.12), arrowprops=arrow_cbt)
ax.annotate('', xy=(10.5, 0.106), xytext=(10.5, 0.2), arrowprops=arrow_not_counted)
ax.annotate('', xy=(14, 0.12), xytext=(18, 0.12), arrowprops=arrow_cbt)
ax.annotate('', xy=(18, 0.12), xytext=(22, 0.12), arrowprops=arrow_cbt)
ax.annotate('', xy=(22.5, 0.106), xytext=(22.5, 0.2), arrowprops=arrow_not_counted)

plt.xlabel('time $t$', fontsize=14)
plt.xticks([])
plt.ylabel('availability factor $avail_t$', fontsize=14)
plt.tight_layout()

plt.text(33.5, 0.11, '$thres = 0.1$', fontsize=15, c='gray')
plt.text(7, 0.13, 'window', fontsize=15, c='darkseagreen')
plt.text(9.1, 0.205, 'not counted', fontsize=15, c='gray')
plt.text(15, 0.13, 'window', fontsize=15, c='darkseagreen')
plt.text(19, 0.13, 'window', fontsize=15, c='darkseagreen')
plt.text(21.1, 0.205, 'not counted', fontsize=15, c='gray')

plt.show()
path_out = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures"
plt.savefig(f'{path_out}\\window_0.1.pdf')

#%%
# plot event - 0.1
path = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\timeseries_example.xlsx"
df = pd.read_excel(path, sheet_name='vre_cbt_fmbt_vmbt_spa')
df = df.iloc[7:,:]
threshold=0.101
fig, ax = plt.subplots(figsize=(15, 3))
x = np.arange(len(df))
x2 = x[3:]
x2 = x2[:-10]
y1 = df['wind'].to_numpy()
y2 = y1[3:]
y2 = y2[:-10]
ax.plot(x, df['wind'], color='black')
ax.axhline(y=threshold, color='gray', linestyle='-', lw=2, alpha=0.7)
ax.axhline(y=0.2, color='gray', linestyle='-', lw=2, alpha=0.7)

ax.fill_between(x, y1, 0.2, where=y1<=0.21, color='darkseagreen', alpha=1)
ax.fill_between(x2, y2, threshold, where=y2<=threshold, color='green', alpha=1)


ax.margins(x=0)

arrow_cbt1 = dict(arrowstyle='<->', linewidth=2, color='green')
arrow_cbt2 = dict(arrowstyle='<->', linewidth=2, color='darkseagreen')
ax.annotate('', xy=(6, 0.12), xytext=(11, 0.12), arrowprops=arrow_cbt1)
ax.annotate('', xy=(14, 0.12), xytext=(23, 0.12), arrowprops=arrow_cbt1)
ax.annotate('', xy=(3.9, 0.22), xytext=(25.1, 0.22), arrowprops=arrow_cbt2)

plt.xlabel('time $t$', fontsize=14)
plt.xticks([])
plt.ylabel('availability factor $avail_t$', fontsize=14)
plt.tight_layout()

plt.text(33.5, 0.11, '$thres_1 = 0.1$', fontsize=15, c='gray')
plt.text(33.5, 0.21, '$thres_2 = 0.2$', fontsize=15, c='gray')
plt.text(7.7, 0.132, 'event$_{thres_1}$', fontsize=15, c='green')
plt.text(17.3, 0.132, 'event$_{thres_1}$', fontsize=15, c='green')
plt.text(13.5, 0.232, 'event$_{thres_2}$', fontsize=15, c='darkseagreen')

plt.show()
plt.savefig(f'{path_out}\\event_0.1_0.2.pdf')


#%%
# plot event - 0.125
path = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\timeseries_example.xlsx"
df = pd.read_excel(path, sheet_name='vre_cbt_fmbt_vmbt_spa')
df = df.iloc[7:,:]
threshold=0.125
fig, ax = plt.subplots(figsize=(15, 3))
x = np.arange(len(df))
x2 = x[3:]
x2 = x2[:-10]
y1 = df['wind'].to_numpy()
y2 = y1[3:]
y2 = y2[:-10]
ax.plot(x, df['wind'], color='black')
ax.axhline(y=threshold, color='gray', linestyle='-', lw=2, alpha=0.7)
ax.fill_between(x2, y2, threshold, where=y2<=threshold, color='darkseagreen', alpha=0.8)
ax.margins(x=0)

arrow_cbt = dict(arrowstyle='<->', linewidth=2, color='darkseagreen')
ax.annotate('', xy=(5, 0.146), xytext=(24, 0.146), arrowprops=arrow_cbt)

plt.xlabel('time $t$', fontsize=14)
plt.xticks([])
plt.ylabel('availability factor $\phi$', fontsize=14)
plt.tight_layout()

plt.text(34.5, 0.13, '$thres = 0.125$', fontsize=14, c='gray')
plt.text(13.5, 0.156, 'event', fontsize=14, c='darkseagreen')


plt.show()
plt.savefig(f'{path_out}\\event_0.125.pdf')


#%%
# plot CBT vs. MBT I - CBT below and MBT above threshold
path = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\timeseries_example.xlsx"
df = pd.read_excel(path, sheet_name='vre_cbt_fmbt_vmbt_spa')
df = df.iloc[7:,:]
threshold=0.101
fig, ax = plt.subplots(figsize=(15, 5))
x = np.arange(len(df))
x2 = x[3:]
x2 = x2[:-9]
y1 = df['wind'].to_numpy()
y2 = y1[3:]
y2 = y2[:-9]
ax.plot(x, df['wind'], color='black')
ax.axhline(y=threshold, color='gray', linestyle='-', lw=2, alpha=0.7)
ax.fill_between(x2, y2, threshold, where=y2<=threshold, color='green', alpha=0.8)
ax.fill_between(x2, y2, threshold, where=y2>threshold-0.01, color='red', alpha=0.9)

ax.margins(x=0)

arrow_cbt = dict(arrowstyle='<->', linewidth=2, color='green')
arrow_mbt = dict(arrowstyle='<->', linewidth=2, color='red')
ax.annotate('', xy=(6, 0.107), xytext=(11, 0.107), arrowprops=arrow_cbt)
ax.annotate('', xy=(14, 0.107), xytext=(23, 0.107), arrowprops=arrow_cbt)
ax.annotate('', xy=(3, 0.19), xytext=(28, 0.19), arrowprops=arrow_mbt)

plt.xlabel('time', fontsize=14)
plt.xticks([])
plt.ylabel('availability factor', fontsize=14)
plt.tight_layout()

plt.text(32.2, 0.091, 'threshold = 0.1', fontsize=14, c='gray')
plt.text(7.9, 0.109, 'CBT', fontsize=14, c='green')
plt.text(17.5, 0.109, 'CBT', fontsize=14, c='green')
plt.text(14, 0.192, 'MBT', fontsize=14, c='red')


plt.show()
plt.savefig(f'{path_out}\\cbt_mbt_areas_below_above_0.1.pdf')

#%%
# plot CBT vs. MBT II - both CBT and MBT below threshold
path = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\timeseries_example.xlsx"
df = pd.read_excel(path, sheet_name='vre_cbt_fmbt_vmbt_spa')
threshold=0.101
# df['ma'] = df['wind'].rolling(window=7).mean()
df = df.iloc[7:, :].reset_index(drop=True)
df.loc[25, 'ma'] = 0.1
fig, ax = plt.subplots(figsize=(15, 5))
x = np.arange(len(df))
ax.plot(x, df['wind'], color='black')
ax.plot(x, df['ma'], color='red')
ax.axhline(y=threshold, color='gray', linestyle='-', lw=2, alpha=0.7)
ax.fill_between(x, df['ma'], threshold, where=df['ma']<=threshold, color='red', alpha=0.9)
ax.fill_between(x, df['wind'], threshold, where=df['wind']<=threshold, color='green', alpha=0.8)

ax.margins(x=0)

arrow_cbt = dict(arrowstyle='<->', linewidth=2, color='green')
arrow_mbt = dict(arrowstyle='<->', linewidth=2, color='red')
ax.annotate('', xy=(6, 0.107), xytext=(11, 0.107), arrowprops=arrow_cbt)
ax.annotate('', xy=(14, 0.107), xytext=(23, 0.107), arrowprops=arrow_cbt)
ax.annotate('', xy=(9.8, 0.121), xytext=(25, 0.121), arrowprops=arrow_mbt)

plt.xlabel('time', fontsize=14)
plt.xticks([])
plt.ylabel('availability factor', fontsize=14)
plt.tight_layout()

plt.text(32.2, 0.091, 'threshold = 0.1', fontsize=14, c='gray')
plt.text(7.7, 0.109, 'CBT', fontsize=14, c='green')
plt.text(17.5, 0.109, 'CBT', fontsize=14, c='green')
plt.text(17, 0.123, 'MBT', fontsize=14, c='red')

plt.show()
plt.savefig(f'{path_out}\\cbt_mbt_areas_below_0.1.pdf')

#%%
# plot CBT vs. MBT vs. SPC
path = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\timeseries_example.xlsx"
df = pd.read_excel(path, sheet_name='vre_cbt_fmbt_vmbt_spa')
threshold=0.101

# Set font to TeX Gyre Termes
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Times New Roman'

#df['ma'] = df['wind2'].rolling(window=7).mean()
#df['ma23'] = df['wind2'].rolling(window=23).mean()
df = df.iloc[7:, :].reset_index(drop=True)
df.loc[10, 'ma'] = 0.1
df.loc[25, 'ma'] = 0.1
df.loc[22, 'spa_0.1'] = 0.398


fig, ax = plt.subplots(2, 1, figsize=(15, 9), gridspec_kw={'height_ratios': [8, 1.5]}, sharex=True)

x = np.arange(len(df))
# ax.plot(x, df['wind'], color='black')
ax[0].axhline(y=threshold, color='lightgray', linestyle='-', lw=2, alpha=0.7)
phi, = ax[0].plot(x, df['wind'], color='black', linewidth=2.5, label=r'availability factor $avail_t$')
ma10, = ax[0].plot(x, df['ma'], color='black', linestyle='dashed', label=r'moving average $MA_t^{avail}(intdur = 10$)') # 'darkorange'
ma25, = ax[0].plot(x, df['ma23'], color='black', linestyle='dashed', dashes=(7, 3), label=r'moving average $MA_t^{avail}(intdur = 25$)')

# Custom Line Style: Small Dash followed by a Dot
custom_line_style = (0, (1, 2, 3))  # Tuple of (dash_length, gap_length)

ma50, = ax[0].plot(x, df['ma40'], color='black', linestyle='dashed', dashes=(10, 6), label=r'moving average $MA_t^{avail}(intdur = 50$)')
ma25_below = ax[0].scatter(x, df['ma23_zero'], color='indianred', marker='o', s=90, label=r'moving average $MA_t^{avail}(intdur = 25) < thres$')
spa, = ax[0].plot(x, df['spa_0.1 (wind)'], color='black', linestyle='dotted', linewidth=1.5, label=r'SPA energy deficit $ED_t^{SPA}$') # 'teal'
spa_max = ax[0].scatter(x, df['spa_0.1_max'], color='cadetblue', marker='D', s=60, label='maximum SPA energy deficit $ED_t^{SPA}$')

ax[0].fill_between(x, df['ma'], threshold, where=df['ma']<=threshold, color='sandybrown', alpha=0.4)
ax[0].fill_between(x, df['wind'], threshold, where=df['wind']<=threshold, color='darkseagreen', alpha=0.5)

ax[0].text(33.5, 0.105, '$thres=0.1$', fontsize=14, c='gray')

ax[0].margins(x=0)
ax[0].set_ylim(0, 0.42)

cbt, = ax[1].plot(x, df['CBT'], color='darkseagreen', linewidth=18, label='CBT')
fmbt, = ax[1].plot(x, df['FMBT15'], color='sandybrown', linewidth=18, label='FMBT ($intdur = 10$)')
vmbt, = ax[1].plot(x, df['VMBT25'], color='indianred', linewidth=18, label='VMBT ($intdur = 25$)')
spa_drought, = ax[1].plot(x, df['SPA_drought'], color='cadetblue', linewidth=18, label='SPA')
# ax[1].plot(x, df['SPA_recovery'], color='mediumpurple', linewidth=10, label='SPA - recovery')

ax[1].set_ylim(0.54, 0.96)

plt.xlabel('time $t$', fontsize=15)
plt.xticks([])
ax[0].tick_params(axis='y', labelsize=13)
ax[1].set_yticks([])
plt.tight_layout()

#handles0=[cbt, fmbt, vmbt, spa_drought, phi, ma10, ma25, ma50, spa, ma25_below, spa_max]
#ax[0].legend(handles=handles0, loc='lower center', prop={'size': 16},
#             bbox_to_anchor=(0.49, -0.55), frameon=False, ncol=3) #, handlelength=2, handletextpad=0.2)
ax[0].legend(loc='lower center', prop={'size': 16}, bbox_to_anchor=(0.49, -0.475), frameon=False, ncol=3)
fig.subplots_adjust(hspace=0.03, bottom=0.25)
ax[1].legend(loc='lower center', prop={'size': 16}, bbox_to_anchor=(0.63, -1.45), frameon=False, ncol=4)

fig.subplots_adjust(hspace=0.03, bottom=0.18)
# =============================================================================
# handles2=[cbt, fmbt, vmbt, spa_drought]
# ax[1].legend(handles=handles2, loc='lower center', prop={'size': 16},
#              bbox_to_anchor=(0.49, -2.8), frameon=False, ncol=7, handlelength=1.1, handletextpad=0.5)
# 
# handles3 = [spa_max]
# # Add the third legend below the two existing legends
# fig.legend(handles=handles3, loc='lower center', prop={'size': 16},
#            bbox_to_anchor=(0.49, 0.01), frameon=False, ncol=2, handlelength=2, handletextpad=0.2)
# 
# =============================================================================

#ax[1].text(-0.8, 0.67, '$\\delta$', fontsize=17, c='black')
ax[1].text(7.2, 0.865, '$dur^{CBT} = 6$', fontsize=15, c='black') # CBT
ax[1].text(17, 0.865, '$dur^{CBT} = 9$', fontsize=15, c='black') # CBT
ax[1].text(15.5, 0.767, '$dur^{FMBT} = 15$', fontsize=15, c='black') # FMBT
ax[1].text(12.5, 0.665, '$dur^{VMBT} = 25$', fontsize=15, c='black') # VMBT
ax[1].text(12.8, 0.565, '$dur^{SPA} = 17$', fontsize=15, c='black') # SPA


# Enable LaTeX rendering for the entire plot
plt.rcParams['text.usetex'] = True

plt.show()
plt.savefig(f'{path_out}\\cbt_mbt_spc_0.1_neu.pdf')

#%%
# plot different means for MBT

from matplotlib.patches import Rectangle
path = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\timeseries_example.xlsx"
df = pd.read_excel(path, sheet_name='mbt_cbt_neu')
df['ma10'] = df['wind'].rolling(window=10).mean()
df['ma26'] = df['wind'].rolling(window=26).mean()
df['ma40'] = df['wind'].rolling(window=40).mean()
df['ma26'].iloc[75] = 0.0999
df = df.iloc[23:, :].reset_index()


threshold = 0.101
fig, ax = plt.subplots(figsize=(15, 5))
x = np.arange(len(df))
x2 = x[27:]
x2 = x2[:-10]
y1 = df['wind'].to_numpy()
y2 = y1[27:]
y2 = y2[:-10]

x3 = np.arange(len(df))
x4 = x3[38:]
x4 = x4[:-9]
y3 = df['ma10'].to_numpy()
y4 = y3[38:]
y4 = y4[:-9]

ax.plot(x[24:], df['wind'].iloc[24:], color='black', label='availability factor')
ax.plot(x[24:], df['ma10'].iloc[24:], color='black', label='moving average (10 time steps)', linestyle='--')
ax.plot(x[24:], df['ma26'].iloc[24:], color='red', label='moving average (26 time steps)')
ax.plot(x[24:], df['ma40'].iloc[24:], color='black', label='moving average (40 time steps)', linestyle=':')
ax.axhline(y=threshold, color='gray', linestyle='-', lw=2.2, alpha=0.7)
ax.axvline(x=52, color='red', linestyle='-', lw=2.2, alpha=0.7)
#ax.add_patch(Rectangle((28, 0), 24.2, 0.101, facecolor='red', alpha=0.5))
ax.margins(x=0)


arrow_one_sided = dict(arrowstyle='<-', linewidth=2, color='black')
arrow_mbt = dict(arrowstyle='<->', linewidth=2, color='red')
ax.annotate('', xy=(47, df['ma10'].min()), xytext=(47, 0.101), arrowprops=arrow_one_sided)
ax.annotate('', xy=(47, df['ma40'].min()+0.001), xytext=(47, 0.1), arrowprops=arrow_one_sided)
#ax.annotate('', xy=(28, 0.033), xytext=(52.2, 0.033), arrowprops=arrow_mbt)
ax.annotate('', xy=(27, 0.195), xytext=(52.1, 0.195), arrowprops=arrow_mbt)

plt.xlabel('time', fontsize=14)
plt.xticks([])
plt.ylabel('availability factor', fontsize=14)
plt.tight_layout()
ax.set_ylim(0.02, 0.21)

plt.text(56, 0.091, 'threshold = 0.1', fontsize=14, c='gray')
# plt.text(7.9, 0.112, 'CBT', fontsize=14, c='green')
# plt.text(17.5, 0.112, 'CBT', fontsize=14, c='green')
#plt.text(33, 0.023, 'MBT event with duration of 26 time steps', fontsize=14, c='red')
plt.text(33, 0.198, 'MBT event with duration of 26 time steps', fontsize=14, c='red')
#plt.text(52, 0.099, 'x', fontsize=14, c='red')

ax.legend(loc='lower center', prop={'size': 10}, bbox_to_anchor=(0.5, -0.13), frameon=False, ncol=4)
fig.subplots_adjust(bottom=0.1)

plt.show()
plt.savefig(f'{path_out}\\mbt_iterative.pdf')


#%%
# plot positive residual load events

path = "C:\\Users\\mkittel\\OneDrive - DIW Berlin\\DIW\\04_Projekte\\01_VRE_droughts\\04_papers\\method_paper\\01_figures\\timeseries_example.xlsx"
df = pd.read_excel(path, sheet_name='rl_cbt_fmbt_vmbt_spa')

# Set font to TeX Gyre Termes
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Times New Roman'

threshold=0
df = df.iloc[20:, :].reset_index(drop=True)
fig, ax = plt.subplots(2, 1, figsize=(15, 9), gridspec_kw={'height_ratios': [8, 2.1]}, sharex=True)

x = np.arange(len(df))

# Custom Line Style: Small Dash followed by a Dot
custom_line_style = (0, (1, 2, 3))  # Tuple of (dash_length, gap_length)
ax[0].axhline(y=threshold, color='lightgray', linestyle='-', lw=2, alpha=0.7)
ax[0].plot(x, df['rl'], color='black', linewidth=2.5, label=r'residual load $rl_t$')
ax[0].plot(x, df['rl_adjusted'], color='gray', linewidth=2.5, label=r'adjusted residual load $rl^{adj}_t$')
ax[0].plot(x, df['ma_10'], color='black', linestyle='dashed', label=r'moving average $MA_t^{rl}(intdur = 10$)') # 'darkorange'
ax[0].plot(x, df['ma_30'], color='black', linestyle='dashed', dashes=(7, 3), label=r'moving average $MA_t^{rl}(intdur = 30$)')

ax[0].scatter(x, df['ma_30_above_zero'], color='indianred', marker='o', s=90, label=r'moving average $MA_t^{rl}(intdur = 30) > 0$')
ax[0].plot(x, df['SPA_curve'], color='black', linestyle='dotted', linewidth=1.5, label=r'SPA energy deficit $ED_t^{SPA}$') # 'teal'
ax[0].scatter(x, df['SPA_curve_max'], color='cadetblue', marker='D', s=60, label=r'maximum SPA energy deficit $ED_t^{SPA}$')
ax[0].plot(x, df['SPA_curve_adj'], color='gray', linestyle='dotted', linewidth=1.5, label='adjusted SPA energy deficit $ED_t^{SPA,adj}$') # 'teal'
ax[0].scatter(x, df['SPA_curve_adj_max'], color='thistle', marker='D', s=60, label=r'maximum adjusted SPA energy deficit $ED_t^{SPA,adj}$')
ax[0].fill_between(x, df['ma_10'], threshold, where=df['ma_10']>=threshold, color='sandybrown', alpha=0.4)
ax[0].fill_between(x, df['rl'], threshold, where=df['rl']>=threshold, color='darkseagreen', alpha=0.5)

# ax[0].text(35, 0.105, '$\\tau=0.1$', fontsize=14, c='gray')

ax[0].margins(x=0)
# ax[0].set_ylim(0, 0.42)

ax[1].plot(x, df['CBT'], color='darkseagreen', linewidth=18, label='CAZ')
ax[1].plot(x, df['FMBT10'], color='sandybrown', linewidth=18, label='FMAZ ($intdur = 10$)')
ax[1].plot(x, df['VMBT30'], color='indianred', linewidth=18, label='VMAZ ($intdur = 30$)')
ax[1].plot(x, df['SPA_drought'], color='cadetblue', linewidth=18, label='SPA duration')
ax[1].plot(x, df['SPA_recovery'], color='teal', linewidth=18, label='SPA recovery period')
ax[1].plot(x, df['SPA_adj_drought'], color='thistle', linewidth=18, label='SPA$^{adj}$ duration')
ax[1].plot(x, df['SPA_adj_recovery'], color='plum', linewidth=18, label='SPA$^{adj}$ recovery period')

ax[1].set_ylim(0.443, 0.96)

plt.xlabel('time $t$', fontsize=15)
plt.xticks([])
ax[1].set_yticks([])
ax[0].tick_params(axis='y', labelsize=13)
ax[0].set_ylabel('GW', fontsize=15)
plt.tight_layout()


ax[0].legend(loc='lower center', prop={'size': 15}, bbox_to_anchor=(0.49, -0.575), frameon=False, ncol=3)
fig.subplots_adjust(hspace=0.03, bottom=0.25)
ax[1].legend(loc='lower center', prop={'size': 15}, bbox_to_anchor=(0.49, -1.7), frameon=False, ncol=4)

# Enable LaTeX rendering for the entire plot
plt.rcParams['text.usetex'] = True

# ax[1].text(-0.82, 0.65, '$\\delta$', fontsize=18, c='black')

ax[1].text(7, 0.87, '$dur^{CAZ} = 7$', fontsize=15, c='black') # CBT
ax[1].text(18.55, 0.87, '$dur^{CAZ} = 4$', fontsize=14, c='black') # CBT
ax[1].text(35, 0.87, '$dur^{CAZ} = 12$', fontsize=15, c='black') # CBT
ax[1].text(13, 0.765, '$dur^{FMAZ} = 9$', fontsize=15, c='black') # FMBT
ax[1].text(41, 0.765, '$dur^{FMAZ} = 12$', fontsize=15, c='black') # FMBT
ax[1].text(29, 0.665, '$dur^{VMAZ} = 30$', fontsize=15, c='black') # VMBT
ax[1].text(12, 0.565, '$dur^{SPA} = 18$', fontsize=15, c='black') # SPA
ax[1].text(22.5, 0.575, '$recper^{SPA} = 4$', fontsize=11, c='black') # SPA
ax[1].text(35, 0.565, '$dur^{SPA} = 13$', fontsize=15, c='black') # SPA
ax[1].text(45.5, 0.565, '$recper^{SPA} = 8$', fontsize=15, c='black') # SPA
ax[1].text(21.5, 0.465, '$dur^{SPA,adj} = 39$', fontsize=15, c='black') # SPA adjusted
ax[1].text(47, 0.465, '$recper^{SPA,adj} = 13$', fontsize=15, c='black') # SPA adjusted

plt.show()
plt.savefig(f'{path_out}\\rl_cbt_mbt_spc.pdf')

