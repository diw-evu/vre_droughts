# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 22:50:15 2023

@author: mkittel
"""

import os
import pandas as pd
import numpy as np
import time
from pathlib import Path
from functools import reduce
import pickle
import gzip
from scipy import stats

# import own modules
from config_check import InputError, ConfigCheck
from plotter import Plotter
from drought_counter import DroughtCounter
from input_extractor import InputExtractor

# TODO: rename to technology analyzer
class RegionAnalyzer():
    '''
    Objects initiate drought counting for a single region varying in threshold.
    Objects store regional results and initiate plotting.
    '''

    def __init__(
            self,
            path_run,
            path_project,
            config,
            technology,
            region
            ):
        self.drought_type = config['drought_type']
        self.counting_algorithm = config['counting_algorithm']
        self.start = config['start']
        self.end = config['end']
        self.absolute_drought_thresholds = config['drought_thresholds']['absolute']
        self.fractions_mean_capacity_factor = config['drought_thresholds']['fractions_mean_capacity_factor']
        self.technology = technology
        self.region = region
        self.excluded = config['excluded']
        self.path_input = os.path.join(path_project, 'input')
        self.drought_thresholds_input = None
        self.drought_thresholds_run = None
        self.drought_thresholds_run_input = None
        self.drought_thresholds_labels = None
        self.drought_thresholds_run_labels = None

        # create directories
        self.create_region_directory(path_run, technology, region)

        # instantiate input extractor
        self.input_extractor = InputExtractor(config, config['years'])

        # container for drought_counter objects
        self.drought_counters = {}

        # aggregated results
        self.drought_q_thresholds = {}
        self.drought_thresholds_fractions_mean_capacity_factor = {}
        self.energy_deficit_cum = {}
        self.energy_deficit_cum_max = {}
        self.mean_yearly_cum = {}
        self.mean_yearly_cum_all_t = {}  # not used in Analyzer instance
        self.mean_yearly_cum_all_t_long = None
        self.mean_quarterly_cum = {}
        self.mean_quarterly_cum_q1 = {}
        self.mean_quarterly_cum_q2 = {}
        self.mean_quarterly_cum_q3 = {}
        self.mean_quarterly_cum_q4 = {}
        self.mean_quarterly_cum_q14 = {}
        self.mean_quarterly_cum_q23 = {}
        self.mean_quarterly_cum_q1_long = {}
        self.mean_quarterly_cum_q2_long = {}
        self.mean_quarterly_cum_q3_long = {}
        self.mean_quarterly_cum_q4_long = {}
        self.mean_quarterly_cum_q14_long = {}
        self.mean_quarterly_cum_q23_long = {}
        self.extremes_monthly = {}
        self.extremes_monthly_mid = {}
        self.extremes_monthly_long = None
        self.extremes_monthly_mid_long = None
        self.extremes_yearly = {}
        self.extremes_yearly_long = None
        self.return_period_all = {}
        self.return_period_q1 = {}
        self.return_period_q2 = {}
        self.return_period_q3 = {}
        self.return_period_q4 = {}
        self.return_period_q14 = {}
        self.return_period_q23 = {}
        self.return_period_all_long = None
        self.return_period_q1_long = None
        self.return_period_q2_long = None
        self.return_period_q3_long = None
        self.return_period_q4_long = None
        self.return_period_q14_long = None
        self.return_period_q23_long = None
        self.time_series_mean = None
        self.flh = None

        # instantiate plotter
        self.plotter = Plotter(
            self.drought_type,
            self.counting_algorithm,
            self.start,
            self.end,
            config['technology_labels'],
            config['plots']
            )

    def __str__(self):
        return (
            f"{type(self).__name__} object with following parameters:\n"
            f"Drought type: {self.drought_type}\n"
            f"Counting algorithm: {self.counting_algorithm}\n"
            f"Range start={self.start}, range end={self.end}\n"
            f"Technology: {self.technology}\n"
            f"Region: {self.region}\n"
            f"User-defined thresholds={self.absolute_drought_thresholds}\n"
            f"Data-inherent thresholds corresponding to user-defined"
            f"Data-inherent percentiles corresponding to user-defined thresholds={self.drought_q_thresholds}\n"
            f"Time-series mean={self.time_series_mean}\n"
            f"Data-inherent thresholds corresponding to fractions of mean={self.drought_thresholds_rel_fractions}\n"
        )

    def parallelly_analyze_region(path_run, path_project, config, technology, region):
        '''
        Used to parallelize the initiation of RegionAnalyzer instances that
        organize the analysis for each region via the subordinate
        DroughtCounter instances.

        Parameters
        ----------
        path_run : TYPE
            DESCRIPTION.
        path_project : TYPE
            DESCRIPTION.
        config : dict
            Configuration of algorithm.
        technology : string
            Technology that is being investigated.
        region : string
            Region that is being investigated.

        Returns
        -------
        ra : RegionAnalyzer object
            Holds all region-specific results.

        '''

        # instantiate new RegionAnalyzer object
        ra = RegionAnalyzer(path_run, path_project, config, technology, region)

        # run analysis for single region
        ra.analyze_region(config, technology, region)

        return ra

    # TODO: inherit from Analyzer
    @staticmethod
    def series_in_dict_to_dataframe(dictionary):
        '''
        Returns a dataframe with columns that are stored as values in dictionary. Column
        names equal dictionary keys.
        '''
        return pd.DataFrame(list(dictionary.values()), index=list(dictionary.keys())).T

    def create_region_directory(self, path_run, technology, region):
        '''
        Creates region output path. Hierarchy: tech -> region -> file format. format is png and pdf.
        '''
        # create region result directory
        if not (technology in self.excluded and region in self.excluded[technology]):
            formats = ['png', 'pdf']
            for f in formats:
                path_new_folder = os.path.join(path_run, technology, region, f)
                Path(path_new_folder).mkdir(parents=True, exist_ok=True)

            # return trimmed output path
            path_output = Path(path_new_folder)
            path_output = path_output.parent.parent

            # create aggregated result directories
            formats = ['html', 'png', 'pdf']
            for f in formats:
                path_new_folder = os.path.join(path_output, 'aggregated', f)
                Path(path_new_folder).mkdir(parents=True, exist_ok=True)

            self.path_output = path_output

            # change current working directory
            os.chdir(path_output)

        else:
            return

    def wide_to_long(self, wide, col_name, value_name, region):
        '''
        Transforms data in wide into long format. Name of value column is derived from name.
        col_name is index name of wide.
        '''
        # name_col = str(name.split('_')[1][:-2] + 's') if 'extremes' in name else name
        # value_name = name if 'extremes' in name else f"max_{name.split('_')[1]}"
        long = wide.reset_index().rename(columns={'index': col_name})
        long = pd.melt(
            long,
            id_vars=col_name,
            value_vars=self.drought_thresholds_run,
            var_name='threshold',
            value_name=value_name
            )
        long['region'] = region
        return long

    @staticmethod
    def mean_quarterly_cum_s_in_dict_to_df(dictionary):
        '''
        Returns dataframe that merges dataframes contained in dictionary.
        '''
        dfs = list(dictionary.values())
        df = reduce(
            lambda left, right: pd.merge(left, right, left_index=True, right_index=True, how='outer'), dfs).fillna(0)
        return df

    def save_region_analyzer_object(self):
        '''
        Stores objects as pickle in current working directory.
        '''
        file_name = f"{self.technology}_{self.region}.gz"
        with gzip.open(file_name, 'wb') as file:
            pickle.dump(self, file, protocol=pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def get_percentile_of_threshold(ts_long, threshold):
        '''
        Finds the percentile of a given threshold in orignal data. Note that
        only those years chosen by the user are considered. If threshold
        is between actual observations, the percentile refers to observations
        that are stricly less than given threshold in line with counting
        algorithms searching for values below this threshold.
        '''
        # find percentile for given threshold
        return round(stats.percentileofscore(ts_long['capacity_factor'], threshold, kind='strict'), 4)

    def get_percentiles_of_thresholds(self):
        '''
        Finds all data-inherent percentiles corresponding to all user-defined thresholds.
        '''
        for thres in self.absolute_drought_thresholds:
            perc = self.get_percentile_of_threshold(self.input_extractor.ts_long, thres)
            self.drought_q_thresholds[thres] = {'percentile': perc}

    @staticmethod
    def get_time_series_mean(ts_long):
        '''
        Finds the average capacity factor in orignal data. Note that
        only those years chosen by the user are considered.
        '''
        return ts_long['capacity_factor'].mean()

    def get_full_load_hours(self):
        '''
        Computes annual full load hours of input data.
        '''
        flh = {}
        for y, group in self.input_extractor.ts_long.groupby('year'):
            flh[y] = round(group['capacity_factor'].sum())
        flh = pd.DataFrame.from_dict(flh, orient='index', columns=['flh'])
        self.flh = flh

    def get_drought_thresholds_of_mean_capacity_factor_fractions(self):
        '''
        Computes relative drought thresholds based on a user-defined
        fractions of the mean capacity factor. The resulting threshold
        is different to every region/technology investigated.
        '''
        drought_thresholds_fractions_mean_capacity_factor = {}
        for frac in self.fractions_mean_capacity_factor:
            thres = self.time_series_mean * frac
            drought_thresholds_fractions_mean_capacity_factor[thres] = {
                'fraction_mean_cf': frac, 'mean': self.time_series_mean
                }
        self.drought_thresholds_fractions_mean_capacity_factor = drought_thresholds_fractions_mean_capacity_factor

    def analyze_region(self, config, technology, region):
        '''
        Manages analysis on region level. Instantiates DroughtCounter object for drought
        counting and stores results from DroughtCounter object. Calls Plotter to plot
        region-specifics figures.
        '''
        # print process id
        print(f'---------------------------------------------------------------------\n'
              f'Process {os.getpid()} working region {region} for {technology}.\n'
              f'---------------------------------------------------------------------\n')

        # if region is excluded for technology exit
        if (technology in self.excluded and region in self.excluded[technology]):
            return

        # working directory
        wd = os.path.join(self.path_output, region)
        os.chdir(wd)

        # time the execution for each region
        start_time_region = time.perf_counter()

        # settings
        self.plotter.region = region
        self.plotter.path_output_region = wd

        # import input data
        path_ts = os.path.join(self.path_input, config['paths'][technology])
        self.input_extractor.get_timeseries_data(path_ts, region)

        # get data-inherent percentiles of all user-defined thresholds
        if self.absolute_drought_thresholds:
            self.get_percentiles_of_thresholds()

        # get average capacity factor
        self.time_series_mean = self.get_time_series_mean(self.input_extractor.ts_long)

        # get annual full load hours
        self.get_full_load_hours()

        # get full load hour-corrected capacity factor based on user-defined fractions of mean capacity factor
        if self.fractions_mean_capacity_factor:
            self.get_drought_thresholds_of_mean_capacity_factor_fractions()

        # create thresholds list used for drought counting runs
        self.drought_thresholds_run = [
            *self.absolute_drought_thresholds,
            *self.drought_thresholds_fractions_mean_capacity_factor.keys(),
            ]

        # create input list of runs based on absolute thresholds or relative thresholds derived from mean
        self.drought_thresholds_input = [
            *self.absolute_drought_thresholds,
            *self.fractions_mean_capacity_factor,
            ]

        # create input list of threshold-, percentile-, or fraction-based labels
        self.drought_thresholds_labels = [
            *[f"a={round(t, 2)}" for t in self.absolute_drought_thresholds],
            *[f"f-mean-cf={f}" for f in self.fractions_mean_capacity_factor],
            ]

        # zip threshold-, percentile or fraction-based list with threshold lists of run
        self.drought_thresholds_run_input = list(zip(self.drought_thresholds_run, self.drought_thresholds_input))

        # zip threshold-, percentile or fraction-based list with threshold lists of general labels
        self.drought_thresholds_run_labels = list(zip(self.drought_thresholds_run, self.drought_thresholds_labels))

        # count droughts for different thresholds
        for (thres_run, thres_label) in self.drought_thresholds_run_labels:

            thres_run = float(thres_run)

            # count droughts
            dc = DroughtCounter(
                self.drought_type,
                self.counting_algorithm,
                thres_run,
                thres_label,
                self.start,
                self.end,
                technology,
                region,
                self.time_series_mean,
                self.input_extractor.years_complete,
                self.input_extractor.years,
                self.input_extractor.leap_years
            )

            # execute counting algorithm
            dc.count_droughts(self.input_extractor.ts_long)

            # save results
            self.energy_deficit_cum[thres_label] = dc.energy_deficit_cum
            self.energy_deficit_cum_max[thres_label] = dc.energy_deficit_cum_max
            self.mean_yearly_cum[thres_run] = dc.mean_yearly_cum
            self.mean_yearly_cum_all_t[thres_run] = dc.mean_yearly_cum.iloc[-1].reset_index()
            self.mean_yearly_cum_all_t[thres_run].rename(columns={'Average': thres_run}, inplace=True)
            self.mean_quarterly_cum[thres_run] = dc.mean_quarterly_cum
            self.mean_quarterly_cum_q1[thres_run] = dc.mean_quarterly_cum.loc['Q1']
            self.mean_quarterly_cum_q1[thres_run].name = thres_run
            self.mean_quarterly_cum_q2[thres_run] = dc.mean_quarterly_cum.loc['Q2']
            self.mean_quarterly_cum_q2[thres_run].name = thres_run
            self.mean_quarterly_cum_q3[thres_run] = dc.mean_quarterly_cum.loc['Q3']
            self.mean_quarterly_cum_q3[thres_run].name = thres_run
            self.mean_quarterly_cum_q4[thres_run] = dc.mean_quarterly_cum.loc['Q4']
            self.mean_quarterly_cum_q4[thres_run].name = thres_run
            self.mean_quarterly_cum_q14[thres_run] = dc.mean_quarterly_cum.loc['Q1+Q4']
            self.mean_quarterly_cum_q14[thres_run].name = thres_run
            self.mean_quarterly_cum_q23[thres_run] = dc.mean_quarterly_cum.loc['Q2+Q3']
            self.mean_quarterly_cum_q23[thres_run].name = thres_run
            self.extremes_monthly[thres_run] = dc.extremes_monthly
            self.extremes_monthly_mid[thres_run] = dc.extremes_monthly_mid
            self.extremes_yearly[thres_run] = dc.extremes_yearly
            self.return_period_all[thres_run] = dc.return_period_all_standard
            self.return_period_q1[thres_run] = dc.return_period_q1_standard
            self.return_period_q2[thres_run] = dc.return_period_q2_standard
            self.return_period_q3[thres_run] = dc.return_period_q3_standard
            self.return_period_q4[thres_run] = dc.return_period_q4_standard
            self.return_period_q14[thres_run] = dc.return_period_q14_standard
            self.return_period_q23[thres_run] = dc.return_period_q23_standard

            # plot frequency for single threshold
            self.plotter.threshold = thres_run
            mean_quarterly_cum_temp = dc.mean_quarterly_cum
            mean_quarterly_cum_temp = mean_quarterly_cum_temp.loc[:, (mean_quarterly_cum_temp != 0).any(axis=0)]

            if config['plots']['frequency']:
                self.plotter.lines_single(
                    mean_quarterly_cum_temp.iloc[0:4, :], 'average frequency per quarter',
                    file_types=config['plots']['frequency']
                    )
                self.plotter.lines_single(
                    mean_quarterly_cum_temp.iloc[0:4, :], 'average frequency per quarter', truncate=True,
                    file_types=config['plots']['frequency']
                    )

            # remove all non-necessary attributes
            if hasattr(dc, 'temp_idx'):
                del dc.temp_idx
            if hasattr(dc, 'temp_cf'):
                del dc.temp_cf
            if hasattr(dc, 'temp_cf_idx'):
                del dc.temp_cf_idx
            if hasattr(dc, 'temp_e_idx'):
                del dc.temp_e_idx
            if hasattr(dc, 'data'):
                del dc.data
            if hasattr(dc, 'energy_deficit_cum_max'):
                del dc.energy_deficit_cum_max
            if hasattr(dc, 'energy_deficit_cum'):
                del dc.energy_deficit_cum
            if hasattr(dc, 'mean_yearly_cum'):
                del dc.mean_yearly_cum
            if hasattr(dc, 'mean_quarterly_cum'):
                del dc.mean_quarterly_cum
            if hasattr(dc, 'mean_monthly_cum'):
                del dc.mean_monthly_cum
            if hasattr(dc, 'extremes_monthly'):
                del dc.extremes_monthly
            if hasattr(dc, 'extremes_yearly'):
                del dc.extremes_yearly
            if hasattr(dc, 'return_period_all_standard'):
                del dc.return_period_all_standard
            if hasattr(dc, 'return_period_q1_standard'):
                del dc.return_period_q1_standard
            if hasattr(dc, 'return_period_q2_standard'):
                del dc.return_period_q2_standard
            if hasattr(dc, 'return_period_q3_standard'):
                del dc.return_period_q3_standard
            if hasattr(dc, 'return_period_q4_standard'):
                del dc.return_period_q4_standard
            if hasattr(dc, 'return_period_q14_standard'):
                del dc.return_period_q14_standard
            if hasattr(dc, 'return_period_q23_standard'):
                del dc.return_period_q23_standard
            if hasattr(dc, 'time_series_mean'):
                del dc.time_series_mean
            if hasattr(dc, 'years_complete'):
                del dc.years_complete
            if hasattr(dc, 'periods'):
                del dc.periods
            if hasattr(dc, 'count_monthly'):
                del dc.count_monthly
            if hasattr(dc, 'cf'):
                del dc.cf
            if hasattr(dc, 'monthly'):
                del dc.monthly
            if hasattr(dc, 'quarterly'):
                del dc.quarterly
            if hasattr(dc, 'mean_yearly_singular'):
                del dc.mean_yearly_singular
            if hasattr(dc, 'mean_monthly_singular'):
                del dc.mean_monthly_singular
            if hasattr(dc, 'mean_quarterly_singular'):
                del dc.mean_quarterly_singular
            if hasattr(dc, 'counting_algorithm'):
                del dc.counting_algorithm
            if hasattr(dc, 'sma_array'):
                del dc.sma_array
            if hasattr(dc, 'cbt_array'):
                del dc.cbt_array
            
            # safe country results
            self.drought_counters[thres_label] = dc

        # concat yearly average frequency values for all thresholds
        dfs = list(self.mean_yearly_cum_all_t.values())
        df = reduce(lambda left, right: pd.merge(left, right, on=['index'], how='outer'), dfs).fillna(0)
        df.set_index('index', inplace=True)
        self.mean_yearly_cum_all_t = df

        # concat quarterly average frequency values for all thresholds
        self.mean_quarterly_cum_q1 = self.mean_quarterly_cum_s_in_dict_to_df(self.mean_quarterly_cum_q1)
        self.mean_quarterly_cum_q2 = self.mean_quarterly_cum_s_in_dict_to_df(self.mean_quarterly_cum_q2)
        self.mean_quarterly_cum_q3 = self.mean_quarterly_cum_s_in_dict_to_df(self.mean_quarterly_cum_q3)
        self.mean_quarterly_cum_q4 = self.mean_quarterly_cum_s_in_dict_to_df(self.mean_quarterly_cum_q4)
        self.mean_quarterly_cum_q14 = self.mean_quarterly_cum_s_in_dict_to_df(self.mean_quarterly_cum_q14)
        self.mean_quarterly_cum_q23 = self.mean_quarterly_cum_s_in_dict_to_df(self.mean_quarterly_cum_q23)

        # plot average yearly frequency for all thresholds
        if config['plots']['frequency']:
            self.plotter.lines_single(self.mean_yearly_cum_all_t.T, 'average frequency across all years',
                                      file_types=config['plots']['frequency'])

        # transform series in dict to dataframe
        self.extremes_yearly = self.series_in_dict_to_dataframe(self.extremes_yearly)
        self.extremes_monthly = self.series_in_dict_to_dataframe(self.extremes_monthly)
        self.extremes_monthly_mid = self.series_in_dict_to_dataframe(self.extremes_monthly_mid)
        self.return_period_all = self.series_in_dict_to_dataframe(self.return_period_all)
        self.return_period_q1 = self.series_in_dict_to_dataframe(self.return_period_q1)
        self.return_period_q2 = self.series_in_dict_to_dataframe(self.return_period_q2)
        self.return_period_q3 = self.series_in_dict_to_dataframe(self.return_period_q3)
        self.return_period_q4 = self.series_in_dict_to_dataframe(self.return_period_q4)
        self.return_period_q14 = self.series_in_dict_to_dataframe(self.return_period_q14)
        self.return_period_q23 = self.series_in_dict_to_dataframe(self.return_period_q23)
        self.energy_deficit_cum_max = self.series_in_dict_to_dataframe(self.energy_deficit_cum_max)
        self.energy_deficit_cum_max.sort_index(inplace=True)

        # plot maximum energy deficit against mean capacity factor for all thresholds
        # TODO: self.plotter.lines_single(self.energy_deficit_cum_max.T, 'maximum energy deficit across all years')

        # transform data in long format
        self.mean_yearly_cum_all_t_long = self.wide_to_long(self.mean_yearly_cum_all_t, 'duration', 'frequency', region)
        self.mean_quarterly_cum_q1_long = self.wide_to_long(self.mean_quarterly_cum_q1, 'duration', 'frequency', region)
        self.mean_quarterly_cum_q2_long = self.wide_to_long(self.mean_quarterly_cum_q2, 'duration', 'frequency', region)
        self.mean_quarterly_cum_q3_long = self.wide_to_long(self.mean_quarterly_cum_q3, 'duration', 'frequency', region)
        self.mean_quarterly_cum_q4_long = self.wide_to_long(self.mean_quarterly_cum_q4, 'duration', 'frequency', region)
        self.mean_quarterly_cum_q14_long = self.wide_to_long(self.mean_quarterly_cum_q14, 'duration', 'frequency', region)
        self.mean_quarterly_cum_q23_long = self.wide_to_long(self.mean_quarterly_cum_q23, 'duration', 'frequency', region)
        self.extremes_yearly_long = self.wide_to_long(self.extremes_yearly, 'years', 'extremes_yearly', region)
        self.extremes_monthly_long = self.wide_to_long(self.extremes_monthly, 'months', 'extremes_monthly', region)
        self.extremes_monthly_mid_long = self.wide_to_long(self.extremes_monthly_mid, 'months', 'extremes_monthly', region)
        self.return_period_all_long = self.wide_to_long(self.return_period_all, 'return_period', 'max_period', region)
        self.return_period_q1_long = self.wide_to_long(self.return_period_q1, 'return_period', 'max_period', region)
        self.return_period_q2_long = self.wide_to_long(self.return_period_q2, 'return_period', 'max_period', region)
        self.return_period_q3_long = self.wide_to_long(self.return_period_q3, 'return_period', 'max_period', region)
        self.return_period_q4_long = self.wide_to_long(self.return_period_q4, 'return_period', 'max_period', region)
        self.return_period_q14_long = self.wide_to_long(self.return_period_q14, 'return_period', 'max_period', region)
        self.return_period_q23_long = self.wide_to_long(self.return_period_q23, 'return_period', 'max_period', region)

        # plot extremes single
        if config['plots']['max_period_yearly']:
            self.plotter.bars_single(self.extremes_yearly/24, 'extremes_yearly', self.plotter.path_output_region,
                                     level='region', file_types=config['plots']['max_period_yearly'])
        if config['plots']['max_period_yearly_flh']:
            self.plotter.bars_sec_y_single(self.extremes_yearly/24, self.flh, 'extremes_yearly_flh',
                                           self.drought_thresholds_run_labels)
        if config['plots']['max_period_monthly']:
            self.plotter.bars_single(self.extremes_monthly/24, 'extremes_monthly', self.plotter.path_output_region,
                                     level='region', file_types=config['plots']['max_period_monthly'])
        if config['plots']['return_period']:
            self.plotter.lines_single(self.return_period_all.T/24, 'duration across all quarters [h]', 'return period',
                                      file_types=config['plots']['return_period'])
            self.plotter.lines_single(self.return_period_q1.T/24, 'duration in Q1 [days]', 'return period',
                                      file_types=config['plots']['return_period'])
            self.plotter.lines_single(self.return_period_q2.T/24, 'duration in Q2 [days]', 'return period',
                                      file_types=config['plots']['return_period'])
            self.plotter.lines_single(self.return_period_q3.T/24, 'duration in Q3 [days]', 'return period',
                                      file_types=config['plots']['return_period'])
            self.plotter.lines_single(self.return_period_q4.T/24, 'duration in Q4 [days]', 'return period',
                                      file_types=config['plots']['return_period'])
            self.plotter.lines_single(self.return_period_q14.T/24, 'duration in Q1 and Q4 combined [days]',
                                      'return period', file_types=config['plots']['return_period'])
            self.plotter.lines_single(self.return_period_q23.T/24, 'duration in Q2 and Q3 combined [days]',
                                      'return period', file_types=config['plots']['return_period'])

        # display process complettion statement plus execution time
        duration = time.perf_counter() - start_time_region
        print(f'------------------------------------------------\n'
              f'Process {os.getpid()} done processing region {region}, duration {round(duration/60, 2)} minutes.\n'
              f'------------------------------------------------\n')

        # pickle results
        self.save_region_analyzer_object()