# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 16:21:50 2024

@author: mkittel
"""

import os
import multiprocess as mp
import pandas as pd
# import numpy as np
import pickle
import psutil
import gzip
from pathlib import Path

# from pympler import asizeof
# import yaml
# from functools import reduce
# from urllib.request import urlopen
# import json
# import datetime
# import math

# import own modules
# from analyzer import Analyzer
# from region_analyzer import RegionAnalyzer
# from input_extractor import InputExtractor
# from drought_counter import DroughtCounter

# temp = pathlib.PosixPath
# pathlib.PosixPath = pathlib.WindowsPath


def load_object(path, file):
    '''
    For debugging: Loads object from pickle.
    '''
    cwd = os.getcwd()
    os.chdir(path)
    with gzip.open(file, 'rb') as file:
        obj = pickle.load(file)
    os.chdir(cwd)
    return obj


def save_object(path, obj, name):
    '''
    For debugging: Stores objects as pickle in path directory.
    '''
    cwd = os.getcwd()
    os.chdir(path)
    file_name = f"{name}.gz"
    with gzip.open(file_name, 'wb') as file:
        pickle.dump(obj, file, protocol=pickle.HIGHEST_PROTOCOL)
    os.chdir(cwd)


def parallelize(function, values):
    '''
    The unbound function is mapped to all components of values. The mapping pairs
    are automatically parallelized.
    '''
    # print id of parent process
    print(f'--------------------------------------------------------------------------------------------------\n'
          f'Parent process {os.getpid()} initializes parallelization.\n'
          f'--------------------------------------------------------------------------------------------------\n')

    # number of physical cores
    no_cpu = psutil.cpu_count(logical=True)

    # iniatialize parllelization
    with mp.Pool(no_cpu) as pool:  # no_cpu
        results = pool.starmap(function, values)
    return results


def weighting_func(x, a):
    '''
    Creates a weight for a discrete threshold.

    Parameters
    ----------
    x : float
        Threshold.
    a : int
        Factor for weighting function.

    Returns
    -------
    float
        Weighting factor.

    '''
    return 1/(x**(a))  # a * x**(-a)  # - a*0.9**(-a)


def weighting_func_doubling(thresholds, largest_weight):
    """
    Assign weights to floats in ascending order. Each float gets half the weight of the next larger float.

    :param floats: A list of floats.
    :param largest_weight: The weight for the largest float.
    :return: A dictionary with floats as keys and their weights as values.
    """
    # Sort the list of floats in ascending order
    sorted_thresholds = sorted(thresholds)

    # Initialize an empty dictionary to hold the float-weight pairs
    weights = {}

    # Calculate and assign weights starting from the largest
    current_weight = largest_weight
    for float_number in reversed(sorted_thresholds):
        weights[round(float_number, 2)] = current_weight
        current_weight *= 2  # Halve the weight for the next smaller float

    return weights


def get_x_weighting_func(y, a):
    '''
    Retrieves the corresponding x value of the weighting_func given a parameter a.

    Parameters
    ----------
    y : float
        Function value of weighting_func.
    a : int or float
        Parameter of weighting_func.

    Returns
    -------
    float
        Corresponding x value of y.

    '''
    return (a / y)**(1/a)  # (a / (y + a * 0.9**(-a)))**(1/a)


def compute_drought_mass(
        region,
        drought_occurence,
        year_pairs,
        weights,
        exclude_thresholds_drought_mass,
        correction
        ):

    # print process id
    print(f'--------------------------------------------------------------------\n'
          f'Process {os.getpid()} runs for region {region}.\n'
          f'--------------------------------------------------------------------\n')

    mass_all_events_avg_all_years = {}
    mass_single_event_max_all_years = {}

    # iterate over year pairs
    for yy in year_pairs:

        df = drought_occurence.query('year in @yy').copy()

        # add column that counts the rows
        df['hour_total'] = df.groupby('threshold').cumcount()

        # correction for demand seasonality, set summer droughts to zero
        if correction == 's2s-yearly':
            df.loc[(df['hour_total'] < 4344) | (df['hour_total'] >= 13104), 'drought_indicator'] = 0
        elif correction == 's2s-winter':
            df.loc[(df['hour_total'] < 6552) | (df['hour_total'] >= 10920), 'drought_indicator'] = 0

        # prepare data in wide format
        df = df.reset_index()
        df['threshold'] = df['threshold'].apply(lambda x: float(x.split('=')[1]))
        df = df.pivot(index=['date', 'hour', 'year'], columns=['threshold'], values='drought_indicator')

        # replace relative threshold with applied weights
        df = df.replace(weights)

        # drop threshold columns that are excluded from drought mass and compute drought mass
        df = df.drop(exclude_thresholds_drought_mass, axis=1)
        df['mass_hour'] = df.sum(axis=1)

        # metric 1: drought mass across all hours of the corresponding time frame (here: two years)
        mass_all_events = df.sum().sum()
        # mass_all_events_avg_threshold = get_x_weighting_func(mass_all_events/df.shape[0], 4)

        # TODO: compute average across all time steps and included threshold columns
        mass_all_events_avg = mass_all_events  # TODO: / (df.shape[0] * (df.shape[1] - 1))
        mass_all_events_avg_all_years[f'{yy[0]}-{yy[1]}'] = {
            'mass_all_events_avg': mass_all_events_avg,
            # 'avg_threshold': mass_all_events_avg_threshold,
            'region': region
        }

        # metric 2: drought mass of single events for each time frame
        event_mask = (df['mass_hour'] > 0).astype(int)

        events = {}
        events_mass = {}

        # group data in droughts and no droughts, compute cumulative drought mass
        for idx, event in event_mask.rename('event_indicator').astype(int).groupby(event_mask.diff().ne(0).cumsum()):
            event = pd.merge(event, df['mass_hour'], on=['date', 'hour', 'year'], how='left')
            event = event['mass_hour'].cumsum().rename('cumulative_mass')
            events[idx] = event
            events_mass[idx] = {'mass': event[-1], 'duration': event.shape[0],
                                'start': event.index[0], 'end': event.index[-1], 'region': region}
        events_mass = pd.DataFrame.from_dict(events_mass, orient='index')

        # retrieve event with max drought mass
        mass_single_event_max_all_years[f'{yy[0]}-{yy[1]}'] = \
            events_mass.loc[events_mass['mass'].idxmax()].rename(region)

    # convert to df in long format
    mass_all_events_avg_all_years = pd.DataFrame.from_dict(
        mass_all_events_avg_all_years, orient='index')
    mass_all_events_avg_all_years.reset_index(names='time_frame', drop=False, inplace=True)

    mass_single_event_max_all_years = pd.DataFrame.from_dict(
        mass_single_event_max_all_years, orient='index')
    mass_single_event_max_all_years.reset_index(names='time_frame', drop=False, inplace=True)

    return dict(
        region=region,
        mass_all_events_avg_all_years=mass_all_events_avg_all_years,
        mass_single_event_max_all_years=mass_single_event_max_all_years
        )


# %% Main entry point


if __name__ == '__main__':

    # import drought occurrences
    print('Importing drought occurrence data.')
    path_results = '/home/mkittel/tsla-tde/mk/vreda/output/2024-08-02_08-42-30_mbt_event'
    # path_results = 'T:\\mk\\vreda\\output\\2024-08-02_08-42-30_mbt_event'

    dm_filter = 1  # , 12, 18, 24, 48]

    print(f'Computation of drought mass using filter {dm_filter}h started.')

    drought_occurence_portfolio = load_object(path_results, f'drought_occurence_portfolio_{dm_filter}.gz')

    year_pairs = [[i, i+1] for i in range(1982, 2019)]

    # import weights
    path_weights = '/home/mkittel/tsla-tde/mk/vreda/input/weights_drought_mass_equal.xlsx'
    # path_weights = "T:\\mk\\vreda\\input\\weights_drought_mass_equal.xlsx"
    weights_all = pd.read_excel(path_weights, index_col='Threshold', skiprows=1)

    # select which thresholds are excluded from drought mass
    exclude_thresholds_drought_mass = [0.8, 0.85, 0.9, 0.95, 1.0]
    corrections = ['s2s-yearly', 's2s-winter']
    
    for correction in corrections:
        
        print(f'Parallel computation for correction {correction} started.')
        
        for weighting_function in weights_all.columns:
    
            print(f'Parallel computation for {weighting_function} started.')
    
            weights = weights_all[weighting_function].to_dict()
    
            # generate mutiprocessing args
            values = tuple([(region, drought_occurence, year_pairs, weights, exclude_thresholds_drought_mass, correction) for region, drought_occurence in drought_occurence_portfolio.groupby('region')])
    
            # parallelized region analysis
            try:
                drought_mass_region_results = parallelize(compute_drought_mass, values)
            except NameError as e:
                print(f"Error: {e}")
    
            # restructure in dictionary
            drought_mass = {}
            for result_dict in drought_mass_region_results:
                region = result_dict['region']
                del result_dict['region']
                drought_mass[region] = result_dict
            del drought_mass_region_results
    
            # result container
            mass_all_events_avg_all_years = {}  # drought mass as severity proxy
            mass_single_event_max_all_years = {}  # single event with highest mass
    
            # reorganize results in dictionaries
            for region, regional_results in drought_mass.items():
                mass_all_events_avg_all_years[region] = regional_results['mass_all_events_avg_all_years']
                mass_single_event_max_all_years[region] = regional_results['mass_single_event_max_all_years']
    
            # identify time frame and single event with highest mass per region
            mass_all_events_avg_max_year = {}
            for region, df in mass_all_events_avg_all_years.items():
                mass_all_events_avg_max_year[region] = df.loc[df['mass_all_events_avg'].idxmax()]
            mass_all_events_avg_max_year = pd.DataFrame.from_dict(mass_all_events_avg_max_year, orient='index')
    
            mass_single_event_max_max_year = {}
            for region, df in mass_single_event_max_all_years.items():
                mass_single_event_max_max_year[region] = df.loc[df['mass'].idxmax()]
            mass_single_event_max_max_year = pd.DataFrame.from_dict(
                mass_single_event_max_max_year, orient='index')
    
            # concat dfs to df with format
            mass_all_events_avg_all_years = pd.concat(mass_all_events_avg_all_years.values())
            mass_single_event_max_all_years = pd.concat(mass_single_event_max_all_years.values())
    
            # pickle results
            cutoff = round(exclude_thresholds_drought_mass[0] - 0.05, 2)
            path_output = os.path.join(path_results, f'drought_mass_filter_{dm_filter}', 'drought_mass_s2s', f'{correction}')
            Path(path_output).mkdir(parents=True, exist_ok=True)
            save_object(path_output, mass_all_events_avg_all_years, 'mass_all_events_avg_all_years')
            save_object(path_output, mass_all_events_avg_max_year, 'mass_all_events_avg_max_year')
            save_object(path_output, mass_single_event_max_all_years, 'mass_single_event_max_all_years')
            save_object(path_output, mass_single_event_max_max_year, 'mass_single_event_max_max_year')
    
            print(f'Parallel computation for {weighting_function} completed.')

