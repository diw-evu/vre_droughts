# -*- coding: utf-8 -*-
"""
This module checks the content and format of the configurations entered by the user.
"""


class InputError(Exception):
    pass


class ConfigCheck():
    # TODO: pythonic way of input sanity checks: https://realpython.com/python-property/
    def __init__(self, config):
        '''
        Input sanity check. Shoots error if type or options are errorneous.
        '''
        # check drought type
        if config['drought_type'] not in ['cbt', 'mbt', 'spa']:
            raise InputError(
                f"Only 'cbt', 'mbt', 'spa' as drought type possible. "
                f"You entered: {config['drought_type']}."
                )

        # only events possible when spa
        if config['drought_type'] == 'spa':
            if config['counting_algorithm'] != 'event':
                raise InputError(
                    f"Only event perspective possible Sequent Peak Algorithm. "
                    f"You entered: {config['counting_algorithm']}."
                    )

        # check counting algorithm
        if config['counting_algorithm'] not in ['window', 'event']:
            raise InputError(
                f"Only window or event counting algorithm possible. "
                f"You entered: {config['counting_algorithm']}."
                )
        if (config['counting_algorithm'] == 'window' and config['start'] >= config['end']):
            raise InputError(
                f"Window counting algorithm requires a period range in "
                f"ascending order, i.e., start < end. \n"
                f"You entered start={config['start']}, end={config['end']}."
                )

        elif (config['counting_algorithm'] == 'event' and config['start'] <= config['end']):
            raise InputError(
                f"Event counting algorithm requires a period range in "
                f"descending order, i.e., start > end. \n"
                f"You entered start={config['start']}, end={config['end']}."
                )

        # check start and end of period duration
        if not (isinstance(config['start'], (int, float)) and config['start'] >= 0):
            raise InputError(
                f"Positive integer or float for start expected. "
                f"You entered: {config['start']}."
                )
        if not (isinstance(config['end'], (int, float)) and config['end'] >= 0):
            raise InputError(
                f"Positive integer or float for end expected. "
                f"You entered: {config['end']}."
                )

        # check correlation parameters
        if not (isinstance(config['start'], (int, float)) and config['start'] >= 0):
            raise InputError(
                f"Positive integer or float for start expected. "
                f"You entered: {config['start']}."
                )
        if not (isinstance(config['end'], (int, float)) and config['end'] >= 0):
            raise InputError(
                f"Positive integer or float for end expected. "
                f"You entered: {config['end']}."
                )

        # check regions
        if not isinstance(config['regions'], dict):
            raise InputError(
                f"Expected dictionary for regions. "
                f"You entered: {type(config['regions'])}."
                )
        if not set(map(type, config['regions'].keys())) == {str}:
            raise InputError(
                f"Expected regions in string format. "
                f"You entered: {config['regions'].keys()}."
                )
        # TODO: check that lat, long, and more region specifics are in the right format
# =============================================================================
#         if not isinstance(config['regions'].values(), dict):
#             raise InputError(
#                 f"Expected region specifics in dictionary format. "
#                 f"You entered: {config['regions'].values()}."
#                 )
# =============================================================================

        # check drought thresholds
        if not isinstance(config['drought_thresholds'], dict):
            raise InputError(
                f"Expected dict for drought thresholds. "
                f"You entered: {config['drought_thresholds']}."
                )

        # check absolute drought thresholds
        if not isinstance(config['drought_thresholds']['absolute'], list):
            raise InputError(
                f"Expected list for absolute drought thresholds. "
                f"You entered: {config['drought_thresholds']['absolute']}."
                )
        if not all(isinstance(x, (int, float)) for x in config['drought_thresholds']['absolute']):
            raise InputError(
                f"Expected list of integers or floats in drought thresholds. "
                f"You entered: {config['drought_thresholds']['absolute']}."
                )

        # check fractions_cumulative_energy
        if not isinstance(config['drought_thresholds']['fractions_cumulative_energy'], list):
            raise InputError(
                f"Expected list for fractions_cumulative_energy. "
                f"You entered: {config['drought_thresholds']['fractions_cumulative_energy']}."
                )
        if not all(isinstance(x, (int, float)) for x in config['drought_thresholds']['fractions_cumulative_energy']):
            raise InputError(
                f"Expected list of integers or floats in fractions_cumulative_energy. "
                f"You entered: {config['drought_thresholds']['fractions_cumulative_energy']}."
                )

        # check fractions_mean_capacity_factor
        if not isinstance(config['drought_thresholds']['fractions_mean_capacity_factor'], list):
            raise InputError(
                f"Expected list for fractions_mean_capacity_factor. "
                f"You entered: {config['drought_thresholds']['fractions_mean_capacity_factor']}."
                )
        if not all(isinstance(x, (int, float)) for x in config['drought_thresholds']['fractions_mean_capacity_factor']):
            raise InputError(
                f"Expected list of integers or floats in fractions_mean_capacity_factor. "
                f"You entered: {config['drought_thresholds']['fractions_mean_capacity_factor']}."
                )

        # check that either drought thresholds or data-inherent fraction of mean capacity factor
        # or cumulative energy are provided
        number_runs = len([v for v in config['drought_thresholds'].values()])
        if number_runs == 0:
            raise InputError(
                f"Provide either drought thresholds or data-inherent fractions_mean_capacity_factor "
                f"or fractions_cumulative_energy for endogenous threshold determindation. You entered: "
                f"{config['drought_thresholds']}, {config['fractions_cumulative_energy']}, and "
                f"{config['fractions_mean_capacity_factor']}."
                )

        # check correlation thresholds
        if not isinstance(config['correlation_thresholds'], list):
            raise InputError(
                f"Expected list for correlation thresholds. "
                f"You entered: {config['correlation_thresholds']}."
                )
        if not all(isinstance(x, (int, float)) for x in config['correlation_thresholds']):
            raise InputError(
                f"Expected list of integers or floats in correlation thresholds. "
                f"You entered: {config['correlation_thresholds']}."
                )
        if not isinstance(config['correlation_type'], list):
            raise InputError(
                f"Expected list for correlation type. "
                f"You entered: {config['correlation_type']}."
                )
        if not len(config['correlation_type']) == 1:
            raise InputError(
                f"Correlation type must contain only one option. "
                f"You entered: {config['correlation_type']}."
                )

        # check technologies
        if not isinstance(config['technologies'], dict):
            raise InputError(
                f"Expected dictionary for technologies. "
                f"You entered: {type(config['technologies'])}."
                )
        if not set(map(type, config['technologies'].keys())) == {str}:
            raise InputError(
                f"Expected technology names in string format. "
                f"You entered: {config['technologies'].keys()}."
                )
# =============================================================================
#         if not set(map(type, config['technologies'].values())) == {str}:
#             raise InputError(
#                 f"Expected technology names in capacity input file in string format. "
#                 f"You entered: {config['technologies'].values()}."
#                 )
# =============================================================================

# =============================================================================
#         # check combined technologies
#         if not isinstance(config['technologies_combined'], (dict, type(None))):
#             raise InputError(
#                 f"Expected dictionary for technologies_combined. "
#                 f"You entered: {type(config['technologies_combined'])}."
#                 )
#         if config['technologies_combined']:
#             if not set(map(type, config['technologies_combined'].keys())) == {str}:
#                 raise InputError(
#                     f"Expected technologies_combined names in string format. "
#                     f"You entered: {config['technologies_combined'].keys()}."
#                     )
#             if not set(map(type, config['technologies_combined'].values())) == {str}:
#                 raise InputError(
#                     f"Expected technologies_combined names in capacity input file"
#                     f"in string format. You entered: "
#                     f"{config['technologies_combined'].values()}."
#                     )
# =============================================================================

# =============================================================================
#         # chech plotting options
#         if not isinstance(config['plots']['interactive_single_plots'], bool):
#             raise InputError(
#                 f"Expected boolean for interactive_single_plots. "
#                 f"You entered: {config['plots']['interactive_single_plots']}."
#                 )
#         if not isinstance(config['plots']['interactive_aggregated_plots'], bool):
#             raise InputError(
#                 f"Expected boolean for interactive_aggregated_plots. "
#                 f"You entered: {config['plots']['interactive_aggregated_plots']}."
#                 )
# =============================================================================

        # check years
        if not isinstance(config['years'], (list, type(None))):
            raise InputError(
                f"Expected either list or None for years. "
                f"You entered: {config['years']}."
                )
        if config['years']:
            if not all(isinstance(x, (int, float)) for x in config['years']):
                raise InputError(
                    f"Expected list of integers or floats in years. "
                    f"You entered: {config['years']}."
                    )

        # test whether selection of technologies and regions are excluded
        for technology in config['technologies']:
            if technology in config['excluded'].keys():
                if all(r in config['excluded'][technology] for r in list(config['regions'].keys())):
                    raise InputError(
                        f"The combination of technology: {technology} and selected "
                        f"regions: {list(config['regions'].keys())} is excluded. Run aborted."
                        )

        # test paths
        if not isinstance(config['paths'], dict):
            raise InputError(
                f"Expected dictionary for paths. "
                f"You entered: {type(config['paths'])}."
                )
        if not set(map(type, config['paths'].keys())) == {str}:
            raise InputError(
                f"Expected path names in string format. "
                f"You entered: {config['paths'].keys()}."
                )
        if not set(map(type, config['paths'].values())) == {str}:
            raise InputError(
                f"Expected paths in string format. "
                f"You entered: {config['paths'].values()}."
                )
