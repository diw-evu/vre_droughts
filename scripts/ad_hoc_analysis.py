# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 20:39:03 2023

@author: mkittel
"""
from dash.dependencies import Input, Output
from dash import html
from dash import dcc
import dash
import os
from pympler import asizeof
import yaml
import pickle
import gzip
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots
import pandas as pd
import numpy as np
# import geopandas as gpd
from functools import reduce
import pathlib
from pathlib import Path
from urllib.request import urlopen
import json
import datetime
import math
import psutil
import multiprocess as mp
import statsmodels.api as sm

# import own modules
from analyzer import Analyzer
from region_analyzer import RegionAnalyzer
from input_extractor import InputExtractor
from drought_counter import DroughtCounter

temp = pathlib.PosixPath
pathlib.PosixPath = pathlib.WindowsPath

custom_cont_color_scale = [
    # Start with Viridis dark blue
    (0.0, '#440154'),
    # Quickly transition to blue/green
    (0.1, '#30678D'),
    # Move towards green
    (0.25, '#208F8C'),
    # Less space for green before yellow
    (0.4, '#35B779'),
    # More space for yellow
    (0.53, '#B2D335'),  # Starting yellow earlier in the gradient
    # Begin transitioning to orange'
    (0.7, '#E3CF21'),  # Keeping some yellow before introducing orange
    # Transition to red
    (0.88, '#E69500'),  # A darker orange
    # End with red
    (1.0, '#A00000'),  # A slightly brighter red
]


def list_attributes_total_memory(obj):
    print(f"Total memory usage of '{obj.__class__.__name__}' object's attributes (in KB and MB):")
    total_size_bytes = 0
    for attr in dir(obj):
        if not attr.startswith('__') and not callable(getattr(obj, attr)):
            attr_value = getattr(obj, attr)
            attr_size_bytes = asizeof.asizeof(attr_value)
            total_size_bytes += attr_size_bytes
            attr_size_kb = attr_size_bytes / 1024
            attr_size_mb = attr_size_bytes / 1048576
            print(f"- {attr}: approximately {attr_size_kb:.2f} KB ({attr_size_mb:.2f} MB)")
    total_size_kb = total_size_bytes / 1024
    total_size_mb = total_size_bytes / 1048576
    print(f"Total memory used by all attributes: approximately {total_size_kb:.2f} KB ({total_size_mb:.2f} MB)")


def save_object(obj, technology, threshold_run):
    '''
    For debugging: Stores objects as pickle in path directory.
    '''
    cwd = os.getcwd()
    path = 'C:\\git\\vre_droughts\\temp'
    os.chdir(path)
    file_name = f"{technology}_{threshold_run}.gz"
    with gzip.open(file_name, 'wb') as file:
        pickle.dump(obj, file, protocol=pickle.HIGHEST_PROTOCOL)
    os.chdir(cwd)


def load_object(path, file):
    '''
    For debugging: Loads object from pickle.
    '''
    cwd = os.getcwd()
    os.chdir(path)
    with gzip.open(file, 'rb') as file:
        obj = pickle.load(file)
    os.chdir(cwd)
    return obj


def get_max_duration_yearly(region_results):
    '''
    TODO: retrieve data from analyzer object, not region_results

    Gets maximum periods for all technologies, regions, thresholds for each year and maximum of all years.

    Returns:
        for each year in one dataframe
        across all years in one dataframe
    '''
    max_period_yearly = {}
    max_period_of_all_years = {}

    for tech in region_results.keys():
        max_period_yearly[tech] = {}
        max_period_of_all_years[tech] = {}

        for ra in region_results[tech]:
            df = ra.extremes_yearly.copy()
            df = df[df.index != 'Maximum']
            col = df.columns.str.split('=').str[-1].astype(float)
            df.columns = col
            df = df.reset_index(names='year')
            df = pd.melt(df, id_vars='year', value_vars=col, var_name='fraction', value_name='max_duration')
            df['region'] = ra.region
            df['technology'] = tech
            max_period_yearly[tech][ra.region] = df

            # extract max duration across all years and corresponding year
            max_period_of_all_years[tech][ra.region] = df.loc[df.groupby('fraction')['max_duration'].idxmax()]

        # concat aross all regions
        max_period_yearly[tech] = pd.concat(max_period_yearly[tech].values())
        max_period_of_all_years[tech] = pd.concat(max_period_of_all_years[tech].values())

    # concat across all technologies
    max_period_yearly = pd.concat(max_period_yearly.values())
    max_period_of_all_years = pd.concat(max_period_of_all_years.values())

    return max_period_yearly, max_period_of_all_years

def get_max_duration_monthly(region_results, kind='end'):
    '''
    TODO: retrieve data from analyzer object, not region_results

    Gets maximum periods for all technologies, regions, thresholds.

    Returns:
        for each year in one dataframe
        across all years in one dataframe
    '''
    max_period_monthly = {}

    for tech in region_results.keys():
        max_period_monthly[tech] = {}

        for ra in region_results[tech]:
            if kind == 'end':
                df = ra.extremes_monthly.copy()
            elif kind == 'mid':
                df = ra.extremes_monthly_mid.copy()
            df = df[df.index != 'Maximum']
            df = df.rename(columns=dict(ra.drought_thresholds_run_labels))
            col = df.columns.str.split('=').str[-1].astype(float)
            df.columns = col
            df = df.reset_index(names='months')
            df = pd.melt(df, id_vars='months', value_vars=col, var_name='fraction', value_name='max_duration')
            df['region'] = ra.region
            df['technology'] = tech
            max_period_monthly[tech][ra.region] = df

        # concat aross all regions
        max_period_monthly[tech] = pd.concat(max_period_monthly[tech].values())

    # concat across all technologies
    max_period_monthly = pd.concat(max_period_monthly.values())

    return max_period_monthly

def get_max_duration_monthly_mid(region_results):
    '''
    TODO: retrieve data from analyzer object, not region_results

    Gets maximum periods for all technologies, regions, thresholds.

    Returns:
        for each year in one dataframe
        across all years in one dataframe
    '''
    max_period_monthly_mid = {}

    for tech in region_results.keys():
        max_period_monthly_mid[tech] = {}

        for ra in region_results[tech]:
            df = ra.extremes_monthly.copy()
            df = df[df.index != 'Maximum']
            df = df.rename(columns=dict(ra.drought_thresholds_run_labels))
            col = df.columns.str.split('=').str[-1].astype(float)
            df.columns = col
            df = df.reset_index(names='months')
            df = pd.melt(df, id_vars='months', value_vars=col, var_name='fraction', value_name='max_duration')
            df['region'] = ra.region
            df['technology'] = tech
            max_period_monthly_mid[tech][ra.region] = df

        # concat aross all regions
        max_period_monthly_mid[tech] = pd.concat(max_period_monthly_mid[tech].values())

    # concat across all technologies
    max_period_monthly_mid = pd.concat(max_period_monthly_mid.values())

    return max_period_monthly_mid


def get_flh(region_results, path_output):
    '''
    Extracts and saves full-load hours for each technology and region in region_results.
    '''
    flh = {}
    for tech in region_results:
        flh[tech] = {}
        for ra in region_results[tech]:
            flh[tech][ra.region] = ra.time_series_mean
        flh[tech] = pd.DataFrame.from_dict(flh[tech], orient='index', columns=['Values'])
    flh = pd.concat(flh.values(), axis=1, keys=flh.keys())
    flh.columns = flh.columns.get_level_values(0)

    os.chdir(path_output)
    file_name = "flh.gz"
    with gzip.open(file_name, 'wb') as file:
        pickle.dump(flh, file, protocol=pickle.HIGHEST_PROTOCOL)


def percentual_reduction_portfolio(max_period_yearly, reference, path_config, flh):
    '''
    Computes the average reduction of maximum drought durations across all years and regions.

    Parameters
    ----------
    max_period_yearly : DataFrame
        Holds max drought period across all regions, technologies, years, and fractions.
    reference : str
        Reference technology.

    Returns
    -------
    change_tech_thres : dict
        Dict of dfs showing average change across all technologies, regions, and fractions.
    change_tech_thres_region_thres : DataFrame
        Change across all technologies and fractions.

    '''
    os.chdir(os.path.dirname(path_config))
    # import settings
    with open('config.yaml', 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    path_cap = os.path.join(config['paths']['input'], config['paths']['capacity'])
    cap = pd.read_csv(path_cap, index_col=0)
    flh = flh.T
    index_names = {'wind onshore': 'wind_onshore', 'wind offshore': 'wind_offshore'}
    cap = cap.rename(index=index_names)
    flh = flh.reindex(index=cap.index, columns=cap.columns)
    generation_potential = cap * flh * 8760
    cp = generation_potential.loc[:, 'CP'].to_frame()
    generation_potential = generation_potential.drop('CP', axis=1)
    generation_potential = generation_potential.dropna(axis=1, how='all')
    weights = generation_potential.div(cp['CP'],axis=0)
    
    data = {}
    change_tech_region_thres = {}
    change_tech_cp_thres = {}
    change_tech_region = {}
    change_tech = {}

    for tech, group in max_period_yearly.groupby('technology'):
        data[tech] = group

    for tech, df in data.items():
        if tech != reference:

            # aggregate across all years
            df = df.merge(data[reference], on=['year', 'fraction', 'region'], how='left',
                          suffixes=[f'_{tech}', f'_{reference}'])
            df['max_duration_change'] = (
                (df[f'max_duration_{reference}'] - df[f'max_duration_{tech}']) / df[f'max_duration_{tech}']) * 100
            df = df.groupby(['region', 'fraction'])['max_duration_change'].mean().unstack()
            df_no_cp = df.query('region != "CP"')
            change_tech_region_thres[tech] = df_no_cp
            df_cp = df.query('region == "CP"')
            df_cp.index = [tech]
            change_tech_cp_thres[tech] = df_cp

            # average across all thresholds
            mean_region = df.mean(axis=1)
            mean_region.name = tech
            change_tech_region[tech] = mean_region.copy()

            # weighted average across all regions
            weights_tech = weights.loc[tech, :]
            weights_tech = weights_tech.reindex(mean_region.drop('CP').index)
            mean = mean_region.drop('CP').mul(weights_tech, axis=0).sum()
            change_tech[tech] = mean
            
    change_tech_cp_thres = pd.concat(change_tech_cp_thres.values())
    change_tech_cp = change_tech_cp_thres.mean(axis=1)
    change_tech_region = pd.DataFrame(change_tech_region)
    change_tech = pd.DataFrame([change_tech])

    return change_tech, change_tech_region, change_tech_region_thres, change_tech_cp, change_tech_cp_thres


def percentual_reduction_balancing(max_period_yearly, reference, path_config, flh):
    '''
    Computes the average reduction of maximum drought durations across all years and regions.

    Parameters
    ----------
    max_period_yearly : DataFrame
        Holds max drought period across all regions, technologies, years, and fractions.
    reference : str
        Reference region.

    Returns
    -------
    change_tech_thres : dict
        Dict of dfs showing average change across all technologies, regions, and fractions.
    change_tech_thres_region_thres : DataFrame
        Change across all technologies and fractions.

    '''
    os.chdir(os.path.dirname(path_config))
    # import settings
    with open('config.yaml', 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    path_cap = os.path.join(config['paths']['input'], config['paths']['capacity'])
    cap = pd.read_csv(path_cap, index_col=0)
    flh = flh.T
    index_names = {'wind onshore': 'wind_onshore', 'wind offshore': 'wind_offshore'}
    cap = cap.rename(index=index_names)
    flh = flh.reindex(index=cap.index, columns=cap.columns)
    generation_potential = cap * flh * 8760
    cp = generation_potential.loc[:, 'CP'].to_frame()
    generation_potential = generation_potential.drop('CP', axis=1)
    generation_potential = generation_potential.dropna(axis=1, how='all')
    weights = generation_potential.div(cp['CP'],axis=0)
    
    data = {}
    change_tech_region_thres = {}
    change_tech_region = {}
    change_tech = {}

    for tech, group in max_period_yearly.groupby('technology'):
        data[tech] = group

    for tech, df in data.items():
        
        change_tech_region_thres[tech] = {}
        df_cp = df.query('region == @reference')
        df = df.query('region != @reference')
        
        # get balancing effect for each region, thresholds
        for region, df_region in df.groupby('region'):
            df_region = df_region.merge(df_cp, on=['year', 'fraction'], how='outer', suffixes=[f'_{region}', f'_{reference}'])
            df_region['max_duration_change'] = (
                (df_region[f'max_duration_{reference}'] - df_region[f'max_duration_{region}']) / df_region[f'max_duration_{region}']) * 100
            df_region['max_duration_change'] = df_region['max_duration_change'].fillna(-100)
            df_region = df_region.groupby([f'region_{region}', 'fraction'])['max_duration_change'].mean().unstack()
            change_tech_region_thres[tech][region] = df_region
        
        # concat dfs
        change_tech_region_thres[tech] = pd.concat(change_tech_region_thres[tech].values())
        
        # get balancing effect for each region averaged across all thresholds
        change_tech_region[tech] = change_tech_region_thres[tech].mean(axis=1)
        change_tech_region[tech].name = tech
        
    # concat
    change_tech_region = pd.concat(change_tech_region, axis=1)
    
    # get weighted average
    weights = weights.T
    weights = weights.reindex(index=change_tech_region.index, columns=change_tech_region.columns)
    change_tech = change_tech_region.mul(weights).sum(axis=0)

    return change_tech, change_tech_region, change_tech_region_thres


def aux_return_period_func(obj, drought_thresholds_run_labels):
    '''
    Collects all return_period averaged across all years outcomes all years in one dataframe.
    '''

    return_period = {}
    
    for tech, df_region in obj.items():

        return_period[tech] = {}

        for region, group in df_region.groupby('region'):

            # create correct labels
            group = group.replace({'threshold': drought_thresholds_run_labels[tech][region]}).copy()
            return_period[tech][region] = group

        # concat regional dfs
        return_period[tech] = pd.concat(list(return_period[tech].values()))
        return_period[tech]['technology'] = tech

    # concat technological dfs
    return_period = pd.concat(list(return_period.values()))

    # replace specific thresholds with fractions
    fractions = return_period['threshold'].unique()
    fractions = {f: float(f.split('=')[-1]) for f in fractions}
    return_period = return_period.replace({'threshold': fractions}).rename(columns={'threshold': 'fraction'})

    return return_period


def get_return_period(analyzer):
    '''
    Collects all return_period averaged across all years or quarters outcomes all years in one dataframe.
    '''
    return_period_yearly = aux_return_period_func(analyzer.return_period_all_long, analyzer.drought_thresholds_run_labels)
    return_period_q1 = aux_return_period_func(analyzer.return_period_q1_long, analyzer.drought_thresholds_run_labels)
    return_period_q2 = aux_return_period_func(analyzer.return_period_q2_long, analyzer.drought_thresholds_run_labels)
    return_period_q3 = aux_return_period_func(analyzer.return_period_q3_long, analyzer.drought_thresholds_run_labels)
    return_period_q4 = aux_return_period_func(analyzer.return_period_q4_long, analyzer.drought_thresholds_run_labels)
    return_period_q14 = aux_return_period_func(analyzer.return_period_q14_long, analyzer.drought_thresholds_run_labels)
    return_period_q23 = aux_return_period_func(analyzer.return_period_q23_long, analyzer.drought_thresholds_run_labels)

    return return_period_yearly, return_period_q1, return_period_q2, return_period_q3, return_period_q4, \
        return_period_q14, return_period_q23


def aux_frequency_func(obj, drought_thresholds_run_labels):
    '''
    Auxillary helper function.
    '''
    frequency_q = {}
    for tech, frequency_region in obj.items():

        frequency_q[tech] = {}

        for region, df in frequency_region.groupby('region'):

            # create correct labels
            df = df.replace({'threshold': drought_thresholds_run_labels[tech][region]})
            frequency_q[tech][region] = df

        # concat regional dfs
        frequency_q[tech] = pd.concat(list(frequency_q[tech].values()))
        frequency_q[tech]['technology'] = tech

    # concat technological dfs
    frequency_q = pd.concat(list(frequency_q.values()))

    # replace specific thresholds with fractions
    fractions = frequency_q['threshold'].unique()
    fractions = {f: float(f.split('=')[-1]) for f in fractions}
    frequency_q = frequency_q.replace({'threshold': fractions}).rename(columns={'threshold': 'fraction'})

    return frequency_q


def get_frequency(analyzer):
    '''
    Collects all frequency averaged across all years or quarters outcomes all years in one dataframe.
    '''
    frequency_yearly = aux_frequency_func(analyzer.mean_frequency_yearly, analyzer.drought_thresholds_run_labels)
    frequency_q1 = aux_frequency_func(analyzer.mean_frequency_q1_long, analyzer.drought_thresholds_run_labels)
    frequency_q2 = aux_frequency_func(analyzer.mean_frequency_q2_long, analyzer.drought_thresholds_run_labels)
    frequency_q3 = aux_frequency_func(analyzer.mean_frequency_q3_long, analyzer.drought_thresholds_run_labels)
    frequency_q4 = aux_frequency_func(analyzer.mean_frequency_q4_long, analyzer.drought_thresholds_run_labels)
    frequency_q14 = aux_frequency_func(analyzer.mean_frequency_q14_long, analyzer.drought_thresholds_run_labels)
    frequency_q23 = aux_frequency_func(analyzer.mean_frequency_q23_long, analyzer.drought_thresholds_run_labels)

    return frequency_yearly, frequency_q1, frequency_q2, frequency_q3, frequency_q4, frequency_q14, frequency_q23


def get_time_series(analyzer):
    '''
    Gets raw time series for all technologies and regions in dataframes in long format.
    '''
    ts = {}

    for tech in analyzer.region_results.keys():
        ts[tech] = {}

        for ra in analyzer.region_results[tech]:
            ts[tech][ra.region] = ra.input_extractor.ts_long

    return ts


def get_drought_ts_occurence_all_thresholds_scatter(min_duration, technology, drought_ts_region, year_selection=None):
    '''
    Retrieves identified drought time series for a specific technology, minimum drought duration,
    and, if desired, a selection of years for all thresholds. The capacity factors are
    converted to hold either zeros or, if the time step qualifies as drought, the fraction
    of the average capacity factor used as threshold for drought identification.

    Parameters
    ----------
    min_duration : int
        Needs to be specified in config.yaml (correlation thresholds) prior to the run.
    technology : string
        Needs to be specified in config.yaml prior to the run.
    drought_ts_region : drought_ts_region attribute of VREDA analyzer object
        Identified doughts stored by global VREDA object holding all results.
    year_selection : list of integers, optional
        Get data only for a subset of year. The default is None, causing data for all years
        to be retrieved.

    Returns
    -------
    data : pandas DataFrame
        Holds desired data. Details on format see in function description.
    '''

    data = {}

    for region in drought_ts_region:

        data[region] = {}

        for threshold, drought_ts_data in drought_ts_region[region][min_duration].items():

            # fetch results
            df = drought_ts_data.copy()

            # filter for out certain years
            if year_selection:
                df = df.query('year in @year_selection')
            
            if technology in df:
                df = df[['year', technology]]
            else:
                continue

            # convert capacity factor of drought periods to threshold value
            df[technology].loc[df[technology] > 0] = float(threshold[-3:])

            # prepare long format
            df = df.reset_index()
            df = df.rename(columns={technology: 'drought_indicator'})
            df['threshold'] = threshold
            df['region'] = region

            # datetime index
            temp = {}
            for year, group in df.groupby('year'):

                # Create a date range for the entire year
                start = pd.Timestamp(f'{year}-01-01 00:00:00')
                end = pd.Timestamp(f'{year}-12-31 23:00:00')
                dt_idx = pd.date_range(start=start, end=end, freq='H')

                # remove leap days
                dt_idx = dt_idx[dt_idx.strftime('%m-%d') != '02-29']

                group['date'] = dt_idx
                temp[year] = group

            # concat all years
            data[region][threshold] = pd.concat(temp.values())

        # concat all thresholds-specific dataframes if there are results
        if data[region]:
            data[region] = pd.concat(data[region].values())
        else:
            # remove region if there are no results
            del data[region]

    # concat all regions
    data = pd.concat(data.values())

    data.set_index('date', inplace=True)

    return data


def get_drought_ts_occurence_min_threshold_scatter(min_duration, technology, drought_ts_region, year_selection=None):
    '''
    Retrieves identified drought time series for a specific technology, minimum drought duration,
    and, if desired, a selection of years. If a time step qualifies as drought then it holds the
    lowest fraction of the average capacity factor that is used for drought identification, and
    zero otherwise in the column "drought_qualification".

    min_duration : int
        Needs to be specified in config.yaml (correlation thresholds) prior to the run.
    technology : string
        Needs to be specified in config.yaml prior to the run.
    drought_ts_region : drought_ts_region attribute of VREDA analyzer object
        Identified drought time series stored by global VREDA object holding all results.
    year_selection : list of integers, optional
        Get data only for a subset of year. The default is None, causing data for all years
        to be retrieved.

    Returns
    -------
    data : pandas DataFrame
        Holds desired data. Details on format see in function description.
    '''
    data = {}

    for region in drought_ts_region:

        data[region] = {}
        thresholds_col = []

        for threshold, drought_ts_data in drought_ts_region[region][min_duration].items():

            data[region][threshold] = {}
            thresholds_col.append(float(threshold[-3:]))

            # fetch results
            df = drought_ts_data.copy()

            # filter for out certain years & tech
            if year_selection:
                df = df.query('year in @year_selection')
            
            # abort if no data available
            if technology in df:
                df = df[['year', technology]]
            else:
                del data[region][threshold]
                continue

            # convert capacity factor of drought periods to threshold value
            df[technology].loc[df[technology] > 0] = float(threshold[-3:])

            # prepare long format
            df = df.reset_index()
            df = df.rename(columns={technology: float(threshold[-3:])})
            # df['threshold'] = threshold
            df['region'] = region

            # datetime index
            temp = {}
            for year, group in df.groupby('year'):

                # Create a date range for the entire year
                start = pd.Timestamp(f'{year}-01-01 00:00:00')
                end = pd.Timestamp(f'{year}-12-31 23:00:00')
                dt_idx = pd.date_range(start=start, end=end, freq='H')

                # remove leap days
                dt_idx = dt_idx[dt_idx.strftime('%m-%d') != '02-29']

                group['date'] = dt_idx
                temp[year] = group

            # concat all years if data available            
            data[region][threshold] = pd.concat(temp.values())

        if data[region]:

            # merge all thresholds
            df_merge = reduce(lambda left, right: pd.merge(
                left, right, on=['hour', 'year', 'region', 'date'], how='outer'), data[region].values()
            )
            # get only threshold time series
            df_values = df_merge[df_merge.columns.difference(['hour', 'year', 'region', 'date'])]
    
            # get lowest threshold that applies for each time step if qualified as drought or zero
            df_merge['drought_qualification'] = df_values[df_values > 0].min(axis=1).fillna(0)
    
            data[region] = df_merge[['hour', 'year', 'region', 'date', 'drought_qualification']]
            
        else:
            # remove empty entries
            del data[region]

    # concat all regions
    data = pd.concat(data.values())

    return data


def weighting_func(x, a):
    '''
    Creates a weight for a discrete threshold.

    Parameters
    ----------
    x : float
        Threshold.
    a : int
        Factor for weighting function.

    Returns
    -------
    float
        Weighting factor.

    '''
    return 1/(x**(a))  # a * x**(-a)  # - a*0.9**(-a)


def weighting_func_doubling(thresholds, largest_weight):
    """
    Assign weights to floats in ascending order. Each float gets half the weight of the next larger float.
    
    :param floats: A list of floats.
    :param largest_weight: The weight for the largest float.
    :return: A dictionary with floats as keys and their weights as values.
    """
    # Sort the list of floats in ascending order
    sorted_thresholds = sorted(thresholds)
    
    # Initialize an empty dictionary to hold the float-weight pairs
    weights = {}
    
    # Calculate and assign weights starting from the largest
    current_weight = largest_weight
    for float_number in reversed(sorted_thresholds):
        weights[round(float_number,2)] = current_weight
        current_weight *= 2  # Halve the weight for the next smaller float
    
    return weights


def get_x_weighting_func(y, a):
    '''
    Retrieves the corresponding x value of the weighting_func given a parameter a.

    Parameters
    ----------
    y : float
        Function value of weighting_func.
    a : int or float
        Parameter of weighting_func.

    Returns
    -------
    float
        Corresponding x value of y.

    '''
    return (a / y)**(1/a)  # (a / (y + a * 0.9**(-a)))**(1/a)


def import_region_results(path):
    '''
    Imports pickled region results for all techs in config from the run.
    '''
    os.chdir(path)
    with open('config.yaml', 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    region_results = {}
    for tech in config['technologies']:
        path_tech = os.path.join(path, tech)
        region_results_tech = f'region_results_{tech}.gz'
        region_results_tech = load_object(path_tech, region_results_tech)
        region_results[tech] = region_results_tech

    return region_results


def import_drought_ts_region(path):
    '''
    Imports pickled drought timeseries.
    '''
    os.chdir(path)

    drought_ts_region = 'drought_ts_region.gz'
    return load_object(path, drought_ts_region)


def import_analyzer(path):
    '''
    Imports pickled analyzer object.
    '''
    os.chdir(path)

    analyzer = 'analyzer.gz'
    return load_object(path, analyzer)


def parallelize(function, values):
    '''
    The unbound function is mapped to all components of values. The mapping pairs
    are automatically parallelized.
    '''
    # print id of parent process
    print(f'------------------------------------------------\n'
          f'Parent process {os.getpid()} initializes parallelization for region {values[0][0]}.\n'
          f'------------------------------------------------\n')

    # number of physical cores
    no_cpu = psutil.cpu_count(logical=True)  # False)

    # iniatialize parllelization
    with mp.Pool(no_cpu) as pool:  # no_cpu
        results = pool.starmap(function, values)
    return results


# %% temp import of drought counter

path = "D:\\git\\vre_droughts\\output\\2024-08-02_11-52-51_mbt_event\\portfolio\\DE"
path = "D:\\git\\vre_droughts\\output\\"
file = 'portfolio_DE.gz'
file = 'wind_onshore_f-mean-cf=0.1_CP.gz'

dc = load_object(path, file)

# %% import vreda results

path = 'T:\\mk\\vreda\\output\\2024-08-07_11-40-34_mbt_event_cp_de_es_uk\\'
# path = 'T:\\mk\\vreda\\output\\2024-06-12_15-54-10_mbt_event_all_mid_idx\\'
#path = 'T:\\mk\\vreda\\output\\2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete'
# path = 'T:\\mk\\vreda\\output\\2024-08-07_11-40-34_mbt_event_cp_de_es_uk'
#path = "T:\\mk\\vreda\\output\\2024-08-02_08-42-30_mbt_event"
#region_results = import_region_results(path)
drought_ts_region = import_drought_ts_region(path)
# a = import_analyzer(path)
# ra = load_object(path, "portfolio_DE.gz")

# %% get full load hours

path_output = 'T:\\mk\\vreda\\input'
get_flh(region_results, path_output)
flh = load_object(path_output, 'flh.gz')


# %% result import long-duration storage energy, reconversion and electrolysis capacity

path = 'C:\\git\\vre_droughts_long-duration_storage\\result_data\\h2_n_ely_all_years.csv'
ely = pd.read_csv(path)

path = 'C:\\git\\vre_droughts_long-duration_storage\\result_data\\h2_n_recon_all_years.csv'
recon = pd.read_csv(path)

# %%########################## drought severity metrices ############################################################

min_duration = 24
year_selection = []

drought_occurence_portfolio = get_drought_ts_occurence_all_thresholds_scatter(min_duration, 'portfolio', drought_ts_region, year_selection)
drought_occurence_pv = get_drought_ts_occurence_all_thresholds_scatter(min_duration, 'pv', drought_ts_region, year_selection)
drought_occurence_won = get_drought_ts_occurence_all_thresholds_scatter(min_duration, 'wind_onshore', drought_ts_region, year_selection)
drought_occurence_woff = get_drought_ts_occurence_all_thresholds_scatter(min_duration, 'wind_offshore', drought_ts_region, year_selection)

### %% pickle drought occurrence

datasets = [drought_occurence_portfolio, drought_occurence_pv, drought_occurence_won, drought_occurence_woff]  # 
datasets_names = ['drought_occurence_portfolio_24', 'drought_occurence_pv_24', 'drought_occurence_won_24', 'drought_occurence_woff_24']  #

path = 'T:\\mk\\vreda\\output\\2024-08-07_11-40-34_mbt_event_cp_de_es_uk'
os.chdir(path)

for idx, d in enumerate(datasets):
    file_name = f'{datasets_names[idx]}.gz'
    with gzip.open(file_name, 'wb') as file:
        pickle.dump(d, file, protocol=pickle.HIGHEST_PROTOCOL)
        
# %% load drought occurrence

path = 'T:\\mk\\vreda\\output\\2024-08-02_08-42-30_mbt_event'
#path = 'T:\\mk\\vreda\\output\\2024-08-02_08-42-30_mbt_event'

drought_occurence_portfolio = load_object(path, 'drought_occurence_portfolio_24.gz')
#drought_occurence_pv = load_object(path, 'drought_occurence_pv_24.gz')
#drought_occurence_won = load_object(path, 'drought_occurence_won_24.gz')
#drought_occurence_woff = load_object(path, 'drought_occurence_woff_24.gz')

# %% drought occurence plot year-pair

year_pairs = [[i, i+1] for i in [1996, 2006]]
order = ['CP', 'AL', 'AT', 'BA', 'BE', 'BG', 'CH', 'CZ', 'DE', 'DK', 'EE',
         'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
         'ME', 'MK', 'MT', 'NL', 'NO', 'PL', 'PT', 'RO', 'RS', 'SE', 'SI',
         'SK', 'UK']

# custom color scale
custom_colors = [
    '#4a006e',  # Dark Violet
    '#611785',  # Between Dark Violet and Violet
    '#78209b',  # Violet
    #'#7f104c',  # Transition between Violet and Dark Red
    '#840826',  # New Color (Enhanced Violet to Dark Red Transition)
    '#880000',  # Dark Red
    '#a41100',  # Between Dark Red and Light Red
    '#c62200',  # Light Red
    '#e84013',  # Between Light Red and Dark Orange
    '#f95927',  # Dark Orange
    '#fc6f20',  # New Color (Enhanced Red to Orange Transition)
    '#ff851a',  # Between Dark Orange and Light Orange
    '#ffa72d',  # Light Orange
    '#ffcc2a',  # Between Light Orange and Yellow
    '#ffdf2d',  # Yellow
    '#d0d830',  # New Color (Enhanced Yellow to Green Transition)
    '#a0d034',  # Between Yellow and Light Green
    '#7cb342',  # Light Green
    '#05723c',  # Dark Green
    '#022915',  # New Color (Enhanced Light Green to Dark Green Transition)
]

for yy in year_pairs:

    data = drought_occurence_portfolio.query('year in @yy')  # ' and threshold != "f-mean-cf=0.9"')

    fig = px.scatter(
        data,
        x=data.index,
        y=data.drought_indicator,
        facet_row=data.region,
        color=data.threshold,
        color_discrete_sequence=custom_colors,  # qualitative.Prism_r,
        facet_row_spacing=0.01,
        category_orders={'region': order}
    )

    fig.update_traces(marker=dict(size=1.00))

    start_year = data.year.unique()[0]
    end_year = data.year.unique()[-1]

    fig.update_layout(
        xaxis_range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
        yaxis_range=[0, 1],
        template='simple_white',
        margin=dict(l=0, r=0, t=30, b=0),
        legend={'itemsizing': 'constant'},
        height=1400, width=1240
    )
    fig.update_xaxes(dtick="M1", tickformat="%b%y")
    fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))
    fig.update_yaxes(visible=False, showticklabels=False)
    fig.update_xaxes(title='')
    # fig.show()

    path = 'T:\\\mk\\vreda\\result_plots'
    path_output = os.path.join(path, 'drought_time_series', 'year_pair_all_regions')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    name = f'all_regions_{start_year}_{end_year}'
    path_output = os.path.join(path_output, name)
    # fig.write_html(f'{path_output}.html')
    fig.write_image(f'{path_output}.pdf')
    fig.write_image(f'{path_output}.png', scale=3)
    
# %% drought occurence plot single region

min_duration = 24
regions = drought_occurence_portfolio.region.unique()
#drought_occurence_no_zero = drought_occurence_portfolio.query('threshold not in "f-mean-cf=1.0"').copy()

technology = 'portfolio'

for r in regions:

    year_pairs = [[i, i+1] for i in range(1982, 2019)]
    
    custom_colors_extended = [
    '#4a006e',  # Dark Violet
    '#611785',  # Between Dark Violet and Violet
    '#78209b',  # Violet
    #'#7f104c',  # Transition between Violet and Dark Red
    '#840826',  # New Color (Enhanced Violet to Dark Red Transition)
    '#880000',  # Dark Red
    '#a41100',  # Between Dark Red and Light Red
    '#c62200',  # Light Red
    '#e84013',  # Between Light Red and Dark Orange
    '#f95927',  # Dark Orange
    '#fc6f20',  # New Color (Enhanced Red to Orange Transition)
    '#ff851a',  # Between Dark Orange and Light Orange
    '#ffa72d',  # Light Orange
    '#ffcc2a',  # Between Light Orange and Yellow
    '#ffdf2d',  # Yellow
    '#d0d830',  # New Color (Enhanced Yellow to Green Transition)
    '#a0d034',  # Between Yellow and Light Green
    '#7cb342',  # Light Green
    '#05723c',  # Dark Green
    '#022915',  # New Color (Enhanced Light Green to Dark Green Transition)
    ]

    
    # minimum drought indicator
    min_drought = drought_occurence_portfolio.query('region in @r').drought_indicator.unique()[1]
    
    # prep data
    drought_occurence_r = drought_occurence_portfolio.query('region in @r')
    drought_occurence_r['drought_indicator'] = drought_occurence_r['drought_indicator'].replace(0, np.nan)
    
    # get number of thresholds
    thresholds = drought_occurence_r.threshold.unique()
    
    # assign colors
    no_col = len(custom_colors_extended) - len(thresholds)
    custom_colors_extended = custom_colors_extended[no_col:]
    
    drought_occurence_year_pair_col = {}
    
    for yy in year_pairs:
    
        # assign facet_row values
        data = drought_occurence_r.query('year in @yy').copy()
        data['year_pair'] = f"{yy[0]}-{yy[1]}"
        
        # assign index values
        for y in yy:
            
            # first year hour 0-8759
            if y == yy[0]:
                for thres in thresholds:
                    data_y = data.query('year == @y and threshold == @thres').copy()
                    data_y['hour_year_pair'] = range(len(data_y))
                    
                    drought_occurence_year_pair_col[f"{yy[0]}-{yy[1]}-({y})-{thres}"] = data_y
            
            # second year hour 8760-17519
            elif y == yy[1]:
                for thres in thresholds:
                    data_y = data.query('year == @y and threshold == @thres').copy()
                    data_y['hour_year_pair'] = range(len(data_y), len(data_y)*2)
                    
                    drought_occurence_year_pair_col[f"{yy[0]}-{yy[1]}-({y})-{thres}"] = data_y
         
    drought_occurence_year_pair_col = pd.concat(drought_occurence_year_pair_col.values())
    drought_occurence_year_pair_col = drought_occurence_year_pair_col.sort_values(by=['threshold', 'year_pair', 'year', 'hour', 'hour_year_pair'])
    
    fig = px.scatter(
        drought_occurence_year_pair_col,
        x=drought_occurence_year_pair_col.hour_year_pair,
        y=drought_occurence_year_pair_col.drought_indicator,
        facet_row=drought_occurence_year_pair_col.year_pair,
        color=drought_occurence_year_pair_col.threshold,
        color_discrete_sequence=custom_colors_extended,  # qualitative.Prism_r,
        facet_row_spacing=0.01,
        #category_orders={'region': order}
    )
    
    total_rows = max(fig._grid_ref)  # This dynamically finds the total number of facet rows
    
    # Loop through each row and add a vertical line at x=8759
    for row in range(len(year_pairs)):
        fig.add_shape(
            type="line",
            x0=8759, y0=0, x1=8759, y1=1,  # Adjust y0 and y1 according to your y-axis scale
            line=dict(color="black", width=1.5),
            #xref="x"+str(row),  # Adjusts reference to the correct x-axis based on the row
            yref="paper",  # 'paper' reference makes the line span the entire y-axis height
            row=row, col=1  # Assumes vertical line should be added to every subplot in the first column
        )
    
    fig.update_traces(marker=dict(size=2.25))
    
    fig.update_layout(
        xaxis_range=[0,17519],
        yaxis_range=[min_drought-0.09, 0.95],
        template='simple_white',
        margin=dict(l=0, r=50, t=30, b=0),
        legend={'itemsizing': 'constant'},
        height=1600, width=1240
    )
    
    fig.update_layout(
        # Other layout properties
        legend=dict(
            orientation='h',  # Horizontal orientation
            x=0.5,  # Center the legend horizontally
            y=-0.04,  # Position the legend below the plot area
            xanchor='center',  # Anchor the legend's horizontal center to x position
            yanchor='top'  # Anchor the legend's top to the y position
        )
    )
    
    fig.update_xaxes(tickformat=',')
    fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1], textangle=0))
    fig.update_yaxes(visible=False, showticklabels=False)
    fig.update_xaxes(title='')
    fig.update_xaxes(title_text="hour", row=1, col=1)
    #fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, 'drought_time_series', 'one_region_all_years')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    name = f'{r}_{technology}_{min_duration}'
    path_output = os.path.join(path_output, name)
    fig.write_html(f'{path_output}.html')
    fig.write_image(f'{path_output}.pdf')
    fig.write_image(f'{path_output}.png', scale=3)
    
# %% drought occurence select regions and technologies

def process_and_assign_data(year, start_hour, data_figure, sel_occurrence, thresholds, technologies, regions):
    for thres in thresholds:
        for tech in technologies:
            for r_code, r in regions.items():
                data_y = sel_occurrence.query('year == @year and threshold == @thres and technology == @tech and region == @r')
                if not data_y.empty:
                    data_y = data_y.copy()
                    data_y.loc[:, 'hour_year_pair'] = range(start_hour, start_hour + len(data_y))
                    data_y.loc[:, 'identifier'] = f'{tech} - {r}'
                    data_figure[f"{year}-{thres}-{tech}-{r}"] = data_y
                    start_hour += len(data_y)
    return start_hour

regions = {'DE':'Germany', 'CP': 'European copperplate', 'ES': 'Spain', 'UK': 'United Kingdom'}

year_pairs = [[i, i+1] for i in range(2013, 2014)]
# years = [1996, 1997]

for years in year_pairs:
    
    # std out
    print(f'Prepare data for {years[0]}-{years[1]}.')
 
    # prep data
    sel_portfolio = drought_occurence_portfolio.query('year in @years and region in @regions.keys()').assign(technology='portfolio')
    sel_pv = drought_occurence_pv.query('year in @years and region in @regions.keys()').assign(technology='pv')
    sel_won = drought_occurence_won.query('year in @years and region in @regions.keys()').assign(technology='onshore wind')
    sel_woff = drought_occurence_woff.query('year in @years and region in @regions.keys()').assign(technology='offshore wind')

    sel_occurrence = pd.concat([sel_portfolio, sel_pv, sel_won, sel_woff])
    sel_occurrence = sel_occurrence.replace(regions)
    min_drought = sorted(list(sel_occurrence.drought_indicator.unique()))[0]
    sel_occurrence['drought_indicator'] = sel_occurrence['drought_indicator'].replace(0, np.nan)
    sel_occurrence['threshold'] = sel_occurrence['threshold'].apply(lambda x: "{:.2f}".format(float(x.split('=')[1])))
                                                                    
    # exclude very high thresholds
    excluded = ['1.00']
    # sel_occurrence = sel_occurrence.query('threshold not in @excluded')
    
    # get number of thresholds
    thresholds = sorted(list(sel_occurrence.threshold.unique()))
    technologies = sorted(list(sel_occurrence.technology.unique()))
        
    custom_colors_extended = [
        '#4a006e',  # Dark Violet
        '#611785',  # Between Dark Violet and Violet
        '#78209b',  # Violet
        #'#7f104c',  # Transition between Violet and Dark Red
        '#840826',  # New Color (Enhanced Violet to Dark Red Transition)
        '#880000',  # Dark Red
        '#a41100',  # Between Dark Red and Light Red
        '#c62200',  # Light Red
        '#e84013',  # Between Light Red and Dark Orange
        '#f95927',  # Dark Orange
        '#fc6f20',  # New Color (Enhanced Red to Orange Transition)
        '#ff851a',  # Between Dark Orange and Light Orange
        '#ffa72d',  # Light Orange
        '#ffcc2a',  # Between Light Orange and Yellow
        '#ffdf2d',  # Yellow
        '#d0d830',  # New Color (Enhanced Yellow to Green Transition)
        '#a0d034',  # Between Yellow and Light Green
        '#7cb342',  # Light Green
        '#05723c',  # Dark Green
        '#022915',  # New Color (Enhanced Light Green to Dark Green Transition)
    ]
    
    # assign colors
    no_col = len(custom_colors_extended) - len(thresholds)
    custom_colors_extended = custom_colors_extended[no_col:]
    
    # assign facet_row values
    sel_occurrence['year_pair'] = f"{years[0]}-{years[1]}"
    
    # Process data for the first and second year
    data_figure = {}
    start_hour = 0
    start_hour = process_and_assign_data(years[0], start_hour, data_figure, sel_occurrence, thresholds, technologies, regions)
    start_hour = process_and_assign_data(years[1], start_hour, data_figure, sel_occurrence, thresholds, technologies, regions)

    
# =============================================================================
#     # first year hour 0-8759
#     for thres in thresholds:
#         for tech in technologies:
#             for r_code, r in regions.items():
#                 data_y = sel_occurrence.query('year == @years[0] and threshold == @thres and technology == @tech and region == @r')
#                 if len(data_y) > 0:
#                     data_y['hour_year_pair'] = range(len(data_y))
#                     data_y['identifier'] = f'{tech} - {r}'
#                     data_figure[f"{years[0]}-{thres}-{tech}-{r}"] = data_y
#                     del data_y
#     
#     # second year hour 8760-17519
#     for thres in thresholds:
#         for tech in technologies:
#             for r_code, r in regions.items():
#                 data_y = sel_occurrence.query('year == @years[1] and threshold == @thres and technology == @tech and region == @r')
#                 if len(data_y) > 0:
#                     data_y['hour_year_pair'] = range(len(data_y), len(data_y)*2)
#                     data_y['identifier'] = f'{tech} - {r}'
#                     data_figure[f"{years[1]}-{thres}-{tech}-{r}"] = data_y
#                     del data_y
# =============================================================================
         
    data_figure = pd.concat(data_figure.values())
    data_figure = data_figure.sort_values(by=['identifier', 'threshold', 'year_pair', 'year', 'hour_year_pair'])
    
    order = [
        'portfolio - European\ncopperplate', 'portfolio - Germany', 'portfolio - Spain', 'portfolio - United Kingdom',
        'pv - European\ncopperplate', 'pv - Germany', 'pv - Spain', 'pv - United Kingdom',
        'onshore wind - European\ncopperplate', 'onshore wind - Germany', 'onshore wind - Spain', 'onshore wind - United Kingdom',
        'offshore wind - European\ncopperplate', 'offshore wind - Germany', 'offshore wind - Spain', 'offshore wind - United Kingdom'
    ]
    
    data_figure.loc[:, 'identifier'] = data_figure['identifier'].str.replace('European copperplate', 'European\ncopperplate')
    
    identifier = list(data_figure.identifier.unique())
    
    data_figure = data_figure.reset_index()
    
    print(f'Plotting for {years[0]}-{years[1]} started.')
    
    fig = px.scatter(
        data_figure,
        x=data_figure.date,
        y=data_figure.drought_indicator,
        facet_row=data_figure.identifier,
        color=data_figure.threshold,
        color_discrete_sequence=custom_colors_extended,  # qualitative.Prism_r,
        facet_row_spacing=0.02,
        category_orders={'identifier': order}
    )
    
    total_rows = max(fig._grid_ref)  # This dynamically finds the total number of facet rows
    
    start_year = data_figure.year.unique()[0]
    end_year = data_figure.year.unique()[-1]
    
    # Loop through each row and add a vertical line at beginning of second year
    line_date = datetime.datetime(end_year, 1, 1)
    
    for row in range(len(identifier)):
        fig.add_shape(
            type="line",
            x0=line_date, y0=0, x1=line_date, y1=1,  # Adjust y0 and y1 according to your y-axis scale
            line=dict(color="black", width=1),
            # opacity=1,
            #xref="x"+str(row),  # Adjusts reference to the correct x-axis based on the row
            yref="paper",  # 'paper' reference makes the line span the entire y-axis height
            row=row, col=1  # Assumes vertical line should be added to every subplot in the first column
        )
    
    fig.update_traces(marker=dict(size=1.9))
    
    
    fig.update_layout(
        #xaxis_range=[0, 17519],
        xaxis_range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
        yaxis_range=[min_drought-0.05, 1],
        template='simple_white',
        margin=dict(l=30, r=120, t=5, b=40),
        legend={'itemsizing': 'constant'},
    )
    
    fig.update_xaxes(dtick="M1", tickformat="%b<br>'%y", tickfont=dict(size=10))
    
    #fig.update_xaxes(tickformat=',')
    fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1], textangle=0))
    fig.for_each_annotation(lambda x: x.update(text=x.text.split("-")[-1], textangle=0))
    fig.update_yaxes(visible=False, showticklabels=False)
    fig.update_xaxes(title='')
    # fig.update_xaxes(title_text="hour", row=1, col=1)
    
    fig.add_annotation(
        text="a) portfolio",  # Text to display
        xref="paper",
        yref="paper",
        x=-0.035,
        y=0.935,  # Adjust 'x' and 'y' to position the text correctly
        textangle=-90,  # Rotate text to vertical
        font=dict(size=12, color="black"),
        showarrow=False
    )
    
    fig.add_annotation(
        text="b) solar PV",  # Text to display
        xref="paper",
        yref="paper",
        x=-0.035,
        y=0.638,  # Adjust 'x' and 'y' to position the text correctly
        textangle=-90,  # Rotate text to vertical
        font=dict(size=12, color="black"),
        showarrow=False
    )
    
    fig.add_annotation(
        text="c) onshore wind",  # Text to display
        xref="paper",
        yref="paper",
        x=-0.035,
        y=0.377,  # Adjust 'x' and 'y' to position the text correctly
        textangle=-90,  # Rotate text to vertical
        font=dict(size=12, color="black"),
        showarrow=False
    )
    
    fig.add_annotation(
        text="d) offshore wind",  # Text to display
        xref="paper",
        yref="paper",
        x=-0.035,
        y=0.062,  # Adjust 'x' and 'y' to position the text correctly
        textangle=-90,  # Rotate text to vertical
        font=dict(size=12, color="black"),
        showarrow=False
    )
    
    fig.add_shape(
        type="line",
        x0=-0.01,
        y0=0.765,
        x1=-0.01,
        y1=1,
        line=dict(color='black', width=2),
        xref="paper", yref="paper",
        )
    
    fig.add_shape(
        type="line",
        x0=-0.01,
        y0=0.51,
        x1=-0.01,
        y1=0.745,
        line=dict(color='black', width=2),
        xref="paper", yref="paper",
        )
    
    fig.add_shape(
        type="line",
        x0=-0.01,
        y0=0.253,
        x1=-0.01,
        y1=0.49,
        line=dict(color='black', width=2),
        xref="paper", yref="paper"
        )
    
    fig.add_shape(
        type="line",
        x0=-0.01,
        y0=0,
        x1=-0.01,
        y1=0.235,
        line=dict(color='black', width=2),
        xref="paper", yref="paper"
        )
    
    fig.update_layout(
        # Other layout properties
        legend=dict(
            title='Identified events according to relative thresholds:',
            font=dict(size=10),
            title_side='top',
            orientation='h',  # Horizontal orientation
            x=0.5,  # Center the legend horizontally
            y=-0.08,  # Position the legend below the plot area
            xanchor='center',  # Anchor the legend's horizontal center to x position
            yanchor='top',  # Anchor the legend's top to the y position
            itemsizing='constant',
            traceorder='normal',
            #itemwidth=20,
            entrywidthmode='fraction',
            entrywidth=0.1
        )
    )
    
    fig.update_layout(height=900, width=1100)
    
    #fig.show()
    
    print(f'Printing plots for {years[0]}-{years[1]}.')
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, 'drought_time_series', f'select_regions_all_techs')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    region_values = list(regions.keys())
    name = f'filter_24_{years[0]}-{years[1]}_{region_values[0]}-{region_values[1]}-{region_values[2]}-{region_values[3]}'
    path_output = os.path.join(path_output, name)
    #fig.write_html(f'{path_output}.html')
    fig.write_image(f'{path_output}.pdf')
    fig.write_image(f'{path_output}.png', scale=3)
    
    del fig
    
    print(f'Plotting for {years[0]}-{years[1]} completed.')
    

# %% severity metrices: single threshold

year_pairs = [[i, i+1] for i in range(1982, 2019)]

# =============================================================================
# weights = {
#     0.1: 256,
#     0.2: 128,
#     0.3: 64,
#     0.4: 32,
#     0.5: 16,
#     0.6: 8,
#     0.7: 4,
#     0.8: 2,
#     0.9: 1
#     }
# =============================================================================

weights = weighting_func_doubling(np.arange(0.1, 1, 0.05), 0.1)
weights[1] = 0

weights = {round(x, 1): weighting_func(x, 4) for x in np.arange(0.1, 1, 0.05)}

exclude_thresholds_drought_mass = [0.8, 0.85, 0.9, 0.95, 1.0]

mass_all_events_avg_regions_all_years = {}  # drought mass as severity proxy
event_max_mass_region_all_years = {}  # single event with highest mass

# compute metrices for all regions
for region, data in drought_occurence_portfolio.groupby('region'):

    mass_all_events_avg_regions_all_years[region] = {}
    event_max_mass_region_all_years[region] = {}

    # iterate over year pairs
    for yy in year_pairs:

        df = data.query('year in @yy').copy()

        # prepare data in wide format
        df = df.reset_index()
        df['threshold'] = df['threshold'].apply(lambda x: float(x.split('=')[1]))

        df = df.pivot(index=['date', 'hour', 'year'], columns=['threshold'], values='drought_indicator')
        df = df.replace(weights)

        # drop threshold columns that are excluded from drought mass and compute drought mass
        df = df.drop(exclude_thresholds_drought_mass, axis=1)
        df['mass_hour'] = df.sum(axis=1)

        # metric 1: drought mass across all hours of the corresponding time frame (here: two years)
        mass_all_events = df.sum().sum()
        mass_all_events_avg_threshold = get_x_weighting_func(mass_all_events/df.shape[0], 4)

        # compute average across all time steps and included threshold columns
        mass_all_events_avg = mass_all_events  # TODO: / (df.shape[0] * (df.shape[1] - 1))
        mass_all_events_avg_regions_all_years[region][f'{yy[0]}-{yy[1]}'] = {
            'mass_all_events_avg': mass_all_events_avg,
            'avg_threshold': mass_all_events_avg_threshold,
            'region': region
        }

        # metric 2: drought mass of single events for each time frame
        event_mask = (df['mass_hour'] > 0).astype(int)

        events = {}
        events_mass = {}

        # group data in droughts and no droughts, compute cumulative drought mass
        for idx, event in event_mask.rename('event_indicator').astype(int).groupby(event_mask.diff().ne(0).cumsum()):
            event = pd.merge(event, df['mass_hour'], on=['date', 'hour', 'year'], how='left')
            event = event['mass_hour'].cumsum().rename('cumulative_mass')
            events[idx] = event
            events_mass[idx] = {'mass': event[-1], 'duration': event.shape[0],
                                'start': event.index[0], 'end': event.index[-1], 'region': region}
        events_mass = pd.DataFrame.from_dict(events_mass, orient='index')

        # retrieve event with max drought mass
        event_max_mass_region_all_years[region][f'{yy[0]}-{yy[1]}'] = \
            events_mass.loc[events_mass['mass'].idxmax()].rename(region)

    # convert to df in long format
    mass_all_events_avg_regions_all_years[region] = pd.DataFrame.from_dict(
        mass_all_events_avg_regions_all_years[region], orient='index')
    mass_all_events_avg_regions_all_years[region].reset_index(names='time_frame', drop=False, inplace=True)

    event_max_mass_region_all_years[region] = pd.DataFrame.from_dict(
        event_max_mass_region_all_years[region], orient='index')
    event_max_mass_region_all_years[region].reset_index(names='time_frame', drop=False, inplace=True)

# identify time frame and single event with highest mass per region
mass_all_events_avg_regions_max_year = {}
for region, df in mass_all_events_avg_regions_all_years.items():
    mass_all_events_avg_regions_max_year[region] = df.loc[df['mass_all_events_avg'].idxmax()]
mass_all_events_avg_regions_max_year = pd.DataFrame.from_dict(mass_all_events_avg_regions_max_year, orient='index')

event_max_mass_region_max_year = {}
for region, df in event_max_mass_region_all_years.items():
    event_max_mass_region_max_year[region] = df.loc[df['mass'].idxmax()]
event_max_mass_region_max_year = pd.DataFrame.from_dict(event_max_mass_region_max_year, orient='index')

# concat dfs to df with format
mass_all_events_avg_regions_all_years = pd.concat(mass_all_events_avg_regions_all_years.values())
event_max_mass_region_all_years = pd.concat(event_max_mass_region_all_years.values())


# %% drought occurence one year-pair many regions plot with identified most severe event

year_pairs = [[i, i+1] for i in range(1983, 2018)]

# prep drought occurrence data
drought_occurence_portfolio['drought_indicator'] = drought_occurence_portfolio['drought_indicator'].replace(0, np.nan)
drought_occurence_portfolio['threshold'] = drought_occurence_portfolio['threshold'].apply(lambda x: "{:.2f}".format(float(x.split('=')[1])))
excluded = ['1.00']
drought_occurence_portfolio = drought_occurence_portfolio.query('threshold not in @excluded')

technology = 'portfolio'
event_max_mass_region_all_years = event_max_mass_region_all_years.replace('EU', 'CP')

order = ['CP', 'AL', 'AT', 'BA', 'BE', 'BG', 'CH', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU',
         'IE', 'IT', 'LT', 'LV', 'ME', 'MK', 'MT', 'NL', 'NO', 'PL', 'PT', 'RO', 'RS', 'SE', 'SI', 'SK', 'UK']

for yy in year_pairs:
    
    print(f'Plotting for {yy[0]}-{yy[1]} started.')

    data = drought_occurence_portfolio.query('year in @yy')  # ' and threshold != "f-mean-cf=0.9"')
   
    custom_colors_extended = [
        '#4a006e',  # Dark Violet
        '#611785',  # Between Dark Violet and Violet
        '#78209b',  # Violet
        #'#7f104c',  # Transition between Violet and Dark Red
        '#840826',  # New Color (Enhanced Violet to Dark Red Transition)
        '#880000',  # Dark Red
        '#a41100',  # Between Dark Red and Light Red
        '#c62200',  # Light Red
        '#e84013',  # Between Light Red and Dark Orange
        '#f95927',  # Dark Orange
        '#fc6f20',  # New Color (Enhanced Red to Orange Transition)
        '#ff851a',  # Between Dark Orange and Light Orange
        '#ffa72d',  # Light Orange
        '#ffcc2a',  # Between Light Orange and Yellow
        '#ffdf2d',  # Yellow
        '#d0d830',  # New Color (Enhanced Yellow to Green Transition)
        '#a0d034',  # Between Yellow and Light Green
        '#7cb342',  # Light Green
        '#05723c',  # Dark Green
        '#022915',  # New Color (Enhanced Light Green to Dark Green Transition)
    ]

    # get number of thresholds
    thresholds = sorted(list(data.threshold.unique()))

    no_col = len(custom_colors_extended) - len(thresholds)
    custom_colors_extended_sel = custom_colors_extended[no_col:]

    # plot drought occurrence
    fig = px.scatter(
        data,
        x=data.index,
        y=data.drought_indicator,
        facet_row=data.region,
        color=data.threshold,
        color_discrete_sequence=custom_colors_extended_sel,  # qualitative.Prism_r,
        facet_row_spacing=0.01,
        category_orders={'region': order},
    )

    fig.update_traces(marker=dict(size=2.25))

    start_year = data.year.unique()[0]
    end_year = data.year.unique()[-1]

    fig.update_layout(
        xaxis_range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
        yaxis_range=[float(thresholds[0])-0.03, 0.95],
        template='simple_white',
        margin=dict(l=0, r=0, t=30, b=0),
        legend={'itemsizing': 'constant'},
        height=1400, width=1240
    )
    
    fig.update_xaxes(dtick="M1", tickformat="%b<br>'%y", tickfont=dict(size=10))
    fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))
    fig.update_yaxes(visible=False, showticklabels=False)
    fig.update_xaxes(title='')

    # plot identified max period
    time_frame = f'{yy[0]}-{yy[1]}'
    for idx, region in enumerate(order[::-1]):
        start = event_max_mass_region_all_years.query(
            'region == @region and time_frame == @time_frame')['start'].iloc[0][0] - datetime.timedelta(hours=30)
        end = event_max_mass_region_all_years.query(
            'region == @region and time_frame == @time_frame')['end'].iloc[0][0] + datetime.timedelta(hours=30)
        fig.add_vrect(x0=start, x1=end, row=idx+1, col=1,
                      # annotation_text="decline", annotation_position="top left",
                      fillcolor='gray', opacity=0.3, line_width=0)

    fig.update_layout(
        # Other layout properties
        legend=dict(
            title='Relative thresholds:',
            font=dict(size=10),
            title_side='top',
            orientation='h',  # Horizontal orientation
            x=0.5,  # Center the legend horizontally
            y=-0.03,  # Position the legend below the plot area
            xanchor='center',  # Anchor the legend's horizontal center to x position
            yanchor='top',  # Anchor the legend's top to the y position
            itemsizing='constant',
            traceorder='normal',
            #itemwidth=20,
            entrywidthmode='fraction',
            entrywidth=0.11
        )
    )

    fig.show()

    path = path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, 'drought_time_series','year_pair_all_regions_marked')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    name = f'all_regions_{start_year}_{end_year}_marked'
    path_output = os.path.join(path_output, name)
    fig.write_html(f'{path_output}.html')
    fig.write_image(f'{path_output}.pdf')
    #fig.write_image(f'{path_output}.png', scale=3)

# %% drought occurence plot with identified most severe event and H2_STO_L

# configuration
sel = ['DE', 'ES', 'FR', 'IT', 'GR', 'SE', 'PL', 'UK']

# import results
path = 'S:\\projekte_paper\\vre-drought-mass\\result_data\\h2_sto_l.csv'
sto_l = pd.read_csv(path)
scenarios = {'island-e': 'island', 'TYNDP-e': 'TYNDP', 'copperplate-e': 'copperplate'}
sto_l = sto_l.replace(scenarios)
sto_l_cp = sto_l.query('custom_flow == "copperplate"')
sto_l_cp = sto_l_cp[['n', 'h', 'value', 'custom_year']]
sto_l_cp = sto_l_cp.groupby(['h', 'custom_year'])['value'].sum().reset_index()
sto_l = sto_l.query('n in @sel and custom_flow != "copperplate"')
sel.insert(0, 'CP')

path = 'T:\\mk\\vreda\\output\\2024-06-12_15-54-10_mbt_event_all_mid_idx\\'
#dm_para = 'drought_mass_weighting_func_doubling_smallest_weight_0.1'
#dm_para = 'drought_mass_weighting_func2_1.5'
dm_para = 'drought_mass_weighting_func2_2'
path = os.path.join(path,dm_para)
file = 'mass_single_events_max_avg_mass_per_time_frame.gz'
mass_single_events_max_avg_mass_per_time_frame = load_object(path, file)
mass_single_events_max_avg_mass_per_time_frame = mass_single_events_max_avg_mass_per_time_frame.query('region in @sel')

path = 'T:\\mk\\vreda\\output\\2024-06-12_15-54-10_mbt_event_all_mid_idx\\'
drought_occurence_portfolio = load_object(path, 'drought_occurence_portfolio_168.gz')

drought_occurence_portfolio = drought_occurence_portfolio.query('region in @sel')
drought_occurence_portfolio['drought_indicator'] = drought_occurence_portfolio['drought_indicator'].replace(0, np.nan)
drought_occurence_portfolio['threshold'] = drought_occurence_portfolio['threshold'].apply(lambda x: "{:.2f}".format(float(x.split('=')[1])))
excluded = ['1.00']
drought_occurence_portfolio = drought_occurence_portfolio.query('threshold not in @excluded')

# define years and colors
year_pairs = [[i, i+1] for i in range(1982, 2019)]

# customize colors
custom_colors_extended = [
    '#4a006e',  # Dark Violet
    '#611785',  # Between Dark Violet and Violet
    '#78209b',  # Violet
    #'#7f104c',  # Transition between Violet and Dark Red
    '#840826',  # New Color (Enhanced Violet to Dark Red Transition)
    '#880000',  # Dark Red
    '#a41100',  # Between Dark Red and Light Red
    '#c62200',  # Light Red
    '#e84013',  # Between Light Red and Dark Orange
    '#f95927',  # Dark Orange
    '#fc6f20',  # New Color (Enhanced Red to Orange Transition)
    '#ff851a',  # Between Dark Orange and Light Orange
    '#ffa72d',  # Light Orange
    '#ffcc2a',  # Between Light Orange and Yellow
    '#ffdf2d',  # Yellow
    '#d0d830',  # New Color (Enhanced Yellow to Green Transition)
    '#a0d034',  # Between Yellow and Light Green
    '#7cb342',  # Light Green
    '#05723c',  # Dark Green
    '#022915',  # New Color (Enhanced Light Green to Dark Green Transition)
]

for yy in year_pairs:
    print(yy)

    # sel = ['AT', 'BE', 'CH', 'CZ', 'DE', 'DK', 'EE', 'ES', 'IE', 'LT', 'LV', 'NO', 'PT']
    #sel = ['DE', 'ES', 'FR', 'IT','UK'] #, 'NL', 'PL', 'SE', 'RO', 'UK']
    sel_no = len(sel)

    drought_occurrence_sel = drought_occurence_portfolio.query('year in @yy')

    # get number of thresholds
    thresholds = sorted(list(drought_occurrence_sel.threshold.unique()))

    no_col = len(custom_colors_extended) - len(thresholds)
    custom_colors_extended_sel = custom_colors_extended[no_col:]

    row_heights = [
        # 0.04, 0.07,  # for 9 regions
        #0.04, 0.06,  # for 10 regions
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07, 0.008,
        0.04, 0.07
        # 0.02, 0.03,  # for 20 regions
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        # 0.02, 0.03,
        ]

    row_no = len(row_heights)

#%%

    fig = make_subplots(rows=row_no, cols=1, row_heights=row_heights, shared_xaxes=True, vertical_spacing=0.01)
    # fig.print_grid()

    # plot drought ts
    regions = []
    row_counter = 1

    for region in sel:

        group_drought_region = drought_occurrence_sel.query('region == @region')

        regions.append(region)
        regions.append(region)
        regions.append(region)

        for idx, (threshold, group_drought_threshold) in enumerate(group_drought_region.groupby('threshold')):

            # with legend
            if row_counter == 1:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_drought_threshold.drought_indicator,
                        marker=dict(size=4.5, color=custom_colors_extended[idx]),
                        name=threshold,
                        mode='markers',
                        legend='legend',
                        #legendgrouptitle=dict(text='relative thresholds')
                    ),
                    row=row_counter, col=1,
                )

            # without legend
            else:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_drought_threshold.drought_indicator,
                        marker=dict(size=4.5, color=custom_colors_extended[idx]),
                        name=threshold,
                        mode='markers',
                        showlegend=False
                    ),
                    row=row_counter, col=1,
                )
        row_counter += 3

    # plot identified max period
    time_frame = f'{yy[0]}-{yy[1]}'
    row_counter = 1

    for region in sel:

        start = mass_single_events_max_avg_mass_per_time_frame.query(
            'region == @region and time_frame == @time_frame')['start'].iloc[0][0] - datetime.timedelta(hours=30)
        end = mass_single_events_max_avg_mass_per_time_frame.query(
            'region == @region and time_frame == @time_frame')['end'].iloc[0][0] + datetime.timedelta(hours=30)

# =============================================================================
#         start = event_max_mass_region_all_years.query(
#             'region == @region and time_frame == @time_frame')['start'].iloc[0][0] - datetime.timedelta(hours=30)
#         end = event_max_mass_region_all_years.query(
#             'region == @region and time_frame == @time_frame')['end'].iloc[0][0] + datetime.timedelta(hours=30)
# =============================================================================

        fig.add_vrect(x0=start, x1=end, row=row_counter, col=1,
                      # annotation_text="decline", annotation_position="top left",
                      fillcolor='gray', opacity=0.4, line_width=0)

        row_counter += 3

    # add storage SOC CP
    sto_l_sel = sto_l_cp.query('custom_year in [@yy[0]]')
    
    fig.add_trace(
        go.Scatter(
            x=group_drought_threshold.index,
            y=sto_l_sel.value,
            line=dict(color='darkred', width=2),
            mode='lines',
            name='copperplate',
            legend='legend2',
            #legendgrouptitle=dict(text='storage level scenarios')
        ),
        row=2, col=1,
    )

    # add storage SOC countries
    scens = ['island', 'TYNDP']
    colors_scen = ['black', 'gray']
    
    sel.remove('CP')
    
    for scen_idx, scen in enumerate(scens):
        
        row_counter = 5
        
        sto_l_sel = sto_l.query('custom_year in [@yy[0]] and custom_flow == @scen')
    
        for idx, region in enumerate(sel):
    
            group_sto = sto_l_sel.query('n == @region')
    
            # group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
            # group_sto = group_sto.iloc[::ts_len]
    
            # with legend
            if idx == 0:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_sto.value,
                        line=dict(color=colors_scen[scen_idx], width=2),
                        mode='lines',
                        name=scen,
                        legend='legend2',
                        #legendgrouptitle=dict(text='storage level scenarios')
                    ),
                    row=row_counter, col=1,
                )
                row_counter += 3
    
            # without legend
            else:
                fig.add_trace(
                    go.Scatter(
                        x=group_drought_threshold.index,
                        y=group_sto.value,
                        line=dict(color=colors_scen[scen_idx], width=2),
                        mode='lines',
                        #name='',
                        showlegend=False
                    ),
                    row=row_counter, col=1,
                )
                row_counter += 3

    sel.insert(0, 'CP')

    # Update xaxis properties
    start_year = yy[0]
    end_year = yy[-1]

    for r in range(row_no):
        if r < row_no-1:
            fig.update_xaxes(
                title_text='',
                range=[datetime.datetime(start_year-1, 12, 31), datetime.datetime(end_year+1, 1, 1)],
                mirror=True,
                dtick="M1",
                row=r+1, col=1)

    fig.update_xaxes(
        title_text='',
        range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
        mirror=True,
        row=row_no, col=1,
        dtick="M1",
        tickformat="%b<br>'%y",
        tickfont=dict(size=10)
        )

    # Update yaxis properties
    for r in range(1, row_no+1, 3):
        fig.update_yaxes(title=dict(text=regions[r], font=dict(size=20), standoff=35), range=[float(thresholds[0])-0.03, 0.95],
                         ticks='', showticklabels=False, mirror=True, row=r, col=1)
        
    fig.update_layout(yaxis2=dict(title=dict(text='TWh', font=dict(size=15)), mirror=True),
                      yaxis5=dict(title='TWh', mirror=True),
                      yaxis8=dict(title='TWh', mirror=True),
                      yaxis11=dict(title='TWh', mirror=True),
                      yaxis14=dict(title='TWh', mirror=True),
                      yaxis17=dict(title='TWh', mirror=True),
                      yaxis20=dict(title='TWh', mirror=True),
                      yaxis23=dict(title='TWh', mirror=True),
                      yaxis26=dict(title='TWh', mirror=True),
                      )

    fig.update_layout(template='simple_white',
                      margin=dict(l=0, r=0, t=0, b=30),
                      height=1200, width=1100
                      )
    
    fig.update_layout(
        legend=dict(
            title='Relative thresholds:',
            font=dict(size=10),
            title_side='top',
            orientation='h',  # Horizontal orientation
            x=0.52,  # Center the legend horizontally
            y=-0.04,  # Move the legend further down
            xanchor='center',  # Anchor the legend's horizontal center to x position
            yanchor='top',  # Anchor the legend's top to the y position
            #itemsizing='constant',
            traceorder='normal',
            entrywidthmode='fraction',
            entrywidth=0.11
            ),
        legend2=dict(
            title='Storage level of the modeled scenarios:',
            font=dict(size=10),
            title_side='top',
            orientation='h',  # Horizontal orientation
            x=0.9,  #14 Center the legend horizontally
            y=-0.12,  # Move the legend further down
            xanchor='left',  # Anchor the legend's horizontal center to x position
            yanchor='top',  # Anchor the legend's top to the y position
            #itemsizing='constant',
            traceorder='normal',
            entrywidthmode='fraction',
            #entrywidth=0.152
            )
        )    

    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, 'drought_time_series_STO_L', 'year_pair_all_regions_sto_l')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    name = f'drought_ts_sto_l_{start_year}_{end_year}'
    path_output = os.path.join(path_output, name)
    # fig.write_html(f'{path_output}.html')
    #fig.write_image(f'{path_output}.pdf')
    #fig.write_image(f'{path_output}.png', scale=1)

# =============================================================================
#     fig.update_layout(
#         xaxis_range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
#         yaxis_range=[0.05, 0.95],
#         template='simple_white',
#         margin=dict(l=0, r=0, t=30, b=0),
#         legend={'itemsizing': 'constant'},
#         height=1400, width=1240
#     )
#     fig.update_xaxes(dtick="M1", tickformat="%b%y")
#     fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))
#     fig.update_yaxes(visible=False, showticklabels=False)
#     fig.update_xaxes(title='')
# =============================================================================


# %% drought occurence plot single region all years with max identified period

# prep data max drought mass period 
event_max_mass_region_all_years[['y1', 'y2']] = event_max_mass_region_all_years['time_frame'].str.split('-', expand=True).astype(int)
event_max_mass_region_all_years['start_hour'] = event_max_mass_region_all_years.apply(
    lambda row: row['start'][1] + 8760 if row['start'][2] == row['y2'] else row['start'][1], axis=1)
event_max_mass_region_all_years['end_hour'] = event_max_mass_region_all_years.apply(
    lambda row: row['end'][1] + 8760 if row['end'][2] == row['y2'] else row['end'][1], axis=1)
event_max_mass_region_all_years.set_index('time_frame', inplace=True)
                                                                
# prep drought occurrence data
drought_occurence_portfolio['drought_indicator'] = drought_occurence_portfolio['drought_indicator'].replace(0, np.nan)
drought_occurence_portfolio['threshold'] = drought_occurence_portfolio['threshold'].apply(lambda x: "{:.2f}".format(float(x.split('=')[1])))
excluded = ['1.00']
drought_occurence_portfolio = drought_occurence_portfolio.query('threshold not in @excluded')

technology = 'portfolio'

# customize colors
custom_colors_extended = [
    '#4a006e',  # Dark Violet
    '#611785',  # Between Dark Violet and Violet
    '#78209b',  # Violet
    #'#7f104c',  # Transition between Violet and Dark Red
    #'#840826',  # New Color (Enhanced Violet to Dark Red Transition)
    '#880000',  # Dark Red
    '#a41100',  # Between Dark Red and Light Red
    '#c62200',  # Light Red
    '#e84013',  # Between Light Red and Dark Orange
    '#f95927',  # Dark Orange
    '#fc6f20',  # New Color (Enhanced Red to Orange Transition)
    '#ff851a',  # Between Dark Orange and Light Orange
    '#ffa72d',  # Light Orange
    '#ffcc2a',  # Between Light Orange and Yellow
    '#ffdf2d',  # Yellow
    '#d0d830',  # New Color (Enhanced Yellow to Green Transition)
    '#a0d034',  # Between Yellow and Light Green
    '#7cb342',  # Light Green
    '#05723c',  # Dark Green
    '#022915',  # New Color (Enhanced Light Green to Dark Green Transition)
]

# get number of thresholds
thresholds = sorted(list(drought_occurence_portfolio.threshold.unique()))

no_col = len(custom_colors_extended) - len(thresholds)
custom_colors_extended_sel = custom_colors_extended[no_col:]

year_pairs = [[i, i+1] for i in range(1982, 2019)]

regions = drought_occurence_portfolio.region.unique()
regions = ['NO', 'PL', 'PT', 'RO', 'RS', 'SE', 'SI', 'SK', 'UK'] # 'DE',

for r in regions:
    
    # prep data droughts
    drought_occurence_year_pair_col = {}
    
    for yy in year_pairs:
    
        # assign facet_row values
        data = drought_occurence_portfolio.query('year in @yy and region == @r').copy()
        data['year_pair'] = f"{yy[0]}-{yy[1]}"
        
        # assign index values
        for y in yy:
            
            # first year hour 0-8759
            if y == yy[0]:
                for thres in thresholds:
                    data_y = data.query('year == @y and threshold == @thres').copy()
                    data_y['hour_year_pair'] = range(len(data_y))
                    
                    drought_occurence_year_pair_col[f"{yy[0]}-{yy[1]}-({y})-{thres}"] = data_y
            
            # second year hour 8760-17519
            elif y == yy[1]:
                for thres in thresholds:
                    data_y = data.query('year == @y and threshold == @thres').copy()
                    data_y['hour_year_pair'] = range(len(data_y), len(data_y)*2)
                    
                    drought_occurence_year_pair_col[f"{yy[0]}-{yy[1]}-({y})-{thres}"] = data_y
         
    drought_occurence_year_pair_col = pd.concat(drought_occurence_year_pair_col.values())

    fig = px.scatter(
        drought_occurence_year_pair_col,
        x=drought_occurence_year_pair_col.hour_year_pair,
        y=drought_occurence_year_pair_col.drought_indicator,
        facet_row=drought_occurence_year_pair_col.year_pair,
        color=drought_occurence_year_pair_col.threshold,
        color_discrete_sequence=custom_colors_extended_sel,  # qualitative.Prism_r,
        facet_row_spacing=0.01,
    )
    
    # vertical gray line to mark 8760th hour
    total_rows = max(fig._grid_ref)
    for row in range(len(year_pairs)):
        fig.add_shape(
            type="line",
            x0=8759, y0=0, x1=8759, y1=1,  # Adjust y0 and y1 according to your y-axis scale
            line=dict(color="black", width=1.5),
            #xref="x"+str(row),  # Adjusts reference to the correct x-axis based on the row
            yref="paper",  # 'paper' reference makes the line span the entire y-axis height
            row=row, col=1  # Assumes vertical line should be added to every subplot in the first column
        )
    
    # plot identified max period
    for idx, yy in enumerate(year_pairs[::-1]):
        time_frame = f"{yy[0]}-{yy[1]}"
        start = event_max_mass_region_all_years.query('region == @r').loc[time_frame, 'start_hour'] - 30
        end = event_max_mass_region_all_years.query('region == @r').loc[time_frame, 'end_hour'] + 30
        fig.add_vrect(x0=start, x1=end, row=idx+1, col=1,
                      # annotation_text="decline", annotation_position="top left",
                      fillcolor='gray', opacity=0.3, line_width=0)
    
    fig.update_traces(marker=dict(size=2.25))
    
    fig.update_layout(
        xaxis_range=[0,17519],
        yaxis_range=[0.05, 0.95],
        template='simple_white',
        margin=dict(l=0, r=50, t=0, b=0),
        #legend={'itemsizing': 'constant'},
        height=1600, width=1240
    )
    
    fig.update_layout(
        # Other layout properties
        legend=dict(
            title='Relative thresholds:',
            font=dict(size=10),
            title_side='top',
            orientation='h',  # Horizontal orientation
            x=0.5,  # Center the legend horizontally
            y=-0.03,  # Position the legend below the plot area
            xanchor='center',  # Anchor the legend's horizontal center to x position
            yanchor='top',  # Anchor the legend's top to the y position
            itemsizing='constant',
            traceorder='normal',
            #itemwidth=20,
            entrywidthmode='fraction',
            entrywidth=0.11
        )
    )
    
    fig.update_xaxes(tickformat=',')
    fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1], textangle=0))
    fig.update_yaxes(visible=False, showticklabels=False)
    fig.update_xaxes(title='')
    fig.update_xaxes(title_text="hour", row=1, col=1)
    fig.show()
    
    path = path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, 'drought_time_series','single_region_all_years_marked')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_output = os.path.join(path_output, f'{r}_{technology}')
    fig.write_html(f'{path_output}.html')
    fig.write_image(f'{path_output}.pdf')
    #fig.write_image(f'{path_output}.png', scale=3)




# %% heatmap: average drought severity (old metric, old version)

mass_all_events_avg_regions_max_year['max'] = 'maxiumum (threshold)'

formatted_numbers = [f'{num:.2f}' for num in mass_all_events_avg_regions_max_year['avg_threshold']]
mass_all_events_avg_regions_max_year['annotations'] = mass_all_events_avg_regions_max_year['time_frame'].astype(str) \
    + ' (' + formatted_numbers + ')'

order = ['CP', 'AT', 'BE', 'CH', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'IE', 'IT', 'LT', 'LV', 'NL',
         'NO', 'PL', 'PT', 'SE', 'UK']

fig = make_subplots(rows=1, cols=2, shared_yaxes=True, column_widths=[0.82, 0.18], horizontal_spacing=(0.035))

# Create the heatmaps
heatmap1 = go.Heatmap(
    z=mass_all_events_avg_regions_all_years['mass_all_events_avg'],
    x=mass_all_events_avg_regions_all_years['time_frame'],
    y=mass_all_events_avg_regions_all_years['region'],
    coloraxis='coloraxis'
    )
heatmap2 = go.Heatmap(
    z=mass_all_events_avg_regions_max_year['mass_all_events_avg'],
    x=mass_all_events_avg_regions_max_year['max'],
    y=mass_all_events_avg_regions_max_year['region'],
    coloraxis='coloraxis',
    text=mass_all_events_avg_regions_max_year['annotations'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# Add the heatmaps to the subplot
fig.add_trace(heatmap1, row=1, col=1)
fig.add_trace(heatmap2, row=1, col=2)

# Update the layout to set the shared y and x axes
fig.update_layout(
    coloraxis={'colorscale': 'amp'},
    coloraxis_colorbar=dict(outlinewidth=0),
    width=1000,
    height=600,
    template='none',
    margin=dict(l=20, r=130, t=0, b=80)
    )

# Stretch the colorbar to span the entire subplot
colorbar = fig.layout.coloraxis.colorbar
colorbar.update(thickness=30, len=1.05)

# format axes
fig.update_yaxes(categoryorder='array', categoryarray=order[::-1], ticks='', showline=False, row=1, col=1)
fig.update_yaxes(ticks='', showline=False, row=1, col=2)

fig.update_xaxes(ticks='', showline=False, row=1, col=1)
fig.update_xaxes(ticks='', showline=False, row=1, col=2)

# title
fig.add_annotation(
    x=1.9,
    y=0.5,
    text='cumulative drought mass of all events',
    showarrow=False,
    xref='x domain',
    yref="y domain",
    font=dict(size=20),
    textangle=90,
    col=2,
    row=1
)

fig.show()

path = path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'drought_mass_heat_map')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_output = os.path.join(path_output, f'drought_mass_all_events_{technology}')
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')


# %% heatmap: single max drought severity (old metric)

event_max_mass_region_max_year['max'] = 'max period (duration [d])'
event_max_mass_region_max_year['duration_days'] = (event_max_mass_region_max_year['duration'] / 24).round(1)

event_max_mass_region_max_year['annotations'] = event_max_mass_region_max_year['time_frame'].astype(str) + ' (' + \
    event_max_mass_region_max_year['duration_days'].astype(str) + ')'

order = ['CP', 'AL', 'AT', 'BA', 'BE', 'BG', 'CH', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE',
         'IT', 'LT', 'LV', 'ME', 'MK', 'MT', 'NL', 'NO', 'PL', 'PT', 'RO', 'RS', 'SE', 'SI', 'SK', 'UK']

fig = make_subplots(rows=1, cols=2, shared_yaxes=True, column_widths=[0.82, 0.18], horizontal_spacing=(0.035))

# Create the heatmaps
heatmap1 = go.Heatmap(
    z=event_max_mass_region_all_years['mass'],
    x=event_max_mass_region_all_years.index, ## ['time_frame'],
    y=event_max_mass_region_all_years['region'],
    coloraxis='coloraxis'
    )
heatmap2 = go.Heatmap(
    z=event_max_mass_region_max_year['mass'],
    x=event_max_mass_region_max_year['max'],
    y=event_max_mass_region_max_year['region'],
    coloraxis='coloraxis',
    text=event_max_mass_region_max_year['annotations'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# Add the heatmaps to the subplot
fig.add_trace(heatmap1, row=1, col=1)
fig.add_trace(heatmap2, row=1, col=2)

# Update the layout to set the shared y and x axes
fig.update_layout(
    coloraxis={'colorscale': 'amp'},
    coloraxis_colorbar=dict(outlinewidth=0),
    width=1000,
    height=600,
    template='none'
    )

# Add the heatmaps to the subplot
fig.add_trace(heatmap1, row=1, col=1)
fig.add_trace(heatmap2, row=1, col=2)

# Update the layout to set the shared y and x axes
fig.update_layout(
    coloraxis={'colorscale': 'amp'},
    coloraxis_colorbar=dict(outlinewidth=0),
    width=1000,
    height=600,
    template='none',
    margin=dict(l=20, r=130, t=0, b=80)
    )

# Stretch the colorbar to span the entire subplot
colorbar = fig.layout.coloraxis.colorbar
colorbar.update(thickness=30, len=1.05)

# format axes
fig.update_yaxes(categoryorder='array', categoryarray=order[::-1], ticks='', showline=False, row=1, col=1)
fig.update_yaxes(ticks='', showline=False, row=1, col=2)

fig.update_xaxes(ticks='', showline=False, row=1, col=1)
fig.update_xaxes(ticks='', showline=False, row=1, col=2)

# title
fig.add_annotation(
    x=1.9,
    y=0.5,
    text='drought mass of most severe drought event',
    showarrow=False,
    xref='x domain',
    yref="y domain",
    font=dict(size=20),
    textangle=90,
    col=2,
    row=1
)

fig.show()

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'drought_mass_heat_map')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_output = os.path.join(path_output, f'drought_mass_single_events_{technology}')
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')


# %% heatmap: single event max drought mass (cut off): paper: quantifying the dunkelflaute

drought_mass_filter = [1] #, 12, 24, 18, 48]  # 
cut_offs = [0.75]  # 0.65, 0.7,
corrections = ['winter']  # , 'all_year']

for dm_filter in drought_mass_filter:
    for cut_off in cut_offs:
        
        # import data
        path = os.path.join('T:\\mk\\vreda\\output\\2024-08-02_08-42-30_mbt_event', f'drought_mass_filter_{dm_filter}',
                            'drought_mass_s2s', f'cutoff_{cut_off}')
        items = os.listdir(path)
        weighting_funcs = [item for item in items if os.path.isdir(os.path.join(path, item))]
        mass_single_event_max_all_years_all = {}
        for weighting_func in weighting_funcs:
            for correction in corrections:
                mass_single_event_max_all_years_all[weighting_func] = {}
                path_import = os.path.join(path, weighting_func, correction)
                file = 'mass_single_event_max_all_years.gz'
                mass_single_event_max_all_years_all[weighting_func][correction] = load_object(path_import, file)
        
        # plot data
        for weighting_func, mass_single_event_max_all_years_corr in mass_single_event_max_all_years_all.items():
            for correction, mass_single_event_max_all_years in mass_single_event_max_all_years_corr.items():
                mass_single_event_max_all_years['max_duration'] = 'year-pair (duration [d])'
                mass_single_event_max_all_years['max_threshold'] = 'cut-off threshold'
                mass_single_event_max_all_years['duration_days'] = (mass_single_event_max_all_years['duration'] / 24).round(0).astype(int)
                
                mass_single_event_max_all_years['annotations'] = mass_single_event_max_all_years['time_frame'].astype(str) + ' (' + \
                    mass_single_event_max_all_years['duration_days'].astype(str) + ')'
                
                order = ['AL', 'AT', 'BA', 'BE', 'BG', 'CH', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE',
                         'IT', 'LT', 'LV', 'LU', 'ME', 'MK', 'MT', 'NL', 'NO', 'PL', 'PT', 'RO', 'RS', 'SE', 'SI', 'SK', 'UK']
                
                fig = make_subplots(rows=7, cols=2, shared_yaxes=True, shared_xaxes=True,
                                    column_widths=[0.8, 0.2], row_heights=[0.1, 0.4, 0.12, 0.08, 0.12, 0.12, 0.3],
                                    horizontal_spacing=(0.03), vertical_spacing=(0.01))
                
                data = mass_single_event_max_all_years.copy()
                data_normalized = {}
                data_max = {}
                
                for region, group in data.groupby('region'):
                    group_max = group['mass'].max()
                    group['mass'] /= group_max
                    data_normalized[region] = group
                    data_max[region] = group.loc[group['mass'].idxmax()].to_frame().T  # TODO: what if there are two?
                
                data_normalized = pd.concat(data_normalized.values())
                data_max = pd.concat(data_max.values())
                
                data_max_duration = data_max['duration_days'].max()
                data_max['duration_days'] /= data_max_duration
            # =============================================================================
            #     data_max['cut_off_threshold'] = data_max['threshold']
            #     data_max['cut_off_threshold'] = pd.to_numeric(data_max['cut_off_threshold'], errors='coerce')
            #     data_max['cut_off_threshold'] = data_max['cut_off_threshold'].round(2)
            #     data_max['cut_off_threshold_annotation'] = data_max['cut_off_threshold'].apply(lambda x: f"{x:.2f}")
            #     data_max['cut_off_threshold'] = 0.01
            # =============================================================================
                cwe = ['AT','BE', 'CH', 'CZ', 'DE', 'DK', 'FR', 'LU', 'NL', 'PL']
                cwe = cwe[::-1]
                sca = ['FI', 'NO', 'SE']
                sca = sca[::-1]
                nwe = ['IE', 'UK']
                nwe = nwe [::-1]
                bal = ['LT', 'LV', 'EE']
                bal = bal[::-1]
                swe = ['IT', 'ES', 'PT']
                swe = swe[::-1]
                see = ['SI', 'HU', 'SK', 'HR', 'BA', 'RS', 'RO', 'ME', 'AL', 'MK', 'BG', 'GR']
                see = see[::-1]
                
                # CP
                heatmap1 = go.Heatmap(
                    z=data_normalized.query('region == "CP"')['mass'],
                    x=data_normalized.query('region == "CP"')['time_frame'],
                    y=data_normalized.query('region == "CP"')['region'],
                    coloraxis='coloraxis'
                    )
                
                heatmap2 = go.Heatmap(
                    z=data_max.query('region == "CP"')['duration_days'],
                    x=data_max.query('region == "CP"')['max_duration'],
                    y=data_max.query('region == "CP"')['region'],
                    coloraxis='coloraxis',
                    text=data_max.query('region == "CP"')['annotations'],
                    texttemplate="%{text}",
                    textfont={"size": 10}
                    )
                
            # =============================================================================
            #     heatmap3 = go.Heatmap(
            #         z=data_max.query('region == "CP"')['cut_off_threshold'],
            #         x=data_max.query('region == "CP"')['max_threshold'],
            #         y=data_max.query('region == "CP"')['region'],
            #         coloraxis='coloraxis',
            #         text=data_max.query('region == "CP"')['cut_off_threshold_annotation'],
            #         texttemplate="%{text}",
            #         textfont={"size": 10}
            #         )
            # =============================================================================
                
                # CWE
                data_norm = data_normalized.query('region in @cwe')
                data_norm['region'] = pd.Categorical(data_norm['region'], categories=cwe, ordered=True)
                data_norm = data_norm.sort_values('region')
                heatmap4 = go.Heatmap(
                    z=data_norm['mass'],
                    x=data_norm['time_frame'],
                    y=data_norm['region'],
                    coloraxis='coloraxis'
                    )
                
                d_max = data_max.query('region in @cwe')
                d_max['region'] = pd.Categorical(d_max['region'], categories=cwe, ordered=True)
                d_max = d_max.sort_values('region')
                heatmap5 = go.Heatmap(
                    z=d_max['duration_days'],
                    x=d_max['max_duration'],
                    y=d_max['region'],
                    coloraxis='coloraxis',
                    text=d_max['annotations'],
                    texttemplate="%{text}",
                    textfont={"size": 10}
                    )
                
            # =============================================================================
            #     heatmap6 = go.Heatmap(
            #         z=d_max['cut_off_threshold'],
            #         x=d_max['max_threshold'],
            #         y=d_max['region'],
            #         coloraxis='coloraxis',
            #         text=d_max['cut_off_threshold_annotation'],
            #         texttemplate="%{text}",
            #         textfont={"size": 10}
            #         )
            # =============================================================================
                
                # SCA
                data_norm = data_normalized.query('region in @sca')
                data_norm['region'] = pd.Categorical(data_norm['region'], categories=sca, ordered=True)
                data_norm = data_norm.sort_values('region')
                heatmap7 = go.Heatmap(
                    z=data_norm['mass'],
                    x=data_norm['time_frame'],
                    y=data_norm['region'],
                    coloraxis='coloraxis'
                    )
                
                d_max = data_max.query('region in @sca')
                d_max['region'] = pd.Categorical(d_max['region'], categories=sca, ordered=True)
                d_max = d_max.sort_values('region')
                heatmap8 = go.Heatmap(
                    z=d_max['duration_days'],
                    x=d_max['max_duration'],
                    y=d_max['region'],
                    coloraxis='coloraxis',
                    text=d_max['annotations'],
                    texttemplate="%{text}",
                    textfont={"size": 10}
                    )
                
            # =============================================================================
            #     heatmap9 = go.Heatmap(
            #         z=d_max['cut_off_threshold'],
            #         x=d_max['max_threshold'],
            #         y=d_max['region'],
            #         coloraxis='coloraxis',
            #         text=d_max['cut_off_threshold_annotation'],
            #         texttemplate="%{text}",
            #         textfont={"size": 10}
            #         )
            # =============================================================================
                
                # NWE
                data_norm = data_normalized.query('region in @nwe')
                data_norm['region'] = pd.Categorical(data_norm['region'], categories=nwe, ordered=True)
                data_norm = data_norm.sort_values('region')
                heatmap10 = go.Heatmap(
                    z=data_norm['mass'],
                    x=data_norm['time_frame'],
                    y=data_norm['region'],
                    coloraxis='coloraxis'
                    )
                
                d_max = data_max.query('region in @nwe')
                d_max['region'] = pd.Categorical(d_max['region'], categories=nwe, ordered=True)
                d_max = d_max.sort_values('region')
                heatmap11 = go.Heatmap(
                    z=d_max['duration_days'],
                    x=d_max['max_duration'],
                    y=d_max['region'],
                    coloraxis='coloraxis',
                    text=d_max['annotations'],
                    texttemplate="%{text}",
                    textfont={"size": 10}
                    )
                
            # =============================================================================
            #     heatmap12 = go.Heatmap(
            #         z=d_max['cut_off_threshold'],
            #         x=d_max['max_threshold'],
            #         y=d_max['region'],
            #         coloraxis='coloraxis',
            #         text=d_max['cut_off_threshold_annotation'],
            #         texttemplate="%{text}",
            #         textfont={"size": 10}
            #         )
            # =============================================================================
                
                # BAL
                data_norm = data_normalized.query('region in @bal')
                data_norm['region'] = pd.Categorical(data_norm['region'], categories=bal, ordered=True)
                data_norm = data_norm.sort_values('region')
                heatmap13 = go.Heatmap(
                    z=data_norm['mass'],
                    x=data_norm['time_frame'],
                    y=data_norm['region'],
                    coloraxis='coloraxis'
                    )
                
                d_max = data_max.query('region in @bal')
                d_max['region'] = pd.Categorical(d_max['region'], categories=bal, ordered=True)
                d_max = d_max.sort_values('region')
                heatmap14 = go.Heatmap(
                    z=d_max['duration_days'],
                    x=d_max['max_duration'],
                    y=d_max['region'],
                    coloraxis='coloraxis',
                    text=d_max['annotations'],
                    texttemplate="%{text}",
                    textfont={"size": 10}
                    )
                
            # =============================================================================
            #     heatmap15 = go.Heatmap(
            #         z=d_max['cut_off_threshold'],
            #         x=d_max['max_threshold'],
            #         y=d_max['region'],
            #         coloraxis='coloraxis',
            #         text=d_max['cut_off_threshold_annotation'],
            #         texttemplate="%{text}",
            #         textfont={"size": 10}
            #         )
            # =============================================================================
                
                # SWE
                data_norm = data_normalized.query('region in @swe')
                data_norm['region'] = pd.Categorical(data_norm['region'], categories=swe, ordered=True)
                data_norm = data_norm.sort_values('region')
                heatmap16 = go.Heatmap(
                    z=data_norm['mass'],
                    x=data_norm['time_frame'],
                    y=data_norm['region'],
                    coloraxis='coloraxis'
                    )
                
                d_max = data_max.query('region in @swe')
                d_max['region'] = pd.Categorical(d_max['region'], categories=swe, ordered=True)
                d_max = d_max.sort_values('region')
                heatmap17 = go.Heatmap(
                    z=d_max['duration_days'],
                    x=d_max['max_duration'],
                    y=d_max['region'],
                    coloraxis='coloraxis',
                    text=d_max['annotations'],
                    texttemplate="%{text}",
                    textfont={"size": 10}
                    )
                
            # =============================================================================
            #     heatmap18 = go.Heatmap(
            #         z=d_max['cut_off_threshold'],
            #         x=d_max['max_threshold'],
            #         y=d_max['region'],
            #         coloraxis='coloraxis',
            #         text=d_max['cut_off_threshold_annotation'],
            #         texttemplate="%{text}",
            #         textfont={"size": 10}
            #         )
            #     
            # =============================================================================
                # SEE
                data_norm = data_normalized.query('region in @see')
                data_norm['region'] = pd.Categorical(data_norm['region'], categories=see, ordered=True)
                data_norm = data_norm.sort_values('region')
                heatmap19 = go.Heatmap(
                    z=data_norm['mass'],
                    x=data_norm['time_frame'],
                    y=data_norm['region'],
                    coloraxis='coloraxis'
                    )
                
                d_max = data_max.query('region in @see')
                d_max['region'] = pd.Categorical(d_max['region'], categories=see, ordered=True)
                d_max = d_max.sort_values('region')
                heatmap20 = go.Heatmap(
                    z=d_max['duration_days'],
                    x=d_max['max_duration'],
                    y=d_max['region'],
                    coloraxis='coloraxis',
                    text=d_max['annotations'],
                    texttemplate="%{text}",
                    textfont={"size": 10}
                    )
                
            # =============================================================================
            #     heatmap21 = go.Heatmap(
            #         z=d_max['cut_off_threshold'],
            #         x=d_max['max_threshold'],
            #         y=d_max['region'],
            #         coloraxis='coloraxis',
            #         text=d_max['cut_off_threshold_annotation'],
            #         texttemplate="%{text}",
            #         textfont={"size": 10}
            #         )
            # =============================================================================
                
                
                # Add the heatmaps to the subplot
                fig.add_trace(heatmap1, row=1, col=1)
                fig.add_trace(heatmap2, row=1, col=2)
                #fig.add_trace(heatmap3, row=1, col=3)
                fig.add_trace(heatmap4, row=2, col=1)
                fig.add_trace(heatmap5, row=2, col=2)
                #fig.add_trace(heatmap6, row=2, col=3)
                fig.add_trace(heatmap7, row=3, col=1)
                fig.add_trace(heatmap8, row=3, col=2)
                #fig.add_trace(heatmap9, row=3, col=3)
                fig.add_trace(heatmap10, row=4, col=1)
                fig.add_trace(heatmap11, row=4, col=2)
                #fig.add_trace(heatmap12, row=4, col=3)
                fig.add_trace(heatmap13, row=5, col=1)
                fig.add_trace(heatmap14, row=5, col=2)
                #fig.add_trace(heatmap15, row=5, col=3)
                fig.add_trace(heatmap16, row=6, col=1)
                fig.add_trace(heatmap17, row=6, col=2)
                #fig.add_trace(heatmap18, row=6, col=3)
                fig.add_trace(heatmap19, row=7, col=1)
                fig.add_trace(heatmap20, row=7, col=2)
                #fig.add_trace(heatmap21, row=7, col=3)
                
                # Update the layout to set the shared y and x axes
                fig.update_layout(
                    coloraxis={'colorscale': 'amp'},
                    coloraxis_colorbar=dict(outlinewidth=0),
                    width=1000,
                    height=800,
                    template='none',
                    margin=dict(l=20, r=130, t=0, b=80)
                    )
                
                # Stretch the colorbar to span the entire subplot
                colorbar = fig.layout.coloraxis.colorbar
                colorbar.update(thickness=30, len=1.03)
                
                # format axes
                fig.update_yaxes(ticks='', showline=False, row=1, col=1)
                fig.update_yaxes(ticks='', showline=False, row=1, col=2)
                #fig.update_yaxes(ticks='', showline=False, row=1, col=3)
                fig.update_yaxes(ticks='', showline=False, row=2, col=1)
                fig.update_yaxes(ticks='', showline=False, row=2, col=2)
                #fig.update_yaxes(ticks='', showline=False, row=2, col=3)
                fig.update_yaxes(ticks='', showline=False, row=3, col=1)
                fig.update_yaxes(ticks='', showline=False, row=3, col=2)
                #fig.update_yaxes(ticks='', showline=False, row=3, col=3)
                fig.update_yaxes(ticks='', showline=False, row=4, col=1)
                fig.update_yaxes(ticks='', showline=False, row=4, col=2)
                #fig.update_yaxes(ticks='', showline=False, row=4, col=3)
                fig.update_yaxes(ticks='', showline=False, row=5, col=1)
                fig.update_yaxes(ticks='', showline=False, row=5, col=2)
                #fig.update_yaxes(ticks='', showline=False, row=5, col=3)
                fig.update_yaxes(ticks='', showline=False, row=6, col=1)
                fig.update_yaxes(ticks='', showline=False, row=6, col=2)
                #fig.update_yaxes(ticks='', showline=False, row=6, col=3)
                fig.update_yaxes(ticks='', showline=False, row=7, col=1, tickfont=dict(size=8))
                fig.update_yaxes(ticks='', showline=False, row=7, col=2)
                #fig.update_yaxes(ticks='', showline=False, row=7, col=3)
                
                fig.update_xaxes(ticks='', showline=False, row=1, col=1)
                fig.update_xaxes(ticks='', showline=False, row=1, col=2)
                #fig.update_xaxes(ticks='', showline=False, row=1, col=3)
                fig.update_xaxes(ticks='', showline=False, row=2, col=1)
                fig.update_xaxes(ticks='', showline=False, row=2, col=2)
                #fig.update_xaxes(ticks='', showline=False, row=2, col=3)
                fig.update_xaxes(ticks='', showline=False, row=3, col=1)
                fig.update_xaxes(ticks='', showline=False, row=3, col=2)
                #fig.update_xaxes(ticks='', showline=False, row=3, col=3)
                fig.update_xaxes(ticks='', showline=False, row=4, col=1)
                fig.update_xaxes(ticks='', showline=False, row=4, col=2)
                #fig.update_xaxes(ticks='', showline=False, row=4, col=3)
                fig.update_xaxes(ticks='', showline=False, row=5, col=1)
                fig.update_xaxes(ticks='', showline=False, row=5, col=2)
                #fig.update_xaxes(ticks='', showline=False, row=5, col=3)
                fig.update_xaxes(ticks='', showline=False, row=6, col=1)
                fig.update_xaxes(ticks='', showline=False, row=6, col=2)
                #fig.update_xaxes(ticks='', showline=False, row=6, col=3)
                fig.update_xaxes(ticks='', showline=False, row=7, col=1)
                fig.update_xaxes(ticks='', showline=False, row=7, col=2)
                #fig.update_xaxes(ticks='', showline=False, row=7, col=3)
                
                # title
                fig.add_annotation(
                    x=1.7,
                    y=0.5,
                    text='normalized drought mass / duration',
                    showarrow=False,
                    xref='x domain',
                    yref="y domain",
                    font=dict(size=20),
                    textangle=90,
                    col=2,
                    row=4
                )
                
                fig.show()
                
                path = 'T:\\mk\\vreda\\result_plots\\'
                path_output = os.path.join(path, 'drought_mass_heat_map', f'drought_mass_filter_{dm_filter}',
                                           f'drought_mass_cut_off_{cut_off}')
                Path(path_output).mkdir(parents=True, exist_ok=True)
                path_output = os.path.join(path_output, f'{weighting_func}_{correction}')
                fig.write_html(f'{path_output}.html')
                fig.write_image(f'{path_output}.pdf')
                fig.write_image(f'{path_output}.png', scale=3)
                
# %% heatmap: single event max drought mass (cut off): paper: coping with the dunkelflaute

dm_filter = 1 #, 12, 24, 18, 48]  # 
corrections = ['s2s-winter', 's2s-yearly']

# import data
path = os.path.join('T:\\mk\\vreda\\output\\2024-08-02_08-42-30_mbt_event', f'drought_mass_filter_{dm_filter}',
                    'drought_mass_s2s')
mass_single_event_max_all_years_corrections = {}
for correction in corrections:
    mass_single_event_max_all_years_corrections[correction] = {}
    path_import = os.path.join(path, correction)
    file = 'mass_single_event_max_all_years.gz'
    mass_single_event_max_all_years_corrections[correction] = load_object(path_import, file)

# plot data
for correction, mass_single_event_max_all_years in mass_single_event_max_all_years_corrections.items():
    mass_single_event_max_all_years['max_duration'] = 'year-pair (duration [d])'
    mass_single_event_max_all_years['max_threshold'] = 'cut-off threshold'
    mass_single_event_max_all_years['duration_days'] = (mass_single_event_max_all_years['duration'] / 24).round(0).astype(int)
    
    mass_single_event_max_all_years['annotations'] = mass_single_event_max_all_years['time_frame'].astype(str) + ' (' + \
        mass_single_event_max_all_years['duration_days'].astype(str) + ')'
    
    order = ['AL', 'AT', 'BA', 'BE', 'BG', 'CH', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE',
             'IT', 'LT', 'LV', 'LU', 'ME', 'MK', 'MT', 'NL', 'NO', 'PL', 'PT', 'RO', 'RS', 'SE', 'SI', 'SK', 'UK']
    
    fig = make_subplots(rows=7, cols=2, shared_yaxes=True, shared_xaxes=True,
                        column_widths=[0.8, 0.2], row_heights=[0.1, 0.4, 0.12, 0.08, 0.12, 0.12, 0.3],
                        horizontal_spacing=(0.03), vertical_spacing=(0.01))
    
    data = mass_single_event_max_all_years.copy()
    data_normalized = {}
    data_max = {}
    
    for region, group in data.groupby('region'):
        group_max = group['mass'].max()
        group['mass'] /= group_max
        data_normalized[region] = group
        data_max[region] = group.loc[group['mass'].idxmax()].to_frame().T  # TODO: what if there are two?
    
    data_normalized = pd.concat(data_normalized.values())
    data_max = pd.concat(data_max.values())
    
    data_max_duration = data_max['duration_days'].max()
    data_max['duration_days'] /= data_max_duration
# =============================================================================
#     data_max['cut_off_threshold'] = data_max['threshold']
#     data_max['cut_off_threshold'] = pd.to_numeric(data_max['cut_off_threshold'], errors='coerce')
#     data_max['cut_off_threshold'] = data_max['cut_off_threshold'].round(2)
#     data_max['cut_off_threshold_annotation'] = data_max['cut_off_threshold'].apply(lambda x: f"{x:.2f}")
#     data_max['cut_off_threshold'] = 0.01
# =============================================================================
    cwe = ['AT','BE', 'CH', 'CZ', 'DE', 'DK', 'FR', 'LU', 'NL', 'PL']
    cwe = cwe[::-1]
    sca = ['FI', 'NO', 'SE']
    sca = sca[::-1]
    nwe = ['IE', 'UK']
    nwe = nwe [::-1]
    bal = ['LT', 'LV', 'EE']
    bal = bal[::-1]
    swe = ['IT', 'ES', 'PT']
    swe = swe[::-1]
    see = ['SI', 'HU', 'SK', 'HR', 'BA', 'RS', 'RO', 'ME', 'AL', 'MK', 'BG', 'GR']
    see = see[::-1]
    
    # CP
    heatmap1 = go.Heatmap(
        z=data_normalized.query('region == "CP"')['mass'],
        x=data_normalized.query('region == "CP"')['time_frame'],
        y=data_normalized.query('region == "CP"')['region'],
        coloraxis='coloraxis'
        )
    
    heatmap2 = go.Heatmap(
        z=data_max.query('region == "CP"')['duration_days'],
        x=data_max.query('region == "CP"')['max_duration'],
        y=data_max.query('region == "CP"')['region'],
        coloraxis='coloraxis',
        text=data_max.query('region == "CP"')['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
# =============================================================================
#     heatmap3 = go.Heatmap(
#         z=data_max.query('region == "CP"')['cut_off_threshold'],
#         x=data_max.query('region == "CP"')['max_threshold'],
#         y=data_max.query('region == "CP"')['region'],
#         coloraxis='coloraxis',
#         text=data_max.query('region == "CP"')['cut_off_threshold_annotation'],
#         texttemplate="%{text}",
#         textfont={"size": 10}
#         )
# =============================================================================
    
    # CWE
    data_norm = data_normalized.query('region in @cwe')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=cwe, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap4 = go.Heatmap(
        z=data_norm['mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @cwe')
    d_max['region'] = pd.Categorical(d_max['region'], categories=cwe, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap5 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
# =============================================================================
#     heatmap6 = go.Heatmap(
#         z=d_max['cut_off_threshold'],
#         x=d_max['max_threshold'],
#         y=d_max['region'],
#         coloraxis='coloraxis',
#         text=d_max['cut_off_threshold_annotation'],
#         texttemplate="%{text}",
#         textfont={"size": 10}
#         )
# =============================================================================
    
    # SCA
    data_norm = data_normalized.query('region in @sca')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=sca, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap7 = go.Heatmap(
        z=data_norm['mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @sca')
    d_max['region'] = pd.Categorical(d_max['region'], categories=sca, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap8 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
# =============================================================================
#     heatmap9 = go.Heatmap(
#         z=d_max['cut_off_threshold'],
#         x=d_max['max_threshold'],
#         y=d_max['region'],
#         coloraxis='coloraxis',
#         text=d_max['cut_off_threshold_annotation'],
#         texttemplate="%{text}",
#         textfont={"size": 10}
#         )
# =============================================================================
    
    # NWE
    data_norm = data_normalized.query('region in @nwe')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=nwe, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap10 = go.Heatmap(
        z=data_norm['mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @nwe')
    d_max['region'] = pd.Categorical(d_max['region'], categories=nwe, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap11 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
# =============================================================================
#     heatmap12 = go.Heatmap(
#         z=d_max['cut_off_threshold'],
#         x=d_max['max_threshold'],
#         y=d_max['region'],
#         coloraxis='coloraxis',
#         text=d_max['cut_off_threshold_annotation'],
#         texttemplate="%{text}",
#         textfont={"size": 10}
#         )
# =============================================================================
    
    # BAL
    data_norm = data_normalized.query('region in @bal')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=bal, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap13 = go.Heatmap(
        z=data_norm['mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @bal')
    d_max['region'] = pd.Categorical(d_max['region'], categories=bal, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap14 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
# =============================================================================
#     heatmap15 = go.Heatmap(
#         z=d_max['cut_off_threshold'],
#         x=d_max['max_threshold'],
#         y=d_max['region'],
#         coloraxis='coloraxis',
#         text=d_max['cut_off_threshold_annotation'],
#         texttemplate="%{text}",
#         textfont={"size": 10}
#         )
# =============================================================================
    
    # SWE
    data_norm = data_normalized.query('region in @swe')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=swe, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap16 = go.Heatmap(
        z=data_norm['mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @swe')
    d_max['region'] = pd.Categorical(d_max['region'], categories=swe, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap17 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
# =============================================================================
#     heatmap18 = go.Heatmap(
#         z=d_max['cut_off_threshold'],
#         x=d_max['max_threshold'],
#         y=d_max['region'],
#         coloraxis='coloraxis',
#         text=d_max['cut_off_threshold_annotation'],
#         texttemplate="%{text}",
#         textfont={"size": 10}
#         )
#     
# =============================================================================
    # SEE
    data_norm = data_normalized.query('region in @see')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=see, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap19 = go.Heatmap(
        z=data_norm['mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @see')
    d_max['region'] = pd.Categorical(d_max['region'], categories=see, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap20 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
# =============================================================================
#     heatmap21 = go.Heatmap(
#         z=d_max['cut_off_threshold'],
#         x=d_max['max_threshold'],
#         y=d_max['region'],
#         coloraxis='coloraxis',
#         text=d_max['cut_off_threshold_annotation'],
#         texttemplate="%{text}",
#         textfont={"size": 10}
#         )
# =============================================================================
    
    
    # Add the heatmaps to the subplot
    fig.add_trace(heatmap1, row=1, col=1)
    fig.add_trace(heatmap2, row=1, col=2)
    #fig.add_trace(heatmap3, row=1, col=3)
    fig.add_trace(heatmap4, row=2, col=1)
    fig.add_trace(heatmap5, row=2, col=2)
    #fig.add_trace(heatmap6, row=2, col=3)
    fig.add_trace(heatmap7, row=3, col=1)
    fig.add_trace(heatmap8, row=3, col=2)
    #fig.add_trace(heatmap9, row=3, col=3)
    fig.add_trace(heatmap10, row=4, col=1)
    fig.add_trace(heatmap11, row=4, col=2)
    #fig.add_trace(heatmap12, row=4, col=3)
    fig.add_trace(heatmap13, row=5, col=1)
    fig.add_trace(heatmap14, row=5, col=2)
    #fig.add_trace(heatmap15, row=5, col=3)
    fig.add_trace(heatmap16, row=6, col=1)
    fig.add_trace(heatmap17, row=6, col=2)
    #fig.add_trace(heatmap18, row=6, col=3)
    fig.add_trace(heatmap19, row=7, col=1)
    fig.add_trace(heatmap20, row=7, col=2)
    #fig.add_trace(heatmap21, row=7, col=3)
    
    # Update the layout to set the shared y and x axes
    fig.update_layout(
        coloraxis={'colorscale': 'amp'},
        coloraxis_colorbar=dict(outlinewidth=0),
        width=1000,
        height=800,
        template='none',
        margin=dict(l=20, r=130, t=0, b=80)
        )
    
    # Stretch the colorbar to span the entire subplot
    colorbar = fig.layout.coloraxis.colorbar
    colorbar.update(thickness=30, len=1.03)
    
    # format axes
    fig.update_yaxes(ticks='', showline=False, row=1, col=1)
    fig.update_yaxes(ticks='', showline=False, row=1, col=2)
    #fig.update_yaxes(ticks='', showline=False, row=1, col=3)
    fig.update_yaxes(ticks='', showline=False, row=2, col=1)
    fig.update_yaxes(ticks='', showline=False, row=2, col=2)
    #fig.update_yaxes(ticks='', showline=False, row=2, col=3)
    fig.update_yaxes(ticks='', showline=False, row=3, col=1)
    fig.update_yaxes(ticks='', showline=False, row=3, col=2)
    #fig.update_yaxes(ticks='', showline=False, row=3, col=3)
    fig.update_yaxes(ticks='', showline=False, row=4, col=1)
    fig.update_yaxes(ticks='', showline=False, row=4, col=2)
    #fig.update_yaxes(ticks='', showline=False, row=4, col=3)
    fig.update_yaxes(ticks='', showline=False, row=5, col=1)
    fig.update_yaxes(ticks='', showline=False, row=5, col=2)
    #fig.update_yaxes(ticks='', showline=False, row=5, col=3)
    fig.update_yaxes(ticks='', showline=False, row=6, col=1)
    fig.update_yaxes(ticks='', showline=False, row=6, col=2)
    #fig.update_yaxes(ticks='', showline=False, row=6, col=3)
    fig.update_yaxes(ticks='', showline=False, row=7, col=1, tickfont=dict(size=8))
    fig.update_yaxes(ticks='', showline=False, row=7, col=2)
    #fig.update_yaxes(ticks='', showline=False, row=7, col=3)
    
    fig.update_xaxes(ticks='', showline=False, row=1, col=1)
    fig.update_xaxes(ticks='', showline=False, row=1, col=2)
    #fig.update_xaxes(ticks='', showline=False, row=1, col=3)
    fig.update_xaxes(ticks='', showline=False, row=2, col=1)
    fig.update_xaxes(ticks='', showline=False, row=2, col=2)
    #fig.update_xaxes(ticks='', showline=False, row=2, col=3)
    fig.update_xaxes(ticks='', showline=False, row=3, col=1)
    fig.update_xaxes(ticks='', showline=False, row=3, col=2)
    #fig.update_xaxes(ticks='', showline=False, row=3, col=3)
    fig.update_xaxes(ticks='', showline=False, row=4, col=1)
    fig.update_xaxes(ticks='', showline=False, row=4, col=2)
    #fig.update_xaxes(ticks='', showline=False, row=4, col=3)
    fig.update_xaxes(ticks='', showline=False, row=5, col=1)
    fig.update_xaxes(ticks='', showline=False, row=5, col=2)
    #fig.update_xaxes(ticks='', showline=False, row=5, col=3)
    fig.update_xaxes(ticks='', showline=False, row=6, col=1)
    fig.update_xaxes(ticks='', showline=False, row=6, col=2)
    #fig.update_xaxes(ticks='', showline=False, row=6, col=3)
    fig.update_xaxes(ticks='', showline=False, row=7, col=1)
    fig.update_xaxes(ticks='', showline=False, row=7, col=2)
    #fig.update_xaxes(ticks='', showline=False, row=7, col=3)
    
    # title
    fig.add_annotation(
        x=1.7,
        y=0.5,
        text='normalized drought mass / duration',
        showarrow=False,
        xref='x domain',
        yref="y domain",
        font=dict(size=20),
        textangle=90,
        col=2,
        row=4
    )
    
    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, 'drought_mass_heat_map', f'drought_mass_filter_{dm_filter}_s2s')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_output = os.path.join(path_output, f'{correction}')
    fig.write_html(f'{path_output}.html')
    fig.write_image(f'{path_output}.pdf')
    fig.write_image(f'{path_output}.png', scale=3)

    
# %% heatmap: single event max drought mass (marginal max) - results looks shitty :( 

# import data
path = 'T:\\mk\\vreda\\output\\2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete\\drought_mass_marginal_max'
items = os.listdir(path)
weighting_funcs = [item for item in items if os.path.isdir(os.path.join(path, item))]
mass_single_events_max_avg_mass_per_time_frame_all = {}
for weighting_func in weighting_funcs:
    path_import = os.path.join(path, weighting_func)
    file = 'mass_single_events_max_avg_mass_per_time_frame.gz'
    mass_single_events_max_avg_mass_per_time_frame_all[weighting_func] = load_object(path_import, file)

# plot data
for weighting_func, mass_single_events_max_avg_mass_per_time_frame in mass_single_events_max_avg_mass_per_time_frame_all.items():
    mass_single_events_max_avg_mass_per_time_frame['max_duration'] = 'timeframe (duration [d])'
    mass_single_events_max_avg_mass_per_time_frame['max_threshold'] = 'cut-off threshold'
    mass_single_events_max_avg_mass_per_time_frame['duration_days'] = (mass_single_events_max_avg_mass_per_time_frame['duration'] / 24).round(1)
    
    mass_single_events_max_avg_mass_per_time_frame['annotations'] = mass_single_events_max_avg_mass_per_time_frame['time_frame'].astype(str) + ' (' + \
        mass_single_events_max_avg_mass_per_time_frame['duration_days'].astype(str) + ')'
    
    order = ['AL', 'AT', 'BA', 'BE', 'BG', 'CH', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE',
             'IT', 'LT', 'LV', 'LU', 'ME', 'MK', 'MT', 'NL', 'NO', 'PL', 'PT', 'RO', 'RS', 'SE', 'SI', 'SK', 'UK']
    
    fig = make_subplots(rows=7, cols=3, shared_yaxes=True, shared_xaxes=True,
                        column_widths=[0.7, 0.18, 0.07], row_heights=[0.1, 0.4, 0.12, 0.08, 0.12, 0.12, 0.3],
                        horizontal_spacing=(0.03), vertical_spacing=(0.01))
    
    data = mass_single_events_max_avg_mass_per_time_frame.copy()
    data_normalized = {}
    data_max = {}
    
    for region, group in data.groupby('region'):
        group_max = group['avg_mass'].max()
        group['avg_mass'] /= group_max
        data_normalized[region] = group
        data_max[region] = group.loc[group['avg_mass'].idxmax()].to_frame().T  # TODO: what if there are two?
    
    data_normalized = pd.concat(data_normalized.values())
    data_max = pd.concat(data_max.values())
    
    data_max_duration = data_max['duration_days'].max()
    data_max['duration_days'] /= data_max_duration
    data_max['cut_off_threshold'] = data_max['threshold']
    data_max['cut_off_threshold'] = pd.to_numeric(data_max['cut_off_threshold'], errors='coerce')
    data_max['cut_off_threshold'] = data_max['cut_off_threshold'].round(2)
    data_max['cut_off_threshold_annotation'] = data_max['cut_off_threshold'].apply(lambda x: f"{x:.2f}")
    data_max['cut_off_threshold'] = 0.01
    
    cwe = ['AT','BE', 'CH', 'CZ', 'DE', 'DK', 'FR', 'LU', 'NL', 'PL']
    cwe = cwe[::-1]
    sca = ['FI', 'NO', 'SE']
    sca = sca[::-1]
    nwe = ['IE', 'UK']
    nwe = nwe [::-1]
    bal = ['LT', 'LV', 'EE']
    bal = bal[::-1]
    swe = ['IT', 'ES', 'PT']
    swe = swe[::-1]
    see = ['SI', 'HU', 'SK', 'HR', 'BA', 'RS', 'RO', 'ME', 'AL', 'MK', 'BG', 'GR']
    see = see[::-1]
    
    # CP
    heatmap1 = go.Heatmap(
        z=data_normalized.query('region == "CP"')['avg_mass'],
        x=data_normalized.query('region == "CP"')['time_frame'],
        y=data_normalized.query('region == "CP"')['region'],
        coloraxis='coloraxis'
        )
    
    heatmap2 = go.Heatmap(
        z=data_max.query('region == "CP"')['duration_days'],
        x=data_max.query('region == "CP"')['max_duration'],
        y=data_max.query('region == "CP"')['region'],
        coloraxis='coloraxis',
        text=data_max.query('region == "CP"')['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    heatmap3 = go.Heatmap(
        z=data_max.query('region == "CP"')['cut_off_threshold'],
        x=data_max.query('region == "CP"')['max_threshold'],
        y=data_max.query('region == "CP"')['region'],
        coloraxis='coloraxis',
        text=data_max.query('region == "CP"')['cut_off_threshold_annotation'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    # CWE
    data_norm = data_normalized.query('region in @cwe')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=cwe, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap4 = go.Heatmap(
        z=data_norm['avg_mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @cwe')
    d_max['region'] = pd.Categorical(d_max['region'], categories=cwe, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap5 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    heatmap6 = go.Heatmap(
        z=d_max['cut_off_threshold'],
        x=d_max['max_threshold'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['cut_off_threshold_annotation'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    # SCA
    data_norm = data_normalized.query('region in @sca')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=sca, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap7 = go.Heatmap(
        z=data_norm['avg_mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @sca')
    d_max['region'] = pd.Categorical(d_max['region'], categories=sca, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap8 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    heatmap9 = go.Heatmap(
        z=d_max['cut_off_threshold'],
        x=d_max['max_threshold'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['cut_off_threshold_annotation'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    # NWE
    data_norm = data_normalized.query('region in @nwe')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=nwe, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap10 = go.Heatmap(
        z=data_norm['avg_mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @nwe')
    d_max['region'] = pd.Categorical(d_max['region'], categories=nwe, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap11 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    heatmap12 = go.Heatmap(
        z=d_max['cut_off_threshold'],
        x=d_max['max_threshold'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['cut_off_threshold_annotation'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    # BAL
    data_norm = data_normalized.query('region in @bal')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=bal, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap13 = go.Heatmap(
        z=data_norm['avg_mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @bal')
    d_max['region'] = pd.Categorical(d_max['region'], categories=bal, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap14 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    heatmap15 = go.Heatmap(
        z=d_max['cut_off_threshold'],
        x=d_max['max_threshold'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['cut_off_threshold_annotation'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    # SWE
    data_norm = data_normalized.query('region in @swe')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=swe, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap16 = go.Heatmap(
        z=data_norm['avg_mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @swe')
    d_max['region'] = pd.Categorical(d_max['region'], categories=swe, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap17 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    heatmap18 = go.Heatmap(
        z=d_max['cut_off_threshold'],
        x=d_max['max_threshold'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['cut_off_threshold_annotation'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    # SEE
    data_norm = data_normalized.query('region in @see')
    data_norm['region'] = pd.Categorical(data_norm['region'], categories=see, ordered=True)
    data_norm = data_norm.sort_values('region')
    heatmap19 = go.Heatmap(
        z=data_norm['avg_mass'],
        x=data_norm['time_frame'],
        y=data_norm['region'],
        coloraxis='coloraxis'
        )
    
    d_max = data_max.query('region in @see')
    d_max['region'] = pd.Categorical(d_max['region'], categories=see, ordered=True)
    d_max = d_max.sort_values('region')
    heatmap20 = go.Heatmap(
        z=d_max['duration_days'],
        x=d_max['max_duration'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['annotations'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    heatmap21 = go.Heatmap(
        z=d_max['cut_off_threshold'],
        x=d_max['max_threshold'],
        y=d_max['region'],
        coloraxis='coloraxis',
        text=d_max['cut_off_threshold_annotation'],
        texttemplate="%{text}",
        textfont={"size": 10}
        )
    
    
    # Add the heatmaps to the subplot
    fig.add_trace(heatmap1, row=1, col=1)
    fig.add_trace(heatmap2, row=1, col=2)
    fig.add_trace(heatmap3, row=1, col=3)
    fig.add_trace(heatmap4, row=2, col=1)
    fig.add_trace(heatmap5, row=2, col=2)
    fig.add_trace(heatmap6, row=2, col=3)
    fig.add_trace(heatmap7, row=3, col=1)
    fig.add_trace(heatmap8, row=3, col=2)
    fig.add_trace(heatmap9, row=3, col=3)
    fig.add_trace(heatmap10, row=4, col=1)
    fig.add_trace(heatmap11, row=4, col=2)
    fig.add_trace(heatmap12, row=4, col=3)
    fig.add_trace(heatmap13, row=5, col=1)
    fig.add_trace(heatmap14, row=5, col=2)
    fig.add_trace(heatmap15, row=5, col=3)
    fig.add_trace(heatmap16, row=6, col=1)
    fig.add_trace(heatmap17, row=6, col=2)
    fig.add_trace(heatmap18, row=6, col=3)
    fig.add_trace(heatmap19, row=7, col=1)
    fig.add_trace(heatmap20, row=7, col=2)
    fig.add_trace(heatmap21, row=7, col=3)
    
    # Update the layout to set the shared y and x axes
    fig.update_layout(
        coloraxis={'colorscale': 'amp'},
        coloraxis_colorbar=dict(outlinewidth=0),
        width=1000,
        height=800,
        template='none',
        margin=dict(l=20, r=130, t=0, b=80)
        )
    
    # Stretch the colorbar to span the entire subplot
    colorbar = fig.layout.coloraxis.colorbar
    colorbar.update(thickness=30, len=1.03)
    
    # format axes
    fig.update_yaxes(ticks='', showline=False, row=1, col=1)
    fig.update_yaxes(ticks='', showline=False, row=1, col=2)
    fig.update_yaxes(ticks='', showline=False, row=1, col=3)
    fig.update_yaxes(ticks='', showline=False, row=2, col=1)
    fig.update_yaxes(ticks='', showline=False, row=2, col=2)
    fig.update_yaxes(ticks='', showline=False, row=2, col=3)
    fig.update_yaxes(ticks='', showline=False, row=3, col=1)
    fig.update_yaxes(ticks='', showline=False, row=3, col=2)
    fig.update_yaxes(ticks='', showline=False, row=3, col=3)
    fig.update_yaxes(ticks='', showline=False, row=4, col=1)
    fig.update_yaxes(ticks='', showline=False, row=4, col=2)
    fig.update_yaxes(ticks='', showline=False, row=4, col=3)
    fig.update_yaxes(ticks='', showline=False, row=5, col=1)
    fig.update_yaxes(ticks='', showline=False, row=5, col=2)
    fig.update_yaxes(ticks='', showline=False, row=5, col=3)
    fig.update_yaxes(ticks='', showline=False, row=6, col=1)
    fig.update_yaxes(ticks='', showline=False, row=6, col=2)
    fig.update_yaxes(ticks='', showline=False, row=6, col=3)
    fig.update_yaxes(ticks='', showline=False, row=7, col=1, tickfont=dict(size=8))
    fig.update_yaxes(ticks='', showline=False, row=7, col=2)
    fig.update_yaxes(ticks='', showline=False, row=7, col=3)
    
    fig.update_xaxes(ticks='', showline=False, row=1, col=1)
    fig.update_xaxes(ticks='', showline=False, row=1, col=2)
    fig.update_xaxes(ticks='', showline=False, row=1, col=3)
    fig.update_xaxes(ticks='', showline=False, row=2, col=1)
    fig.update_xaxes(ticks='', showline=False, row=2, col=2)
    fig.update_xaxes(ticks='', showline=False, row=2, col=3)
    fig.update_xaxes(ticks='', showline=False, row=3, col=1)
    fig.update_xaxes(ticks='', showline=False, row=3, col=2)
    fig.update_xaxes(ticks='', showline=False, row=3, col=3)
    fig.update_xaxes(ticks='', showline=False, row=4, col=1)
    fig.update_xaxes(ticks='', showline=False, row=4, col=2)
    fig.update_xaxes(ticks='', showline=False, row=4, col=3)
    fig.update_xaxes(ticks='', showline=False, row=5, col=1)
    fig.update_xaxes(ticks='', showline=False, row=5, col=2)
    fig.update_xaxes(ticks='', showline=False, row=5, col=3)
    fig.update_xaxes(ticks='', showline=False, row=6, col=1)
    fig.update_xaxes(ticks='', showline=False, row=6, col=2)
    fig.update_xaxes(ticks='', showline=False, row=6, col=3)
    fig.update_xaxes(ticks='', showline=False, row=7, col=1)
    fig.update_xaxes(ticks='', showline=False, row=7, col=2)
    fig.update_xaxes(ticks='', showline=False, row=7, col=3)
    
    # title
    fig.add_annotation(
        x=2.9,
        y=0.5,
        text='normalized drought mass / duration',
        showarrow=False,
        xref='x domain',
        yref="y domain",
        font=dict(size=20),
        textangle=90,
        col=3,
        row=4
    )
    
    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, 'drought_mass_heat_map', 'marginal_max')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_output = os.path.join(path_output, f'{weighting_func}')
    fig.write_html(f'{path_output}.html')
    fig.write_image(f'{path_output}.pdf')


# %% heatmap: storage energy with E2P -> Paper Quantifying the Dunkelflaute

path = "S:\\projekte_paper\\vre-drought-mass\\result_data\\h2_n_sto_e.csv"
h2_n_sto_e = pd.read_csv(path)
h2_n_sto_e = h2_n_sto_e.rename(columns={'value': 'energy'})
h2_n_sto_e = h2_n_sto_e.query('custom_year != 2016')

path = "S:\\projekte_paper\\vre-drought-mass\\result_data\\h2_n_recon.csv"
h2_n_recon = pd.read_csv(path)
h2_n_recon = h2_n_recon.rename(columns={'value': 'capacity'})
h2_n_recon = h2_n_recon.query('custom_year != 2016')

# merge data
h2_n_sto_e = pd.merge(h2_n_sto_e, h2_n_recon, on=['n', 'custom_flow', 'custom_year'], how='outer')
h2_n_sto_e = h2_n_sto_e.drop(columns=['tech_x', 'tech_y'])

# compute E2P
h2_n_sto_e['E2P'] = (h2_n_sto_e['energy'] * 1e3) / h2_n_sto_e['capacity']

# prepare investment data
h2_n_sto_e['end_year'] = h2_n_sto_e['custom_year'] + 1
h2_n_sto_e['time_frame'] = h2_n_sto_e.apply(lambda row: f"{row['custom_year']}-{row['end_year']}", axis=1)
h2_n_sto_e['annotation'] = h2_n_sto_e['time_frame'].astype(str) + ' (' + (h2_n_sto_e['E2P'] / 24).round(1).astype(str) + ')'
h2_n_sto_e['max_E2P'] = 'timeframe (E2P [d])'

# get cumulative European copperplate value
temp = {}
for (custom_flow, time_frame), df in h2_n_sto_e.groupby(['custom_flow', 'time_frame']):
    df = df.reset_index(drop=True)
    df_CP = pd.DataFrame(columns=df.columns, index=[0])
    df_CP.loc[0] = ['CP', df['energy'].sum(), custom_flow, df.loc[0]['custom_year'], df['capacity'].sum(), 'E2P', df.loc[0]['end_year'], df.loc[0]['time_frame'], 'annotation', df.loc[0]['max_E2P']]
    df_CP['E2P'] = (df_CP['energy'] * 1e3) / df_CP['capacity']
    df_CP['annotation'] = df_CP.loc[0]['time_frame'] + ' (' + (df_CP.loc[0]['E2P'] / 24).round(1).astype(str) + ')'
    df = pd.concat([df, df_CP]).reset_index(drop=True)
    temp[(custom_flow, time_frame)] = df
h2_n_sto_e = pd.concat(temp.values())
h2_n_sto_e['energy'] = pd.to_numeric(h2_n_sto_e['energy'], errors='coerce')
h2_n_sto_e = h2_n_sto_e.reset_index(drop=True)

# get max value
h2_n_sto_e_max = {}
for (region, custom_flow), df in h2_n_sto_e.groupby(['n', 'custom_flow']):    
    df = df.copy().reset_index(drop=True)
    h2_n_sto_e_max[(region, custom_flow)] = df.loc[df['energy'].idxmax()]
h2_n_sto_e_max = pd.DataFrame.from_dict(h2_n_sto_e_max, orient='index')
h2_n_sto_e_max = h2_n_sto_e_max.reset_index(drop=True)

# normalize max values
temp = {}
for custom_flow, df in h2_n_sto_e_max.groupby(['custom_flow']):
    df['E2P_normalized'] = df['E2P'] / df['E2P'].max()
    temp[custom_flow] = df
h2_n_sto_e_max = pd.concat(temp.values())

# normalize investment region-wise
h2_n_sto_e_norm = {}
for (region, custom_flow), df in h2_n_sto_e.groupby(['n', 'custom_flow']):
    df = df.copy().reset_index(drop=True)
    df['energy'] /= df['energy'].max()
    h2_n_sto_e_norm[(region, custom_flow)] = df
h2_n_sto_e_norm = pd.concat(h2_n_sto_e_norm.values())
h2_n_sto_e_norm = h2_n_sto_e_norm.reset_index(drop=True)

cwe = ['AT','BE', 'CH', 'CZ', 'DE', 'DK', 'FR', 'LU', 'NL', 'PL']
cwe = cwe[::-1]
sca = ['FI', 'NO', 'SE']
sca = sca[::-1]
nwe = ['IE', 'UK']
nwe = nwe [::-1]
bal = ['LT', 'LV', 'EE']
bal = bal[::-1]
swe = ['IT', 'ES', 'PT']
swe = swe[::-1]
see = ['SI', 'HU', 'SK', 'HR', 'BA', 'RS', 'RO', 'ME', 'AL', 'MK', 'BG', 'GR']
see = see[::-1]


fig = make_subplots(rows=7, cols=2, shared_yaxes=True, shared_xaxes=True,
                    column_widths=[0.8, 0.2], row_heights=[0.1, 0.4, 0.12, 0.08, 0.12, 0.12, 0.3],
                    horizontal_spacing=(0.015), vertical_spacing=(0.015))

# CP
heatmap1 = go.Heatmap(
    z=h2_n_sto_e_norm.query('n == "CP" and custom_flow == "copperplate-e"')['energy'],
    x=h2_n_sto_e_norm.query('n == "CP" and custom_flow == "copperplate-e"')['time_frame'],
    y=h2_n_sto_e_norm.query('n == "CP" and custom_flow == "copperplate-e"')['n'],
    coloraxis='coloraxis'
    )

heatmap2 = go.Heatmap(
    z=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-e"')['E2P_normalized'],
    x=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-e"')['max_E2P'],
    y=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-e"')['n'],
    coloraxis='coloraxis',
    text=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-e"')['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# cwe
data_norm = h2_n_sto_e_norm.query('n == @cwe and custom_flow == "island-e"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=cwe, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap3 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n == @cwe and custom_flow == "island-e"')
data_max['n'] = pd.Categorical(data_max['n'], categories=cwe, ordered=True)
data_max = data_max.sort_values('n')
heatmap4 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# sca
data_norm = h2_n_sto_e_norm.query('n in @sca and custom_flow == "island-e"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=sca, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap5 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @sca and custom_flow == "island-e"')
data_max['n'] = pd.Categorical(data_max['n'], categories=sca, ordered=True)
data_max = data_max.sort_values('n')
heatmap6 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# nwe
data_norm = h2_n_sto_e_norm.query('n in @nwe and custom_flow == "island-e"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=nwe, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap7 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @nwe and custom_flow == "island-e"')
data_max['n'] = pd.Categorical(data_max['n'], categories=nwe, ordered=True)
data_max = data_max.sort_values('n')
heatmap8 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )


# bal
data_norm = h2_n_sto_e_norm.query('n in @bal and custom_flow == "island-e"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=bal, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap9 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @bal and custom_flow == "island-e"')
data_max['n'] = pd.Categorical(data_max['n'], categories=bal, ordered=True)
data_max = data_max.sort_values('n')
heatmap10 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=h2_n_sto_e_max.query('n in @swe and custom_flow == "island-e"')['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# swe
data_norm = h2_n_sto_e_norm.query('n in @swe and custom_flow == "island-e"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=swe, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap11 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @swe and custom_flow == "island-e"')
data_max['n'] = pd.Categorical(data_max['n'], categories=swe, ordered=True)
data_max = data_max.sort_values('n')
heatmap12 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# see
data_norm = h2_n_sto_e_norm.query('n in @see and custom_flow == "island-e"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=see, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap13 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @see and custom_flow == "island-e"')
data_max['n'] = pd.Categorical(data_max['n'], categories=see, ordered=True)
data_max = data_max.sort_values('n')
heatmap14 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# Add the heatmaps to the subplot
fig.add_trace(heatmap1, row=1, col=1)
fig.add_trace(heatmap2, row=1, col=2)
fig.add_trace(heatmap3, row=2, col=1)
fig.add_trace(heatmap4, row=2, col=2)
fig.add_trace(heatmap5, row=3, col=1)
fig.add_trace(heatmap6, row=3, col=2)
fig.add_trace(heatmap7, row=4, col=1)
fig.add_trace(heatmap8, row=4, col=2)
fig.add_trace(heatmap9, row=5, col=1)
fig.add_trace(heatmap10, row=5, col=2)
fig.add_trace(heatmap11, row=6, col=1)
fig.add_trace(heatmap12, row=6, col=2)
fig.add_trace(heatmap13, row=7, col=1)
fig.add_trace(heatmap14, row=7, col=2)

# Update the layout to set the shared y and x axes
fig.update_layout(
    coloraxis={'colorscale': 'amp'},
    coloraxis_colorbar=dict(outlinewidth=0),
    width=1000,
    height=800,
    template='none',
    margin=dict(l=20, r=130, t=0, b=80)
    )

# Stretch the colorbar to span the entire subplot
colorbar = fig.layout.coloraxis.colorbar
colorbar.update(thickness=30, len=1.03)

# format axes
fig.update_yaxes(ticks='', showline=False, row=1, col=1)
fig.update_yaxes(ticks='', showline=False, row=1, col=2)
fig.update_yaxes(ticks='', showline=False, row=2, col=1) #categoryorder='array', categoryarray=order[::-1], 
fig.update_yaxes(ticks='', showline=False, row=2, col=2)
fig.update_yaxes(ticks='', showline=False, row=3, col=1)
fig.update_yaxes(ticks='', showline=False, row=3, col=2)
fig.update_yaxes(ticks='', showline=False, row=4, col=1)
fig.update_yaxes(ticks='', showline=False, row=4, col=2)
fig.update_yaxes(ticks='', showline=False, row=5, col=1)
fig.update_yaxes(ticks='', showline=False, row=5, col=2)
fig.update_yaxes(ticks='', showline=False, row=6, col=1)
fig.update_yaxes(ticks='', showline=False, row=6, col=2)
fig.update_yaxes(ticks='', showline=False, row=7, col=1, tickfont=dict(size=8))
fig.update_yaxes(ticks='', showline=False, row=7, col=2, tickfont=dict(size=8))

fig.update_xaxes(ticks='', showline=False, row=1, col=1)
fig.update_xaxes(ticks='', showline=False, row=1, col=2)
fig.update_xaxes(ticks='', showline=False, row=2, col=1)
fig.update_xaxes(ticks='', showline=False, row=2, col=2)
fig.update_xaxes(ticks='', showline=False, row=3, col=1)
fig.update_xaxes(ticks='', showline=False, row=3, col=2)
fig.update_xaxes(ticks='', showline=False, row=4, col=1)
fig.update_xaxes(ticks='', showline=False, row=4, col=2)
fig.update_xaxes(ticks='', showline=False, row=5, col=1)
fig.update_xaxes(ticks='', showline=False, row=5, col=2)
fig.update_xaxes(ticks='', showline=False, row=6, col=1)
fig.update_xaxes(ticks='', showline=False, row=6, col=2)
fig.update_xaxes(ticks='', showline=False, row=7, col=1)
fig.update_xaxes(ticks='', showline=False, row=7, col=2)

# title
fig.add_annotation(
    x=1.7,
    y=0.5,
    text='normalized storage energy / E-to-P ratio',
    showarrow=False,
    xref='x domain',
    yref="y domain",
    font=dict(size=20),
    textangle=90,
    col=2,
    row=3
)

fig.show()

path = 'S:\\projekte_paper\\vre-drought-mass\\result_plots\\'
path_output = os.path.join(path, 'h2_n_sto_e_heatmap')
Path(path_output).mkdir(parents=True, exist_ok=True)
name = 'storage_energy'
path_output = os.path.join(path_output, name)
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')

# %% heatmap: storage energy with E2P -> Paper Coping with the Dunkelflaute

path = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-11-02_n_sto_e.csv"
h2_n_sto_e = pd.read_csv(path)
h2_n_sto_e = h2_n_sto_e.rename(columns={'value': 'energy'})

path = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-10-31_capacity.csv"
h2_n_recon = pd.read_csv(path)
h2_n_recon = h2_n_recon.rename(columns={'value': 'capacity'})
h2_n_recon = h2_n_recon.query('custom_year != 2016 and tech == "H2_GT"')

# merge data
h2_n_sto_e = pd.merge(h2_n_sto_e, h2_n_recon, on=['n', 'custom_flow', 'custom_year'], how='left')
h2_n_sto_e = h2_n_sto_e.drop(columns=['tech_x', 'tech_y', 'aux_info', 'id', 'dem_scenario'])

# compute E2P
h2_n_sto_e['E2P'] = (h2_n_sto_e['energy'] * 1e3) / h2_n_sto_e['capacity']

# prepare investment data
# h2_n_sto_e['star_year'] = h2_n_sto_e['custom_year'].str.split('-').str[0]
# h2_n_sto_e['end_year'] = h2_n_sto_e['custom_year'].str.split('-').str[1]
h2_n_sto_e = h2_n_sto_e.rename(columns={'custom_year': 'time_frame'})
h2_n_sto_e['annotation'] = h2_n_sto_e['time_frame'].astype(str) + ' (' + (h2_n_sto_e['E2P'] / 24).round(1).astype(str) + ')'
h2_n_sto_e['max_E2P'] = 'timeframe (E2P [d])'

# get cumulative European copperplate value
temp = {}
for (custom_flow, time_frame), df in h2_n_sto_e.groupby(['custom_flow', 'time_frame']):
    df = df.reset_index(drop=True)
    df_CP = pd.DataFrame(columns=df.columns, index=[0])
    df_CP.loc[0] = ['CP', df['energy'].sum(), custom_flow, time_frame, df['capacity'].sum(), 'E2P', 'annotation', df.loc[0]['max_E2P']]
    df_CP['E2P'] = (df_CP['energy'] * 1e3) / df_CP['capacity']
    df_CP['annotation'] = df_CP.loc[0]['time_frame'] + ' (' + (df_CP.loc[0]['E2P'] / 24).round(1).astype(str) + ')'
    df = pd.concat([df, df_CP]).reset_index(drop=True)
    temp[(custom_flow, time_frame)] = df
h2_n_sto_e = pd.concat(temp.values())
h2_n_sto_e['energy'] = pd.to_numeric(h2_n_sto_e['energy'], errors='coerce')
h2_n_sto_e['capacity'] = pd.to_numeric(h2_n_sto_e['capacity'], errors='coerce')
h2_n_sto_e['E2P'] = pd.to_numeric(h2_n_sto_e['E2P'], errors='coerce')
h2_n_sto_e = h2_n_sto_e.reset_index(drop=True)

# get max value
h2_n_sto_e_max = {}
for (region, custom_flow), df in h2_n_sto_e.groupby(['n', 'custom_flow']):    
    df = df.copy().reset_index(drop=True)
    h2_n_sto_e_max[(region, custom_flow)] = df.loc[df['energy'].idxmax()]
h2_n_sto_e_max = pd.DataFrame.from_dict(h2_n_sto_e_max, orient='index')
h2_n_sto_e_max = h2_n_sto_e_max.reset_index(drop=True)

# normalize max values
temp = {}
for custom_flow, df in h2_n_sto_e_max.groupby(['custom_flow']):
    df['E2P_normalized'] = df['E2P'] / df['E2P'].max()
    temp[custom_flow] = df
h2_n_sto_e_max = pd.concat(temp.values())

# normalize investment region-wise
h2_n_sto_e_norm = {}
for (region, custom_flow), df in h2_n_sto_e.groupby(['n', 'custom_flow']):
    df = df.copy().reset_index(drop=True)
    df['energy'] /= df['energy'].max()
    h2_n_sto_e_norm[(region, custom_flow)] = df
h2_n_sto_e_norm = pd.concat(h2_n_sto_e_norm.values())
h2_n_sto_e_norm = h2_n_sto_e_norm.reset_index(drop=True)

cwe = ['AT','BE', 'CH', 'CZ', 'DE', 'DK', 'FR', 'LU', 'NL', 'PL']
cwe = cwe[::-1]
sca = ['FI', 'NO', 'SE']
sca = sca[::-1]
nwe = ['IE', 'UK']
nwe = nwe [::-1]
bal = ['LT', 'LV', 'EE']
bal = bal[::-1]
swe = ['IT', 'ES', 'PT']
swe = swe[::-1]
see = ['SI', 'HU', 'SK', 'HR', 'BA', 'RS', 'RO', 'ME', 'AL', 'MK', 'BG', 'GR']
see = see[::-1]


fig = make_subplots(rows=7, cols=2, shared_yaxes=True, shared_xaxes=True,
                    column_widths=[0.8, 0.2], row_heights=[0.1, 0.4, 0.12, 0.08, 0.12, 0.12, 0.3],
                    horizontal_spacing=(0.015), vertical_spacing=(0.015))

# CP
heatmap1 = go.Heatmap(
    z=h2_n_sto_e_norm.query('n == "CP" and custom_flow == "copperplate-eh"')['energy'],
    x=h2_n_sto_e_norm.query('n == "CP" and custom_flow == "copperplate-eh"')['time_frame'],
    y=h2_n_sto_e_norm.query('n == "CP" and custom_flow == "copperplate-eh"')['n'],
    coloraxis='coloraxis'
    )

heatmap2 = go.Heatmap(
    z=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-eh"')['E2P_normalized'],
    x=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-eh"')['max_E2P'],
    y=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-eh"')['n'],
    coloraxis='coloraxis',
    text=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-eh"')['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# cwe
data_norm = h2_n_sto_e_norm.query('n == @cwe and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=cwe, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap3 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n == @cwe and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=cwe, ordered=True)
data_max = data_max.sort_values('n')
heatmap4 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# sca
data_norm = h2_n_sto_e_norm.query('n in @sca and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=sca, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap5 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @sca and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=sca, ordered=True)
data_max = data_max.sort_values('n')
heatmap6 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# nwe
data_norm = h2_n_sto_e_norm.query('n in @nwe and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=nwe, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap7 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @nwe and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=nwe, ordered=True)
data_max = data_max.sort_values('n')
heatmap8 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )


# bal
data_norm = h2_n_sto_e_norm.query('n in @bal and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=bal, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap9 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @bal and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=bal, ordered=True)
data_max = data_max.sort_values('n')
heatmap10 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=h2_n_sto_e_max.query('n in @swe and custom_flow == "island-eh"')['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# swe
data_norm = h2_n_sto_e_norm.query('n in @swe and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=swe, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap11 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @swe and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=swe, ordered=True)
data_max = data_max.sort_values('n')
heatmap12 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# see
data_norm = h2_n_sto_e_norm.query('n in @see and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=see, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap13 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @see and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=see, ordered=True)
data_max = data_max.sort_values('n')
heatmap14 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# Add the heatmaps to the subplot
fig.add_trace(heatmap1, row=1, col=1)
fig.add_trace(heatmap2, row=1, col=2)
fig.add_trace(heatmap3, row=2, col=1)
fig.add_trace(heatmap4, row=2, col=2)
fig.add_trace(heatmap5, row=3, col=1)
fig.add_trace(heatmap6, row=3, col=2)
fig.add_trace(heatmap7, row=4, col=1)
fig.add_trace(heatmap8, row=4, col=2)
fig.add_trace(heatmap9, row=5, col=1)
fig.add_trace(heatmap10, row=5, col=2)
fig.add_trace(heatmap11, row=6, col=1)
fig.add_trace(heatmap12, row=6, col=2)
fig.add_trace(heatmap13, row=7, col=1)
fig.add_trace(heatmap14, row=7, col=2)

# Update the layout to set the shared y and x axes
fig.update_layout(
    coloraxis={'colorscale': 'amp'},
    coloraxis_colorbar=dict(outlinewidth=0),
    width=1000,
    height=800,
    template='none',
    margin=dict(l=20, r=130, t=0, b=80)
    )

# Stretch the colorbar to span the entire subplot
colorbar = fig.layout.coloraxis.colorbar
colorbar.update(thickness=30, len=1.03)

# format axes
fig.update_yaxes(ticks='', showline=False, row=1, col=1)
fig.update_yaxes(ticks='', showline=False, row=1, col=2)
fig.update_yaxes(ticks='', showline=False, row=2, col=1) #categoryorder='array', categoryarray=order[::-1], 
fig.update_yaxes(ticks='', showline=False, row=2, col=2)
fig.update_yaxes(ticks='', showline=False, row=3, col=1)
fig.update_yaxes(ticks='', showline=False, row=3, col=2)
fig.update_yaxes(ticks='', showline=False, row=4, col=1)
fig.update_yaxes(ticks='', showline=False, row=4, col=2)
fig.update_yaxes(ticks='', showline=False, row=5, col=1)
fig.update_yaxes(ticks='', showline=False, row=5, col=2)
fig.update_yaxes(ticks='', showline=False, row=6, col=1)
fig.update_yaxes(ticks='', showline=False, row=6, col=2)
fig.update_yaxes(ticks='', showline=False, row=7, col=1, tickfont=dict(size=8))
fig.update_yaxes(ticks='', showline=False, row=7, col=2, tickfont=dict(size=8))

fig.update_xaxes(ticks='', showline=False, row=1, col=1)
fig.update_xaxes(ticks='', showline=False, row=1, col=2)
fig.update_xaxes(ticks='', showline=False, row=2, col=1)
fig.update_xaxes(ticks='', showline=False, row=2, col=2)
fig.update_xaxes(ticks='', showline=False, row=3, col=1)
fig.update_xaxes(ticks='', showline=False, row=3, col=2)
fig.update_xaxes(ticks='', showline=False, row=4, col=1)
fig.update_xaxes(ticks='', showline=False, row=4, col=2)
fig.update_xaxes(ticks='', showline=False, row=5, col=1)
fig.update_xaxes(ticks='', showline=False, row=5, col=2)
fig.update_xaxes(ticks='', showline=False, row=6, col=1)
fig.update_xaxes(ticks='', showline=False, row=6, col=2)
fig.update_xaxes(ticks='', showline=False, row=7, col=1)
fig.update_xaxes(ticks='', showline=False, row=7, col=2)

# title
fig.add_annotation(
    x=1.7,
    y=0.5,
    text='normalized storage energy / E-to-P ratio',
    showarrow=False,
    xref='x domain',
    yref="y domain",
    font=dict(size=20),
    textangle=90,
    col=2,
    row=3
)

fig.show()

path = 'I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_plots'
path_output = os.path.join(path, 'h2_n_sto_e_heatmap')
Path(path_output).mkdir(parents=True, exist_ok=True)
name = 'storage_energy'
path_output = os.path.join(path_output, name)
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')


# %% heatmap: storage energy with E2P: coping with the DF

path = "S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_data\\2024-10-16_capacity.csv"
cap = pd.read_csv(path)

h2_n_sto_e = cap.query('aux_info == "energy" and tech == "cavern"')
h2_n_sto_e = h2_n_sto_e.rename(columns={'value': 'energy'})
h2_n_sto_e = h2_n_sto_e.query('custom_year != 2016')

h2_n_recon = cap.query('aux_info == "power" and tech == "H2_GT"')
h2_n_recon = h2_n_recon.rename(columns={'value': 'capacity'})
h2_n_recon = h2_n_recon.query('custom_year != 2016')

# merge data
h2_n_sto_e = pd.merge(h2_n_sto_e, h2_n_recon, on=['n', 'custom_flow', 'custom_year'], how='outer')
h2_n_sto_e = h2_n_sto_e.drop(columns=['tech_x', 'tech_y', 'aux_info_x', 'aux_info_y'])

# compute E2P
h2_n_sto_e['E2P'] = (h2_n_sto_e['energy'] * 1e3) / h2_n_sto_e['capacity']

# prepare investment data
h2_n_sto_e = h2_n_sto_e.rename(columns={'custom_year': 'time_frame'})
h2_n_sto_e['annotation'] = h2_n_sto_e['time_frame'].astype(str) + ' (' + (h2_n_sto_e['E2P'] / 24).round(1).astype(str) + ')'
h2_n_sto_e['max_E2P'] = 'timeframe (E2P [d])'

# get cumulative European copperplate value
temp = {}
for (custom_flow, time_frame), df in h2_n_sto_e.groupby(['custom_flow', 'time_frame']):
    df = df.reset_index(drop=True)
    df_CP = pd.DataFrame(columns=df.columns, index=[0])
    df_CP.loc[0] = ['CP', df['energy'].sum(), custom_flow, df.loc[0]['time_frame'], df['capacity'].sum(), 'E2P', 'annotation', df.loc[0]['max_E2P']]
    df_CP['E2P'] = (df_CP['energy'] * 1e3) / df_CP['capacity']
    df_CP['annotation'] = df_CP.loc[0]['time_frame'] + ' (' + (df_CP.loc[0]['E2P'] / 24).round(1).astype(str) + ')'
    df = pd.concat([df, df_CP]).reset_index(drop=True)
    temp[(custom_flow, time_frame)] = df
h2_n_sto_e = pd.concat(temp.values())
h2_n_sto_e['energy'] = pd.to_numeric(h2_n_sto_e['energy'], errors='coerce')
h2_n_sto_e = h2_n_sto_e.reset_index(drop=True)

# get max value
h2_n_sto_e_max = {}
for (region, custom_flow), df in h2_n_sto_e.groupby(['n', 'custom_flow']):    
    df = df.copy().reset_index(drop=True)
    h2_n_sto_e_max[(region, custom_flow)] = df.loc[df['energy'].idxmax()]
h2_n_sto_e_max = pd.DataFrame.from_dict(h2_n_sto_e_max, orient='index')
h2_n_sto_e_max = h2_n_sto_e_max.reset_index(drop=True)

# normalize max values
temp = {}
for custom_flow, df in h2_n_sto_e_max.groupby(['custom_flow']):
    df['E2P_normalized'] = df['E2P'] / df['E2P'].max()
    temp[custom_flow] = df
h2_n_sto_e_max = pd.concat(temp.values())

# normalize investment region-wise
h2_n_sto_e_norm = {}
for (region, custom_flow), df in h2_n_sto_e.groupby(['n', 'custom_flow']):
    df = df.copy().reset_index(drop=True)
    df['energy'] /= df['energy'].max()
    h2_n_sto_e_norm[(region, custom_flow)] = df
h2_n_sto_e_norm = pd.concat(h2_n_sto_e_norm.values())
h2_n_sto_e_norm = h2_n_sto_e_norm.reset_index(drop=True)

cwe = ['AT','BE', 'CH', 'CZ', 'DE', 'DK', 'FR', 'LU', 'NL', 'PL']
cwe = cwe[::-1]
sca = ['FI', 'NO', 'SE']
sca = sca[::-1]
nwe = ['IE', 'UK']
nwe = nwe [::-1]
bal = ['LT', 'LV', 'EE']
bal = bal[::-1]
swe = ['IT', 'ES', 'PT']
swe = swe[::-1]
see = ['SI', 'HU', 'SK', 'HR', 'BA', 'RS', 'RO', 'ME', 'AL', 'MK', 'BG', 'GR']
see = see[::-1]


fig = make_subplots(rows=7, cols=2, shared_yaxes=True, shared_xaxes=True,
                    column_widths=[0.8, 0.2], row_heights=[0.1, 0.4, 0.12, 0.08, 0.12, 0.12, 0.3],
                    horizontal_spacing=(0.015), vertical_spacing=(0.015))

# CP
heatmap1 = go.Heatmap(
    z=h2_n_sto_e_norm.query('n == "CP" and custom_flow == "copperplate-eh"')['energy'],
    x=h2_n_sto_e_norm.query('n == "CP" and custom_flow == "copperplate-eh"')['time_frame'],
    y=h2_n_sto_e_norm.query('n == "CP" and custom_flow == "copperplate-eh"')['n'],
    coloraxis='coloraxis'
    )

heatmap2 = go.Heatmap(
    z=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-eh"')['E2P_normalized'],
    x=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-eh"')['max_E2P'],
    y=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-eh"')['n'],
    coloraxis='coloraxis',
    text=h2_n_sto_e_max.query('n == "CP" and custom_flow == "copperplate-eh"')['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# cwe
data_norm = h2_n_sto_e_norm.query('n == @cwe and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=cwe, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap3 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n == @cwe and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=cwe, ordered=True)
data_max = data_max.sort_values('n')
heatmap4 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# sca
data_norm = h2_n_sto_e_norm.query('n in @sca and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=sca, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap5 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @sca and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=sca, ordered=True)
data_max = data_max.sort_values('n')
heatmap6 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# nwe
data_norm = h2_n_sto_e_norm.query('n in @nwe and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=nwe, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap7 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @nwe and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=nwe, ordered=True)
data_max = data_max.sort_values('n')
heatmap8 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )


# bal
data_norm = h2_n_sto_e_norm.query('n in @bal and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=bal, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap9 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @bal and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=bal, ordered=True)
data_max = data_max.sort_values('n')
heatmap10 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=h2_n_sto_e_max.query('n in @swe and custom_flow == "island-eh"')['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# swe
data_norm = h2_n_sto_e_norm.query('n in @swe and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=swe, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap11 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @swe and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=swe, ordered=True)
data_max = data_max.sort_values('n')
heatmap12 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# see
data_norm = h2_n_sto_e_norm.query('n in @see and custom_flow == "island-eh"')
data_norm['n'] = pd.Categorical(data_norm['n'], categories=see, ordered=True)
data_norm = data_norm.sort_values('n')
heatmap13 = go.Heatmap(
    z=data_norm['energy'],
    x=data_norm['time_frame'],
    y=data_norm['n'],
    coloraxis='coloraxis'
    )

data_max = h2_n_sto_e_max.query('n in @see and custom_flow == "island-eh"')
data_max['n'] = pd.Categorical(data_max['n'], categories=see, ordered=True)
data_max = data_max.sort_values('n')
heatmap14 = go.Heatmap(
    z=data_max['E2P_normalized'],
    x=data_max['max_E2P'],
    y=data_max['n'],
    coloraxis='coloraxis',
    text=data_max['annotation'],
    texttemplate="%{text}",
    textfont={"size": 10}
    )

# Add the heatmaps to the subplot
fig.add_trace(heatmap1, row=1, col=1)
fig.add_trace(heatmap2, row=1, col=2)
fig.add_trace(heatmap3, row=2, col=1)
fig.add_trace(heatmap4, row=2, col=2)
fig.add_trace(heatmap5, row=3, col=1)
fig.add_trace(heatmap6, row=3, col=2)
fig.add_trace(heatmap7, row=4, col=1)
fig.add_trace(heatmap8, row=4, col=2)
fig.add_trace(heatmap9, row=5, col=1)
fig.add_trace(heatmap10, row=5, col=2)
fig.add_trace(heatmap11, row=6, col=1)
fig.add_trace(heatmap12, row=6, col=2)
fig.add_trace(heatmap13, row=7, col=1)
fig.add_trace(heatmap14, row=7, col=2)

# Update the layout to set the shared y and x axes
fig.update_layout(
    coloraxis={'colorscale': 'amp'},
    coloraxis_colorbar=dict(outlinewidth=0),
    width=1000,
    height=800,
    template='none',
    margin=dict(l=20, r=130, t=0, b=80)
    )

# Stretch the colorbar to span the entire subplot
colorbar = fig.layout.coloraxis.colorbar
colorbar.update(thickness=30, len=1.03)

# format axes
fig.update_yaxes(ticks='', showline=False, row=1, col=1)
fig.update_yaxes(ticks='', showline=False, row=1, col=2)
fig.update_yaxes(ticks='', showline=False, row=2, col=1) #categoryorder='array', categoryarray=order[::-1], 
fig.update_yaxes(ticks='', showline=False, row=2, col=2)
fig.update_yaxes(ticks='', showline=False, row=3, col=1)
fig.update_yaxes(ticks='', showline=False, row=3, col=2)
fig.update_yaxes(ticks='', showline=False, row=4, col=1)
fig.update_yaxes(ticks='', showline=False, row=4, col=2)
fig.update_yaxes(ticks='', showline=False, row=5, col=1)
fig.update_yaxes(ticks='', showline=False, row=5, col=2)
fig.update_yaxes(ticks='', showline=False, row=6, col=1)
fig.update_yaxes(ticks='', showline=False, row=6, col=2)
fig.update_yaxes(ticks='', showline=False, row=7, col=1, tickfont=dict(size=8))
fig.update_yaxes(ticks='', showline=False, row=7, col=2, tickfont=dict(size=8))

fig.update_xaxes(ticks='', showline=False, row=1, col=1)
fig.update_xaxes(ticks='', showline=False, row=1, col=2)
fig.update_xaxes(ticks='', showline=False, row=2, col=1)
fig.update_xaxes(ticks='', showline=False, row=2, col=2)
fig.update_xaxes(ticks='', showline=False, row=3, col=1)
fig.update_xaxes(ticks='', showline=False, row=3, col=2)
fig.update_xaxes(ticks='', showline=False, row=4, col=1)
fig.update_xaxes(ticks='', showline=False, row=4, col=2)
fig.update_xaxes(ticks='', showline=False, row=5, col=1)
fig.update_xaxes(ticks='', showline=False, row=5, col=2)
fig.update_xaxes(ticks='', showline=False, row=6, col=1)
fig.update_xaxes(ticks='', showline=False, row=6, col=2)
fig.update_xaxes(ticks='', showline=False, row=7, col=1)
fig.update_xaxes(ticks='', showline=False, row=7, col=2)

# title
fig.add_annotation(
    x=1.7,
    y=0.5,
    text='normalized storage energy / E-to-P ratio',
    showarrow=False,
    xref='x domain',
    yref="y domain",
    font=dict(size=20),
    textangle=90,
    col=2,
    row=3
)

fig.show()

path = 'S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_plots\\'
path_output = os.path.join(path, 'h2_n_sto_e_heatmap')
Path(path_output).mkdir(parents=True, exist_ok=True)
name = 'storage_energy'
path_output = os.path.join(path_output, name)
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')


# %% regression of drought mass of most severe event and H2_N_STO_E: all countries (coping with the DF)

# import drought mass data
dm_filter = 1 #, 12, 24, 18, 48]  # 
cut_off = 0.75  # 0.65, 0.7,
corrections = ['s2s-winter']  # 's2s-yearly',
demand_switch = ['yearly', 2009]  # 2009
demand_reference = ['electricity_demand', 'total_demand']
filter_twh = [0, 1, 2, 3, 5]
flexs = ['long-duration', 'mid-long-duration']

path = os.path.join('T:\\mk\\vreda\\output\\2024-08-02_08-42-30_mbt_event', f'drought_mass_filter_{dm_filter}',
                    'drought_mass_s2s')
items = os.listdir(path)
dm = {}
for item in items:
    path_import = os.path.join(path, item)
    file = 'mass_single_event_max_all_years.gz'
    dm[item] = load_object(path_import, file)

for correction in corrections:
    for dem_scen in demand_switch:
        for dem_ref in demand_reference:
            for f_twh in filter_twh:
                for flex_scope in flexs:
    
                    # import storage data
                    if dem_scen == 2009:
                        path = "S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_data\\2024-10-31_capacity.csv"
                    elif dem_scen == 'yearly':
                        path ="I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-10-31_capacity.csv"
                    cap = pd.read_csv(path)
    
                    h2_n_sto_e_island = cap.query('aux_info == "energy" and tech == "cavern" and custom_flow == "island-eh"')
                    h2_n_sto_e_island = h2_n_sto_e_island.rename(columns={'value': 'energy', 'n': 'region', 'custom_year': 'time_frame'})
                    h2_n_sto_e_island = h2_n_sto_e_island.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
                    
                    # remove countries with no capacity
                    temp = {}
                    for r, group in h2_n_sto_e_island.groupby('region'):
                        if group['energy'].sum() > 0:
                            temp[r] = group
                    h2_n_sto_e_island = pd.concat(temp.values())
                    
                    # filter out countries that have very low investments or reach capacity bounds in all scenarios
                    n_out_small = ['AL', 'NO']
                    n_out_capacity = ['BE', 'CZ', 'IT', 'HU', 'SK'] 
                    h2_n_sto_e_island = h2_n_sto_e_island.query('region not in @n_out_small and region not in @n_out_capacity')  # no of regions = 15
                    # h2_n_sto_e_island = h2_n_sto_e_island.query('region not in @n_out_capacity')  # no of regions = 15
                    
                    # filter out countries with average investment below 1 TWh
                    h2_n_sto_e_island = h2_n_sto_e_island.groupby('region').filter(lambda x: x['energy'].mean() >= f_twh)  # no of regions = 11
                    
                    h2_n_sto_e_island_regions = h2_n_sto_e_island.region.unique()
                    
                    if flex_scope == 'mid-long-duration':
                        rsvr_island = cap.query('aux_info == "energy" and tech in ["rsvr", "phs_open", "phs_closed"] and custom_flow == "island-eh"')
                        rsvr_island = rsvr_island.rename(columns={'value': 'energy', 'n': 'region', 'custom_year': 'time_frame'})
                        rsvr_island = rsvr_island.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
                        rsvr_island = rsvr_island.query("region in @h2_n_sto_e_island_regions")
                        h2_n_sto_e_island = pd.concat([h2_n_sto_e_island, rsvr_island])
                        h2_n_sto_e_island = h2_n_sto_e_island.groupby(['region', 'custom_flow', 'time_frame', 'aux_info','dem_scenario']).sum().reset_index()
    
                    h2_n_sto_e_island['energy'] *= 1e3
                    
                    # import demand data
                    path_dem = 'S:\\projekte_paper\\power-sector-droughts_net_drive_4\\project_files\\data_input\\demand2050_electricity_hydrogen.csv'
                    dem = pd.read_csv(path_dem)
                    dem = dem[['region', dem_ref]]  # total
                    
                    df = pd.merge(h2_n_sto_e_island, dm[correction], on=['region', 'time_frame'], how='inner')
                    df = pd.merge(df, dem, on=['region'], how='inner')
                    df['energy_over_total_dem'] = df['energy'] / df[dem_ref]  # electricity demand
                    
                    color_sequence = [
                        '#1f77b4',  # blue
                        '#ff7f0e',  # orange
                        '#2ca02c',  # green
                        '#d62728',  # red
                        '#9467bd',  # purple
                        '#8c564b',  # brown
                        '#e377c2',  # pink
                        '#7f7f7f',  # gray
                        '#bcbd22',  # olive
                        '#17becf',  # teal
                        '#ffbb78',  # light orange
                        '#98df8a',  # light green
                        '#ff9896',  # light red
                        '#c5b0d5',  # light purple
                        '#aec7e8',  # light blue
                        '#1f9f88',  # darker teal
                        '#FFD700'   # gold
                        ]
        
                    fig = go.Figure()
                    
                    regions = df['region'].unique()
                    
                    # Add each region as a separate scatter trace
                    for i, region in enumerate(regions):
                        region_df = df[df['region'] == region]
                        
                        # Add scatter trace for each region
                        fig.add_trace(go.Scatter(
                            x=region_df['mass'],
                            y=region_df['energy_over_total_dem'],
                            mode='markers',
                            name='',
                            marker=dict(size=3, color=color_sequence[i]),
                            #showlegend=True
                            legend='legend1',
                        ))
                    
                    # OLS regression for each group
                    region_color_map = {region: color_sequence[i] for i, region in enumerate(regions)}
                    
                    for region in regions:
                        # Subset the data for each region
                        region_df = df[df['region'] == region].copy()
                        
                        # Prepare the data for OLS regression
                        X = region_df['mass']
                        X = sm.add_constant(X)  # Adds an intercept (constant) to the independent variable
                        y = region_df['energy_over_total_dem']
                        
                        # Fit the OLS model
                        model = sm.OLS(y, X)
                        results = model.fit()
                        
                        # Add the predicted values (fitted line) back to the region's dataframe
                        region_df['predicted_energy_over_total_dem'] = results.fittedvalues
                        
                        # Add the OLS regression line for this region with the same color as the scatter plot
                        fig.add_traces(go.Scatter(
                            x=region_df['mass'],
                            y=region_df['predicted_energy_over_total_dem'],
                            mode='lines',
                            line=dict(color=region_color_map[region]),
                            name=f'{region}',  # Include a name for the legend
                            #showlegend=True,  # Ensure the regression lines appear in the legend
                            legend='legend2'
                        ))
                    
                    # Prepare the data for OLS regression for CP
                    if dem_scen == 2009:
                        path = "S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_data\\2024-10-31_capacity_agg.csv"
                    elif dem_scen == 'yearly':
                        path ="I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-10-31_capacity_agg.csv"
                    cap = pd.read_csv(path)
                    
                    if flex_scope == 'long-duration':
                        h2_n_sto_e_cp = cap.query('aux_info == "energy" and tech == "cavern" and custom_flow == "copperplate-eh"')
                        h2_n_sto_e_cp = h2_n_sto_e_cp.rename(columns={'value': 'energy', 'n': 'region', 'custom_year': 'time_frame'})
                        h2_n_sto_e_cp = h2_n_sto_e_cp.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
                        h2_n_sto_e_cp['region'] = 'CP'
                        h2_n_sto_e_cp['energy'] *= 1e3
                    elif flex_scope == 'mid-long-duration':
                        h2_n_sto_e_cp = cap.query('aux_info == "energy" and tech in ["cavern","rsvr","phs_open","phs_closed"] and custom_flow == "island-eh"')
                        h2_n_sto_e_cp = h2_n_sto_e_cp.rename(columns={'value': 'energy', 'n': 'region', 'custom_year': 'time_frame'})
                        h2_n_sto_e_cp = h2_n_sto_e_cp.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
                        h2_n_sto_e_cp = h2_n_sto_e_cp.groupby(['custom_flow', 'time_frame', 'aux_info','dem_scenario']).sum().reset_index()
                        h2_n_sto_e_cp['region'] = 'CP'
                        h2_n_sto_e_cp['energy'] *= 1e3
                    
                    df = pd.merge(h2_n_sto_e_cp, dm[correction], on=['region', 'time_frame'], how='inner')
                    df = pd.merge(df, dem, on=['region'], how='inner')
                    df['energy_over_total_dem'] = df['energy'] / df[dem_ref]  # total
                    
                    X = df['mass']
                    X = sm.add_constant(X)  # Adds an intercept (constant) to the independent variable
                    y = df['energy_over_total_dem']
                    
                    # Fit the OLS model
                    model = sm.OLS(y, X)
                    results = model.fit()
                    
                    # Add the predicted values (fitted line) back to the region's dataframe
                    df['predicted_energy_over_total_dem_CP'] = results.fittedvalues
                    
                    # Add the OLS regression line for this region with the same color as the scatter plot
                    fig.add_traces(go.Scatter(
                        x=df['mass'],
                        y=df['predicted_energy_over_total_dem_CP'],
                        mode='lines',
                        line=dict(color='black'),
                        name='CP',  # Include a name for the legend
                        #showlegend=True,  # Ensure the regression lines appear in the legend
                        legend='legend2'
                    ))
                    
                    # Add scatter trace for CP
                    fig.add_trace(go.Scatter(
                        x=df['mass'],
                        y=df['energy_over_total_dem'],
                        mode='markers',
                        name='',
                        marker=dict(size=3, color='black'),
                        #showlegend=True
                        legend='legend1',
                    ))               
                    
                    if flex_scope == 'long-duration':
                        fig.update_layout(
                            font_size=11,
                            template="simple_white",
                            yaxis=dict(
                                title='normalized long-duration storage energy',
                                title_font=dict(size=13)
                            ),
                            xaxis=dict(
                                title='drought mass',
                                title_font=dict(size=13)
                            ),
                            width=500,
                            height=400,
                            margin=dict(l=0, r=20, t=10, b=0)
                        )
                        
                    elif flex_scope == 'mid-long-duration':
                        fig.update_layout(
                            font_size=11,
                            template="simple_white",
                            yaxis=dict(
                                title='normalized mid- and long-duration storage energy',
                                title_font=dict(size=13)
                            ),
                            xaxis=dict(
                                title='drought mass',
                                title_font=dict(size=13)
                            ),
                            width=500,
                            height=400,
                            margin=dict(l=0, r=20, t=10, b=0)
                        )

                    
                    fig.update_layout(
                        legend1=dict(
                            title='',
                            font=dict(size=8),
                            title_side='top',
                            orientation='v',  # Horizontal orientation
                            x=1.13,  # Center the legend horizontally
                            y=0.9,  # Move the legend further down
                            xanchor='center',  # Anchor the legend's horizontal center to x position
                            yanchor='top',  # Anchor the legend's top to the y position
                            # itemsizing='constant',
                            traceorder='normal',
                            entrywidthmode='fraction',
                            entrywidth=0.12,
                            itemwidth=30,
                            )
                        )
                    
                    fig.update_layout(
                        legend2=dict(
                            #title='data / regression:',
                            font=dict(size=8),
                            title_side='top',
                            orientation='v',  # Horizontal orientation
                            x=1.25,  # Center the legend horizontally
                            y=0.9,  # Move the legend further down
                            xanchor='center',  # Anchor the legend's horizontal center to x position
                            yanchor='top',  # Anchor the legend's top to the y position
                            #itemsizing='constant',
                            traceorder='normal',
                            entrywidthmode='fraction',
                            entrywidth=0.12,
                            itemwidth=30,
                            )
                        )
                    
                    fig.add_annotation(
                        text="data / regression:",  # Text of the annotation
                        xref="paper",  # Position relative to the plot area
                        yref="paper",
                        x=1.2,  # Position slightly to the right of the plot
                        y=0.95,  # Position slightly above the plot
                        showarrow=False,  # Hide the arrow for a clean text label
                        font=dict(size=10),  # Adjust font size as needed
                        xanchor='center',  # Center-align the text horizontally
                        yanchor='top'      # Align the text to the top
                    )
                    
                    
                    #fig.update_xaxes(range=[0, 30000])
                    fig.update_yaxes(range=[0, 0.14])
                    fig.update_xaxes(showline=True, mirror=True, linecolor='black', linewidth=0.2)  # Adds top and bottom borders
                    fig.update_yaxes(showline=True, mirror=True, linecolor='black', linewidth=0.2)  # Adds left and right borders
                    
                    # fig.show()
                    
                    if dem_scen == 2009:
                        path_output = os.path.join('S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_plots\\regression_drought_mass_storage_energy', 'regression')
                    elif dem_scen == 'yearly':
                        path_output = os.path.join('I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_plots\\regression_drought_mass_storage_energy', 'regression')
                        
                    file_path = f'{path_output}_{dem_scen}_dm-{correction}_{dem_ref}_filter_{f_twh}TWh_{flex_scope}'
                    
                    fig.write_html(f'{file_path}.html')
                    fig.write_image(f'{file_path}.pdf')
                    fig.write_image(f'{file_path}.png', scale=3)

# %% regression of drought mass of most severe event and H2_N_STO_E: DE only (coping with the DF)

# import drought mass data
dm_filter = 1 #, 12, 24, 18, 48]  # 
cut_off = 0.75  # 0.65, 0.7,
corrections = ['s2s-yearly', 's2s-winter']
scenarios = {'flat_demand': 'flat demand profile', 'fixed_capas': 'fixed capacities', 'flat_demand-fixed_capas': 'flat demand profile and fixed capacities'}

path = os.path.join('T:\\mk\\vreda\\output\\2024-08-02_08-42-30_mbt_event', f'drought_mass_filter_{dm_filter}',
                    'drought_mass_s2s')
items = os.listdir(path)
dm = {}
for item in items:
    path_import = os.path.join(path, item)
    file = 'mass_single_event_max_all_years.gz'
    dm[item] = load_object(path_import, file)

for correction in corrections:

    # import storage data DE only
    path = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_6\\result_data\\2024-11-15_capacity.csv"
    cap = pd.read_csv(path)
    h2_n_sto_e_de = cap.query('aux_info == "energy" and tech == "cavern"')
    h2_n_sto_e_de = h2_n_sto_e_de.replace(scenarios)
    h2_n_sto_e_de = h2_n_sto_e_de.rename(columns={'value': 'energy', 'n': 'region', 'custom_year': 'time_frame'})
    h2_n_sto_e_de = h2_n_sto_e_de.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
    h2_n_sto_e_de['energy'] *= 1e3
    
    # import storage data all countries yearly demand
    path = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-10-31_capacity.csv"
    cap = pd.read_csv(path)
    h2_n_sto_e_all = cap.query('n == "DE" and aux_info == "energy" and tech == "cavern" and custom_flow == "island-eh"')
    h2_n_sto_e_all = h2_n_sto_e_all.replace(scenarios)
    h2_n_sto_e_all = h2_n_sto_e_all.rename(columns={'value': 'energy', 'n': 'region', 'custom_year': 'time_frame', 'custom_flow': 'custom_name'})
    h2_n_sto_e_all = h2_n_sto_e_all.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
    h2_n_sto_e_all['energy'] *= 1e3
    h2_n_sto_e_all = h2_n_sto_e_all.drop('dem_scenario', axis=1)
    
    h2_n_sto_e = pd.concat([h2_n_sto_e_de, h2_n_sto_e_all])
    h2_n_sto_e = h2_n_sto_e.replace({'island-eh': 'yearly demand profile'})
    
    # import demand data
    path_dem = 'S:\\projekte_paper\\power-sector-droughts_net_drive_4\\project_files\\data_input\\demand2050_electricity_hydrogen.csv'
    dem = pd.read_csv(path_dem)
    dem = dem[['region', 'total_demand']]
    
    df = pd.merge(h2_n_sto_e, dm[correction], on=['region', 'time_frame'], how='inner')
    df = pd.merge(df, dem, on=['region'], how='inner')
    df['energy_over_total_dem'] = df['energy'] / df['total_demand']

    color_sequence = [
        '#1f77b4',  # blue
        '#ff7f0e',  # orange
        '#2ca02c',  # green
        '#d62728',  # red
        '#9467bd',  # purple
        '#8c564b',  # brown
        '#e377c2',  # pink
        '#7f7f7f',  # gray
        '#bcbd22',  # olive
        '#17becf',  # teal
        '#ffbb78',  # light orange
        '#98df8a',  # light green
        '#ff9896',  # light red
        '#c5b0d5',  # light purple
        '#aec7e8',  # light blue
        '#1f9f88',  # darker teal
        '#FFD700'   # gold
        ]

    fig = go.Figure()
    
    # Add each region as a separate scatter trace
    for i, scen in enumerate(df['custom_name'].unique()):
        scen_df = df[df['custom_name'] == scen]
        
        # Add scatter trace for each region
        fig.add_trace(go.Scatter(
            x=scen_df['mass'],
            y=scen_df['energy_over_total_dem'],
            mode='markers',
            name='',
            marker=dict(size=3, color=color_sequence[i]),
            #showlegend=True
            legend='legend1',
            hovertemplate='custom_year',
        ))
    
    # Group the dataframe by 'region' and perform OLS regression for each group
    scens = df['custom_name'].unique()
    
    # Create a dictionary mapping each region to a color
    scen_color_map = {scen: color_sequence[i] for i, scen in enumerate(scens)}
    
    r_squared = {}
    
    for scen in scens:
        # Subset the data for each region
        scen_df = df[df['custom_name'] == scen].copy()
        
        # Prepare the data for OLS regression
        X = scen_df['mass']
        X = sm.add_constant(X)  # Adds an intercept (constant) to the independent variable
        y = scen_df['energy_over_total_dem']
        
        # Fit the OLS model
        model = sm.OLS(y, X)
        results = model.fit()
        
        # Get R²
        r_squared[scen] = results.rsquared
        
        # Add the predicted values (fitted line) back to the region's dataframe
        scen_df['predicted_energy_over_total_dem'] = results.fittedvalues
        
        # Add the OLS regression line for this region with the same color as the scatter plot
        fig.add_traces(go.Scatter(
            x=scen_df['mass'],
            y=scen_df['predicted_energy_over_total_dem'],
            mode='lines',
            line=dict(color=scen_color_map[scen]),
            name=f'{scen}',  # Include a name for the legend
            #showlegend=True,  # Ensure the regression lines appear in the legend
            legend='legend2'
        ))
        
        fig.update_traces(
        hovertemplate="<b>Year:</b> %{text}<br><b>X:</b> %{x}<br><b>Y:</b> %{y}<extra></extra>",
        text=scen_df['time_frame']  # Display custom_year column on hover
        )
    
    fig.update_layout(
        font_size=10,
        template="simple_white",
        yaxis_title='storage energy normalized by annual demand',
        xaxis_title='drought mass',
        width=500,
        height=400,
        margin=dict(l=0, r=20, t=5, b=0),
        xaxis_title_font=dict(size=10),
        yaxis_title_font=dict(size=10)
    )
    
    fig.update_layout(
        legend1=dict(
            title='',
            font=dict(size=8),
            title_side='top',
            orientation='v',  # Horizontal orientation
            x=0.001,  # Center the legend horizontally
            y=-0.25,  # Move the legend further down
            xanchor='center',  # Anchor the legend's horizontal center to x position
            yanchor='top',  # Anchor the legend's top to the y position
            # itemsizing='constant',
            traceorder='normal',
            entrywidthmode='fraction',
            entrywidth=0.12,
            itemwidth=30,
            )
        )
    
    fig.update_layout(
        legend2=dict(
            #title='data / regression:',
            font=dict(size=8),
            title_side='top',
            orientation='v',  # Horizontal orientation
            x=0.3,  # Center the legend horizontally
            y=-0.25,  # Move the legend further down
            xanchor='center',  # Anchor the legend's horizontal center to x position
            yanchor='top',  # Anchor the legend's top to the y position
            #itemsizing='constant',
            traceorder='normal',
            entrywidthmode='fraction',
            entrywidth=0.12,
            itemwidth=30,
            )
        )
    
    fig.add_annotation(
        text="data / regression:",  # Text of the annotation
        xref="paper",  # Position relative to the plot area
        yref="paper",
        x=0.08,  # Position slightly to the right of the plot
        y=-0.2,  # Position slightly above the plot
        showarrow=False,  # Hide the arrow for a clean text label
        font=dict(size=9),  # Adjust font size as needed
        xanchor='center',  # Center-align the text horizontally
        yanchor='top'      # Align the text to the top
    )
    
    #fig.update_xaxes(range=[0, 30000])
    #fig.update_yaxes(range=[0, 0.12])
    fig.update_xaxes(showline=True, mirror=True, linecolor='black', linewidth=0.2)  # Adds top and bottom borders
    fig.update_yaxes(showline=True, mirror=True, linecolor='black', linewidth=0.2)  # Adds left and right borders
    
    fig.show()
    
    path_output = os.path.join('I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_6\\result_plots\\regression_drought_mass_storage_energy', 'regression')
    
    fig.write_html(f'{path_output}_DE_{correction}.html')
    fig.write_image(f'{path_output}_DE_{correction}.pdf')
    fig.write_image(f'{path_output}_DE_{correction}.png', scale=3)

# %% generation time series: coping with the DF

# import data
path_dem = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-11-13_demand_1996-1997.csv"
D_EL_df = pd.read_csv(path_dem)
path_gen = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-11-13_generation_1996-1997.csv"
G_df = pd.read_csv(path_gen)
path_h2_sto = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-11-13_h2_sto_l_1996-1997.csv"
H2_STO_L_df = pd.read_csv(path_h2_sto)

colors_tech = {'onshore wind': '#518696',
               'offshore wind': '#104c5a',
               'pv': '#ffff97',
               'bio': '#aee571',
               'ror': '#002171',
               'rsvr': '#5472d3',
               'rsvr charge': '#5472d3',
               'rsvr discharge': '#5472d3',
               'nuc': '#e4696a',
               'lig': '#a67561',
               'hc': '#74655c',
               'oil': '#4b4b47',
               'other': '#cbdae3',
               'CCGT': '#f95827',
               'OCGT': '#aa0000',
               'li-ion': '#dad8ff',
               'li-ion charge': '#dad8ff',
               'li-ion discharge': '#dad8ff',
               'pumped hydro (closed)': '#0054dc',
               'pumped hydro (closed) charge': '#0054dc',
               'pumped hydro (closed) discharge': '#0054dc',
               'pumped hydro (open) charge': '#0d47a1',
               'pumped hydro (open)': '#0d47a1',
               'pumped hydro (open) discharge': '#0d47a1',
               'electricity_exchange': '#620086',
               'electricity_exports': '#620086',
               #'electricity_exports': '#9526b7',
               #'electricity_imports': '#4a006e',
               'hp_gs': '#518696',
               'hp_as': '#104c5a',
               'PEM electrolysis': '#96ffff',
               'hydrogen gas turbine': '#26c6da',
               'cavern': '#0095a8'
               #'p2g2p':'#fffff',
               #'p2g2p_charge': '#ffffff',
               #'p2g2p_discharge': '#ffffff',
               }


# plot generation time series for a specific year
year = ['1996-1997']
nodes = ['FR'] #, 'ES', 'FR', 'IT', 'NO', 'PL', 'UK']
hour_start = 't0001'  # 't0001'  #   # t2935 = 01.11.96 00:00:00, t3673 = 2000-12-01 00:00:00
hour_end =  't8760'  # 't8760'  #  # t5832 = 02.28.97 23:00:00, t5160 = 2001-01-31 23:00:00
scen = ['island-eh']

# data generation and loads
data_tech = G_df.query('custom_year in @year and h >= @hour_start and h <= @hour_end and n in @nodes and custom_flow in @scen').reset_index(drop=True)
gen_vres = data_tech.query('tech in ["wind_on", "pv", "wind_off"]')
gen_vres = gen_vres.groupby(['n', 'h', 'custom_flow', 'custom_year']).sum().reset_index(drop=True)

# data demand for electricty
data_load_el = D_EL_df.query('custom_year in @year and h >= @hour_start and h <= @hour_end and n in @nodes and custom_flow in @scen').reset_index(drop=True)

# data residual laod
data_residual_load = (data_load_el['value'] - gen_vres['value']).to_frame()
data_residual_load['h'] = data_load_el['h']

# =============================================================================
# # data net exports
# exports = data_tech.query('tech == "net_exports"')
# exports = exports[exports['value'] > 0]
# exports.loc[:, 'value'] *= -1
# exports = exports[exports['value'] < -0.01]
# imports = data_tech.query('tech == "net_exports"')
# imports = imports[imports['value'] <= 0]
# imports.loc[:, 'value'] *= -1
# imports = imports[imports['value'] > 0.01]
# =============================================================================

# data for long-duration storage
data_h2_sto = H2_STO_L_df.query('custom_year in @year and h >= @hour_start and h <= @hour_end and n in @nodes and custom_flow in @scen').reset_index(drop=True)

# data H2 Importe
# h2_imports = data_tech.query('tech == "hydrogen imports"')

gens = {'bio': 'bio',
        'rsvr.discharge': 'rsvr',
        'ror': 'ror',
        'phs_closed.discharge': 'pumped hydro (closed)',
        'phs_open.discharge': 'pumped hydro (open)',
        'wind_on': 'onshore wind',
        'wind_off': 'offshore wind',
        'pv': 'pv',
        'H2_GT': 'hydrogen gas turbine',
        'li-ion.discharge': 'li-ion'
        }

loads = {'PEM': 'PEM electrolysis',
         'phs_closed.charge': 'pumped hydro (closed) charge',
         'phs_open.charge': 'pumped hydro (open) charge',
         'li-ion.charge': 'li-ion charge',
         }

# create hourly index
hourly_index = data_load_el.h.str.replace("t", "").astype(int)
start_date = datetime.datetime(1996, 7, 1)
data_load_el['h_datetime'] = hourly_index.apply(lambda x: start_date + datetime.timedelta(hours=x - 1))
data_load_el = data_load_el.set_index('h_datetime')
hourly_index = data_load_el.reset_index()[['h_datetime', 'h']]

data_tech = data_tech.replace(gens)
data_tech = data_tech.replace(loads)

data_residual_load = pd.merge(data_residual_load, hourly_index, on='h', how='right')
data_residual_load['value'] = data_residual_load['value'].where(data_residual_load['h'].isin(hourly_index['h']), 0)
data_h2_sto = pd.merge(data_h2_sto, hourly_index, on='h', how='right')
data_h2_sto['value'] = data_h2_sto['value'].where(data_h2_sto['h'].isin(hourly_index['h']), 0)

# %%

legend_switch = True

# generate figure, subplots and secondary y axis
fig = make_subplots(specs=[[{"secondary_y": True}]])

# add demand for electricty line
fig.add_trace(
    go.Scatter(
        name='load',
        x=data_load_el.index,
        y=data_load_el.value,
        mode='lines',
        line=dict(width=3, color='#d05094'),
        showlegend=legend_switch,
    ))

# add generation
gen_counter = 0
for gen in gens.values():

    # get data
    data_gen = data_tech.query('tech == @gen and custom_flow == @scen')
    data_gen = pd.merge(data_gen, hourly_index, on='h', how='right')
    data_gen['value'] = data_gen['value'].replace(np.nan, 0)

    # first gen tech fills to zero y axis
    if gen_counter == 0:
        fig.add_trace(
            go.Scatter(
                name=gen,  #_name_plain,
                x=data_gen.h_datetime,
                y=data_gen.value,
                stackgroup='one',
                fill="tozeroy",
                fillcolor=colors_tech[gen],
                mode='none',
                showlegend=legend_switch,
            ))
        gen_counter += 1
    
    # all other gen techs stack on top
    else:
        fig.add_trace(
            go.Scatter(
                name=gen,  #_name_plain,
                x=data_gen.h_datetime,
                y=data_gen.value,
                stackgroup='one',
                fill="tonexty",
                fillcolor=colors_tech[gen],
                mode='none',
                showlegend=legend_switch,
            ))

# add loads
load_counter = 0
for load in loads.values():

    # get data
    data_loads = data_tech.query('tech == @load and custom_flow == @scen')
    data_loads = pd.merge(data_loads, hourly_index, on='h', how='right')
    data_loads['value'] = data_loads['value'].replace(np.nan, 0)

    if load == 'PEM electrolysis':
    
        # first load tech fills to zero y axis
        if load_counter == 0:
            fig.add_trace(
                go.Scatter(
                    name=load,
                    x=data_loads.h_datetime,
                    y=data_loads.value,
                    stackgroup='two',
                    fill="tozeroy",
                    fillcolor=colors_tech[load],
                    mode='none',
                    showlegend=legend_switch,
                ))
            load_counter += 1
        # all other load techs stack below
        else:
            fig.add_trace(
                go.Scatter(
                    name=load,
                    x=data_loads.h_datetime,
                    y=data_loads.value,
                    stackgroup='two',
                    fill="tonexty",
                    fillcolor=colors_tech[load],
                    mode='none',
                    showlegend=False
                ))

    else:
        # first load tech fills to zero y axis
        if load_counter == 0:
            fig.add_trace(
                go.Scatter(
                    name=load,
                    x=data_loads.h_datetime,
                    y=data_loads.value,
                    stackgroup='two',
                    fill="tozeroy",
                    fillcolor=colors_tech[load],
                    mode='none',
                    showlegend=False
                ))
            load_counter += 1
        
        # all other load techs stack below
        else:
            fig.add_trace(
                go.Scatter(
                    name=load,
                    x=data_loads.h_datetime,
                    y=data_loads.value,
                    stackgroup='two',
                    fill="tonexty",
                    fillcolor=colors_tech[load],
                    mode='none',
                    showlegend=False
                ))
                    

# add residual demand for electricty line
fig.add_trace(
    go.Scatter(
        name='residual load',
        x=data_residual_load.h_datetime,
        y=data_residual_load.value,
        mode='lines',
        line=dict(width=2, color='red'), #, dash='dot')
        showlegend=legend_switch,
    ))

# add H2 storage state of charge on secondary axis
fig.add_trace(
    go.Scatter(
        name='hydrogen storage',
        x=data_h2_sto.h_datetime,
        y=data_h2_sto.value,
        mode='lines',
        line=dict(width=3, color='#007a8c', dash='dash'),
        showlegend=legend_switch,
    ),
    secondary_y=True
    )

# rename y axis label of each subplot
fig['layout']['yaxis']['title']['text'] = 'GW'
fig.update_yaxes(title_text="TWh", secondary_y=True,)

fig.update_layout(
    legend=dict(
        orientation="h",
        yanchor="bottom",
        y=-0.35,
        xanchor="center",
        x=0.47,
        #tracegroupgap=0,
        itemwidth=30
    )
)

fig.update_layout(
    xaxis=dict(
        mirror=True,  # Mirrors the x-axis lines
        showline=True,  # Ensures the x-axis line is drawn
        linewidth=1,    # Adjust the line width
        linecolor='black',  # Set the line color
        dtick="D1",  # Tick every day
        tickformat="%d<br>%b",
    ),
    yaxis=dict(
        mirror=True,  # Mirrors the y-axis lines
        showline=True,  # Ensures the y-axis line is drawn
        linewidth=1,    # Adjust the line width
        linecolor='black'  # Set the line color
    )
)

# =============================================================================
# fig.update_yaxes(
#     range=[-110, 220],  # Adjust the range for the left y-axis
#     title_text="GW",    # Set title for the left y-axis
#     secondary_y=False   # Specifies this is for the primary (left) y-axis
# )
# 
# fig.update_yaxes(
#     range=[0, 100],     # Adjust the range for the right y-axis
#     title_text="TWh",   # Set title for the right y-axis
#     secondary_y=True    # Specifies this is for the secondary (right) y-axis
# )
# =============================================================================

# Final layout adjustments
fig.update_layout(
    height=400,
    width=1350,
    font_size=10,
    template="simple_white",
    margin=dict(l=0, r=0, t=0, b=0)
)

#fig.write_html('.\\result_plots\\capacities_p_abs_cal.html')
fig.show()



# %% demand time series: coping with the DF

# Load data
path_dem = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-10-31_demand_all.csv"
dem = pd.read_csv(path_dem)
dem = dem.query('custom_flow == "island-eh" and custom_year not in ["2016-2017", "2017-2018"]')
dem = dem[['n', 'h', 'value', 'custom_year']]
dem_cp = dem.groupby(['h', 'custom_year'])['value'].sum().reset_index()
dem_cp['n'] = 'CP'

dem = pd.concat([dem, dem_cp])
# Filter data for specific regions and years
dem = dem.query('n in ["CP", "DE", "FR", "ES", "NL", "PL", "RO", "UK"]').reset_index(drop=True)
# dem = dem.sort_values(by=['n', 'custom_year', 'h'])

# Compute climatological mean and variance by 'n' and 'h'
climatological_stats = dem.groupby(['n', 'h']).agg(
    climatological_mean=('value', 'mean'),
    climatological_variance=('value', 'var'),
    climatological_min=('value', 'min'),
    climatological_max=('value', 'max'),
    ).reset_index()

# Calculate standard deviation from the variance
climatological_stats['std_dev'] = np.sqrt(climatological_stats['climatological_variance'])

# Convert 'h' from "t00001" to integer
climatological_stats['h'] = climatological_stats['h'].str.replace("t", "").astype(int)

# Map 'h' to dates starting from July 1, 2000
start_date = datetime.datetime(2000, 7, 1)
climatological_stats['date'] = climatological_stats['h'].apply(lambda x: start_date + datetime.timedelta(hours=x - 1))

# Define the rolling window size
window_size = 168
window_size_min_max = 24

# Compute the ordinary trailing rolling mean and standard deviation, dropping NaN values
climatological_stats['rolling_mean'] = climatological_stats.groupby('n')['climatological_mean'].transform(
    lambda x: x.rolling(window=window_size, min_periods=window_size, center=False).mean()
)
climatological_stats['rolling_std_dev'] = climatological_stats.groupby('n')['std_dev'].transform(
    lambda x: x.rolling(window=window_size, min_periods=window_size, center=False).mean()
)
# Compute the ordinary trailing rolling mean and standard deviation, dropping NaN values
climatological_stats['climatological_min_rolling_mean'] = climatological_stats.groupby('n')['climatological_min'].transform(
    lambda x: x.rolling(window=window_size_min_max, min_periods=window_size_min_max, center=False).mean()
)
# Compute the ordinary trailing rolling mean and standard deviation, dropping NaN values
climatological_stats['climatological_max_rolling_mean'] = climatological_stats.groupby('n')['climatological_max'].transform(
    lambda x: x.rolling(window=window_size_min_max, min_periods=window_size_min_max, center=False).mean()
)
# Drop rows where rolling mean or std dev is NaN (first `window_size - 1` rows in each group)
# climatological_stats = climatological_stats.dropna(subset=['rolling_mean', 'rolling_std_dev'])

# Calculate the upper and lower bounds for the shaded area
climatological_stats['upper_bound_rolling'] = climatological_stats['rolling_mean'] + climatological_stats['rolling_std_dev']
climatological_stats['lower_bound_rolling'] = climatological_stats['rolling_mean'] - climatological_stats['rolling_std_dev']

# Define the custom order for regions (n)
custom_order = ["CP", "DE", "ES", "FR", "NL", "PL", "RO", "UK"]
climatological_stats['n'] = pd.Categorical(climatological_stats['n'], categories=custom_order, ordered=True)

# Sort by the custom order of 'n' for the facet rows
climatological_stats = climatological_stats.sort_values(by=['n', 'h'])

yaxis_ranges = {}
max_diff = 0
max_diff_region = None
for region in custom_order:
    region_data = climatological_stats[climatological_stats['n'] == region]
    
    # Find the minimum of the lower bound and maximum of the upper bound
    min_lower_bound = region_data['lower_bound_rolling'].min()
    max_upper_bound = region_data['upper_bound_rolling'].max()
    relative_difference = (max_upper_bound - min_lower_bound) / max_upper_bound
        
    min_mean = region_data['rolling_mean'].min()
    max_mean = region_data['rolling_mean'].max()
    relative_difference_mean = (max_mean - min_mean) / max_mean

    if relative_difference_mean > max_diff:
        max_diff = relative_difference_mean
        max_diff_region = region
    
    yaxis_ranges[region] = [min_lower_bound, max_upper_bound, relative_difference, relative_difference_mean]

# Define the number of regions for facet rows
num_regions = len(custom_order)

# Create a subplot with one column and `num_regions` rows
fig = make_subplots(rows=num_regions, cols=1, shared_xaxes=True, vertical_spacing=0.02)
                    #subplot_titles=custom_order)  # Reverse order for correct top-to-bottom arrangement


# Add rolling mean line and shaded area for each region
for idx, region in enumerate(custom_order):  # [::-1]):
    region_data = climatological_stats[climatological_stats['n'] == region]
    
    # Create the x and y coordinates for the shaded area around the rolling mean
    x_coords = region_data['date'].tolist() + region_data['date'][::-1].tolist()
    y_coords = region_data['upper_bound_rolling'].tolist() + region_data['lower_bound_rolling'][::-1].tolist()
    
    # Add the shaded area for rolling mean ± rolling std deviation
    fig.add_trace(
        go.Scatter(
            x=x_coords,
            y=y_coords,
            fill='toself',
            fillcolor='#72a99e',  # Increased opacity for visibility
            line=dict(color='rgba(255,255,255,0)'),  # No border line for the shaded area
            showlegend=False,
            name='Rolling Mean ± 1 Std Dev'
        ),
        row=idx + 1, col=1
    )
    
    # Create the x and y coordinates for the shaded area of climatological min and max
    x_coords = region_data['date'].tolist() + region_data['date'][::-1].tolist()
    y_coords = region_data['climatological_max_rolling_mean'].tolist() + region_data['climatological_min_rolling_mean'][::-1].tolist()
    
    # Add the shaded area for rolling mean ± rolling std deviation
    fig.add_trace(
        go.Scatter(
            x=x_coords,
            y=y_coords,
            fill='toself',
            fillcolor='rgba(0,100,80,0.25)',  # Increased opacity for visibility
            line=dict(color='rgba(255,255,255,0)'),  # No border line for the shaded area
            showlegend=False,
            name='Rolling Mean ± 1 Std Dev'
        ),
        row=idx + 1, col=1
    )
    
    # Add rolling mean line
    fig.add_trace(
        go.Scatter(
            x=region_data['date'],
            y=region_data['rolling_mean'],
            mode='lines',
            line=dict(color="black", width=1),  # Black and thinner
            showlegend=False,
            name='Rolling Mean'
        ),
        row=idx + 1, col=1
    )
    
    # set y range to show maximum relative range across all regions
    min_y = yaxis_ranges[region][0] - yaxis_ranges[region][0]*0.15
    max_y = yaxis_ranges[region][0] + yaxis_ranges[region][1] * (max_diff + 0.42)
    
    fig.update_yaxes(range=[min_y, max_y], row=idx + 1, col=1)

    max_diff_region = int(yaxis_ranges[region][3] * 100)
    
    fig.add_annotation(
        xref="x domain",
        yref="y domain",
        x=0.996,
        y=0.95,
        text=f"<b>{region} ({max_diff_region}%)</b>",  # Bold text for region name
        showarrow=False,
        font=dict(size=12),
        row=idx + 1, col=1
    )

# Update x-axis settings
#fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))

for axis in fig.layout:
    if axis.startswith("yaxis") and axis != "yaxis":
        fig.layout[axis].title.text = ""

fig.update_xaxes(
    title_text='',
    # range=[datetime.datetime(2000, 7, 1), datetime.datetime(2001, 7, 1)],
    mirror=True,
    dtick="M1",
    tickformat="%b",
)

fig.update_layout(
    yaxis1=dict(title=dict(text='demand [GW]', font=dict(size=12), standoff=8), mirror=True, tickfont=dict(size=8)),  # automargin=True),
    yaxis2=dict(title=dict(text='demand [GW]', font=dict(size=12), standoff=8), mirror=True, tickfont=dict(size=8)),  # automargin=True),
    yaxis3=dict(title=dict(text='demand [GW]', font=dict(size=12), standoff=8), mirror=True, tickfont=dict(size=8)),  # automargin=True),
    yaxis4=dict(title=dict(text='demand [GW]', font=dict(size=12), standoff=8), mirror=True, tickfont=dict(size=8)),  # automargin=True),
    yaxis5=dict(title=dict(text='demand [GW]', font=dict(size=12), standoff=8), mirror=True, tickfont=dict(size=8)),  # automargin=True),
    yaxis6=dict(title=dict(text='demand [GW]', font=dict(size=12), standoff=8), mirror=True, tickfont=dict(size=8)),  # automargin=True),
    yaxis7=dict(title=dict(text='demand [GW]', font=dict(size=12), standoff=8), mirror=True, tickfont=dict(size=8)),  # automargin=True),
    yaxis8=dict(title=dict(text='demand [GW]', font=dict(size=12), standoff=8), mirror=True, tickfont=dict(size=8)),  # automargin=True),
    )

# Update layout for each region
fig.update_layout(
    xaxis_title="",
    showlegend=True,
    template='simple_white',
    margin=dict(l=0, r=0, t=30, b=0),
    height=1000, width=1240
)

fig.show()

path_output = os.path.join("I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_plots\\demand_seasonality")

fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')
fig.write_image(f'{path_output}.png', scale=3)



# %% box plots H2_N_STO_E for demand scenarios: 2009 | yearly (coping with the DF)

import pandas as pd
import plotly.express as px
import os

# Load your data
# import storage data
# path = "S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_data\\2024-10-31_capacity_agg.csv"
path = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-11-02_n_sto_e_agg.csv"
h2_n_sto_e = pd.read_csv(path)

# Define your scenario mappings with <br> for line breaks
scens = {
    "island-eh": "(1) electricity: island<br>hydrogen: island",
    "TYNDP-e/island-h": "(2) electricity: TYNDP<br>hydrogen: island",
    "TYNDP-eh": "(3) electricity: TYNDP<br>hydrogen: TYNDP",
    "copperplate-eh": "(4) electricity: copperplate<br>hydrogen: copperplate"
}

# Replace 'custom_flow' column values based on the mappings with HTML line breaks
data_figure = h2_n_sto_e.replace({"custom_flow": scens})

# Define the category order with HTML line breaks to match the replaced values
category_order_with_breaks = [
    "(1) electricity: island<br>hydrogen: island",
    "(2) electricity: TYNDP<br>hydrogen: island",
    "(3) electricity: TYNDP<br>hydrogen: TYNDP",
    "(4) electricity: copperplate<br>hydrogen: copperplate"
]

# Create the box plot with Plotly
fig = px.box(
    data_figure,
    x='custom_flow',
    y="value",
    points="all",
    boxmode="overlay",
    facet_col_spacing=0.05,
    template="simple_white",
    height=600, width=1000,
    hover_data=["custom_year"],
    category_orders={"custom_flow": category_order_with_breaks},
    color_discrete_sequence=["gray"]
)

# Update axis properties for styling
fig.update_xaxes(type="category", title=None, tickfont=dict(size=16))
fig.update_yaxes(
    title=dict(
        text="storage energy [TWh]",
        standoff=20,
        font=dict(size=18),
    ),
    tickfont=dict(size=14),
)

fig.update_layout(
    font=dict(
        family="Times New Roman",  # Set the font for the entire plot
    ),
    margin=dict(l=0, r=5, t=5, b=20),
)

# Add borders to the plot area
fig.update_xaxes(showline=True, mirror=True, linecolor='black', linewidth=1)
fig.update_yaxes(showline=True, mirror=True, linecolor='black', linewidth=1)

# Show the figure
fig.show()

# Define output paths and save the figure
path_output = os.path.join('I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_plots\\box_plot_h2_n_sto_e_yearly')
fig.write_html(f'{path_output}_all_years_all_countries.html')
fig.write_image(f'{path_output}_all_years_all_countries.pdf')
fig.write_image(f'{path_output}_all_years_all_countries.png', scale=3)

# %% confetti plot: coping with the DF

import pandas as pd
import plotly.graph_objects as go
import numpy as np
import os
import plotly.express as px

# Load your data
path = "I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-11-02_n_sto_e_agg.csv"
h2_n_sto_e = pd.read_csv(path)

# Define scenario mappings with <br> for line breaks in x-axis labels
scens = {
    "island-eh": "(1) electricity: island<br>hydrogen: island",
    "TYNDP-e/island-h": "(2) electricity: TYNDP<br>hydrogen: island",
    "TYNDP-eh": "(3) electricity: TYNDP<br>hydrogen: TYNDP",
    "copperplate-eh": "(4) electricity: copperplate<br>hydrogen: copperplate"
}

# Replace 'custom_flow' column values with formatted labels
data_figure = h2_n_sto_e.replace({"custom_flow": scens})

# Define the category order with formatted labels
category_order_with_breaks = [
    "(1) electricity: island<br>hydrogen: island",
    "(2) electricity: TYNDP<br>hydrogen: island",
    "(3) electricity: TYNDP<br>hydrogen: TYNDP",
    "(4) electricity: copperplate<br>hydrogen: copperplate"
]

# Define custom color scale
custom_cont_color_scale = [
    '#440154', '#30678D', '#208F8C', '#35B779', '#B2D335',
    '#E3CF21', '#E69500', '#A00000'
]

# Select only rows from the leftmost scenario and sort by 'value'
data_figure_left = data_figure.query("custom_flow == '(1) electricity: island<br>hydrogen: island'")
data_figure_left = data_figure_left.sort_values(by='value').reset_index(drop=True)

# Generate 34 colors from the custom color scale
num_colors_needed = len(data_figure_left)
colors = px.colors.sample_colorscale(custom_cont_color_scale, [i / (num_colors_needed - 1) for i in range(num_colors_needed)])

# Assign colors based on the sorted 'value' order
data_figure_left['color'] = colors
data_figure_left = data_figure_left[['custom_year', 'color']]

# Merge color information back to the full data_figure DataFrame
data_figure = pd.merge(data_figure, data_figure_left, on=['custom_year'], how='left')

# Map each 'custom_flow' category to a numerical value for adding random horizontal jitter
category_mapping = {category: i for i, category in enumerate(category_order_with_breaks)}
data_figure['x_numeric'] = data_figure['custom_flow'].map(category_mapping)

# Add a small random noise to the x-coordinate for horizontal jitter and create x_jittered column
np.random.seed(0)  # Seed for reproducibility
data_figure['x_jittered'] = data_figure['x_numeric'] + np.random.uniform(-0.1, 0.1, size=len(data_figure))

# Create the scatter plot using Plotly Graph Objects with colors specified in 'color' column
fig = go.Figure()

# Add a scatter trace using the custom colors from the "color" column
fig.add_trace(go.Scatter(
    x=data_figure['x_jittered'],
    y=data_figure['value'],
    mode='markers',
    marker=dict(
        color=data_figure['color'],  # Use the custom colors specified in the "color" column
        size=8  # Adjust marker size as needed
    ),
    text=data_figure['custom_year'],  # Display custom_year on hover
    hovertemplate="Year: %{text}<br>Value: %{y}<extra></extra>"
))

fig.update_layout(
    template="simple_white",
    height=600,
    width=1000,
    xaxis=dict(
        tickvals=list(category_mapping.values()), 
        ticktext=category_order_with_breaks,
        title=None,
        tickfont=dict(size=16),
        showline=True, mirror=True, linecolor='black', linewidth=1,
        tickangle=0
    ),
    yaxis=dict(
        title=dict(text="storage energy [TWh]", standoff=20, font=dict(size=18)),
        tickfont=dict(size=14),
        showline=True, mirror=True, linecolor='black', linewidth=1
    ),
    margin=dict(l=0, r=20, t=5, b=0)
)

fig.update_layout(font=dict(family="Times New Roman"))

# Show the plot
fig.show()

# Define output paths and save the figure
path_output = os.path.join('I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_plots\\scatter_plot_h2_n_sto_e_yearly_confetti')
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')
fig.write_image(f'{path_output}.png', scale=3)




# %% combined box plots H2_N_STO_E for demand scenarios: 2009 & yearly: coping with the DF

# import storage data
path = "S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_data\\2024-10-31_capacity_agg.csv"
h2_n_sto_e_2009 = pd.read_csv(path)
h2_n_sto_e_2009 = h2_n_sto_e_2009.query('aux_info == "energy" and tech == "cavern"')
h2_n_sto_e_2009 = h2_n_sto_e_2009.rename(columns={'value': 'energy', 'custom_year': 'time_frame'})
h2_n_sto_e_2009 = h2_n_sto_e_2009.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
h2_n_sto_e_2009['plot_identifyer'] = 'fixed demand'
path ="I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-10-31_capacity_agg.csv"
h2_n_sto_e_yearly = pd.read_csv(path)
h2_n_sto_e_yearly = h2_n_sto_e_yearly.query('aux_info == "energy" and tech == "cavern"')
h2_n_sto_e_yearly = h2_n_sto_e_yearly.rename(columns={'value': 'energy', 'custom_year': 'time_frame'})
h2_n_sto_e_yearly = h2_n_sto_e_yearly.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
h2_n_sto_e_yearly['plot_identifyer'] = 'yearly demand'
path ="I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_8\\result_data\\2024-11-18_capacity_agg.csv"
h2_n_sto_e_nuc = pd.read_csv(path)
h2_n_sto_e_nuc = h2_n_sto_e_nuc.query('aux_info == "energy" and tech == "cavern"')
h2_n_sto_e_nuc = h2_n_sto_e_nuc.rename(columns={'value': 'energy', 'custom_year': 'time_frame'})
h2_n_sto_e_nuc = h2_n_sto_e_nuc.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
h2_n_sto_e_nuc['plot_identifyer'] = 'yearly demand with nuclear power'

data_figure = pd.concat([h2_n_sto_e_2009, h2_n_sto_e_yearly, h2_n_sto_e_nuc])

fig = px.box(
    data_figure,
    x='plot_identifyer',
    y="energy",
    color='plot_identifyer',
    facet_col="custom_flow",
    points="all",
    boxmode="overlay",
    facet_col_spacing=0.05,
    template="simple_white",
    height=600, width=1000,
    hover_data="time_frame",
    category_orders={"custom_flow":["island-eh", "TYNDP-e/island-h", "TYNDP-eh", "copperplate-eh"]}
    )
fig.update_xaxes(type="category", title=None, tickfont=dict(size=16)) #automargin=True, title_standoff=0
fig.update_yaxes(
    title=dict(
        text="storage energy [TWh]",
        standoff=20,
        font=dict(size=18),
    ),
    tickfont=dict(size=14),
)

fig.update_layout(
    font=dict(
        family="Times New Roman",  # Set the serif font for the entire plot
        #size=14  # You can adjust the font size here
    ),
    margin=dict(l=50, r=0, t=0, b=0),
    #boxmode='group', 
    #boxgroupgap=0,
    #boxgap=0
)

fig.update_xaxes(
    mirror=True,  # Mirrors the x-axes
    showline=True,  # Ensures the axis line is visible
    linewidth=1,  # Adjust line thickness
    linecolor="black",  # Set line color
)

# Update all y-axes
fig.update_yaxes(
    mirror=True,  # Mirrors the y-axes
    showline=True,  # Ensures the axis line is visible
    linewidth=1,  # Adjust line thickness
    linecolor="black",  # Set line color
)

# Remove y-axis titles for all subplots except the leftmost one
for i, axis in enumerate(fig.layout):
    if "yaxis" in axis and i != 2:  # Check if it's a y-axis and not the first one
        fig.layout[axis].title.text = None


fig.show()

path_output = os.path.join('S:\\projekte_paper\\power-sector-droughts_net_drive_5\\result_plots\\box_plot_h2_n_sto_e',
                           '2009_yearly_nuc')

fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')
fig.write_image(f'{path_output}.png', scale=3)

# %% combined scatter plots H2_N_STO_E for demand scenarios: 2009 & yearly: coping with the DF

# import storage data
path = "S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_data\\2024-10-31_capacity_agg.csv"
h2_n_sto_e_2009 = pd.read_csv(path)
h2_n_sto_e_2009 = h2_n_sto_e_2009.query('aux_info == "energy" and tech == "cavern"')
h2_n_sto_e_2009 = h2_n_sto_e_2009.rename(columns={'value': 'energy', 'custom_year': 'time_frame'})
h2_n_sto_e_2009 = h2_n_sto_e_2009.query('time_frame != "2016-2017" and time_frame != "2017-2018"')
path ="I:\\TESLA\\dieter-vre\\power-sector-droughts-2_net_drive_5\\result_data\\2024-10-31_capacity_agg.csv"
h2_n_sto_e_yearly = pd.read_csv(path)
h2_n_sto_e_yearly = h2_n_sto_e_yearly.query('aux_info == "energy" and tech == "cavern"')
h2_n_sto_e_yearly = h2_n_sto_e_yearly.rename(columns={'value': 'energy', 'custom_year': 'time_frame'})
h2_n_sto_e_yearly = h2_n_sto_e_yearly.query('time_frame != "2016-2017" and time_frame != "2017-2018"')

data_figure = pd.concat([h2_n_sto_e_2009,h2_n_sto_e_yearly])

fig = px.scatter(
    data_figure,
    x='custom_flow',
    y="energy",
    color='dem_scenario',
    facet_col="dem_scenario",
    points="all",
    boxmode="overlay",
    facet_col_spacing=0.05,
    template="simple_white",
    height=600, width=1000,
    hover_data="time_frame",
    category_orders={"custom_flow":["island-eh", "TYNDP-e/island-h", "TYNDP-eh", "copperplate-eh"]}
    )
fig.update_xaxes(type="category", title=None, tickfont=dict(size=16)) #automargin=True, title_standoff=0
fig.update_yaxes(
    title=dict(
        text="storage energy [TWh]",
        standoff=20,
        font=dict(size=18),
    ),
    tickfont=dict(size=14),
)

fig.update_layout(
    font=dict(
        family="Times New Roman",  # Set the serif font for the entire plot
        #size=14  # You can adjust the font size here
    ),
    margin=dict(l=0, r=0, t=0, b=0),
    #boxmode='group', 
    #boxgroupgap=0,
    #boxgap=0
)

fig.show()

path_output = os.path.join('S:\\projekte_paper\\power-sector-droughts_net_drive_4\\result_plots\\box_plot_h2_n_sto_e',
                           '2009_yearly')

fig.write_html(f'{path_output}_all_years_all_countries.html')  # _filter_1TWh_regr_filter_ALNO
fig.write_image(f'{path_output}_all_years_all_countries.pdf')
fig.write_image(f'{path_output}_all_years_all_countries.png', scale=3)


# %%########################## most extreme periods ####################################################################

# %% get most extreme droughts & average change in duration

max_period_yearly, max_period_of_all_years = get_max_duration_yearly(region_results)
max_period_monthly = get_max_duration_monthly(region_results, 'end')
max_period_monthly_mid = get_max_duration_monthly(region_results, 'mid')

#%%

datasets = [max_period_yearly, max_period_of_all_years, max_period_monthly, max_period_monthly_mid]
datasets_names = ['max_period_yearly', 'max_period_of_all_years', 'max_period_monthly', 'max_period_monthly_mid'] 

path = 'T:\\mk\\vreda\\output\\2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete'
os.chdir(path)

for idx, d in enumerate(datasets):
    file_name = f'{datasets_names[idx]}.gz'
    with gzip.open(file_name, 'wb') as file:
        pickle.dump(d, file, protocol=pickle.HIGHEST_PROTOCOL)

#%% 

path = 'T:\\mk\\vreda\\output\\2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete'
max_period_yearly = load_object(path, 'max_period_yearly.gz')
max_period_of_all_years = load_object(path, 'max_period_of_all_years.gz')
max_period_monthly = load_object(path, 'max_period_monthly.gz')
max_period_monthly_mid = load_object(path, 'max_period_monthly_mid.gz')



#%%

max_period_portfolio = max_period_of_all_years.query('technology == "portfolio"')
max_period_portfolio_count = {}
for f, group in max_period_portfolio.groupby('fraction'):
    df = group['year'].value_counts()
    df = df.reset_index(drop=False)
    df.columns = ['year', f]
    max_period_portfolio_count[f] = df

max_period_portfolio_count = reduce(lambda left, right: pd.merge(left, right, on=['year'], how='outer'),
                                    max_period_portfolio_count.values())
max_period_portfolio_count['sum'] = max_period_portfolio_count.iloc[:, 1:].sum(axis=1)
max_period_portfolio_count = max_period_portfolio_count.sort_values(0.2, ascending=False)

max_period_eu_portfolio = max_period_of_all_years.query('region == "EU" and technology == "portfolio"')


flh = load_object('T:\\mk\\vreda\\input', 'flh.gz')
path_config = 'T:\\mk\\vreda\\config\\config.yaml'
change_tech, change_tech_region, change_tech_region_thres, change_tech_cp, change_tech_cp_thres = percentual_reduction_portfolio(max_period_yearly, 'portfolio', path_config, flh)
change_tech, change_tech_region, change_tech_region_thres = percentual_reduction_balancing(max_period_yearly, 'CP', path_config, flh)

change_tech_region_thres_long = []
for tech, df in change_tech_region_thres.items():
    df = df.unstack()
    df.name = 'change'
    df = df.reset_index()
    df['technology'] = tech
    change_tech_region_thres_long.append(df)
change_tech_region_thres_long = pd.concat(change_tech_region_thres_long)


#%% 3d plot most extreme period for one region for all years

techs = ['pv', 'wind_onshore', 'portfolio'] #'wind_offshore',
regions = ['DE', 'ES', 'EU']

for tech in techs:
    for region in regions:

        data = max_period_yearly.query('technology == @tech and region == @region').copy()
        data_wide = data.pivot(index='year', columns='fraction', values='max_duration')
        data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
        data_wide_y = data_wide.columns.to_numpy()
        data_wide_z = data_wide.T.values / 24
        max_z = np.nanmax(data_wide_z)
        rounded_z = math.ceil(max_z / 50) * 50
        
        full_days = np.floor(data_wide_z)
        hours = ((data_wide_z - full_days) * 24).round(1)
        
        custom_data_shape = full_days.shape
        custom_data = np.empty(custom_data_shape, dtype=object)  # Create an empty array of objects

        for i in range(custom_data_shape[0]):
            for j in range(custom_data_shape[1]):
                custom_data[i, j] = [full_days[i, j], hours[i, j]]
        custom_data = custom_data.T

        fig = go.Figure()

        fig.add_trace(
            go.Surface(
                x=data_wide_x,
                y=data_wide_y,
                z=data_wide_z,
                colorscale=custom_cont_color_scale,
                customdata=custom_data,
                opacity=0.91,
                hovertemplate='year: %{x}<br>' + 'threshold: %{y}<br>' + 'duration: %{customdata[0]} days <br>' \
                    + '%{customdata[1]:.1f} hours<br>' + '<extra></extra>',
                contours={"y":{"show": True, "start":0, "end":1, "size": 0.05, "color":"white"},
                          #"x":{"show": True, "start":1, "end":rounded_z+1, "size":rounded_z/5, "color":"white"},
                          },
                colorbar=dict(title='maximum duration [d]', titleside='right', len=0.7, thickness=30),
                )
            )
        
        #fig.update_traces(contours_z=dict(show=True, usecolormap=True,highlightcolor="limegreen", project_z=True))
        
        tech_clean = tech.replace('_', ' ')
        
        fig.update_layout(
            template='plotly_white',
            scene=dict(
                xaxis=dict(title=''),
                yaxis=dict(title='relative threshold', range=[0.1, 1], dtick=0.1),
                zaxis=dict(title='', range=[-1, rounded_z], dtick=rounded_z/10),
                aspectmode='manual',  # 'auto', 'cube', 'data', 'manual' are the options
                aspectratio=dict(x=1, y=1, z=1.1), 
                camera=dict(eye=dict(x=1.5, y=-1.7, z=0.2))
            ),
            width=600,
            height=600,
            margin=dict(r=00, b=70, l=00, t=00)
        )
        
        fig.show()
        
        
        path = path = 'T:\\mk\\vreda\\result_plots\\'
        path_output = os.path.join(path, 'max_duration_yearly_surface')
        Path(path_output).mkdir(parents=True, exist_ok=True)
        path_file = os.path.join(path_output, f'max_duration_surface_{region}_{tech}.html')
        fig.write_html(path_file)
        path_file = os.path.join(path_output, f'max_duration_surface_{region}_{tech}.pdf')
        fig.write_image(path_file, format='pdf', width=600, height=600, scale=3)
        path_file = os.path.join(path_output, f'max_duration_surface_{region}_{tech}.png')
        fig.write_image(path_file, format='png', width=600, height=600, scale=3)

#%% 3d plot most extreme period in each year for three regions and three techs for all years, cmobined 3x3 plot

techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'portfolio': 'portfolio'}  # Mapping tech codes to names
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}

fig = make_subplots(
    rows=3, cols=3,
    specs=[[{'type': 'surface'}]*3]*3,
    horizontal_spacing=0,
    vertical_spacing=0
)

annotations = []

max_val = 0

for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
    for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
        data = max_period_yearly.query('technology == @tech_code and region == @region_code').copy()
        data_wide = data.pivot(index='year', columns='fraction', values='max_duration')
        data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
        data_wide_y = data_wide.columns.to_numpy()
        data_wide_z = data_wide.T.values / 24
        max_z = np.nanmax(data_wide_z)
        rounded_z = math.ceil(max_z / 50) * 50
        
        if max_z > max_val:
            max_val = max_z

        full_days = np.floor(data_wide_z)
        hours = ((data_wide_z - full_days) * 24).round(1)

        custom_data_shape = full_days.shape
        custom_data = np.empty(custom_data_shape, dtype=object)  # Create an empty array of objects

        for i in range(custom_data_shape[0]):
            for j in range(custom_data_shape[1]):
                custom_data[i, j] = [full_days[i, j], hours[i, j]]
        custom_data = custom_data.T

        fig.add_trace(
            go.Surface(
                x=data_wide_x,
                y=data_wide_y,
                z=data_wide_z,
                colorscale=custom_cont_color_scale,
                customdata=custom_data,
                hovertemplate='year: %{x}<br>' + 'threshold: %{y}<br>' + 'duration: %{customdata[0]} days <br>' \
                              + '%{customdata[1]:.1f} hours<br>' + '<extra></extra>',
                contours={"y": {"show": True, "start": 0, "end": 1, "size": 0.05, "color": "white"},
                          # "x":{"show": True, "start":1, "end":rounded_z+1, "size":rounded_z/5, "color":"white"},
                          },
                coloraxis='coloraxis',  # Link the trace to the shared color axis
            ),
            row=row_idx, col=col_idx
        )

# Add annotations for region names at the top of each column
for col_idx, region_name in enumerate(regions.values(), start=1):
    annotations.append(
        dict(
            text=region_name,
            x=(col_idx - 1) / 3 + 1 / 6,
            y=1 - 0.03,  # Position above the first row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=30),
            xanchor='center'
        )
    )

# Add annotations for technology names to the right of each row
for row_idx, tech_name in enumerate(techs.values(), start=1):
    annotations.append(
        dict(
            text=tech_name,
            x=1,  # Position to the right of the last column
            y=1 - (row_idx - 1) / 3 - 1 / 6,
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=30),
            textangle=90,
            yanchor='middle'
        )
    )
   
# add y axis title
for col_idx in range(1, 4):
    annotations.append(
        dict(
            text='relative threshold',
            x=(col_idx - 1) / 3 + 1 / 6 + 0.12,
            y=-0.02,  # Position below the third row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=23),
            xanchor='center',
            textangle=-38
        )
    )

# Configure the shared color axis and colorbar
fig.update_layout(
    template='plotly_white',
    coloraxis=dict(
        colorscale=custom_cont_color_scale,
        cmin=0,
        cmax=max_val,
        colorbar=dict(
            title='maximum duration [d]',
            titleside='top',  # Keep the title on the right
            titlefont=dict(size=24),
            tickfont=dict(size=20),
            lenmode='fraction',
            len=1,
            thickness=30,
            orientation='h',
            x=0.5,
            xanchor='center',
            y=-0.1,  # Position the colorbar below the plot
        )
    ),
    width=1800,
    height=1800, 
    margin=dict(l=0, r=0, t=0, b=0),  # l=80, r=50, t=50, b=100
    annotations=annotations  # Add annotations for subplot titles
)

scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 'scene7', 'scene8', 'scene9']

# Adjust axes settings for all subplots
for i, scene_id in enumerate(scenes):
    col_idx = (i % 3) + 1
    row_idx = (i // 3) + 1
    fig.update_layout(**{scene_id: dict(
        xaxis=dict(title='', range=[1981, 2019.9], tickfont=dict(size=14.7)),
        yaxis=dict(title='', range=[0.07, 1], dtick=0.2, tickfont=dict(size=16)),
        zaxis=dict(title='maximum duration [d]' if col_idx == 1 else '', range=[-5, rounded_z], dtick=rounded_z / 10, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=15)),
        aspectmode='manual',
        aspectratio=dict(x=1.29, y=1, z=1.1),
        camera=dict(eye=dict(x=1.5, y=-1.7, z=0.2))
    )})

fig.show()

regions_str = "_".join(regions.keys())

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'max_duration_yearly_surface')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'max_duration_surface_{regions_str}_3_tech.html')
fig.write_html(path_file)
path_file = os.path.join(path_output, f'max_duration_surface_{regions_str}_3_tech.pdf')
fig.write_image(path_file, format='pdf', width=1800, height=1800, scale=3)
path_file = os.path.join(path_output, f'max_duration_surface_{regions_str}_3_tech.png')
fig.write_image(path_file, format='png', width=1800, height=1800, scale=3)

#%% 3d plot most extreme period in each year for three regions and four techs for all years, combined 4x3 plot

# Define technology and region mappings
techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'wind_offshore': 'wind offshore', 'portfolio': 'portfolio'}
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}

# Create subplots with an extra row for wind offshore
fig = make_subplots(
    rows=4, cols=3,
    specs=[[{'type': 'surface'}]*3]*4,
    horizontal_spacing=0,
    vertical_spacing=0
)

annotations = []

max_val = 0

# Update loop to include the new row for wind offshore
for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
    for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
        data = max_period_yearly.query('technology == @tech_code and region == @region_code').copy()
        data_wide = data.pivot(index='year', columns='fraction', values='max_duration')
        data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
        data_wide_y = data_wide.columns.to_numpy()
        data_wide_z = data_wide.T.values / 24
        max_z = np.nanmax(data_wide_z)
        rounded_z = math.ceil(max_z / 50) * 50
        
        if max_z > max_val:
            max_val = max_z

        full_days = np.floor(data_wide_z)
        hours = ((data_wide_z - full_days) * 24).round(1)

        custom_data_shape = full_days.shape
        custom_data = np.empty(custom_data_shape, dtype=object)  # Create an empty array of objects

        for i in range(custom_data_shape[0]):
            for j in range(custom_data_shape[1]):
                custom_data[i, j] = [full_days[i, j], hours[i, j]]
        custom_data = custom_data.T

        fig.add_trace(
            go.Surface(
                x=data_wide_x,
                y=data_wide_y,
                z=data_wide_z,
                opacity=0.95,
                colorscale=custom_cont_color_scale,
                customdata=custom_data,
                hovertemplate='year: %{x}<br>' + 'threshold: %{y}<br>' + 'duration: %{customdata[0]} days <br>' \
                              + '%{customdata[1]:.1f} hours<br>' + '<extra></extra>',
                contours={"y": {"show": True, "start": 0, "end": 1, "size": 0.05, "color": "white"},
                          # "x":{"show": True, "start":1, "end":rounded_z+1, "size":rounded_z/5, "color":"white"},
                          },
                coloraxis='coloraxis',  # Link the trace to the shared color axis
            ),
            row=row_idx, col=col_idx
        )
            
# =============================================================================
#         # Find the years with the highest and lowest z-values for y=0.75
#         if 0.75 in data_wide_y:
#             y_index = np.where(data_wide_y == 0.75)[0][0]
#             z_values = data_wide_z[y_index, :]
#             x_values = data_wide_x
# 
#             max_z_value = np.nanmax(z_values)
#             min_z_value = np.nanmin(z_values)
# 
#             max_x_value = x_values[np.nanargmax(z_values)]
#             min_x_value = x_values[np.nanargmin(z_values)]
# 
#             fig.add_trace(
#                 go.Scatter3d(
#                     x=[max_x_value, min_x_value],
#                     y=[0.75, 0.75],
#                     z=[max_z_value, min_z_value],
#                     mode='markers+text',
#                     marker=dict(size=8, color='palevioletred', symbol='circle'),
#                     text=[f'{max_z_value:.0f}', f'{min_z_value:.0f}'],
#                     textposition='top left',
#                     textfont=dict(color='palevioletred', size=18)
#                 ),
#                 row=row_idx, col=col_idx
#             )
# =============================================================================

# Add annotations for region names at the top of each column
for col_idx, region_name in enumerate(regions.values(), start=1):
    annotations.append(
        dict(
            text=region_name,
            x=(col_idx - 1) / 3 + 1 / 6,
            y=1 - 0.03,  # Position above the first row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=30),
            xanchor='center'
        )
    )

# Add annotations for technology names to the right of each row
for row_idx, tech_name in enumerate(techs.values(), start=1):
    annotations.append(
        dict(
            text=tech_name,
            x=1,  # Position to the right of the last column
            y=1 - (row_idx - 1) / 4 - 1 / 8,
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=30),
            textangle=90,
            yanchor='middle'
        )
    )
   
# Add y axis title
for col_idx in range(1, 4):
    annotations.append(
        dict(
            text='relative threshold',
            x=(col_idx - 1) / 3 + 1 / 6 + 0.12,
            y=-0.02,  # Position below the fourth row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=23),
            xanchor='center',
            textangle=-38
        )
    )

# Configure the shared color axis and colorbar
fig.update_layout(
    template='plotly_white',
    coloraxis=dict(
        colorscale=custom_cont_color_scale,
        cmin=0,
        cmax=max_val,
        colorbar=dict(
            title='maximum duration [d]',
            titleside='top',  # Keep the title on the right
            titlefont=dict(size=24),
            tickfont=dict(size=20),
            lenmode='fraction',
            len=1,
            thickness=30,
            orientation='h',
            x=0.5,
            xanchor='center',
            y=-0.1,  # Position the colorbar below the plot
        )
    ),
    showlegend=False,
    width=1800,
    height=2400,  # Increase height to accommodate extra row
    margin=dict(l=0, r=0, t=0, b=0),  # l=80, r=50, t=50, b=100
    annotations=annotations  # Add annotations for subplot titles
)

scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 'scene7', 'scene8', 'scene9', 'scene10', 'scene11', 'scene12']

# Adjust axes settings for all subplots
for i, scene_id in enumerate(scenes):
    col_idx = (i % 3) + 1
    row_idx = (i // 3) + 1
    fig.update_layout(**{scene_id: dict(
        xaxis=dict(title='', range=[1981, 2019.9], tickfont=dict(size=14.7)),
        yaxis=dict(title='', range=[0.07, 1], dtick=0.2, tickfont=dict(size=16)),
        zaxis=dict(title='maximum duration [d]' if col_idx == 1 else '', range=[-5, rounded_z], dtick=rounded_z / 10, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=15)),
        aspectmode='manual',
        aspectratio=dict(x=1.29, y=1, z=1.1),
        camera=dict(eye=dict(x=1.5, y=-1.7, z=0.2))
    )})

fig.show()

regions_str = "_".join(regions.keys())

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'max_duration_yearly_surface')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'max_duration_surface_{regions_str}_4_tech.html')
fig.write_html(path_file)
path_file = os.path.join(path_output, f'max_duration_surface_{regions_str}_4_tech.pdf')
fig.write_image(path_file, format='pdf', width=1800, height=2400, scale=3)
path_file = os.path.join(path_output, f'max_duration_surface_{regions_str}_4_tech.png')
fig.write_image(path_file, format='png', width=1800, height=2400, scale=3)



#%% 3d plot most extreme period in each year for three regions and one tech for all years, combined 1x3 plot

techs = {'wind_offshore': 'Wind Offshore'}  # Only showing wind_offshore
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}
data_orig = [max_period_yearly]  # Example dataset

fig = make_subplots(
    rows=1, cols=3,
    specs=[[{'type': 'surface'}]*3],
    horizontal_spacing=0,
    vertical_spacing=0
)

annotations = []
max_val = 0

for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
    data = max_period_yearly.query('technology == "wind_offshore" and region == @region_code').copy()
    data_wide = data.pivot(index='year', columns='fraction', values='max_duration')
    data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
    data_wide_y = data_wide.columns.to_numpy()
    data_wide_z = data_wide.T.values / 24
    max_z = np.nanmax(data_wide_z)
    rounded_z = math.ceil(max_z / 50) * 50

    if max_z > max_val:
        max_val = max_z

    full_days = np.floor(data_wide_z)
    hours = ((data_wide_z - full_days) * 24).round(1)

    custom_data_shape = full_days.shape
    custom_data = np.empty(custom_data_shape, dtype=object)  # Create an empty array of objects

    for i in range(custom_data_shape[0]):
        for j in range(custom_data_shape[1]):
            custom_data[i, j] = [full_days[i, j], hours[i, j]]
    custom_data = custom_data.T

    fig.add_trace(
        go.Surface(
            x=data_wide_x,
            y=data_wide_y,
            z=data_wide_z,
            colorscale=custom_cont_color_scale,
            customdata=custom_data,
            hovertemplate='year: %{x}<br>' + 'threshold: %{y}<br>' + 'duration: %{customdata[0]} days <br>' \
                          + '%{customdata[1]:.1f} hours<br>' + '<extra></extra>',
            contours={"y": {"show": True, "start": 0, "end": 1, "size": 0.05, "color": "white"}},
            coloraxis='coloraxis',  # Link the trace to the shared color axis
        ),
        row=1, col=col_idx
    )

# Add annotations for region names at the top of each column
for col_idx, region_name in enumerate(regions.values(), start=1):
    annotations.append(
        dict(
            text=region_name,
            x=(col_idx - 1) / 3 + 1 / 6,
            y=1 - 0.1,  # Position above the first row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=30),
            xanchor='center'
        )
    )

# Add y-axis title
for col_idx in range(1, 4):
    annotations.append(
        dict(
            text='relative threshold',
            x=(col_idx - 1) / 3 + 1 / 6 + 0.12,
            y=-0.1,  # Position below the row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=23),
            xanchor='center',
            textangle=-38
        )
    )

# Configure the shared color axis and colorbar
fig.update_layout(
    template='plotly_white',
    coloraxis=dict(
        colorscale=custom_cont_color_scale,
        cmin=0,
        cmax=max_val,
        colorbar=dict(
            title='maximum duration [d]',
            titleside='top',
            titlefont=dict(size=24),
            tickfont=dict(size=20),
            lenmode='fraction',
            len=1,
            thickness=30,
            orientation='h',
            x=0.5,
            xanchor='center',
            y=-0.3,  # Position the colorbar below the plot
        )
    ),
    width=1800,
    height=700, 
    margin=dict(l=0, r=0, t=0, b=0),
    annotations=annotations  # Add annotations for subplot titles
)

scenes = ['scene', 'scene2', 'scene3']

# Adjust axes settings for all subplots
for i, scene_id in enumerate(scenes):
    col_idx = (i % 3) + 1
    fig.update_layout(**{scene_id: dict(
        xaxis=dict(title='', range=[1981, 2019.9], tickfont=dict(size=14.7)),
        yaxis=dict(title='', range=[0.07, 1], dtick=0.2, tickfont=dict(size=16)),
        zaxis=dict(title='maximum duration [d]' if col_idx == 1 else '', range=[-5, rounded_z], dtick=rounded_z / 10, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=15)),
        aspectmode='manual',
        aspectratio=dict(x=1.29, y=1, z=1.1),
        camera=dict(eye=dict(x=1.5, y=-1.7, z=0.2))
    )})

fig.show()

regions_str = "_".join(regions.keys())

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'max_duration_yearly_surface')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'max_duration_surface_{regions_str}_wind_offshore.html')
fig.write_html(path_file)
path_file = os.path.join(path_output, f'max_duration_surface_{regions_str}_wind_offshore.pdf')
fig.write_image(path_file, format='pdf', width=1800, height=600, scale=3)
path_file = os.path.join(path_output, f'max_duration_surface_{regions_str}_wind_offshore.png')
fig.write_image(path_file, format='png', width=1800, height=600, scale=3)
        
#%% 3d plot most extreme period across all years for all regions

techs = ['pv', 'wind_onshore', 'wind_offshore', 'portfolio']
data_orig = [max_period_yearly, max_period_of_all_years]

for tech in techs:
    data = max_period_of_all_years.query('technology == @tech').copy()
    data_wide = data.pivot(index='region', columns='fraction', values='max_duration')
    
    # Sort countries based on the duration for a threshold of 0.75
    if 0.75 in data_wide.columns:
        sorting_values = data_wide[0.75].sort_values(ascending=False)  # Sort in descending order
        data_wide = data_wide.loc[sorting_values.index]  # Reorder the DataFrame
    else:
        print(f"Threshold 0.75 not found in the data for technology {tech}")
        continue
    
    
    data_wide_x = data_wide.index
    data_wide_y = data_wide.columns.to_numpy()
    data_wide_z = data_wide.T.values / 24
    max_z = np.nanmax(data_wide_z)
    rounded_z = math.ceil(max_z / 50) * 50
    
    full_days = np.floor(data_wide_z)
    hours = ((data_wide_z - full_days) * 24).round(1)
    
    custom_data_shape = full_days.shape
    custom_data = np.empty(custom_data_shape, dtype=object)  # Create an empty array of objects

    for i in range(custom_data_shape[0]):
        for j in range(custom_data_shape[1]):
            custom_data[i, j] = [full_days[i, j], hours[i, j]]
    custom_data = custom_data.T

    fig = go.Figure()

    fig.add_trace(
        go.Surface(
            x=np.arange(len(data_wide_x)),
            y=data_wide_y,
            z=data_wide_z,
            colorscale=custom_cont_color_scale,
            customdata=custom_data,
            hovertemplate='country: %{x}<br>' + 'threshold: %{y}<br>' + 'duration: %{customdata[0]} days <br>' \
                + '%{customdata[1]:.1f} hours<br>' + '<extra></extra>',
            contours={"y":{"show": True, "start":0, "end":1, "size": 0.05, "color":"white"},
                      "x":{"show": True, "start":1, "end":len(data_wide_x), "size":1, "color":"white"},
                      },
            colorbar=dict(title='maximum duration [d]', titleside='right', len=0.5, thickness=30),
            )
        )
    
    #fig.update_traces(contours_z=dict(show=True, usecolormap=True,highlightcolor="limegreen", project_z=True))
    
    tech_clean = tech.replace('_', ' ')
    
    fig.update_layout(
        template='plotly_white',
        scene=dict(
            xaxis=dict(title='', tickvals=np.arange(len(data_wide_x)),
            ticktext=data_wide_x, range=[-1, len(data_wide_x) - 1], dtick=1),
            yaxis=dict(title='relative threshold', range=[0.1, 1], dtick=0.1),
            zaxis=dict(title='maximum duration [d]', range=[-1, rounded_z], dtick=rounded_z/10),
            aspectmode='manual',  # 'auto', 'cube', 'data', 'manual' are the options
            aspectratio=dict(x=2.5, y=1, z=1), 
            camera=dict(eye=dict(x=1.5, y=-2.5, z=0.5)) # x=1.5, y=-1.7, z=0.2)
        ),
        width=1300,
        height=900,
        margin=dict(r=00, b=0, l=00, t=00)
    )
    
    fig.show() 
    
    path = path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, 'max_duration_max_year_surface')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'max_duration_surface_all_{tech}.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'max_duration_surface_all_{tech}.pdf')
    fig.write_image(path_file, format='pdf', width=1300, height=900, scale=3)
    path_file = os.path.join(path_output, f'max_duration_surface_all_{tech}.png')
    fig.write_image(path_file, format='png', width=1300, height=900, scale=3)

# %% most extreme single in each year period as 2d scatter

techs = ['pv', 'wind_onshore', 'wind_offshore', 'portfolio']
regions = ['DE', 'ES']
data_orig = [max_period_yearly, max_period_of_all_years]

for tech in techs:
    for region in regions:

        data = max_period_yearly.query('technology == @tech and region == @region').copy()
        data.loc[:, 'max_duration'] /= 24
        
        rounded_max = math.ceil(data.max_duration.max() / 50) * 50
        
        fig = px.scatter(data, x='fraction', y='max_duration', color="year", color_continuous_scale=custom_cont_color_scale)
        fig.update_traces(marker_size=6)
        #, hover_data=['petal_width'])size='petal_length', )
        fig.update_layout(
            yaxis=dict(
                range=[None, rounded_max+1],  # Set the max value of y-axis to 100; keep min value auto-adjusted
                showgrid=True,  # Enable grid lines on the y-axis
                gridwidth=1,  # Set the width of grid lines
            ),
            xaxis=dict(range=[0, 1.01]),
            xaxis_title="threshold (fraction of mean)",  # Alternative way to set the x-axis title
            yaxis_title="maximum duration [d]",
            scattermode="group",
            #scattergap=0.1,
            template='plotly_white',
            width=600,
            height=600,
            )
        fig.show()
        
        path = path = 'T:\\mk\\vreda\\result_plots\\'
        path_output = os.path.join(path, 'max_duration_yearly_2d_scatter')
        Path(path_output).mkdir(parents=True, exist_ok=True)
        path_file = os.path.join(path_output, f'max_duration_yearly_2d_scatter_{region}_{tech}.html')
        fig.write_html(path_file)
        path_file = os.path.join(path_output, f'max_duration_yearly_2d_scatter_{region}_{tech}.pdf')
        fig.write_image(path_file, format='pdf', width=600, height=600, scale=3)
        path_file = os.path.join(path_output, f'max_duration_yearly_2d_scatter_{region}_{tech}.png')
        fig.write_image(path_file, format='png', width=600, height=600, scale=3)

# %% most extreme single period in each year as 3d scatter, one for each region

regions = ['DE', 'ES', 'EU']

for region in regions:
    data = max_period_yearly.query('region == @region').copy() # max_period_yearly
    #rounded_max = math.ceil(data.max_duration.max() / 1000) * 1000
    
    names_clean = {'wind_onshore': 'onshore wind', 'wind_offshore': 'offshore wind', 'pv': 'PV'}
    data.technology.replace(names_clean, inplace=True)
    
    # Manually specify the order of technologies
    technology_order = ['PV', 'onshore wind', 'offshore wind', 'portfolio']

    # Ensure the data is sorted or ordered according to the technology_order
    # This step is necessary if using category_orders does not apply to scatter_3d directly
    data['technology'] = pd.Categorical(data['technology'], categories=technology_order, ordered=True)
    data.sort_values(by='technology', inplace=True)
    
    fig = px.scatter_3d(data, x='technology', y='fraction', z='max_duration', color='year',
                        color_continuous_scale=custom_cont_color_scale)
    fig.update_traces(marker=dict(size=4))
    fig.update_layout(
        template='plotly_white',
        scene=dict(
            xaxis=dict(title='', range=[-0.3, 3.3], dtick=1),
            yaxis=dict(title='relative threshold', range=[0.1, 1.05], dtick=0.1),
            zaxis=dict(title='maximum duration [d]', range=[-10, 9000], dtick=1000),
            aspectmode='manual',  # 'auto', 'cube', 'data', 'manual' are the options
            aspectratio=dict(x=1, y=1, z=1.1), 
            camera=dict(eye=dict(x=1.6, y=-1.8, z=0.4))
        ),
        coloraxis_colorbar=dict(
                title='year',
                titleside='right',
                len=0.7,
                thickness=30,
        ),
        width=600,
        height=600,
        margin=dict(r=0, b=60, l=0, t=00)  # Increased top margin to accommodate title
    )
    fig.show()
    
    path = path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, 'max_duration_yearly_3d_scatter')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'max_duration_yearly_3d_scatter_{region}.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'max_duration_yearly_3d_scatter_{region}.pdf')
    fig.write_image(path_file, format='pdf', width=600, height=600, scale=3)
    path_file = os.path.join(path_output, f'max_duration_yearly_3d_scatter_{region}.png')
    fig.write_image(path_file, format='png', width=600, height=600, scale=3)
    
# %% most extreme single period per year as 3d scatter, one for three regions

fig = make_subplots(
    rows=1,
    cols=3,
    specs=[[{'type': 'scatter3d'}]*3],
    horizontal_spacing=0
    )

regions = ['DE', 'ES', 'CP']
technology_order = ['PV', 'onshore wind', 'offshore wind', 'portfolio']
names_clean = {'wind_onshore': 'onshore wind', 'wind_offshore': 'offshore wind', 'pv': 'PV'}

for i, region in enumerate(regions, 1):
    
    data = max_period_yearly.query('region == @region').copy()
    data.technology.replace(names_clean, inplace=True)
    data['technology'] = pd.Categorical(data['technology'], categories=technology_order, ordered=True)
    data.sort_values(by='technology', inplace=True)

    trace = go.Scatter3d(
        x=data['technology'],
        y=data['fraction'],
        z=data['max_duration'],
        mode='markers',
        marker=dict(size=4, color=data['year'], coloraxis='coloraxis')
    )

    fig.add_trace(trace, row=1, col=i)


title_y = 0.9  # Adjust this value based on your layout needs
fig.update_layout(
    annotations=[
        dict(text="Germany", x=0.12, y=title_y, xref="paper", yref="paper",
             showarrow=False, font=dict(size=18)),
        dict(text="Spain", x=0.48, y=title_y, xref="paper", yref="paper",
             showarrow=False, font=dict(size=18)),
        dict(text="European copperplate", x=0.9, y=title_y, xref="paper", yref="paper",
             showarrow=False, font=dict(size=18))
    ]
)

fig.update_layout(
    template='plotly_white',
    coloraxis=dict(
        colorscale=custom_cont_color_scale,
        colorbar=dict(
            title='',
            #title_side='top',
            lenmode='fraction',
            len=1,
            thickness=20,
            orientation='h',
            x=0.5,
            xanchor='center',
            y=-0.2
            )
        ),
    scene=dict(
        xaxis=dict(title='', range=[-0.7, 3.3], dtick=1, tickangle=-10, tickfont=dict(size=13)),
        yaxis=dict(title='relative threshold', range=[0, 1.19], dtick=0.2, title_font=dict(size=15), tickfont=dict(size=12)),
        zaxis=dict(title='maximum duration [d]', range=[-10, 9000], dtick=1000, title_font=dict(size=15), tickfont=dict(size=12)),
        aspectmode='manual',
        aspectratio=dict(x=1, y=1, z=1.1),
        camera=dict(eye=dict(x=1.6, y=-1.8, z=0.4))
    ),
    scene2=dict(
        xaxis=dict(title='', range=[-0.7, 3.3], dtick=1, tickangle=-10, tickfont=dict(size=13)),
        yaxis=dict(title='relative threshold', range=[0, 1.19], dtick=0.2, title_font=dict(size=15), tickfont=dict(size=12)),
        zaxis=dict(title='', range=[-10, 9000], dtick=1000, tickfont=dict(size=12)),
        aspectmode='manual',
        aspectratio=dict(x=1, y=1, z=1.1),
        camera=dict(eye=dict(x=1.6, y=-1.8, z=0.4))
    ),
    scene3=dict(
        xaxis=dict(title='', range=[-0.7, 3.3], dtick=1, tickangle=-10, tickfont=dict(size=13)),
        yaxis=dict(title='relative threshold', range=[0, 1.19], dtick=0.2, title_font=dict(size=15), tickfont=dict(size=12)),
        zaxis=dict(title='', range=[-10, 9000], dtick=1000, tickfont=dict(size=12)),
        aspectmode='manual',
        aspectratio=dict(x=1, y=1, z=1.1),
        camera=dict(eye=dict(x=1.6, y=-1.8, z=0.4))
    ),
    width=1475,
    height=600,
    #margin=dict(t=0, b=0, l=0, r=0)
)

fig.update_layout(
    autosize=True
)

fig.show()
    
path = path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'max_duration_yearly_3d_scatter')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'max_duration_yearly_3d_scatter_{regions[0]}_{regions[1]}_{regions[2]}.html')
fig.write_html(path_file)
path_file = os.path.join(path_output, f'max_duration_yearly_3d_scatter_{regions[0]}_{regions[1]}_{regions[2]}.pdf')
fig.write_image(path_file, format='pdf', width=1475, height=600, scale=3)
path_file = os.path.join(path_output, f'max_duration_yearly_3d_scatter_{regions[0]}_{regions[1]}_{regions[2]}.png')
fig.write_image(path_file, format='png', width=1475, height=600, scale=3)


# %% most extreme single period of all years as 3d scatter, one for three regions

fig = make_subplots(
    rows=1,
    cols=3,
    specs=[[{'type': 'scatter3d'}]*3],
    horizontal_spacing=0
    )

regions = ['DE', 'ES', 'CP']
technology_order = ['PV', 'onshore wind', 'offshore wind', 'portfolio']
names_clean = {'wind_onshore': 'onshore wind', 'wind_offshore': 'offshore wind', 'pv': 'PV'}

for i, region in enumerate(regions, 1):
    
    data = max_period_of_all_years.query('region == @region').copy()
    data.technology.replace(names_clean, inplace=True)
    data['technology'] = pd.Categorical(data['technology'], categories=technology_order, ordered=True)
    data.sort_values(by='technology', inplace=True)

    trace = go.Scatter3d(
        x=data['technology'],
        y=data['fraction'],
        z=data['max_duration'],
        mode='markers',
        marker=dict(size=4, color=data['year'], coloraxis='coloraxis')
    )

    fig.add_trace(trace, row=1, col=i)


title_y = 0.9  # Adjust this value based on your layout needs
fig.update_layout(
    annotations=[
        dict(text="Germany", x=0.12, y=title_y, xref="paper", yref="paper",
             showarrow=False, font=dict(size=18)),
        dict(text="Spain", x=0.48, y=title_y, xref="paper", yref="paper",
             showarrow=False, font=dict(size=18)),
        dict(text="European copperplate", x=0.9, y=title_y, xref="paper", yref="paper",
             showarrow=False, font=dict(size=18))
    ]
)

fig.update_layout(
    template='plotly_white',
    coloraxis=dict(
        colorscale=custom_cont_color_scale,
        colorbar=dict(
            title='',
            #title_side='top',
            lenmode='fraction',
            len=1,
            thickness=20,
            orientation='h',
            x=0.5,
            xanchor='center',
            y=-0.2
            )
        ),
    scene=dict(
        xaxis=dict(title='', range=[-0.7, 3.3], dtick=1, tickangle=-10, tickfont=dict(size=13)),
        yaxis=dict(title='relative threshold', range=[0, 1.19], dtick=0.2, title_font=dict(size=15), tickfont=dict(size=12)),
        zaxis=dict(title='maximum duration [d]', range=[-10, 9000], dtick=1000, title_font=dict(size=15), tickfont=dict(size=12)),
        aspectmode='manual',
        aspectratio=dict(x=1, y=1, z=1.1),
        camera=dict(eye=dict(x=1.6, y=-1.8, z=0.4))
    ),
    scene2=dict(
        xaxis=dict(title='', range=[-0.7, 3.3], dtick=1, tickangle=-10, tickfont=dict(size=13)),
        yaxis=dict(title='relative threshold', range=[0, 1.19], dtick=0.2, title_font=dict(size=15), tickfont=dict(size=12)),
        zaxis=dict(title='', range=[-10, 9000], dtick=1000, tickfont=dict(size=12)),
        aspectmode='manual',
        aspectratio=dict(x=1, y=1, z=1.1),
        camera=dict(eye=dict(x=1.6, y=-1.8, z=0.4))
    ),
    scene3=dict(
        xaxis=dict(title='', range=[-0.7, 3.3], dtick=1, tickangle=-10, tickfont=dict(size=13)),
        yaxis=dict(title='relative threshold', range=[0, 1.19], dtick=0.2, title_font=dict(size=15), tickfont=dict(size=12)),
        zaxis=dict(title='', range=[-10, 9000], dtick=1000, tickfont=dict(size=12)),
        aspectmode='manual',
        aspectratio=dict(x=1, y=1, z=1.1),
        camera=dict(eye=dict(x=1.6, y=-1.8, z=0.4))
    ),
    width=1475,
    height=600,
    #margin=dict(t=0, b=0, l=0, r=0)
)

fig.update_layout(
    autosize=True
)

fig.show()
    
path = path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'max_duration_of_all_years_3d_scatter')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'max_duration_of_all_years_3d_scatter_{regions[0]}_{regions[1]}_{regions[2]}.html')
fig.write_html(path_file)
path_file = os.path.join(path_output, f'max_duration_of_all_years_3d_scatter_{regions[0]}_{regions[1]}_{regions[2]}.pdf')
fig.write_image(path_file, format='pdf', width=1475, height=600, scale=3)
path_file = os.path.join(path_output, f'max_duration_of_all_years_3d_scatter_{regions[0]}_{regions[1]}_{regions[2]}.png')
fig.write_image(path_file, format='png', width=1475, height=600, scale=3)

#%% 3d plot most extreme period in each month for three regions and three techs for all years, combined 3x3 plot

techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'portfolio': 'portfolio'}  # Mapping tech codes to names
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}

# Mapping full month names to their abbreviations
months_mapping = {'January': 'Jan', 'February': 'Feb', 'March': 'Mar', 'April': 'Apr', 'May': 'May', 'June': 'Jun',
                  'July': 'Jul', 'August': 'Aug', 'September': 'Sep', 'October': 'Oct', 'November': 'Nov', 'December': 'Dec'}
months_order = list(months_mapping.values())

fig = make_subplots(
    rows=3, cols=3,
    specs=[[{'type': 'surface'}]*3]*3,
    horizontal_spacing=0,
    vertical_spacing=0
)

annotations = []

max_val = 0

for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
    for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
        data = max_period_monthly.query('technology == @tech_code and region == @region_code').copy()
        data['months'] = data['months'].map(months_mapping)
        data['months'] = pd.Categorical(data['months'], categories=months_order, ordered=True)
        data_wide = data.pivot(index='months', columns='fraction', values='max_duration')
        data_wide = data_wide.sort_index()
        data_wide_x = data_wide.index
        data_wide_y = data_wide.columns.to_numpy()
        data_wide_z = data_wide.T.values / 24
        max_z = np.nanmax(data_wide_z)
        rounded_z = math.ceil(max_z / 50) * 50
        
        if max_z > max_val:
            max_val = max_z

        full_days = np.floor(data_wide_z)
        hours = ((data_wide_z - full_days) * 24).round(1)

        custom_data_shape = full_days.shape
        custom_data = np.empty(custom_data_shape, dtype=object)  # Create an empty array of objects

        for i in range(custom_data_shape[0]):
            for j in range(custom_data_shape[1]):
                custom_data[i, j] = [full_days[i, j], hours[i, j]]
        custom_data = custom_data.T

        fig.add_trace(
            go.Surface(
                x=data_wide_x,
                y=data_wide_y,
                z=data_wide_z,
                colorscale=custom_cont_color_scale,
                customdata=custom_data,
                hovertemplate='year: %{x}<br>' + 'threshold: %{y}<br>' + 'duration: %{customdata[0]} days <br>' \
                              + '%{customdata[1]:.1f} hours<br>' + '<extra></extra>',
                contours={"y": {"show": True, "start": 0, "end": 1, "size": 0.05, "color": "white"},
                          "x":{"show": True, "size":1, "color":"white"},
                          },
                coloraxis='coloraxis',  # Link the trace to the shared color axis
            ),
            row=row_idx, col=col_idx
        )

# Add annotations for region names at the top of each column
for col_idx, region_name in enumerate(regions.values(), start=1):
    annotations.append(
        dict(
            text=region_name,
            x=(col_idx - 1) / 3 + 1 / 6,
            y=1 - 0.03,  # Position above the first row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=30),
            xanchor='center'
        )
    )

# Add annotations for technology names to the right of each row
for row_idx, tech_name in enumerate(techs.values(), start=1):
    annotations.append(
        dict(
            text=tech_name,
            x=1,  # Position to the right of the last column
            y=1 - (row_idx - 1) / 3 - 1 / 6,
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=30),
            textangle=90,
            yanchor='middle'
        )
    )
   
# add y axis title
for col_idx in range(1, 4):
    annotations.append(
        dict(
            text='relative threshold',
            x=(col_idx - 1) / 3 + 1 / 6 + 0.13,
            y=-0.02,  # Position below the third row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=23),
            xanchor='center',
            textangle=-83
        )
    )

# Configure the shared color axis and colorbar
fig.update_layout(
    template='plotly_white',
    coloraxis=dict(
        colorscale=custom_cont_color_scale,
        cmin=0,
        cmax=max_val,
        colorbar=dict(
            title='maximum duration [d]',
            titleside='top',  # Keep the title on the right
            titlefont=dict(size=24),
            tickfont=dict(size=20),
            lenmode='fraction',
            len=1,
            thickness=30,
            orientation='h',
            x=0.5,
            xanchor='center',
            y=-0.1,  # Position the colorbar below the plot
        )
    ),
    width=1800,
    height=1800, 
    margin=dict(l=0, r=0, t=0, b=0),  # l=80, r=50, t=50, b=100
    annotations=annotations  # Add annotations for subplot titles
)

scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 'scene7', 'scene8', 'scene9']

# Define the y-axis tick values excluding 0
y_tickvals = [0.2, 0.4, 0.6, 0.8, 1.0]
y_ticktext = ['0.2', '0.4', '0.6', '0.8', '1.0']

# Adjust axes settings for all subplots
for i, scene_id in enumerate(scenes):
    col_idx = (i % 3) + 1
    row_idx = (i // 3) + 1
    fig.update_layout(**{scene_id: dict(
        xaxis=dict(title='', range=[-0.8, 11.2], dtick=1, tickfont=dict(size=15)),
        yaxis=dict(
            title='', range=[0.1, 1], tickvals=y_tickvals, ticktext=y_ticktext, tickfont=dict(size=15)
        ),
        zaxis=dict(title='maximum duration [d]' if col_idx == 1 else '', range=[-5, rounded_z], dtick=rounded_z / 10, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=15)),
        aspectmode='manual',
        aspectratio=dict(x=1.3, y=1, z=1.14),
        camera=dict(eye=dict(x=0.8, y=-2.25, z=0.4)) # x=1.6, y=-1.8, z=0.4
    )})

fig.show()

regions_str = "_".join(regions.keys())

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'max_duration_monthly_surface')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'max_duration_monthly_surface_{regions_str}_3_tech.html')
fig.write_html(path_file)
path_file = os.path.join(path_output, f'max_duration_monthly_surface_{regions_str}_3_tech.pdf')
fig.write_image(path_file, format='pdf', width=1800, height=1800, scale=3)
path_file = os.path.join(path_output, f'max_duration_monthly_surface_{regions_str}_3_tech.png')
fig.write_image(path_file, format='png', width=1800, height=1800, scale=3)


#%% 3d plot most extreme period in each month for three regions and four techs for all years, combined 4x3 plot

import pandas as pd
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import math

# Define technology and region mappings
techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'wind_offshore': 'wind offshore', 'portfolio': 'portfolio'}  
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}

# Mapping full month names to their abbreviations
months_mapping = {'January': 'Jan', 'February': 'Feb', 'March': 'Mar', 'April': 'Apr', 'May': 'May', 'June': 'Jun',
                  'July': 'Jul', 'August': 'Aug', 'September': 'Sep', 'October': 'Oct', 'November': 'Nov', 'December': 'Dec'}
months_order = list(months_mapping.values())

# Create subplots with an extra row for wind offshore
fig = make_subplots(
    rows=4, cols=3,
    specs=[[{'type': 'surface'}]*3]*4,
    horizontal_spacing=0,
    vertical_spacing=0
)

annotations = []

max_val = 0

for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
    for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
        data = max_period_monthly_mid.query('technology == @tech_code and region == @region_code').copy()
        data['months'] = data['months'].map(months_mapping)
        data['months'] = pd.Categorical(data['months'], categories=months_order, ordered=True)
        data_wide = data.pivot(index='months', columns='fraction', values='max_duration')
        data_wide = data_wide.sort_index()
        data_wide_x = data_wide.index
        data_wide_y = data_wide.columns.to_numpy()
        data_wide_z = data_wide.T.values / 24
        max_z = np.nanmax(data_wide_z)
        rounded_z = math.ceil(max_z / 50) * 50
        
        if max_z > max_val:
            max_val = max_z

        full_days = np.floor(data_wide_z)
        hours = ((data_wide_z - full_days) * 24).round(1)

        custom_data_shape = full_days.shape
        custom_data = np.empty(custom_data_shape, dtype=object)  # Create an empty array of objects

        for i in range(custom_data_shape[0]):
            for j in range(custom_data_shape[1]):
                custom_data[i, j] = [full_days[i, j], hours[i, j]]
        custom_data = custom_data.T

        fig.add_trace(
            go.Surface(
                x=data_wide_x,
                y=data_wide_y,
                z=data_wide_z,
                #opacity=0.99,
                colorscale=custom_cont_color_scale,
                customdata=custom_data,
                hovertemplate='month: %{x}<br>' + 'relative threshold: %{y}<br>' + 'duration: %{customdata[0]} days' + '%{customdata[1]:.1f} hours<br>' + '<extra></extra>',
                contours={"y": {"show": True, "start": 0, "end": 1, "size": 0.05, "color": "white"},
                          "x":{"show": True, "size":1, "color":"white"},
                          },
                coloraxis='coloraxis',  # Link the trace to the shared color axis
            ),
            row=row_idx, col=col_idx
        )
            
# =============================================================================
#         # Find the years with the highest and lowest z-values for y=0.7
#         if 0.75 in data_wide_y:
#             y_index = np.where(data_wide_y == 0.75)[0][0]
#             z_values = data_wide_z[y_index, :]
#             x_values = data_wide_x
# 
#             max_z_value = np.nanmax(z_values)
#             min_z_value = np.nanmin(z_values)
# 
#             max_x_value = x_values[np.nanargmax(z_values)]
#             min_x_value = x_values[np.nanargmin(z_values)]
# 
#             max_full_days = np.floor(max_z_value)
#             max_hours = ((max_z_value - max_full_days) * 24).round(1)
#             
#             min_full_days = np.floor(min_z_value)
#             min_hours = ((min_z_value - min_full_days) * 24).round(1)
# 
#             fig.add_trace(
#                 go.Scatter3d(
#                     x=[max_x_value, min_x_value],
#                     y=[0.75, 0.75],
#                     z=[max_z_value, min_z_value],
#                     mode='markers+text',
#                     marker=dict(size=8, color='palevioletred', symbol='circle'),
#                     text=[f'{max_full_days:.0f}', f'{min_full_days:.0f}'],
#                     textposition='top right',
#                     textfont=dict(color='palevioletred', size=18),
#                     customdata=[[max_full_days, max_hours], [min_full_days, min_hours]],
#                     hovertemplate='month: %{x}<br>' + 'threshold: 0.75<br>' + 'duration: %{customdata[0]:.0f} days, %{customdata[1]:.1f} hours<br>' + '<extra></extra>',
#                 ),
#                 row=row_idx, col=col_idx
#             )
# =============================================================================

# Add annotations for region names at the top of each column
for col_idx, region_name in enumerate(regions.values(), start=1):
    annotations.append(
        dict(
            text=region_name,
            x=(col_idx - 1) / 3 + 1 / 6,
            y=1 - 0.03,  # Position above the first row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=30),
            xanchor='center'
        )
    )

# Add annotations for technology names to the right of each row
for row_idx, tech_name in enumerate(techs.values(), start=1):
    annotations.append(
        dict(
            text=tech_name,
            x=1,  # Position to the right of the last column
            y=1 - (row_idx - 1) / 4 - 1 / 8,
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=30),
            textangle=90,
            yanchor='middle'
        )
    )
   
# add y axis title
for col_idx in range(1, 4):
    annotations.append(
        dict(
            text='relative threshold',
            x=(col_idx - 1) / 3 + 1 / 6 + 0.14,
            y=-0.02,  # Position below the fourth row
            xref="paper",
            yref="paper",
            showarrow=False,
            font=dict(size=23),
            xanchor='center',
            textangle=-82
        )
    )

# Configure the shared color axis and colorbar
fig.update_layout(
    template='plotly_white',
    coloraxis=dict(
        colorscale=custom_cont_color_scale,
        cmin=0,
        cmax=max_val,
        colorbar=dict(
            title='maximum duration [d]',
            titleside='top',  # Keep the title on the right
            titlefont=dict(size=24),
            tickfont=dict(size=20),
            lenmode='fraction',
            len=1,
            thickness=30,
            orientation='h',
            x=0.5,
            xanchor='center',
            y=-0.1,  # Position the colorbar below the plot
        )
    ),
    showlegend=False,
    width=1800,
    height=2400,  # Increase height to accommodate extra row
    margin=dict(l=0, r=0, t=0, b=0),  # l=80, r=50, t=50, b=100
    annotations=annotations  # Add annotations for subplot titles
)

scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 'scene7', 'scene8', 'scene9', 'scene10', 'scene11', 'scene12']

# Define the y-axis tick values excluding 0
y_tickvals = [0.2, 0.4, 0.6, 0.8, 1.0]
y_ticktext = ['0.2', '0.4', '0.6', '0.8', '1.0']

# Adjust axes settings for all subplots
for i, scene_id in enumerate(scenes):
    col_idx = (i % 3) + 1
    row_idx = (i // 3) + 1
    fig.update_layout(**{scene_id: dict(
        xaxis=dict(title='', range=[-0.8, 11.2], dtick=1, tickfont=dict(size=15)),
        yaxis=dict(
            title='', range=[0.1, 1], tickvals=y_tickvals, ticktext=y_ticktext, tickfont=dict(size=16)
        ),
        zaxis=dict(title='maximum duration [d]' if col_idx == 1 else '', range=[-5, rounded_z], dtick=rounded_z / 10, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=15)),
        aspectmode='manual',
        aspectratio=dict(x=1.3, y=1, z=1.14),
        camera=dict(eye=dict(x=0.85, y=-2.25, z=0.4)) # x=1.6, y=-1.8, z=0.4
    )})

fig.show()

regions_str = "_".join(regions.keys())

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'max_duration_monthly_surface_mid')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'max_duration_monthly_surface_mid_{regions_str}_4_tech.html')
fig.write_html(path_file)
path_file = os.path.join(path_output, f'max_duration_monthly_surface_mid_{regions_str}_4_tech.pdf')
fig.write_image(path_file, format='pdf', width=1800, height=2400, scale=3)
path_file = os.path.join(path_output, f'max_duration_monthly_surface_mid_{regions_str}_4_tech.png')
fig.write_image(path_file, format='png', width=1800, height=2400, scale=3)


# %% choropleth map max periods across all years with threshold slider

# ISO-3 country codes
iso_3 = pd.read_csv("D:\\git\\vre_droughts\\input\\countries_codes_and_coordinates.csv")
iso_3['Alpha-2 code'] = iso_3['Alpha-2 code'].str.replace('"', '')
iso_3['Alpha-2 code'] = iso_3['Alpha-2 code'].str.replace(' ', '')
iso_3['Alpha-3 code'] = iso_3['Alpha-3 code'].str.replace('"', '')
iso_3['Alpha-3 code'] = iso_3['Alpha-3 code'].str.replace(' ', '')
iso_3 = iso_3[['Alpha-2 code', 'Alpha-3 code']]

# data
df = max_period_of_all_years.query("technology == 'portfolio'")
df = df.query("region != 'EU'")
df = df.replace("UK", "GB")
df = df.merge(iso_3, left_on='region', right_on='Alpha-2 code', how='left')
df = df.rename(columns={'Alpha-3 code': 'region3'})
df = df[df['fraction'] != 0.9]

# map option 1
px.choropleth(df,
              locations='region3',
              color="max_duration",
              animation_frame="fraction",
              color_continuous_scale="Burgyl",
              locationmode='ISO-3',
              scope="europe",
              range_color=(0, 310),
              title='Maximum VRE portfolio drought duration',
              height=600,
              width=600,
              projection="mercator"
              )

# map option 2
with urlopen('https://gisco-services.ec.europa.eu/distribution/v2/nuts/geojson/NUTS_RG_60M_2021_4326_LEVL_0.geojson') as response:
    ccaa = json.load(response)

fig = px.choropleth_mapbox(df,
                           geojson=ccaa,
                           featureidkey='properties.CNTR_CODE',
                           locations='region',
                           color='max_duration',
                           color_continuous_scale="Burgyl",
                           animation_frame="fraction",
                           range_color=(0, 310),
                           # height=950,
                           # width=850,
                           # opacity=0.5,
                           labels={'max_duration': 'maximum drought duration'}
                           )
fig.update_layout(mapbox_style="white-bg",
                  mapbox_zoom=3,
                  mapbox_center={"lat": 57.5, "lon": 10}
                  )
# fig.update_layout(autosize=True)# , geo=dict(projection_scale=5))
fig.show()
fig.write_html("D:\\git\\vre_droughts\\output\\2023-07-21_15-39-36_mbt_event\\additional_plots\\max_period_map.html")


# =============================================================================
# # static choropleth map with plt
#
# nuts_path = "D:\\owncloud_mkittel\\04_Projekte\\01_VRE_droughts\\01_data\\europe\\eurostat\\NUTS_RG_20M_2021_3857.geojson"
# gdf = gpd.read_file(nuts_path)
# gdf = gdf[gdf['LEVL_CODE'] == 0]
# gdf = gdf[['CNTR_CODE', 'geometry']]
# gdf.columns = ['region', 'geometry']
#
# max_period_of_all_years = max_period_of_all_years.merge(gdf, on='region', how='left')
# max_period_eu = max_period_of_all_years.query("region == 'EU'")
# max_period_of_all_years = max_period_of_all_years[max_period_of_all_years['region'] != 'EU']
#
# import geopandas as gpd
# import matplotlib.pyplot as plt
#
# # Define the desired coordinates to limit the plot
# x_min, x_max = -1200000, 3535875  # Specify the longitude range (e.g., from -10 to 30 degrees)
# y_min, y_max = 4302455, 11488626   # Specify the latitude range (e.g., from 35 to 72 degrees)
#
# for (fraction, tech), group in max_period_of_all_years.groupby(['fraction', 'technology']):
#     group=group
#     group = gpd.GeoDataFrame(group, geometry="geometry")
#     fig, ax = plt.subplots(figsize=(7, 8))
#     if tech == 'portfolio':
#         colorbar_min = 0  # Minimum value of the color scale
#         colorbar_max = 320  # Maximum value of the color scal
#     group.plot(ax=ax, column='max_duration', legend=True) #, vmin=colorbar_min, vmax=colorbar_max)
#     ax.set_xlim(x_min, x_max)  # Set the x-axis limits
#     ax.set_ylim(y_min, y_max)
#     path = 'D:\\git\\vre_droughts\\output\\2023-07-20_16-34-51_mbt_event\\temp\\'
#     plt.savefig(f'{path}map_max_period_{tech}_{fraction}.png', dpi=400, bbox_inches='tight')
#     plt.close(fig)
# =============================================================================


# %% choropleth map max periods for certain threshold with year slider

# map option 2
url = 'https://gisco-services.ec.europa.eu/distribution/v2/nuts/geojson/NUTS_RG_60M_2021_4326_LEVL_0.geojson'

with urlopen(url) as response:
    ccaa = json.load(response)

for (fraction, tech), group in max_period_yearly.groupby(['fraction', 'technology']):

    max_val = group['max_duration'].max()
    min_val = group['max_duration'].min()

    fig = px.choropleth_mapbox(group,
                               geojson=ccaa,
                               featureidkey='properties.CNTR_CODE',
                               locations='region',
                               color='max_duration',
                               color_continuous_scale="Burgyl",
                               animation_frame="year",
                               range_color=(min_val, max_val),
                               # height=950,
                               # width=850,
                               # opacity=0.5,
                               labels={'max_duration': 'maximum drought duration'},
                               )
    fig.update_layout(mapbox_style="white-bg",
                      mapbox_zoom=3,
                      mapbox_center={"lat": 57.5, "lon": 10},
                      title_text=f'Maximum {tech} drought duration for thresholds based on the {fraction} fraction of the mean capacity factor',
                      )
    fig.update_geos(fitbounds="locations", visible=False)
    # fig.update_layout(autosize=True)# , geo=dict(projection_scale=5))
    # fig.show()
    path_output = os.path.join(a.path_run, 'additional_plots', 'max_period_maps')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'max_period_map_yearly_{tech}_{fraction}.html')
    fig.write_html(path_file)

# %% max period box plot across all regions, facet_row = tech

data_figure = max_period_yearly.copy()
data_figure.replace({'wind_onshore': 'wind onshore', 'wind_offshore': 'wind offshore', 'pv': 'PV'}, inplace=True)

fig = px.box(
    data_figure,
    x='fraction',
    y='max_duration',
    # points="all",
    # color='region',
    # facet_col='region',
    facet_row='technology',
    facet_row_spacing=0.04,
    facet_col_spacing=0.0,
    facet_col_wrap=10,
    # color_discrete_sequence=px.colors.qualitative.G10
)

fig.update_layout(
    font_size=10,
    template="simple_white",
    # xaxis_title=None,
    yaxis_title='',
    # title=f'{t}'
    width=1080,
    # height=1980,
)

# deactivate all y and x axes titles
for axis in fig.layout:
    if type(fig.layout[axis]) == go.layout.YAxis:
        fig.layout[axis].title.text = ''
    if type(fig.layout[axis]) == go.layout.XAxis:
        fig.layout[axis].title.text = ''

# add global y and x axes title
fig.add_annotation(
    showarrow=False,
    xanchor='center',
    xref='paper',
    x=0.5,
    yref='paper',
    y=-0.05,
    text='threshold as fraction of mean capacity factor',
    font_size=12
)
fig.add_annotation(
    showarrow=False,
    xanchor='center',
    xref='paper',
    x=-0.05,
    yanchor='middle',
    yref='paper',
    y=0.5,
    textangle=270,
    text='maximum duration [days]',
    font_size=12
)

# add horizontal area
# fig.add_hrect(y0=14, y1=300, annotation_text="severe", annotation_position="top left",
#              fillcolor="green", opacity=0.25, line_width=0)

fig.for_each_annotation(lambda a: a.update(text=a.text.split("=")[-1]))
# fig.update_yaxes(showticklabels=True)
# fig.update_xaxes(showticklabels=True)
fig.show()

path_output = os.path.join(a.path_run, 'additional_plots', 'max_period_boxplot')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'max_period_boxplot.html')
fig.write_html(path_file)

# %% max period across all thresholds for each year and all techs (facet col = year)

for region, group in max_period_yearly.groupby('region'):

    data_figure = group.copy()
    data_figure.replace({'wind_onshore': 'wind onshore', 'wind_offshore': 'wind offshore', 'pv': 'PV'}, inplace=True)
    color_dict = {'wind onshore': '#669aaa', 'wind offshore': '#104c5a', 'PV': '#f5e01f', 'portfolio': '#7cb342'}

    fig = px.line(
        data_figure,
        x='fraction',
        y='max_duration',
        color='technology',
        facet_col='year',
        # facet_row='year',
        facet_row_spacing=0.04,
        facet_col_spacing=0.0,
        facet_col_wrap=10,
        color_discrete_map=color_dict,
        # line_dash=line_style_dict
    )

    fig.update_layout(
        font_size=10,
        template="simple_white",
        # xaxis_title=None,
        yaxis_title='',
        title=f'Maximum drought duration in {region}',
        legend=dict(orientation="h", yanchor="bottom", y=-0.1, xanchor="right", x=0.65, font=dict(size=12), title='')
    )

    # deactivate all y and x axes titles
    for axis in fig.layout:
        if type(fig.layout[axis]) == go.layout.YAxis:
            fig.layout[axis].title.text = ''
        if type(fig.layout[axis]) == go.layout.XAxis:
            fig.layout[axis].title.text = ''

    # add global y and x axes title
    fig.add_annotation(
        showarrow=False,
        xanchor='center',
        xref='paper',
        x=0.5,
        yref='paper',
        y=-0.07,
        text='threshold as fraction of mean capacity factor',
        font_size=12
    )
    fig.add_annotation(
        showarrow=False,
        xanchor='center',
        xref='paper',
        x=-0.05,
        yanchor='middle',
        yref='paper',
        y=0.5,
        textangle=270,
        text='maximum duration [days]',
        font_size=12
    )

    # add horizontal area
    # fig.add_hrect(y0=14, y1=300, annotation_text="severe", annotation_position="top left",
    #              fillcolor="green", opacity=0.25, line_width=0)

    fig.for_each_annotation(lambda a: a.update(text=a.text.split("=")[-1]))
    fig.update_yaxes(mirror=True)  # , ticks='outside', showline=True)
    fig.update_xaxes(mirror=True)  # , ticks='outside', showline=True)
#    fig.show()

    path_output = os.path.join(a.path_run, 'additional_plots', 'max_period_line')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'max_period_line_{region}.html')
    fig.write_html(path_file)


# %% plot most extreme periods DE

thres = de_extremes_yearly_long['f-mean-cf'].unique()
len_years = len(de_extremes_yearly_long.year.unique())

for t in [*thres]:
    data_figure_de = de_extremes_yearly_long.loc[de_extremes_yearly_long['f-mean-cf'] == t].copy()
    data_figure_de['max_duration'] /= 24
    fig = px.bar(data_figure_de, x='f-mean-cf', y='max_duration', color='technology', barmode='group',
                 facet_col='year', facet_row='technology')  # symbol='technology')##, #, log_y=True)
    fig.update_layout(font_size=10, xaxis_title=None, title=f'{t}')  # template = "simple_white"
    fig.update_xaxes(nticks=len_years*2)
    fig.show()

# %% max period DE (one col for each year)

data_figure_de = max_period_of_all_years.query('region == "DE"').copy()
data_figure_de.replace({'wind_onshore': 'wind onshore', 'wind_offshore': 'wind offshore', 'pv': 'PV'}, inplace=True)

fig = px.bar(
    data_figure_de,
    x='fraction',
    y='max_duration',
    # color='f-mean-cf',
    facet_col='year',
    facet_row='technology',
    facet_row_spacing=0.01,
    facet_col_spacing=0.001,
    # facet_col_wrap=8,
    barmode='group'
)

fig.update_layout(
    font_size=12,
    template="simple_white",
    # width=1000,
    # height=1500,
    # xaxis_title=None,
    yaxis_title='',
    # title=f'{t}'
    legend=dict(orientation="h", yanchor="bottom", y=-0.08, xanchor="center", x=.5),
    legend_title_text=''
)

# deactivate all y and x axes titles
for axis in fig.layout:
    if type(fig.layout[axis]) == go.layout.YAxis:
        fig.layout[axis].title.text = ''
    if type(fig.layout[axis]) == go.layout.XAxis:
        fig.layout[axis].title.text = ''

# add global y and x axes title
fig.add_annotation(
    showarrow=False,
    xanchor='center',
    xref='paper',
    x=0.5,
    yref='paper',
    y=-0.05,
    text='threshold as fraction of mean capacity factor',
    font_size=14
)
fig.add_annotation(
    showarrow=False,
    xanchor='center',
    xref='paper',
    x=-0.06,
    yanchor='middle',
    yref='paper',
    y=0.5,
    textangle=270,
    text='maximum duration [days]',
    font_size=14
)

# add horizontal area
# fig.add_hrect(y0=14, y1=300, annotation_text="severe", annotation_position="top left",
#              fillcolor="green", opacity=0.25, line_width=0)

fig.for_each_annotation(lambda a: a.update(text=a.text.split("=")[-1]))
# fig.update_yaxes(showticklabels=True)
# fig.update_xaxes(showticklabels=True)
fig.show()
path_output = os.path.join(path, 'additional_plots', 'max_period_DE')
fig.write_html(f"{path_output}.html")
# fig.write_image(f"{path_output}.png")# , width=, height=)
# fig.write_image(f"{path_output}.pdf")#, width=1980, height=1080)


#%%########################## average change of most extreme period ####################################################

# %% plot average change in max duration

fig = px.line(change_tech_region_thres_long,
              x='fraction',
              y='change',
              color='region',
              color_discrete_sequence=px.colors.qualitative.Light24,
              facet_row='technology')
fig.show()

# change_tech_thres.plot()


# %% ######################## return period ############################################################################

return_period_yearly, return_period_q1, return_period_q2, return_period_q3, return_period_q4, \
    return_period_q14, return_period_q23 = get_return_period(a)

#%% 3d plot for one region: 3d surface

techs = ['portfolio'] #'pv', 'wind_onshore', 'portfolio'] # 'wind_offshore',
regions = ['DE'] #, 'ES', 'EU']
data_orig = [return_period_yearly] #, return_period_q1, return_period_q2, return_period_q3, return_period_q4,
             #return_period_q14, return_period_q23]
kind = ['yearly'] #, 'Q1', 'Q2', 'Q3', 'Q4', 'Q1+Q4', 'Q2+Q3']

for idx, data_set in enumerate(data_orig):
    for tech in techs:
        for region in regions:

            data = data_set.query('technology == @tech and region == @region')
            data.max_period.replace(0, np.nan, inplace=True)
            data_wide = data.pivot(index='return_period', columns='fraction', values='max_period')
            data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
            data_wide_y = data_wide.columns.to_numpy()
            data_wide_z = data_wide.T.values
            max_z = np.nanmax(data_wide_z)
            rounded_z = math.ceil(max_z / 50) * 50

            fig = go.Figure()

            fig.add_trace(
                go.Surface(
                    x=data_wide_y,
                    y=data_wide_x,
                    z=data_wide_z,
                    colorscale=custom_cont_color_scale,
                    contours={"y":{"show": True, "start":0, "end":1, "size": 0.1, "color":"white"},
                              #"z":{"show": True, "start":1, "end":rounded_z+1, "size":rounded_z/5, "color":"white"},
                              },
                    colorbar=dict(title='duration [h]', titleside='top', len=0.7, thickness=30),
                    hovertemplate='return period [y]: %{x}<br>' + 'threshold: %{y}<br>' + 'duration [d]: %{z}<extra></extra>'
                    )
                )
            
            tech_clean = tech.replace('_', ' ')
            
            fig.update_layout(
                title={
                    'text': f'{tech_clean} drought return period {kind[idx]}',
                    'y':0.9,  # Moves the title down towards the figure
                    'x':0.4,  # Centers the title
                    'xanchor': 'center',
                    'yanchor': 'top'
                },
                template='plotly_white',
                scene=dict(
                    xaxis=dict(title='duration [h]', range=[-1, rounded_z], dtick=rounded_z/5),
                    yaxis=dict(title='threshold (fraction of mean)', range=[0.1, 0.9], dtick=0.1),
                    zaxis=dict(title='return period [y]', range=[1, None], dtick=5), #autorange='reversed'),
                    camera=dict(eye=dict(x=1.5, y=-1.5, z=0.2))
                ),
                width=700,
                height=600,
                margin=dict(r=0, b=50, l=0, t=50)  # Increased top margin to accommodate title
            )
            
            fig.show()
            
# =============================================================================
#             path_output = os.path.join(a.path_run, 'additional_plots', f'return_period_{kind[idx]}')
#             Path(path_output).mkdir(parents=True, exist_ok=True)
#             path_file = os.path.join(path_output, f'return_period_3d_{region}_{tech}.html')
#             fig.write_html(path_file)
#             path_file = os.path.join(path_output, f'return_period_3d_{region}_{tech}.pdf')
#             fig.write_image(path_file, format='pdf', width=750, height=600, scale=3)
# =============================================================================
    
#%% 3d return period plot for three regions and three techs: combined 3x3

techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'portfolio': 'portfolio'} # 'wind_offshore',
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}
data_orig = [return_period_yearly] #, return_period_q1, return_period_q2, return_period_q3, return_period_q4, return_period_q14, return_period_q23]
kind = ['yearly'] #, 'Q1', 'Q2', 'Q3', 'Q4', 'Q1+Q4', 'Q2+Q3']

for idx, data_set in enumerate(data_orig):

    # Create subplot figure with 3 rows and 3 columns
    fig = make_subplots(
        rows=3, cols=3,
        specs=[[{'type': 'surface'}]*3]*3,
        horizontal_spacing=0,
        vertical_spacing=0
    )

    annotations = []
    
    max_val = 0
    
    for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
        for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
            data = data_set.query('technology == @tech_code and region == @region_code').copy()
            data.max_period.replace(0, np.nan, inplace=True)            
            data_wide = data.pivot(index='return_period', columns='fraction', values='max_period')
            data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
            data_wide_y = data_wide.columns.to_numpy()
            data_wide_z = data_wide.T.values
            max_z = np.nanmax(data_wide_z)
            rounded_z = math.ceil(max_z / 50) * 50
            
            if max_z > max_val:
                max_val = max_z
    
            fig.add_trace(
                go.Surface(
                    x=data_wide_x,
                    y=data_wide_y,
                    z=data_wide_z,
                    cmin=0,
                    cmax=rounded_z,
                    colorscale=custom_cont_color_scale,
                    coloraxis='coloraxis',  # Link the trace to the shared color axis
                    contours={
                        "y": {"show": True, "start": 0.1, "end": 1, "size": 0.05, "color": "white"},
                    },
                    hovertemplate='return period [y]: %{x}<br>' + 'threshold: %{y}<br>' + 'duration [d]: %{z}<extra></extra>'
                ),
                row=row_idx, col=col_idx
            )

    # Add annotations for region names at the top of each column
    for col_idx, region_name in enumerate(regions.values(), start=1):
        annotations.append(
            dict(
                text=region_name,
                x=(col_idx - 1) / 3 + 1 / 6,
                y=1 - 0.04,  # Position above the first row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                xanchor='center'
            )
        )
    
    # Add annotations for technology names to the right of each row
    for row_idx, tech_name in enumerate(techs.values(), start=1):
        annotations.append(
            dict(
                text=tech_name,
                x=1,  # Position to the right of the last column
                y=1 - (row_idx - 1) / 3 - 1 / 6,
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                textangle=90,
                yanchor='middle'
            )
        )
    
    # add x axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='return period [y]',
                x=(col_idx - 1) / 3 + 1 / 6 - 0.07,
                y=-0.003,  # Position below the third row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=19
            )
        )

    # add y axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='relative threshold',
                x=(col_idx - 1) / 3 + 1 / 6 + 0.125,
                y=-0.035,  # Position below the third row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=-47
            )
        )
   
    # Configure the shared color axis and colorbar
    fig.update_layout(
        template='plotly_white',
        coloraxis=dict(
            colorscale=custom_cont_color_scale,
            cmin=0,
            cmax=max_val,
            colorbar=dict(
                title='duration [h]',
                title_side='top',
                lenmode='fraction',
                len=1,
                thickness=30,
                orientation='h',
                x=0.5,
                xanchor='center',
                y=-0.1,  # Position the colorbar below the plot
                title_font=dict(size=24),
                tickfont=dict(size=20)
            )
        ),
        width=1800,
        height=1800, 
        margin=dict(l=0, r=0, t=0, b=0),
        annotations=annotations  # Add annotations for subplot titles
    )


    scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 
              'scene7', 'scene8', 'scene9']

    # Adjust axes settings for all subplots
    for i, scene_id in enumerate(scenes):
        col_idx = (i % 3) + 1
        row_idx = (i // 3) + 1
        fig.update_layout(**{scene_id: dict(
            xaxis=dict(title='', range=[0, 39], dtick=5, tickfont=dict(size=15)),
            yaxis=dict(title='', range=[0.08, 1], dtick=0.2, tickfont=dict(size=15)),
            zaxis=dict(title='duration [h]' if col_idx == 1 else '', range=[-1, 9000], dtick=1000, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=14.5)),
            aspectmode='manual',
            aspectratio=dict(x=1.5, y=1, z=1.1),
            camera=dict(eye=dict(x=1.5, y=-2, z=0.2)) #x=1.4, y=-2, z=0.2))
        )})
        
    fig.show()
    
    path = path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, f'return_period_{kind[idx]}')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'return_period_3d_{"_".join(regions.keys())}_3_techs.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'return_period_3d_{"_".join(regions.keys())}_3_techs.pdf')
    fig.write_image(path_file, format='pdf', width=1800, height=1800, scale=3)
    path_file = os.path.join(path_output, f'return_period_3d_{"_".join(regions.keys())}_3_techs.png')
    fig.write_image(path_file, format='png', width=1800, height=1800, scale=3)
    
#%% 3d return period plot for three regions and four techs: combined 4x3

techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'wind_offshore': 'wind offshore', 'portfolio': 'portfolio'}
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}
data_orig = [return_period_yearly]  # , return_period_q1, return_period_q2, return_period_q3, return_period_q4, return_period_q14, return_period_q23]
kind = ['yearly']  # , 'Q1', 'Q2', 'Q3', 'Q4', 'Q1+Q4', 'Q2+Q3']

for idx, data_set in enumerate(data_orig):

    # Create subplot figure with 4 rows and 3 columns
    fig = make_subplots(
        rows=4, cols=3,
        specs=[[{'type': 'surface'}]*3]*4,
        horizontal_spacing=0,
        vertical_spacing=0
    )

    annotations = []
    
    max_val = 0
    
    for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
        for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
            data = data_set.query('technology == @tech_code and region == @region_code').copy()
            data['max_period'] /= 24
            data_wide = data.pivot(index='return_period', columns='fraction', values='max_period')
            data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
            data_wide_y = data_wide.columns.to_numpy()
            data_wide_z = data_wide.T.values
            max_z = np.nanmax(data_wide_z)
            rounded_z = np.ceil(max_z / 50) * 50
            
            if max_z > max_val:
                max_val = max_z
    
            fig.add_trace(
                go.Surface(
                    x=data_wide_x,
                    y=data_wide_y,
                    z=data_wide_z,
                    cmin=0,
                    cmax=rounded_z,
                    colorscale=custom_cont_color_scale,
                    coloraxis='coloraxis',  # Link the trace to the shared color axis
                    #opacity=0.91,
                    contours={
                        "y": {"show": True, "start": 0.1, "end": 1, "size": 0.05, "color": "white"},
                    },
                    hovertemplate='return period [y]: %{x}<br>' + 'threshold: %{y}<br>' + 'duration [d]: %{z}<extra></extra>'
                ),
                row=row_idx, col=col_idx
            )
    
    # Adding scatter3d markers after all surface plots
    for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
        for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
            data = data_set.query('technology == @tech_code and region == @region_code').copy()
            data['max_period'] /= 24
            data_wide = data.pivot(index='return_period', columns='fraction', values='max_period')
            data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
            data_wide_y = data_wide.columns.to_numpy()
            data_wide_z = data_wide.T.values
            
            # Find the z-value for x=10, x=20, and x=30 at y=0.7
            for x_value in [10, 20, 30]:
                if x_value in data_wide_x and 0.75 in data_wide_y:
                    x_index = np.where(data_wide_x == x_value)[0][0]
                    y_index = np.where(data_wide_y == 0.75)[0][0]
                    z_value = data_wide_z[y_index, x_index]
                    
                    z_marker = z_value + 0

                    fig.add_trace(
                        go.Scatter3d(
                            x=[x_value],
                            y=[0.7],
                            z=[z_marker],
                            mode='markers+text',
                            marker=dict(size=6.5, color='palevioletred', symbol='circle'), # line=dict(width=2, color='white'), opacity=0.9),
                            text=[f'{z_value:.0f}'],
                            textposition='top left',  # Adjust text position to avoid overlap
                            textfont=dict(color='palevioletred', size=18)
                        ),
                        row=row_idx, col=col_idx
                    )

    # Add annotations for region names at the top of each column
    for col_idx, region_name in enumerate(regions.values(), start=1):
        annotations.append(
            dict(
                text=region_name,
                x=(col_idx - 1) / 3 + 1 / 6,
                y=1 - 0.03,  # Position above the first row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                xanchor='center'
            )
        )
    
    # Add annotations for technology names to the right of each row
    for row_idx, tech_name in enumerate(techs.values(), start=1):
        annotations.append(
            dict(
                text=tech_name,
                x=1,  # Position to the right of the last column
                y=1 - (row_idx - 1) / 4 - 1 / 8,
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                textangle=90,
                yanchor='middle'
            )
        )
    
    # Add x axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='return period [y]',
                x=(col_idx - 1) / 3 + 1 / 6 - 0.07,
                y=-0.001,  # Position below the fourth row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=19
            )
        )

    # Add y axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='relative threshold',
                x=(col_idx - 1) / 3 + 1 / 6 + 0.12,
                y=-0.03,  # Position below the fourth row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=-47
            )
        )
   
    # Configure the shared color axis and colorbar
    fig.update_layout(
        template='plotly_white',
        coloraxis=dict(
            colorscale=custom_cont_color_scale,
            cmin=0,
            cmax=max_val,
            colorbar=dict(
                title='duration [d]',
                title_side='top',
                lenmode='fraction',
                len=1,
                thickness=30,
                orientation='h',
                x=0.5,
                xanchor='center',
                y=-0.1,  # Position the colorbar below the plot
                title_font=dict(size=24),
                tickfont=dict(size=20)
            )
        ),
        showlegend=False,
        width=1800,
        height=2400,  # Increase height to accommodate extra row
        margin=dict(l=0, r=0, t=0, b=0),
        annotations=annotations  # Add annotations for subplot titles
    )


    scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 
              'scene7', 'scene8', 'scene9', 'scene10', 'scene11', 'scene12']

    # Adjust axes settings for all subplots
    for i, scene_id in enumerate(scenes):
        col_idx = (i % 3) + 1
        row_idx = (i // 3) + 1
        fig.update_layout(**{scene_id: dict(
            xaxis=dict(title='', range=[0, 39], dtick=5, tickfont=dict(size=15)),
            yaxis=dict(title='', range=[0.08, 1], dtick=0.2, tickfont=dict(size=15)),
            zaxis=dict(title='duration [d]' if col_idx == 1 else '', range=[-1, 400], dtick=40, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=14.5)),
            aspectmode='manual',
            aspectratio=dict(x=1.5, y=1, z=1.1),
            camera=dict(eye=dict(x=1.5, y=-2, z=0.2)) #x=1.4, y=-2, z=0.2))
        )})
        
    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, f'return_period_{kind[idx]}')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'return_period_3d_{"_".join(regions.keys())}_4_techs.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'return_period_3d_{"_".join(regions.keys())}_4_techs.pdf')
    fig.write_image(path_file, format='pdf', width=1800, height=2400, scale=3)
    path_file = os.path.join(path_output, f'return_period_3d_{"_".join(regions.keys())}_4_techs.png')
    fig.write_image(path_file, format='png', width=1800, height=2400, scale=3)

    
#%% 3d return period plot for one regions, three seasons (summer, winter, yearly) and four techs: combined 4x3

techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'wind_offshore': 'wind offshore', 'portfolio': 'portfolio'}
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}
data_orig = [return_period_q23, return_period_q14, return_period_yearly] # , return_period_q1, return_period_q2, return_period_q3, return_period_q4]
kind = ['Q2+Q3', 'Q1+Q4', 'yearly'] # , 'Q1', 'Q2', 'Q3', 'Q4', 'Q1+Q4', 'Q2+Q3']

for region_code, region_name in regions.items():

    # Create subplot figure with 4 rows and 3 columns
    fig = make_subplots(
        rows=4, cols=3,
        specs=[[{'type': 'surface'}]*3]*4,
        horizontal_spacing=0,
        vertical_spacing=0
    )

    annotations = []
    
    max_val = 0
    
    for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
        for col_idx, data_set in enumerate(data_orig, start=1):
            data = data_set.query('technology == @tech_code and region == @region_code').copy()
            data.max_period.replace(0, np.nan, inplace=True)            
            data_wide = data.pivot(index='return_period', columns='fraction', values='max_period')
            data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
            data_wide_y = data_wide.columns.to_numpy()
            data_wide_z = data_wide.T.values
            max_z = np.nanmax(data_wide_z)
            rounded_z = math.ceil(max_z / 50) * 50
            
            if max_z > max_val:
                max_val = max_z
    
            fig.add_trace(
                go.Surface(
                    x=data_wide_x,
                    y=data_wide_y,
                    z=data_wide_z,
                    cmin=0,
                    cmax=rounded_z,
                    colorscale=custom_cont_color_scale,
                    coloraxis='coloraxis',  # Link the trace to the shared color axis
                    opacity=0.91,
                    contours={
                        "y": {"show": True, "start": 0.1, "end": 1, "size": 0.05, "color": "white"},
                    },
                    hovertemplate='return period [y]: %{x}<br>' + 'threshold: %{y}<br>' + 'duration [d]: %{z}<extra></extra>'
                ),
                row=row_idx, col=col_idx
            )
            
            # Find the z-value for x=168, x=336 and y=0.7
            for x_value in [10, 20, 30]:
                if x_value in data_wide_x and 0.7 in data_wide_y:
                    x_index = np.where(data_wide_x == x_value)[0][0]
                    y_index = np.where(data_wide_y == 0.7)[0][0]
                    z_value = data_wide_z[y_index, x_index]

                    fig.add_trace(
                        go.Scatter3d(
                            x=[x_value],
                            y=[0.7],
                            z=[z_value],
                            mode='markers+text',
                            marker=dict(size=6.5, color='palevioletred', symbol='circle'),
                            text=[f'{z_value:.0f}'],
                            textposition='top left',
                            textfont=dict(color='palevioletred', size=18)
                        ),
                        row=row_idx, col=col_idx
                    )

    # Add annotations for region names at the top of each column
    for col_idx, col_name in enumerate(kind, start=1):
        annotations.append(
            dict(
                text=col_name,
                x=(col_idx - 1) / 3 + 1 / 6,
                y=1 - 0.03,  # Position above the first row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                xanchor='center'
            )
        )
    
    # Add annotations for technology names to the right of each row
    for row_idx, tech_name in enumerate(techs.values(), start=1):
        annotations.append(
            dict(
                text=tech_name,
                x=1,  # Position to the right of the last column
                y=1 - (row_idx - 1) / 4 - 1 / 8,
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                textangle=90,
                yanchor='middle'
            )
        )
    
    # Add x axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='return period [y]',
                x=(col_idx - 1) / 3 + 1 / 6 - 0.07,
                y=-0.001,  # Position below the fourth row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=16
            )
        )

    # Add y axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='relative threshold',
                x=(col_idx - 1) / 3 + 1 / 6 + 0.13,
                y=-0.017,  # Position below the fourth row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=-59
            )
        )
   
    # Configure the shared color axis and colorbar
    fig.update_layout(
        template='plotly_white',
        coloraxis=dict(
            colorscale=custom_cont_color_scale,
            cmin=0,
            cmax=max_val,
            colorbar=dict(
                title='duration [h]',
                title_side='top',
                lenmode='fraction',
                len=1,
                thickness=30,
                orientation='h',
                x=0.5,
                xanchor='center',
                y=-0.1,  # Position the colorbar below the plot
                title_font=dict(size=24),
                tickfont=dict(size=20)
            )
        ),
        showlegend=False,
        width=1800,
        height=2400,  # Increase height to accommodate extra row
        margin=dict(l=0, r=0, t=0, b=0),
        annotations=annotations  # Add annotations for subplot titles
    )


    scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 
              'scene7', 'scene8', 'scene9', 'scene10', 'scene11', 'scene12']

    # Adjust axes settings for all subplots
    for i, scene_id in enumerate(scenes):
        col_idx = (i % 3) + 1
        row_idx = (i // 3) + 1
        fig.update_layout(**{scene_id: dict(
            xaxis=dict(title='', range=[0, 39], dtick=5, tickfont=dict(size=15)),
            yaxis=dict(title='', range=[0.08, 1], dtick=0.2, tickfont=dict(size=15)),
            zaxis=dict(title='duration [h]' if col_idx == 1 else '', range=[-1, 9000], dtick=1000, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=14.5)),
            aspectmode='manual',
            aspectratio=dict(x=1.4, y=1, z=1.1),
            camera=dict(eye=dict(x=1.2, y=-2.15, z=0.2)) #x=1.4, y=-2, z=0.2))
        )})
        
    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, f'return_period_seasonal')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'return_period_seasonal_3d_{region_code}_4_techs.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'return_period_seasonal_3d_{region_code}_4_techs.pdf')
    fig.write_image(path_file, format='pdf', width=1800, height=2400, scale=3)
    path_file = os.path.join(path_output, f'return_period_seasonal_3d_{region_code}_4_techs.png')
    fig.write_image(path_file, format='png', width=1800, height=2400, scale=3)
   

#%% 3d return period  plot for three regions and one tech

techs = {'wind_offshore': 'Wind Offshore'}  # Only showing wind_offshore
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}
data_orig = [return_period_yearly]  # Example dataset
kind = ['yearly']  # Example kind

for idx, data_set in enumerate(data_orig):

    # Create subplot figure with 1 row and 3 columns
    fig = make_subplots(
        rows=1, cols=3,
        specs=[[{'type': 'surface'}]*3],
        horizontal_spacing=0,
        vertical_spacing=0
    )

    annotations = []
    
    for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
        data = data_set.query('technology == "wind_offshore" and region == @region_code').copy()
        data.max_period.replace(0, np.nan, inplace=True)
        data_wide = data.pivot(index='return_period', columns='fraction', values='max_period')
        data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
        data_wide_y = data_wide.columns.to_numpy()
        data_wide_z = data_wide.T.values
        max_z = np.nanmax(data_wide_z)
        rounded_z = math.ceil(max_z / 50) * 50

        fig.add_trace(
            go.Surface(
                x=data_wide_x,
                y=data_wide_y,
                z=data_wide_z,
                cmin=0,
                cmax=rounded_z,
                colorscale=custom_cont_color_scale,
                coloraxis='coloraxis',  # Link the trace to the shared color axis
                contours={
                    "y": {"show": True, "start": 0.1, "end": 1, "size": 0.05, "color": "white"},
                },
                hovertemplate='return period [y]: %{x}<br>' + 'threshold: %{y}<br>' + 'duration [d]: %{z}<extra></extra>'
            ),
            row=1, col=col_idx
        )

    # Add annotations for region names at the top of each column
    for col_idx, region_name in enumerate(regions.values(), start=1):
        annotations.append(
            dict(
                text=region_name,
                x=(col_idx - 1) / 3 + 1 / 6,
                y=1 - 0.12,  # Position above the first row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                xanchor='center'
            )
        )

    # Add x-axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='return period [y]',
                x=(col_idx - 1) / 3 + 1 / 6 - 0.07,
                y=-0.009,  # Position below the row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=19
            )
        )

    # Add y-axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='relative threshold',
                x=(col_idx - 1) / 3 + 1 / 6 + 0.12,
                y=-0.12,  # Position below the row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=-47
            )
        )
    
    # Configure the shared color axis and colorbar
    fig.update_layout(
        template='plotly_white',
        coloraxis=dict(
            colorscale=custom_cont_color_scale,
            cmin=0,
            cmax=max_val,
            colorbar=dict(
                title='duration [h]',
                title_side='top',
                lenmode='fraction',
                len=1,
                thickness=30,
                orientation='h',
                x=0.5,
                xanchor='center',
                y=-0.3,  # Position the colorbar below the plot
                title_font=dict(size=24),
                tickfont=dict(size=20)
            )
        ),
        width=1800,
        height=600, 
        margin=dict(l=0, r=0, t=0, b=0),
        annotations=annotations  # Add annotations for subplot titles
    )

    scenes = ['scene', 'scene2', 'scene3']

    # Adjust axes settings for all subplots
    for i, scene_id in enumerate(scenes):
        col_idx = (i % 3) + 1
        fig.update_layout(**{scene_id: dict(
            xaxis=dict(title='', range=[0, 39], dtick=5, tickfont=dict(size=15)),
            yaxis=dict(title='', range=[0.08, 1], dtick=0.2, tickfont=dict(size=15)),
            zaxis=dict(title='duration [h]' if col_idx == 1 else '', range=[-1, 9000], dtick=1000, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=14.5)),
            aspectmode='manual',
            aspectratio=dict(x=1.5, y=1, z=1.1),
            camera=dict(eye=dict(x=1.5, y=-2, z=0.2))
        )})
        
    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, f'return_period_{kind[idx]}')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'return_period_3d_{"_".join(regions.keys())}_wind_offshore.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'return_period_3d_{"_".join(regions.keys())}_wind_offshore.pdf')
    fig.write_image(path_file, format='pdf', width=1800, height=600, scale=3)
    path_file = os.path.join(path_output, f'return_period_3d_{"_".join(regions.keys())}_wind_offshore.png')
    fig.write_image(path_file, format='png', width=1800, height=600, scale=3)



#%%########################## frequency ################################################################################

frequency_yearly, frequency_q1, frequency_q2, frequency_q3, frequency_q4, frequency_q14, frequency_q23 \
    = get_frequency(a)

#%% 3d frequency plot for one region: 3d surface

def custom_round_up(value):
    # Define the thresholds
    thresholds = [25, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

    # Iterate through thresholds to find the minimum threshold greater than the value
    for threshold in thresholds:
        if value <= threshold:
            return threshold

    # If the value is above all thresholds, return the last one (or handle differently if needed)
    return thresholds[-1]

techs = ['pv', 'wind_onshore', 'wind_offshore', 'portfolio']
regions = ['CP'] #,'EU'] #,'ES'] #, 'ES', 'NO', 'FR', 'IT']
data_orig = [frequency_yearly, frequency_q1, frequency_q2, frequency_q3, frequency_q4, frequency_q14, frequency_q23] # 
kind = ['yearly', 'Q1', 'Q2', 'Q3', 'Q4', 'Q1+Q4', 'Q2+Q3'] # 

for idx, data_set in enumerate(data_orig):
    for tech in techs:
        for region in regions:

            data = data_set.query('technology == @tech and region == @region').copy()
            data.frequency.replace(0, np.nan, inplace=True)
            data_wide = data.pivot(index='duration', columns='fraction', values='frequency')
            data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
            data_wide_y = data_wide.columns.to_numpy()
            data_wide_z = data_wide.T.values
            max_z = np.nanmax(data_wide_z)
            rounded_z = custom_round_up(max_z)

            fig = go.Figure()

            fig.add_trace(
                go.Surface(
                    x=data_wide_x,
                    y=data_wide_y,
                    z=data_wide_z,
                    cmin=0,
                    cmax=350,
                    colorscale=custom_cont_color_scale,
                    contours={"y":{"show": True, "start":0.1, "end":1, "size": 0.05, "color":"white"},
                              "z":{"show": True, "start":1, "end":350, "size":70, "color":"white"},  # rounded_z+1, rounded_z/5
                              },
                    colorbar=dict(title='frequency', titleside='top', len=0.7, thickness=30, title_font=dict(size=20),
                                  tickfont=dict(size=20))
                    )
                )
            
            tech_clean = tech.replace('_', ' ')
            
            fig.update_layout(
                template='plotly_white',
                scene=dict(
                    xaxis=dict(title='duration (h)', range=[1,336], dtick=168, title_font=dict(size=22), tickfont=dict(size=16)),
                    yaxis=dict(title='relative threshold', range=[0.1,1], dtick=0.2, title_font=dict(size=22), tickfont=dict(size=16)),
                    zaxis=dict(title='frequency', range=[-1,350], dtick=70, title_font=dict(size=22), tickfont=dict(size=16)),  # rounded_z, rounded_z/5
                    camera=dict(eye=dict(x=1.65, y=-1.5, z=0.15)),
                    #annotations=[dict(showarrow=False, x=500, y=1.5, z=0, text="", xanchor="center", yanchor="top", textangle=45)],
                    aspectmode='manual',
                    aspectratio=dict(x=1, y=1, z=1),
                ),
                width=1000,
                height=1000,
                margin=dict(r=0, b=25, l=0, t=10),
            )
            
            fig.show()
      
            path = path = 'T:\\mk\\vreda\\result_plots\\'
            path_output = os.path.join(path, f'frequency_{kind[idx]}')
            Path(path_output).mkdir(parents=True, exist_ok=True)
            path_file = os.path.join(path_output, f'frequency_3d_{region}_{tech}.html')
            fig.write_html(path_file)
            path_file = os.path.join(path_output, f'frequency_3d_{region}_{tech}.pdf')
            fig.write_image(path_file, format='pdf', width=1000, height=1000, scale=3)
            path_file = os.path.join(path_output, f'frequency_3d_{region}_{tech}.png')
            fig.write_image(path_file, format='png', width=1000, height=1000, scale=3)
            
#%% 3d frequency plot for with three regions and three technologies: 3x3 3d surface

techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'portfolio': 'portfolio'}  # Mapping tech codes to names
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}
data_orig = [frequency_yearly]# , frequency_q1, frequency_q2, frequency_q3, frequency_q4, frequency_q14, frequency_q23]
kind = ['yearly']  # , 'Q1', 'Q2', 'Q3', 'Q4', 'Q1+Q4', 'Q2+Q3']

# Custom rounding function
def custom_round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return np.ceil(n * multiplier) / multiplier

for idx, data_set in enumerate(data_orig):

    # Create subplot figure with 3 rows and 3 columns
    fig = make_subplots(
        rows=3, cols=3,
        specs=[[{'type': 'surface'}]*3]*3,
        horizontal_spacing=0,
        vertical_spacing=0
    )

    annotations = []
    
    max_val = 0
    
    for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
        for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
            data = data_set.query('technology == @tech_code and region == @region_code').copy()
            data.frequency.replace(0, np.nan, inplace=True)
            data_wide = data.pivot(index='duration', columns='fraction', values='frequency')
            data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
            data_wide_y = data_wide.columns.to_numpy()
            data_wide_z = data_wide.T.values
            max_z = np.nanmax(data_wide_z)
            rounded_z = custom_round_up(max_z)
            
            if max_z > max_val:
                max_val = max_z

            fig.add_trace(
                go.Surface(
                    x=data_wide_x,
                    y=data_wide_y,
                    z=data_wide_z,
                    cmin=0,
                    cmax=max_z,
                    colorscale=custom_cont_color_scale,
                    coloraxis='coloraxis',  # Link the trace to the shared color axis
                    contours={
                        "y": {"show": True, "start": 0.1, "end": 1, "size": 0.05, "color": "white"},
                        "z": {"show": True, "start": 0, "end": 350, "size": 50, "color": "white"},
                    }
                ),
                row=row_idx, col=col_idx
            )

    # Add annotations for region names at the top of each column
    for col_idx, region_name in enumerate(regions.values(), start=1):
        annotations.append(
            dict(
                text=region_name,
                x=(col_idx - 1) / 3 + 1 / 6,
                y=1 - 0.04,  # Position above the first row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                xanchor='center'
            )
        )

    # Add annotations for technology names to the right of each row
    for row_idx, tech_name in enumerate(techs.values(), start=1):
        annotations.append(
            dict(
                text=tech_name,
                x=1,  # Position to the right of the last column
                y=1 - (row_idx - 1) / 3 - 1 / 6,
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                textangle=90,
                yanchor='middle'
            )
        )
        
    # add x axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='duration (h)',
                x=(col_idx - 1) / 3 + 1 / 6 - 0.09,
                y=0,  # Position below the third row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=28
            )
        )
    
    # add y axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='relative threshold',
                x=(col_idx - 1) / 3 + 1 / 6 + 0.11,
                y=-0.025,  # Position below the third row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=-29
            )
        )

    # Configure the shared color axis and colorbar
    fig.update_layout(
        template='plotly_white',
        coloraxis=dict(
            colorscale=custom_cont_color_scale,
            cmin=0,
            cmax=max_val,
            colorbar=dict(
                title='frequency',
                title_side='top',
                lenmode='fraction',
                len=1,
                thickness=30,
                orientation='h',
                x=0.5,
                xanchor='center',
                y=-0.1,  # Position the colorbar below the plot
                title_font=dict(size=24),
                tickfont=dict(size=20)
            )
        ),
        width=1800,
        height=1800, 
        margin=dict(l=0, r=0, t=0, b=0),
        annotations=annotations  # Add annotations for subplot titles
    )

    scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 'scene7', 'scene8', 'scene9']

    # Adjust axes settings for all subplots
    for i, scene_id in enumerate(scenes):
        col_idx = (i % 3) + 1
        row_idx = (i // 3) + 1
        fig.update_layout(**{scene_id: dict(
            xaxis=dict(title='', range=[1, 360], dtick=48, tickfont=dict(size=15)),
            yaxis=dict(title='', range=[0.1, 1], dtick=0.2, tickfont=dict(size=15)),
            zaxis=dict(title='frequency' if col_idx == 1 else '', range=[-1, 400], dtick=50, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=15)),
            aspectmode='manual',
            aspectratio=dict(x=1.35, y=1, z=1),
            camera=dict(eye=dict(x=1.65, y=-1.5, z=0.15))
        )})
        
    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, f'frequency_{kind[idx]}')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'frequency_3d_{"_".join(regions.keys())}_3_techs.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'frequency_3d_{"_".join(regions.keys())}_3_techs.pdf')
    fig.write_image(path_file, format='pdf', width=1800, height=1800, scale=3)
    path_file = os.path.join(path_output, f'frequency_3d_{"_".join(regions.keys())}_3_techs.png')
    fig.write_image(path_file, format='png', width=1800, height=1800, scale=3)
    
#%% 3d frequency plot for with three regions and four technologies: 4x3 3d surface

techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'wind_offshore': 'wind offshore', 'portfolio': 'portfolio'}
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}
data_orig = [frequency_yearly]  # , frequency_q1, frequency_q2, frequency_q3, frequency_q4, frequency_q14, frequency_q23]
kind = ['yearly']  # , 'Q1', 'Q2', 'Q3', 'Q4', 'Q1+Q4', 'Q2+Q3']

# Custom rounding function
def custom_round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return np.ceil(n * multiplier) / multiplier

for idx, data_set in enumerate(data_orig):

    # Create subplot figure with 4 rows and 3 columns
    fig = make_subplots(
        rows=4, cols=3,
        specs=[[{'type': 'surface'}]*3]*4,
        horizontal_spacing=0,
        vertical_spacing=0
    )

    annotations = []
    
    max_val = 0
    
    for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
        for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
            data = data_set.query('technology == @tech_code and region == @region_code').copy()
            data.frequency.replace(0, np.nan, inplace=True)
            data_wide = data.pivot(index='duration', columns='fraction', values='frequency')
            data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
            data_wide_y = data_wide.columns.to_numpy()
            data_wide_z = data_wide.T.values
            max_z = np.nanmax(data_wide_z)
            rounded_z = custom_round_up(max_z)
            
            if max_z > max_val:
                max_val = max_z

            fig.add_trace(
                go.Surface(
                    x=data_wide_x,
                    y=data_wide_y,
                    z=data_wide_z,
                    cmin=0,
                    cmax=max_z,
                    #opacity=0.99,
                    colorscale=custom_cont_color_scale,
                    coloraxis='coloraxis',  # Link the trace to the shared color axis
                    contours={
                        "y": {"show": True, "start": 0.1, "end": 1, "size": 0.05, "color": "white"},
                        "z": {"show": True, "start": 0, "end": 350, "size": 50, "color": "white"},
                    }
                ),
                row=row_idx, col=col_idx
            )

            # Find the z-value for x=168, x=336 and y=0.7
            for x_value in [48, 168, 336]:
                if x_value in data_wide_x and 0.75 in data_wide_y:
                    x_index = np.where(data_wide_x == x_value)[0][0]
                    y_index = np.where(data_wide_y == 0.75)[0][0]
                    z_value = data_wide_z[y_index, x_index]

                    fig.add_trace(
                        go.Scatter3d(
                            x=[x_value],
                            y=[0.75],
                            z=[z_value],
                            mode='markers+text',
                            marker=dict(size=6.5, color='palevioletred', symbol='circle'),
                            text=[f'{z_value:.1f}'],
                            textposition='top right',
                            textfont=dict(color='palevioletred', size=18)
                        ),
                        row=row_idx, col=col_idx
                    )

    # Add annotations for region names at the top of each column
    for col_idx, region_name in enumerate(regions.values(), start=1):
        annotations.append(
            dict(
                text=region_name,
                x=(col_idx - 1) / 3 + 1 / 6,
                y=1 - 0.03,  # Position above the first row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                xanchor='center'
            )
        )

    # Add annotations for technology names to the right of each row
    for row_idx, tech_name in enumerate(techs.values(), start=1):
        annotations.append(
            dict(
                text=tech_name,
                x=1,  # Position to the right of the last column
                y=1 - (row_idx - 1) / 4 - 1 / 8,
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                textangle=90,
                yanchor='middle'
            )
        )
        
    # add x axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='duration (h)',
                x=(col_idx - 1) / 3 + 1 / 6 - 0.09,
                y=0,  # Position below the fourth row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=28
            )
        )
    
    # add y axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='relative threshold',
                x=(col_idx - 1) / 3 + 1 / 6 + 0.11,
                y=-0.02,
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=-29
            )
        )

    # Configure the shared color axis and colorbar
    fig.update_layout(
        template='plotly_white',
        coloraxis=dict(
            colorscale=custom_cont_color_scale,
            cmin=0,
            cmax=max_val,
            colorbar=dict(
                title='frequency',
                title_side='top',
                lenmode='fraction',
                len=1,
                thickness=30,
                orientation='h',
                x=0.5,
                xanchor='center',
                y=-0.1,  # Position the colorbar below the plot
                title_font=dict(size=24),
                tickfont=dict(size=20)
            )
        ),
        showlegend=False,
        width=1800,
        height=2400,  # Increase height to accommodate extra row
        margin=dict(l=0, r=0, t=0, b=0),
        annotations=annotations  # Add annotations for subplot titles
    )

    scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 'scene7', 'scene8', 'scene9', 'scene10', 'scene11', 'scene12']

    # Adjust axes settings for all subplots
    for i, scene_id in enumerate(scenes):
        col_idx = (i % 3) + 1
        row_idx = (i // 3) + 1
        fig.update_layout(**{scene_id: dict(
            xaxis=dict(title='', range=[1, 360], dtick=48, tickfont=dict(size=15)),
            yaxis=dict(title='', range=[0.1, 1], dtick=0.2, tickfont=dict(size=15)),
            zaxis=dict(title='frequency' if col_idx == 1 else '', range=[-1, 400], dtick=50, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=15)),
            aspectmode='manual',
            aspectratio=dict(x=1.35, y=1, z=1),
            camera=dict(eye=dict(x=1.65, y=-1.5, z=0.15))
        )})
        
    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, f'frequency_{kind[idx]}')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'frequency_3d_{"_".join(regions.keys())}_4_techs.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'frequency_3d_{"_".join(regions.keys())}_4_techs.pdf')
    fig.write_image(path_file, format='pdf', width=1800, height=2400)
    path_file = os.path.join(path_output, f'frequency_3d_{"_".join(regions.keys())}_4_techs.png')
    fig.write_image(path_file, format='png', width=1800, height=2400, scale=3)



# %% 3d frequency plot for with three regions and one technology: 1x3 3d surface

# Example data preparation
techs = {'wind_offshore': 'Wind Offshore'}  # Only showing wind_offshore
regions = {'DE': 'Germany', 'ES': 'Spain', 'CP': 'European copperplate'}
data_orig = [frequency_yearly]
kind = ['yearly']

# Custom rounding function
def custom_round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return np.ceil(n * multiplier) / multiplier

for idx, data_set in enumerate(data_orig):
    # Create subplot figure with 1 row and 3 columns
    fig = make_subplots(
        rows=1, cols=3,
        specs=[[{'type': 'surface'}]*3],
        horizontal_spacing=0,
        vertical_spacing=0
    )

    annotations = []
    
    for col_idx, (region_code, region_name) in enumerate(regions.items(), start=1):
        data = data_set.query('technology == "wind_offshore" and region == @region_code').copy()
        data.frequency.replace(0, np.nan, inplace=True)
        data_wide = data.pivot(index='duration', columns='fraction', values='frequency')
        data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
        data_wide_y = data_wide.columns.to_numpy()
        data_wide_z = data_wide.T.values
        max_z = np.nanmax(data_wide_z)
        rounded_z = custom_round_up(max_z)

        fig.add_trace(
            go.Surface(
                x=data_wide_x,
                y=data_wide_y,
                z=data_wide_z,
                cmin=0,
                cmax=max_z,
                colorscale=custom_cont_color_scale,
                coloraxis='coloraxis',  # Link the trace to the shared color axis
                contours={
                    "y": {"show": True, "start": 0.1, "end": 1, "size": 0.05, "color": "white"},
                    "z": {"show": True, "start": 0, "end": 350, "size": 50, "color": "white"},
                }
            ),
            row=1, col=col_idx
        )

    # Add annotations for region names at the top of each column
    for col_idx, region_name in enumerate(regions.values(), start=1):
        annotations.append(
            dict(
                text=region_name,
                x=(col_idx - 1) / 3 + 1 / 6,
                y=1 - 0.12,  # Position above the first row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                xanchor='center'
            )
        )

    # Add x-axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='duration (h)',
                x=(col_idx - 1) / 3 + 1 / 6 - 0.09,
                y=0,  # Position below the row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=30
            )
        )

    # Add y-axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='relative threshold',
                x=(col_idx - 1) / 3 + 1 / 6 + 0.105,
                y=-0.08,  # Position below the row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=-28
            )
        )

    # Configure the shared color axis and colorbar
    fig.update_layout(
        template='plotly_white',
        coloraxis=dict(
            colorscale=custom_cont_color_scale,
            cmin=0,
            cmax=max_val,
            colorbar=dict(
                title='frequency',
                title_side='top',
                lenmode='fraction',
                len=1,
                thickness=30,
                orientation='h',
                x=0.5,
                xanchor='center',
                y=-0.3,  # Position the colorbar below the plot
                title_font=dict(size=24),
                tickfont=dict(size=20)
            )
        ),
        width=1800,
        height=600, 
        margin=dict(l=0, r=0, t=0, b=0),
        annotations=annotations  # Add annotations for subplot titles
    )

    scenes = ['scene', 'scene2', 'scene3']

    # Adjust axes settings for all subplots
    for i, scene_id in enumerate(scenes):
        col_idx = (i % 3) + 1
        fig.update_layout(**{scene_id: dict(
            xaxis=dict(title='', range=[1, 360], dtick=48, tickfont=dict(size=15)),
            yaxis=dict(title='', range=[0.1, 1], dtick=0.2, tickfont=dict(size=15)),
            zaxis=dict(title='frequency' if col_idx == 1 else '', range=[-1, 400], dtick=50, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=15)),
            aspectmode='manual',
            aspectratio=dict(x=1.35, y=1, z=1),
            camera=dict(eye=dict(x=1.65, y=-1.5, z=0.15))
        )})
        
    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, f'frequency_{kind[idx]}')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'frequency_3d_{"_".join(regions.keys())}_wind_offshore.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'frequency_3d_{"_".join(regions.keys())}_wind_offshore.pdf')
    fig.write_image(path_file, format='pdf', width=1800, height=600, scale=3)
    path_file = os.path.join(path_output, f'frequency_3d_{"_".join(regions.keys())}_wind_offshore.png')
    fig.write_image(path_file, format='png', width=1800, height=600, scale=3)

#%% 3d frequency plot for one region with three seasons (summer, winter, yearly) and four technologies: 4x3 3d surface

techs = {'pv': 'PV', 'wind_onshore': 'wind onshore', 'wind_offshore': 'wind offshore', 'portfolio': 'portfolio'}
regions = {'CP': 'European copperplate'} # 'DE': 'Germany', 'ES': 'Spain', 
data_orig = [frequency_q23, frequency_q14, frequency_yearly] # , frequency_q1, frequency_q2, frequency_q3, frequency_q4]
kind = ['Q2+Q3', 'Q1+Q4', 'yearly'] # , 'Q1', 'Q2', 'Q3', 'Q4', , ]

# Custom rounding function
def custom_round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return np.ceil(n * multiplier) / multiplier

for region_code, region_name in regions.items():

    # Create subplot figure with 4 rows and 3 columns
    fig = make_subplots(
        rows=4, cols=3,
        specs=[[{'type': 'surface'}]*3]*4,
        horizontal_spacing=0,
        vertical_spacing=0
    )

    annotations = []
    
    max_val = 0
    
    for row_idx, (tech_code, tech_name) in enumerate(techs.items(), start=1):
        for col_idx, data_set in enumerate(data_orig, start=1):
            data = data_set.query('technology == @tech_code and region == @region_code').copy()
            data.frequency.replace(0, np.nan, inplace=True)
            data_wide = data.pivot(index='duration', columns='fraction', values='frequency')
            data_wide_x = data_wide.index.to_numpy().astype(float).round(0)
            data_wide_y = data_wide.columns.to_numpy()
            data_wide_z = data_wide.T.values
            max_z = np.nanmax(data_wide_z)
            rounded_z = custom_round_up(max_z)
            
            if max_z > max_val:
                max_val = max_z

            fig.add_trace(
                go.Surface(
                    x=data_wide_x,
                    y=data_wide_y,
                    z=data_wide_z,
                    cmin=0,
                    cmax=max_z,
                    # opacity=0.91,
                    colorscale=custom_cont_color_scale,
                    coloraxis='coloraxis',  # Link the trace to the shared color axis
                    contours={
                        "y": {"show": True, "start": 0.1, "end": 1, "size": 0.05, "color": "white"},
                        "z": {"show": True, "start": 0, "end": 350, "size": 50, "color": "white"},
                    }
                ),
                row=row_idx, col=col_idx
            )
            
            # Find the z-value for x=168, x=336 and y=0.7
            for x_value in [48, 168, 336]:
                if x_value in data_wide_x and 0.75 in data_wide_y:
                    x_index = np.where(data_wide_x == x_value)[0][0]
                    y_index = np.where(data_wide_y == 0.75)[0][0]
                    z_value = data_wide_z[y_index, x_index]

                    fig.add_trace(
                        go.Scatter3d(
                            x=[x_value],
                            y=[0.7],
                            z=[z_value],
                            mode='markers+text',
                            marker=dict(size=6.5, color='palevioletred', symbol='circle'),
                            text=[f'{z_value:.1f}'],
                            textposition='top right',
                            textfont=dict(color='palevioletred', size=18)
                        ),
                        row=row_idx, col=col_idx
                    )

    # Add annotations for region names at the top of each column
    for col_idx, col_name in enumerate(kind, start=1):
        annotations.append(
            dict(
                text=col_name,
                x=(col_idx - 1) / 3 + 1 / 6,
                y=1 - 0.03,  # Position above the first row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                xanchor='center'
            )
        )

    # Add annotations for technology names to the right of each row
    for row_idx, tech_name in enumerate(techs.values(), start=1):
        annotations.append(
            dict(
                text=tech_name,
                x=1,  # Position to the right of the last column
                y=1 - (row_idx - 1) / 4 - 1 / 8,
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=30),
                textangle=90,
                yanchor='middle'
            )
        )
        
    # add x axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='duration (h)',
                x=(col_idx - 1) / 3 + 1 / 6 - 0.09,
                y=0,  # Position below the fourth row
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=28
            )
        )
    
    # add y axis title
    for col_idx in range(1, 4):
        annotations.append(
            dict(
                text='relative threshold',
                x=(col_idx - 1) / 3 + 1 / 6 + 0.11,
                y=-0.02,
                xref="paper",
                yref="paper",
                showarrow=False,
                font=dict(size=24),
                xanchor='center',
                textangle=-29
            )
        )

    # Configure the shared color axis and colorbar
    fig.update_layout(
        showlegend=False,
        template='plotly_white',
        coloraxis=dict(
            colorscale=custom_cont_color_scale,
            cmin=0,
            cmax=max_val,
            colorbar=dict(
                title='frequency',
                title_side='top',
                lenmode='fraction',
                len=1,
                thickness=30,
                orientation='h',
                x=0.5,
                xanchor='center',
                y=-0.1,  # Position the colorbar below the plot
                title_font=dict(size=24),
                tickfont=dict(size=20)
            )
        ),
        width=1800,
        height=2400,  # Increase height to accommodate extra row
        margin=dict(l=0, r=0, t=0, b=0),
        annotations=annotations  # Add annotations for subplot titles
    )

    scenes = ['scene', 'scene2', 'scene3', 'scene4', 'scene5', 'scene6', 'scene7', 'scene8', 'scene9', 'scene10', 'scene11', 'scene12']

    # Adjust axes settings for all subplots
    for i, scene_id in enumerate(scenes):
        col_idx = (i % 3) + 1
        row_idx = (i // 3) + 1
        fig.update_layout(**{scene_id: dict(
            xaxis=dict(title='', range=[1, 360], dtick=48, tickfont=dict(size=15)),
            yaxis=dict(title='', range=[0.1, 1], dtick=0.2, tickfont=dict(size=15)),
            zaxis=dict(title='frequency' if col_idx == 1 else '', range=[-1, 400], dtick=50, title_font=dict(size=24) if col_idx == 1 else None, tickfont=dict(size=15)),
            aspectmode='manual',
            aspectratio=dict(x=1.35, y=1, z=1),
            camera=dict(eye=dict(x=1.65, y=-1.5, z=0.15))
        )})
        
    fig.show()
    
    path = 'T:\\mk\\vreda\\result_plots\\'
    path_output = os.path.join(path, f'frequency_seasonal')
    Path(path_output).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_output, f'frequency_seasonal_3d_{region_code}_4_techs.html')
    fig.write_html(path_file)
    path_file = os.path.join(path_output, f'frequency_seasonal_3d_{region_code}_4_techs.pdf')
    fig.write_image(path_file, format='pdf', width=1800, height=2400, scale=3)
    path_file = os.path.join(path_output, f'frequency_season_3d_{region_code}_4_techs.png')
    fig.write_image(path_file, format='png', width=1800, height=2400, scale=3)




#%%########################## drought occurrence #######################################################################

# %% plot droughts as scatter plot for given time period, technology, a number of thresholds

drought_ts_region = a.drought_ts_region
del a

# %% static scatter plot of drought occurrence across 2 years for all regions & different thresholds

min_duration = 168
year_selection = [1990, 1991]
technology = 'portfolio'
thresholds = ['f-mean-cf=0.1', 'f-mean-cf=0.2', 'f-mean-cf=0.3', 'f-mean-cf=0.4',
              'f-mean-cf=0.5', 'f-mean-cf=0.6', 'f-mean-cf=0.7', 'f-mean-cf=0.8']  # , , 'f-mean-cf=0.9'

data = get_drought_ts_occurence_all_thresholds_scatter(min_duration, technology, drought_ts_region, year_selection)
data = data.query('threshold == @thresholds')

# %% static plot

fig = px.scatter(
    data,
    x=data.index,
    y=data.drought_indicator,
    facet_row=data.region,
    color=data.threshold,
    color_discrete_sequence=px.colors.qualitative.Prism,
    facet_row_spacing=0.01,
    category_orders={'region': ['EU', 'AT', 'BE', 'CH', 'CZ', 'DE', 'DK', 'FR', 'ES', 'PT', 'IT', 'NL', 'PL', 'UK',
                                'IE', 'EE', 'FI', 'LT', 'LV', 'NO', 'SE']}
)

fig.update_traces(marker=dict(size=3))

start_year = data.year.unique()[0]
end_year = data.year.unique()[-1]

fig.update_layout(
    xaxis_range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
    yaxis_range=[0.05, 0.85],
    template='simple_white',
    margin=dict(l=0, r=0, t=0, b=0),
    legend={'itemsizing': 'constant'}
)
fig.update_xaxes(dtick="M1", tickformat="%b%y")
fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))
fig.update_yaxes(visible=False, showticklabels=False)
fig.update_xaxes(title='')
fig.show()

path_output = os.path.join(path, 'additional_plots', 'drought_time_series',
                           f'all_regions_all_thresholds_{start_year}_{end_year}.html')
fig.write_html(path_output)

# %%########################## drought occurrence, SOC, NEX ############################################################

# %% scatter plot of drought occurrence across 2 years for all regions & different thresholds + H2_STO_L + net exports

min_duration = 48
years_vreda = [1996, 1997]
year_dieterpy = 1996
technology = 'portfolio'
thresholds = ['f-mean-cf=0.1', 'f-mean-cf=0.2', 'f-mean-cf=0.3', 'f-mean-cf=0.4', 'f-mean-cf=0.5',
              'f-mean-cf=0.6', 'f-mean-cf=0.7']  # 'f-mean-cf=0.8'], 'f-mean-cf=0.9']

# VRE drought occurrence
drought_occurrence = get_drought_ts_occurence_all_thresholds_scatter(min_duration, technology,
                                                                     drought_ts_region, years_vreda)
drought_occurrence = drought_occurrence.query('threshold == @thresholds')
drought_occurrence_no_eu = drought_occurrence.query('region != "EU"').copy()
drought_occurrence_eu = drought_occurrence.query('region == "EU"').copy()

# H2 SOC data [TWh]
path_h2_sto_l = f"D:\\git\\vre_droughts_power_sector\\result_data\\h2_sto_l_{year_dieterpy}.csv"
h2_sto_l = pd.read_csv(path_h2_sto_l)
h2_sto_l_no = h2_sto_l.query('custom_flow == "no"')
h2_sto_l_entsoe = h2_sto_l.query('custom_flow == "ENTSO-E"')
h2_sto_l_copper = h2_sto_l.query('custom_flow == "limitless"')

# NEX data
path_nex = f"D:\\git\\vre_droughts_power_sector\\result_data\\nex_{year_dieterpy}.csv"
nex = pd.read_csv(path_nex)
nex_copper = nex.query('custom_flow == "limitless"')
nex_entsoe = nex.query('custom_flow == "ENTSO-E"')

# NEX from MWh to GWh
nex_copper.loc[:, 'value'] /= 1e3
nex_entsoe.loc[:, 'value'] /= 1e3

# pickle
datasets = [drought_occurrence, h2_sto_l_no, h2_sto_l_entsoe, h2_sto_l_copper, nex_copper, nex_entsoe]
datasets_names = ['drought_occurrence', 'h2_sto_l_no', 'h2_sto_l_entsoe', 'h2_sto_l_copper', 'nex_copper', 'nex_entsoe']
path = os.chdir(os.path.join(a.path_run, 'additional_plots', 'data'))
for idx, d in enumerate(datasets):
    file_name = f'{datasets_names[idx]}.gz'
    with gzip.open(file_name, 'wb') as file:
        pickle.dump(d, file, protocol=pickle.HIGHEST_PROTOCOL)
path = a.path_run
colors_threshold = px.colors.qualitative.Prism_r
colors = [
    "#1f77b4",  # Blue
    "#ff7f0e",  # Orange
    "#2ca02c",  # Green
    "#9467bd",  # Purple
    "#8c564b",  # Brown
    "#7f7f7f",  # Gray
    "#bcbd22",  # Olive
    "#17becf",  # Cyan
    "#aec7e8",  # Light Blue
    "#98df8a",  # Light Green
    "#c5b0d5",  # Light Purple
    "#c49c94",  # Light Brown
    "#ffbb78",  # Light Orange
    "#ff9896",  # Light Red
    "#f7b6d2",  # Light Pink
    "#1f78b4",  # Another Shade of Blue
    "#d62728",  # Another Shade of Red
    "#aec7e8",  # Another Shade of Light Blue
    "#c7c7c7",  # Another Shade of Gray
    "#e377c2",  # Another Shade of Pink
]

# %% plot EU copperplate, three sections: drought occurrence top, SOC, NEX

ts_len = 6

fig = make_subplots(rows=3, cols=1, row_heights=[0.1, 0.45, 0.45], shared_xaxes=True, vertical_spacing=0.08,
                    subplot_titles=(
                        "Drought occurence (limitless)",
                        "Long-duration storage operation (limitless)",
                        "Net exports (limitless)"),
                    )

# add drought ts
for idx, (threshold, group_drought) in enumerate(drought_occurrence_eu.groupby('threshold')):

    group_drought = group_drought.iloc[::ts_len]

    fig.append_trace(
        go.Scatter(
            x=group_drought.index,
            y=group_drought.drought_indicator,
            marker=dict(size=3, color=colors_threshold[idx]),
            name=threshold,
            mode='markers',
        ),
        row=1, col=1
    )

# add storage SOC
for idx, (region, group_sto) in enumerate(h2_sto_l_copper.groupby('n')):

    group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
    group_sto = group_sto.iloc[::ts_len]

    fig.append_trace(
        go.Scatter(
            x=group_drought.index,
            y=group_sto.MA,  # value,
            line=dict(width=2, color=colors[idx]),
            mode='lines',
            name=region,
            showlegend=False
        ),
        row=2, col=1
    )

# add net exports
for idx, (region, group_nex) in enumerate(nex_copper.groupby('n')):

    # compute rolling average
    group_nex['MA'] = group_nex['value'].rolling(window=ts_len).mean()
    group_nex = group_nex.iloc[::ts_len]

    fig.append_trace(
        go.Scatter(
            x=group_drought.index,
            y=group_nex.MA,  # value,
            line=dict(width=2, color=colors[idx]),
            mode='lines',
            name=region,
            # showlegend=False
        ),
        row=3, col=1,
    )

# Update xaxis properties
start_year = drought_occurrence_eu.year.unique()[0]
end_year = drought_occurrence_eu.year.unique()[-1]

fig.update_xaxes(
    title_text='Europe',
    range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
    showticklabels=False,
    dtick="M1",
    mirror=True,
    row=1, col=1)
fig.update_xaxes(
    title_text='',
    range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
    # showgrid=True,
    dtick="M1",
    mirror=True,
    row=2, col=1)
fig.update_xaxes(
    title_text='',
    range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
    # showgrid=True,
    row=3, col=1,
    dtick="M1",
    tickformat="%b%y",
    mirror=True)

# Update yaxis properties
fig.update_yaxes(title_text='', range=[0.05, 0.85], showticklabels=False,
                 ticks='', mirror=True, row=1, col=1)  # mirror=True
fig.update_yaxes(title_text='TWh', mirror=True, row=2, col=1)
fig.update_yaxes(title_text='GWh', mirror=True, row=3, col=1)

fig.update_layout(template='simple_white',
                  margin=dict(l=0, r=0, t=30, b=0),
                  legend={'itemsizing': 'constant'}  # height=600, width=800
                  )
fig.show()
path_output = os.path.join(path, 'additional_plots', 'drought_time_series_H2_STO_L_NEX',
                           f'copper_{start_year}_{end_year}')
fig.write_html(f'{path_output}.html')
fig.update_layout(height=1200, width=1600)
fig.write_image(f'{path_output}.pdf')

# %% plot EU copperplate selected countries: three sections: drought occurrence top, SOC, NEX

ts_len = 1
sel = ['DE', 'ES', 'FR', 'IT', 'NO', 'UK']
sel_no = len(sel)

drought_occurrence_sel = drought_occurrence.query('region in @sel')
h2_sto_l_copper_sel = h2_sto_l_copper.query('n in @sel')
nex_copper_sel = nex_copper.query('n in @sel')

# row_hights_drought_ts = [0.04, 0.12, 0.04, 0.12, 0.04, 0.12, 0.04, 0.12, 0.04, 0.12, 0.04, 0.12] # for i in range(6)]
row_heights = [0.04, 0.12, 0.04, 0.12, 0.04, 0.12, 0.04, 0.12, 0.04, 0.12, 0.04, 0.12]
row_no = len(row_heights)

specs_y = [[{"secondary_y": True}]]*12

fig = make_subplots(rows=row_no, cols=1, row_heights=row_heights,
                    shared_xaxes=True, vertical_spacing=0.02, specs=[*specs_y])
fig.print_grid()

# add drought ts
regions = []
row_counter = 1
for region, group_drought_region in drought_occurrence_sel.groupby('region'):

    regions.append(region)
    regions.append(region)

    for idx, (threshold, group_drought_threshold) in enumerate(group_drought_region.groupby('threshold')):

        group_drought_threshold = group_drought_threshold.iloc[::ts_len]

        # with legend
        if row_counter == 1:
            fig.add_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold,
                    mode='markers',
                ),
                row=row_counter, col=1,
                secondary_y=False
            )
        # without legend
        else:
            fig.add_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold,
                    mode='markers',
                    showlegend=False
                ),
                row=row_counter, col=1,
                secondary_y=False
            )
    row_counter += 2

# add net exports
row_counter = 2
for idx, (region, group_nex) in enumerate(nex_copper_sel.groupby('n')):

    # compute rolling average
    group_nex['MA'] = group_nex['value'].rolling(window=ts_len).mean()
    group_nex = group_nex.iloc[::ts_len]

    # with legend
    if row_counter == 2:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_nex.MA,
                line=dict(width=1.5, color=colors[1]),
                mode='lines',
                name='net exports',  # <- '' for final plot
                # showlegend=False <- activate for final plot
            ),
            row=row_counter, col=1,
            secondary_y=False
        )
        row_counter += 2
    # without legend
    else:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_nex.MA,
                line=dict(width=1.5, color=colors[1]),
                mode='lines',
                # name='net exports', # <- '' for final plot
                showlegend=False
            ),
            row=row_counter, col=1,
            secondary_y=False
        )
        row_counter += 2

# add storage SOC
row_counter = 2
for idx, (region, group_sto) in enumerate(h2_sto_l_copper_sel.groupby('n')):

    group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
    group_sto = group_sto.iloc[::ts_len]

    # with legend
    if row_counter == 2:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_sto.MA,
                line=dict(width=3, color=colors[0]),
                mode='lines',
                name='state of charge',
            ),
            row=row_counter, col=1,
            secondary_y=True
        )
        row_counter += 2
    else:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_sto.MA,
                line=dict(width=3, color=colors[0]),
                mode='lines',
                #name='state of charge',
                showlegend=False
            ),
            row=row_counter, col=1,
            secondary_y=True
        )
        row_counter += 2


# Update xaxis properties
start_year = drought_occurrence_sel.year.unique()[0]
end_year = drought_occurrence_sel.year.unique()[-1]

for r in range(row_no):
    if r < row_no-1:
        fig.update_xaxes(
            title_text='',
            range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
            # showgrid=True,
            dtick="M1",
            row=r+1, col=1)

fig.update_xaxes(
    title_text='',
    range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
    # showgrid=True,
    row=row_no, col=1,
    dtick="M1",
    tickformat="%b%y")

# Update yaxis properties
for r in range(1, row_no+1, 2):
    fig.update_yaxes(title=dict(text=regions[r], font=dict(size=20)), range=[
                     0.05, 0.85], ticks='', showticklabels=False, showline=False, row=r, col=1)
fig.update_layout(yaxis3=dict(title=dict(text='GW', font=dict(size=15))), yaxis4=dict(title='TWh'),
                  yaxis7=dict(title='GW'), yaxis8=dict(title='TWh'),
                  yaxis11=dict(title='GW'), yaxis12=dict(title='TWh'),
                  yaxis15=dict(title='GW'), yaxis16=dict(title='TWh'),
                  yaxis19=dict(title='GW'), yaxis20=dict(title='TWh'),
                  yaxis23=dict(title='GW'), yaxis24=dict(title='TWh')
                  )

fig.update_layout(template='simple_white',
                  margin=dict(l=0, r=0, t=30, b=0),
                  legend={'itemsizing': 'constant'}  # height=600, width=800
                  )

# fig.show()
path_output = os.path.join(path, 'additional_plots', 'drought_time_series_H2_STO_L_NEX',
                           f'copper_sel_{start_year}_{end_year}')
fig.write_html(f'{path_output}.html')
fig.update_layout(height=1200, width=1600)
fig.write_image(f'{path_output}.pdf')


# %% plot ENTSO-e: three sections: drought occurrence top, SOC, NEX

ts_len = 6

row_hights_drought_ts = [0.03 for i in range(20)]
row_heights = [*row_hights_drought_ts, 0.2, 0.2]
row_no = len(row_heights)

fig = make_subplots(rows=22, cols=1, row_heights=row_heights, shared_xaxes=True, vertical_spacing=0.02)

# add drought ts
regions = []
for row_counter, (region, group_drought_region) in enumerate(drought_occurrence_no_eu.groupby('region')):

    regions.append(region)

    for idx, (threshold, group_drought_threshold) in enumerate(group_drought_region.groupby('threshold')):

        group_drought_threshold = group_drought_threshold.iloc[::ts_len]

        # with legend
        if row_counter == 0:
            fig.append_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold,
                    mode='markers',
                ),
                row=row_counter+1, col=1
            )
        # without legend
        else:
            fig.append_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold,
                    mode='markers',
                    showlegend=False
                ),
                row=row_counter+1, col=1
            )

# add storage SOC
for idx, (region, group_sto) in enumerate(h2_sto_l_entsoe.groupby('n')):

    group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
    group_sto = group_sto.iloc[::ts_len]

    fig.append_trace(
        go.Scatter(
            x=group_drought_threshold.index,
            y=group_sto.MA,
            line=dict(width=2, color=colors[idx]),
            mode='lines',
            name=region,
        ),
        row=row_counter+2, col=1
    )

# add net exports
for idx, (region, group_nex) in enumerate(nex_entsoe.groupby('n')):

    # compute rolling average
    group_nex['MA'] = group_nex['value'].rolling(window=ts_len*4).mean()
    group_nex = group_nex.iloc[::ts_len]

    fig.append_trace(
        go.Scatter(
            x=group_drought_threshold.index,
            y=group_nex.MA,
            line=dict(width=2, color=colors[idx]),
            mode='lines',
            name=region,  # <- '' for final plot
            # showlegend=False <- activate for final plot
        ),
        row=row_counter+3, col=1,
    )

# Update xaxis properties
start_year = drought_occurrence_eu.year.unique()[0]
end_year = drought_occurrence_eu.year.unique()[-1]

for r in range(row_no):
    if r < row_no-1:
        fig.update_xaxes(
            title_text='',
            range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
            # showgrid=True,
            dtick="M1",
            mirror=True,
            row=r+1, col=1)

fig.update_xaxes(
    title_text='',
    range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
    # showgrid=True,
    row=row_no, col=1,
    dtick="M1",
    tickformat="%b%y",
    mirror=True)

# Update yaxis properties
for r in range(row_no):
    if r < row_no-2:
        fig.update_yaxes(title_text=regions[r], range=[0.05, 0.85], ticks='',
                         showticklabels=False, mirror=True, row=r+1, col=1)
fig.update_yaxes(title_text='TWh', mirror=True, row=row_no-1, col=1)
fig.update_yaxes(title_text='GW', mirror=True, row=row_no, col=1)

fig.update_layout(template='simple_white',
                  margin=dict(l=0, r=0, t=30, b=0),
                  legend={'itemsizing': 'constant'}  # height=600, width=800
                  )

# fig.show()
path_output = os.path.join(path, 'additional_plots', 'drought_time_series_H2_STO_L_NEX',
                           f'ENTSO-e_{start_year}_{end_year}')
fig.write_html(f'{path_output}.html')
fig.update_layout(height=1200, width=1600)
fig.write_image(f'{path_output}.pdf')

# %% plot ENTSO-e selected countries: three sections: drought occurrence top, SOC, NEX

ts_len = 12
sel = ['DE', 'DK', 'ES', 'FR', 'IT', 'FI', 'PL', 'SE', 'UK']  # ['DE', 'ES', 'FR', 'IT', 'NO', 'UK']
sel_no = len(sel)

drought_occurrence_sel = drought_occurrence.query('region in @sel')
h2_sto_l_entsoe_sel = h2_sto_l_entsoe.query('n in @sel')
h2_sto_l_no_sel = h2_sto_l_no.query('n in @sel')
nex_entsoe_sel = nex_entsoe.query('n in @sel')

row_heights = [0.04, 0.07, 0.04, 0.07, 0.04, 0.07, 0.04, 0.07,
               0.04, 0.07, 0.04, 0.07, 0.04, 0.07, 0.04, 0.07, 0.04, 0.07]
#row_heights = [0.04, 0.12, 0.04, 0.12, 0.04, 0.12, 0.04, 0.12, 0.04, 0.12, 0.04, 0.12]
row_no = len(row_heights)

specs_y = [[{"secondary_y": True}]]*18

fig = make_subplots(rows=row_no, cols=1, row_heights=row_heights,
                    shared_xaxes=True, vertical_spacing=0.02, specs=[*specs_y])
# fig.print_grid()

# add drought ts
regions = []
row_counter = 1
for region, group_drought_region in drought_occurrence_sel.groupby('region'):

    regions.append(region)
    regions.append(region)

    for idx, (threshold, group_drought_threshold) in enumerate(group_drought_region.groupby('threshold')):

        group_drought_threshold = group_drought_threshold.iloc[::ts_len]

        # with legend
        if row_counter == 1:
            fig.add_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold,
                    mode='markers',
                ),
                row=row_counter, col=1,
                secondary_y=False
            )
        # without legend
        else:
            fig.add_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold,
                    mode='markers',
                    showlegend=False
                ),
                row=row_counter, col=1,
                secondary_y=False
            )
    row_counter += 2

# add net exports
row_counter = 2
for idx, (region, group_nex) in enumerate(nex_entsoe_sel.groupby('n')):

    # compute rolling average
    group_nex['MA'] = group_nex['value'].rolling(window=ts_len).mean()
    group_nex = group_nex.iloc[::ts_len]

    # with legend
    if row_counter == 2:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_nex.MA,
                line=dict(width=1.5, color=colors[1]),
                mode='lines',
                name='net exports',
            ),
            row=row_counter, col=1,
            secondary_y=False
        )
        row_counter += 2
    # no legend
    else:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_nex.MA,
                line=dict(width=1.5, color=colors[1]),
                mode='lines',
                showlegend=False,
            ),
            row=row_counter, col=1,
            secondary_y=False
        )
        row_counter += 2

# add storage SOC ENTSO-E
row_counter = 2
for idx, (region, group_sto) in enumerate(h2_sto_l_entsoe_sel.groupby('n')):

    group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
    group_sto = group_sto.iloc[::ts_len]

    # with legend
    if row_counter == 2:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_sto.MA,
                line=dict(width=3, color=colors[0]),
                mode='lines',
                name='SOC (ENTSO-E)',
            ),
            row=row_counter, col=1,
            secondary_y=True
        )
        row_counter += 2
# =============================================================================
#     # no legend
#     else:
#         fig.add_trace(
#             go.Scatter(
#                 x=group_drought_threshold.index,
#                 y=group_sto.MA,
#                 line=dict(width=3, color=colors[0]),
#                 mode='lines',
#                 showlegend=False,
#                 ),
#             row=row_counter, col=1,
#             secondary_y=True
#             )
#         row_counter += 2
# =============================================================================

# add storage SOC - NO
row_counter = 2
for idx, (region, group_sto) in enumerate(h2_sto_l_no_sel.groupby('n')):

    group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
    group_sto = group_sto.iloc[::ts_len]

    # with legend
    if row_counter == 2:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_sto.MA,
                line=dict(width=3, color='k'),
                mode='lines',
                name='SOC (no interconnection)',
            ),
            row=row_counter, col=1,
            secondary_y=True
        )
        row_counter += 2
# =============================================================================
#     # no legend
#     else:
#         fig.add_trace(
#             go.Scatter(
#                 x=group_drought_threshold.index,
#                 y=group_sto.MA,
#                 line=dict(width=3, color=colors[0]),
#                 mode='lines',
#                 showlegend=False,
#                 ),
#             row=row_counter, col=1,
#             secondary_y=True
#             )
#         row_counter += 2
#
# =============================================================================

# Update xaxis properties
start_year = drought_occurrence_sel.year.unique()[0]
end_year = drought_occurrence_sel.year.unique()[-1]

for r in range(row_no):
    if r < row_no-1:
        fig.update_xaxes(
            title_text='',
            range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
            # showgrid=True,
            dtick="M1",
            mirror=True,
            row=r+1, col=1)

fig.update_xaxes(
    title_text='',
    range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
    # showgrid=True,
    row=row_no, col=1,
    dtick="M1",
    tickformat="%b%y",
    mirror=True)

# Update yaxis properties
for r in range(1, row_no+1, 2):
    fig.update_yaxes(title=dict(text=regions[r], font=dict(size=20)), range=[
                     0.05, 0.85], ticks='', showticklabels=False, mirror=True, row=r, col=1)
fig.update_layout(yaxis3=dict(title=dict(text='GW', font=dict(size=15)), mirror=True,), yaxis4=dict(title='TWh', mirror=True),
                  yaxis7=dict(title='GW', mirror=True), yaxis8=dict(title='TWh', mirror=True),
                  yaxis11=dict(title='GW', mirror=True), yaxis12=dict(title='TWh', mirror=True),
                  yaxis15=dict(title='GW', mirror=True), yaxis16=dict(title='TWh', mirror=True),
                  yaxis19=dict(title='GW', mirror=True), yaxis20=dict(title='TWh', mirror=True),
                  yaxis23=dict(title='GW', mirror=True), yaxis24=dict(title='TWh', mirror=True)
                  )

fig.update_layout(template='simple_white',
                  margin=dict(l=0, r=0, t=30, b=0),
                  legend={'itemsizing': 'constant'}  # height=600, width=800
                  )

# fig.show()
path_output = os.path.join(path, 'additional_plots', 'drought_time_series_H2_STO_L_NEX',
                           f'ENTSO-e_sel_{start_year}_{end_year}')
fig.write_html(f'{path_output}.html')
fig.update_layout(height=1200, width=1600)
fig.write_image(f'{path_output}.pdf')


# %% plot countries only (no interconnection)

ts_len = 1

row_hights_drought_ts = [0.03 for i in range(20)]
row_heights = [*row_hights_drought_ts, 0.4]
row_no = len(row_heights)

fig = make_subplots(rows=row_no, cols=1, row_heights=row_heights, shared_xaxes=True, vertical_spacing=0.02)

# add drought ts
regions = []
for row_counter, (region, group_drought_region) in enumerate(drought_occurrence_no_eu.groupby('region')):

    regions.append(region)

    for idx, (threshold, group_drought_threshold) in enumerate(group_drought_region.groupby('threshold')):

        group_drought_threshold = group_drought_threshold.iloc[::ts_len]

        # with legend
        if row_counter == 0:
            fig.append_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold,
                    mode='markers',
                ),
                row=row_counter+1, col=1
            )
        # without legend
        else:
            fig.append_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold,
                    mode='markers',
                    showlegend=False
                ),
                row=row_counter+1, col=1
            )

# add storage SOC
for idx, (region, group_sto) in enumerate(h2_sto_l_no.groupby('n')):

    group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
    group_sto = group_sto.iloc[::ts_len]

    fig.append_trace(
        go.Scatter(
            x=group_drought_threshold.index,
            y=group_sto.value,  # .MA,
            line=dict(width=2, color=colors[0]),
            mode='lines',
            name='state of charge (no interconnection)',
        ),
        row=row_counter+2, col=1
    )

# add storage SOC
for idx, (region, group_sto) in enumerate(h2_sto_l_entsoe_sel.groupby('n')):

    group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
    group_sto = group_sto.iloc[::ts_len]

    fig.append_trace(
        go.Scatter(
            x=group_drought_threshold.index,
            y=group_sto.value,  # .MA,
            line=dict(width=2, color=colors[1]),
            mode='lines',
            name='state of charge (no interconnection)',
        ),
        row=row_counter+2, col=1
    )

# Update xaxis properties
start_year = drought_occurrence_no_eu.year.unique()[0]
end_year = drought_occurrence_no_eu.year.unique()[-1]

for r in range(row_no):
    if r < row_no-2:
        fig.update_xaxes(
            title_text='',
            range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
            mirror=True,
            dtick="M1",
            row=r+1, col=1)

fig.update_xaxes(
    title_text='',
    range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
    mirror=True,
    row=row_no, col=1,
    dtick="M1",
    tickformat="%b%y")

# Update yaxis properties
for r in range(row_no):
    if r < row_no-1:
        fig.update_yaxes(title_text=regions[r], range=[0.05, 0.85], mirror=True,
                         showticklabels=False, ticks='', row=r+1, col=1)
fig.update_yaxes(title_text='State of charge [TWh]', mirror=True, row=row_no, col=1)

fig.update_layout(template='simple_white',
                  margin=dict(l=0, r=0, t=30, b=0),
                  legend={'itemsizing': 'constant'}  # height=600, width=800
                  )

# fig.show()
path_output = os.path.join(path, 'additional_plots', 'drought_time_series_H2_STO_L_NEX',
                           f'no_interconnection_{start_year}_{end_year}')
# fig.write_html(f'{path_output}.html')
fig.update_layout(height=1200, width=1600)
fig.write_image(f'{path_output}.pdf')

# %% plot no interconnection selected countries: two sections: drought occurrence top, SOC

ts_len = 1
sel = ['DE', 'DK', 'ES', 'FR', 'IT', 'FI', 'PL', 'SE', 'UK']
sel_no = len(sel)

drought_occurrence_sel = drought_occurrence.query('region in @sel')
h2_sto_l_no_sel = h2_sto_l_no.query('n in @sel')
h2_sto_l_entsoe_sel = h2_sto_l_entsoe.query('n in @sel')

row_heights = [0.04, 0.07, 0.04, 0.07, 0.04, 0.07, 0.04, 0.07,
               0.04, 0.07, 0.04, 0.07, 0.04, 0.07, 0.04, 0.07, 0.04, 0.07, ]
row_no = len(row_heights)

fig = make_subplots(rows=row_no, cols=1, row_heights=row_heights, shared_xaxes=True, vertical_spacing=0.01)
# fig.print_grid()

# add drought ts
regions = []
row_counter = 1
for region, group_drought_region in drought_occurrence_sel.groupby('region'):

    regions.append(region)
    regions.append(region)

    for idx, (threshold, group_drought_threshold) in enumerate(group_drought_region.groupby('threshold')):

        group_drought_threshold = group_drought_threshold.iloc[::ts_len]

        # with legend
        if row_counter == 1:
            fig.add_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold[-3:],
                    mode='markers',
                ),
                row=row_counter, col=1,
            )
        # without legend
        else:
            fig.add_trace(
                go.Scatter(
                    x=group_drought_threshold.index,
                    y=group_drought_threshold.drought_indicator,
                    marker=dict(size=3, color=colors_threshold[idx]),
                    name=threshold,
                    mode='markers',
                    showlegend=False
                ),
                row=row_counter, col=1,
            )
    row_counter += 2

# add storage SOC (no interconnection)
row_counter = 2
for idx, (region, group_sto) in enumerate(h2_sto_l_no_sel.groupby('n')):

    group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
    group_sto = group_sto.iloc[::ts_len]

    # legend
    if idx == 0:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_sto.MA,
                line=dict(width=3, color=colors[0]),
                mode='lines',
                name='state of charge\n(no interconnection)',
            ),
            row=row_counter, col=1,
        )
        row_counter += 2
    # legend
    else:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_sto.MA,
                line=dict(width=3, color=colors[0]),
                mode='lines',
                name='',
                showlegend=False
            ),
            row=row_counter, col=1,
        )
        row_counter += 2

# add storage SOC (ENTSO-e)
row_counter = 2
for idx, (region, group_sto) in enumerate(h2_sto_l_entsoe_sel.groupby('n')):

    group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
    group_sto = group_sto.iloc[::ts_len]

    # legend
    if idx == 0:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_sto.MA,
                line=dict(width=3, color=colors[1]),
                mode='lines',
                name='state of charge\n(TYNDP)',
            ),
            row=row_counter, col=1,
        )
        row_counter += 2
    # legend
    else:
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=group_sto.MA,
                line=dict(width=3, color=colors[1]),
                mode='lines',
                name='',
                showlegend=False
            ),
            row=row_counter, col=1,
        )
        row_counter += 2


# Update xaxis properties
start_year = drought_occurrence_sel.year.unique()[0]
end_year = drought_occurrence_sel.year.unique()[-1]

for r in range(row_no):
    if r < row_no-1:
        fig.update_xaxes(
            title_text='',
            range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
            mirror=True,
            dtick="M1",
            row=r+1, col=1)

fig.update_xaxes(
    title_text='',
    range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
    mirror=True,
    row=row_no, col=1,
    dtick="M1",
    tickformat="%b%y")

# Update yaxis properties
for r in range(1, row_no+1, 2):
    fig.update_yaxes(title=dict(text=regions[r], font=dict(size=20)), range=[
                     0.05, 0.85], ticks='', showticklabels=False, mirror=True, row=r, col=1)
fig.update_layout(yaxis2=dict(title=dict(text='TWh', font=dict(size=15)), mirror=True), yaxis4=dict(title='TWh', mirror=True),
                  yaxis6=dict(title='TWh', mirror=True), yaxis8=dict(title='TWh', mirror=True),
                  yaxis10=dict(title='TWh', mirror=True), yaxis12=dict(title='TWh', mirror=True),
                  yaxis16=dict(title='TWh', mirror=True), yaxis18=dict(title='TWh', mirror=True),
                  )

fig.update_layout(template='simple_white',
                  margin=dict(l=0, r=0, t=30, b=0),
                  legend={'itemsizing': 'constant'}
                  )

# fig.show()
path_output = os.path.join(path, 'additional_plots', 'drought_time_series_H2_STO_L_NEX',
                           f'no_interconnection-e_sel2_{start_year}_{end_year}')
# fig.write_html(f'{path_output}.html')
fig.update_layout(height=1200, width=2000)
fig.write_image(f'{path_output}.pdf')
fig.write_image(f'{path_output}.png')


# %% animated choropleth map of drought occurrence across multiple years for all regions with minimum threshold

min_duration = 1
year_selection = [1996, 1997]
start_date = '1996-12-01'
end_date = '1997-01-31'
technologies = ['portfolio']  # 'pv','wind_onshore','wind_offshore',
nthhour = 4

data = {}
data_eu = {}
data_eu_country_resolution = {}

for technology in technologies:
    # retrieve data
    data[technology] = get_drought_ts_occurence_min_threshold_scatter(min_duration, technology, drought_ts_region, year_selection)
    
    # exclude data that has been identified by threshold fraction 1.0 * mean cf
    #data = data.query('drought_qualification != 1.0')
    
    # replace no drought 0-value with 1.1
    data[technology]['drought_qualification'] = data[technology]['drought_qualification'].replace({0: 1.1})
    
    # reduce to every n-th hour
    temp = {}
    for region, group in data[technology].groupby('region'):
        # group = group.iloc[11:, :]
        group = group.iloc[::nthhour]
        temp[region] = group
    data[technology] = pd.concat(temp.values())
    
    # select certain period
    data[technology]['date_only'] = data[technology]['date'].dt.date.astype(str)
    data[technology] = data[technology].query('date_only >= @start_date and date_only <= @end_date')
    
    # split data
    data_eu[technology] = data[technology].query('region == "CP"').copy()
    data[technology] = data[technology].query('region != "CP"').copy()
    
    # prepare data_eu
    data_eu_country_resolution[technology] = data[technology].copy()
    temp = {}
    for r, group in data_eu_country_resolution[technology].groupby('region'):
        group.loc[:, 'drought_qualification'] = data_eu[technology].loc[:, 'drought_qualification']
        temp[r] = group
    data_eu_country_resolution[technology] = pd.concat(temp.values())
    
    path = 'T:\\mk\\vreda\\result_data'
    path_data_eu = os.path.join(path, f'data_eu_country_resolution_{technology}_{start_date}_{end_date}_nthhour_{nthhour}.csv')
    data_eu_country_resolution[technology].to_csv(path_data_eu, index=False)
    
    path_data = os.path.join(path, f'data_country_resolution_{technology}_{start_date}_{end_date}_nthhour_{nthhour}.csv')
    data[technology].to_csv(path_data, index=False)

# %% choropleth map of each hour of certain period with date slider

url = 'https://gisco-services.ec.europa.eu/distribution/v2/nuts/geojson/NUTS_RG_60M_2021_4326_LEVL_0.geojson'

with urlopen(url) as response:
    geojson_data = json.load(response)

# =============================================================================
# file_path = "T:\\mk\\vreda\\input\\NUTS_RG_60M_2021_4326.geojson"
# 
# with open(file_path, 'r', encoding='utf-8') as f:
#     geojson_data = json.load(f)
#     
# filtered_features = [feature for feature in geojson_data['features'] if len(feature['id']) <= 2]
# geojson_data['features'] = filtered_features
# =============================================================================

max_val = data['drought_qualification'].max()
min_val = data['drought_qualification'].min()

data = data.replace({'GR': 'EL'})

# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

# =============================================================================
# custom_colors_extended = [
#     (0, '#4a006e'),  # Dark Violet
#     (0.05,'#611785'),  # Between Dark Violet and Violet
#     (0.1, '#78209b'),  # Violet
#     #(0.25, '#7f104c'),  # Transition between Violet and Dark Red
#     (0.15, '#840826'),  # New Color (Enhanced Violet to Dark Red Transition)
#     (0.2, '#880000'),  # Dark Red
#     (0.25, '#a41100'),  # Between Dark Red and Light Red
#     (0.3, '#c62200'),  # Light Red
#     (0.35, '#e84013'),  # Between Light Red and Dark Orange
#     (0.4, '#f95927'),  # Dark Orange
#     (0.45, '#fc6f20'),  # New Color (Enhanced Red to Orange Transition)
#     (0.5, '#ff851a'),  # Between Dark Orange and Light Orange
#     (0.55, '#ffa72d'),  # Light Orange
#     (0.6, '#ffcc2a'),  # Between Light Orange and Yellow
#     (0.65, '#ffdf2d'),  # Yellow
#     (0.7, '#d0d830'),  # New Color (Enhanced Yellow to Green Transition)
#     (0.75, '#a0d034'),  # Between Yellow and Light Green
#     (0.8, '#7cb342'),  # Light Green
#     (0.85, '#05723c'),  # Dark Green
#     (0.9, '#022915'),  # New Color (Enhanced Light Green to Dark Green Transition)
#     (1.0, '#ffffff'),  # white
# ]
# =============================================================================


fig = px.choropleth_mapbox(
    data,
    geojson=geojson_data, #ccaa,
    featureidkey='properties.CNTR_CODE',
    locations='region',
    color='drought_qualification',
    color_continuous_scale=custom_colors, #custom_colors_extended,
    animation_frame="date_only",
    range_color=[min_val, max_val],
    height=950,
    width=850,
    labels={'drought_qualification': 'minimum relative drought threshold'},
)
    
tick_text = [*[round(0.1*i, 1) for i in range(1, 11)], 'no drought']

fig.update_layout(
    mapbox_style="white-bg",
    mapbox_zoom=3,
    mapbox_center={"lat": 57.5, "lon": 10},
    coloraxis_colorbar=dict(
        title=dict(
            text="minimum drought threshold",
            side="right"  # Move the label to the top
        ),
        tickvals=[round(0.1*i, 1) for i in range(1,12)],
        ticktext=tick_text,
        lenmode="pixels",
        
    )
)
fig.update_geos(visible=False, resolution=50)  # , projection_type="equirectangular", fitbounds="locations",

# increase animation speed
fig.layout.updatemenus[0].buttons[0].args[1]['frame']['duration'] = 150
fig.layout.updatemenus[0].buttons[0].args[1]['transition']['duration'] = 30

# fig.update_layout(autosize=True)# , geo=dict(projection_scale=5))
fig.show()

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'threshold_maps')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'threshold_map_{technology}_{year_selection[0]}_{year_selection[-1]}.html')
fig.write_html(path_file)

# %% choropleth map of each hour of certain period with date slider EU

tech = 'portfolio'

url = 'https://gisco-services.ec.europa.eu/distribution/v2/nuts/geojson/NUTS_RG_60M_2021_4326_LEVL_0.geojson'

with urlopen(url) as response:
    geojson_data = json.load(response)

file_path = "T:\\mk\\vreda\\input\\NUTS_RG_60M_2021_4326.geojson"

with open(file_path, 'r', encoding='utf-8') as f:
    geojson_data = json.load(f)
    
filtered_features = [feature for feature in geojson_data['features'] if len(feature['id']) <= 2]
geojson_data['features'] = filtered_features

max_val = data[tech]['drought_qualification'].max()
min_val = data[tech]['drought_qualification'].min()

data_eu_country_resolution[tech] = data_eu_country_resolution[tech].replace({'GR': 'EL'})

# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

fig = px.choropleth_mapbox(
    data_eu_country_resolution[tech],
    geojson=geojson_data, #ccaa,
    featureidkey='properties.CNTR_CODE',
    locations='region',
    color='drought_qualification',
    color_continuous_scale=custom_colors, #custom_colors_extended,
    animation_frame="date_only",
    range_color=[min_val, max_val],
    height=950,
    width=850,
    labels={'drought_qualification': 'minimum relative drought threshold'},
)
    
tick_text = [*[round(0.1*i, 1) for i in range(1, 11)], 'no drought']

fig.update_layout(
    mapbox_style="white-bg",
    mapbox_zoom=3,
    mapbox_center={"lat": 57.5, "lon": 10},
    coloraxis_colorbar=dict(
        title=dict(
            text="minimum drought threshold",
            side="right"
        ),
        tickvals=[round(0.1*i, 1) for i in range(1,12)],
        ticktext=tick_text,
        lenmode="pixels",
        
    )
)
fig.update_geos(visible=False, resolution=50)  # , projection_type="equirectangular", fitbounds="locations",

# increase animation speed
fig.layout.updatemenus[0].buttons[0].args[1]['frame']['duration'] = 150
fig.layout.updatemenus[0].buttons[0].args[1]['transition']['duration'] = 30

# fig.update_layout(autosize=True)# , geo=dict(projection_scale=5))
fig.show()

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'threshold_maps')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'threshold_map_EU_{technology}_{year_selection[0]}_{year_selection[-1]}.html')
fig.write_html(path_file)

#%% two animated maps side by side

from datetime import datetime

min_duration = 1
year_selection = [1996, 1997]
start_date = '1996-12-01'
end_date = '1997-01-31'
technologies = ['portfolio']  # 'pv','wind_onshore','wind_offshore',
nthhour = 12

tech = 'portfolio'
data_eu_country_resolution = {}
data = {}
data_eu_country_resolution[tech] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_eu_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_12.csv")
data[tech] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_12.csv")

path = "T:\\mk\\vreda\\input\\ref-countries-2020-20M\\CNTR_RG_20M_2020_4326.geojson"
with open(path, 'r', encoding='utf-8') as file:
    geojson_data = json.load(file)

# Replace 'GR' with 'EL' in the data
data_eu_country_resolution[tech] = data_eu_country_resolution[tech].replace({'GR': 'EL'})
data[tech] = data[tech].replace({'GR': 'EL'})
#data_eu_country_resolution[tech] = data_eu_country_resolution[tech].replace({'UK': 'GB'})
#data[tech] = data[tech].replace({'UK': 'GB'})

# Determine the range for the color scale
max_val = max(data[tech]['drought_qualification'].max(), data_eu_country_resolution[tech]['drought_qualification'].max())
min_val = min(data[tech]['drought_qualification'].min(), data_eu_country_resolution[tech]['drought_qualification'].min())

# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

# Define tick text and tick values
tick_text = [*[round(0.1 * i, 1) for i in range(1, 11)], 'no drought']
tickvals = [round(0.1 * i, 1) for i in range(1, 12)]

# Create subplots with custom titles
fig = make_subplots(rows=1, cols=2, 
                    subplot_titles=("island scenario", "European copperplate scenario"),
                    specs=[[{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}]])

# Add initial traces
initial_date = data[tech]['date_only'].unique()[0]
df1 = data[tech][data[tech]['date_only'] == initial_date]
df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date_only'] == initial_date]

trace1 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df1['region'],
    z=df1['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace2 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df2['region'],
    z=df2['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

fig.add_trace(trace1, row=1, col=1)
fig.add_trace(trace2, row=1, col=2)

# Create frames
frames = []
for date in data[tech]['date_only'].unique():
    df1 = data[tech][data[tech]['date_only'] == date]
    df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date_only'] == date]
    
    frames.append(go.Frame(data=[
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_ID',
            locations=df1['region'],
            z=df1['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox1"  # Link to the first map
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_ID',
            locations=df2['region'],
            z=df2['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox2"  # Link to the second map
        )
    ], name=str(date)))


def format_date(date_str):
    try:
        # Try to parse the date in the 'YYYY-MM-DD' format (no time information)
        date_obj = datetime.strptime(date_str, "%Y-%m-%d")
    except ValueError:
        try:
            # If it fails, try to parse in the 'DD.MM.YYYY' format (no time information)
            date_obj = datetime.strptime(date_str, "%d.%m.%Y")
        except ValueError:
            raise ValueError(f"Time data '{date_str}' does not match expected formats.")
    
    # Return the formatted string: day month year
    return date_obj.strftime("%d %b '%y")

# Add frames to the figure
fig.frames = frames

# Update layout
fig.update_layout(
    mapbox1=dict(
        domain=dict(x=[0, 0.5], y=[0, 1]),
        style="white-bg",
        zoom=3.1,
        center={"lat": 56.9, "lon": 10}
    ),
    mapbox2=dict(
        domain=dict(x=[0.5, 1], y=[0, 1]),
        style="white-bg",
        zoom=3.1,
        center={"lat": 56.9, "lon": 10}
    ),
    height=950,
    width=1300,  # Adjust width to fit both maps side by side
    margin=dict(l=0, r=0, t=40, b=0),  # Adjust margins to control white space
    coloraxis=dict(
        colorscale=custom_colors,
        cmin=min_val,
        cmax=max_val,
        colorbar=dict(
            title=dict(
                text="minimum drought threshold",
                side="right"
            ),
            titlefont=dict(size=18),
            tickvals=tickvals,
            ticktext=tick_text,
            tickfont=dict(size=14),
            outlinewidth=0.1  # Remove the frame around the colorbar
        )
    ),
    sliders=[{
        'x': 0.1,  # Move slider slightly to the right
        'len': 0.9,  # Shrink the slider
        'currentvalue': {
            'prefix': 'displayed time frame: ',
            'visible': True,
            'xanchor': 'right',
            'font': {'size': 17, 'color': '#666'}
        },
        'steps': [{
            'args': [
                [str(date)],
                {'frame': {'duration': 150, 'redraw': True}, 'mode': 'immediate'}
            ],
            'label': format_date(str(date)),  # Format date for the slider label
            'method': 'animate'
        } for date in data[tech]['date_only'].unique()],
        'transition': {'duration': 30},
    }],
    updatemenus=[{
        'buttons': [
            {
                'args': [None, {'frame': {'duration': 150, 'redraw': True}, 'fromcurrent': True, 'mode': 'immediate'}],
                'label': '⏵',  # Unicode symbol for play
                'method': 'animate'
            },
            {
                'args': [[None], {'frame': {'duration': 0, 'redraw': True}, 'mode': 'immediate'}],
                'label': '⏸',  # Unicode symbol for pause
                'method': 'animate'
            }
        ],
        'direction': 'left',
        'pad': {'r': 10, 't': 70},
        'showactive': False,
        'type': 'buttons',
        'x': 0.02,  # Position the buttons in the new empty space
        'xanchor': 'left',
        'y': 0.05,  # Adjust y position to move below the slider
        'yanchor': 'top'
    }]
)

# fig.update_geos(visible=False, resolution=50)

# Show the figure
fig.show()

# =============================================================================
# path = 'T:\\mk\\vreda\\result_plots\\'
# path = 'C:\\Users\\mkittel\\OneDrive - DIW Berlin\\Desktop'
# path_output = os.path.join(path, 'threshold_maps')
# Path(path_output).mkdir(parents=True, exist_ok=True)
# path_file = os.path.join(path_output, f'threshold_map_island_EU_{tech}_{year_selection[0]}_{year_selection[-1]}_midnight.html')
# fig.write_html(path_file)
# =============================================================================

# %% two animated maps side by side with hour for displayed time frame and date for slider

from datetime import datetime
tech = 'portfolio'

min_duration = 1
year_selection = [1996, 1997]
start_date = '1996-12-01'
end_date = '1997-01-31'
technologies = ['portfolio']  # 'pv','wind_onshore','wind_offshore',
nthhour = 12

tech = 'portfolio'
data_eu_country_resolution = {}
data = {}
data_eu_country_resolution[tech] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_eu_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_12.csv")
data[tech] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_12.csv")


path = "T:\\mk\\vreda\\input\\ref-countries-2020-20M\\CNTR_RG_20M_2020_4326.geojson"
with open(path, 'r', encoding='utf-8') as file:
    geojson_data = json.load(file)

# Replace 'GR' with 'EL' in the data
data_eu_country_resolution[tech] = data_eu_country_resolution[tech].replace({'GR': 'EL'})
data[tech] = data[tech].replace({'GR': 'EL'})

# Determine the range for the color scale
max_val = max(data[tech]['drought_qualification'].max(), data_eu_country_resolution[tech]['drought_qualification'].max())
min_val = min(data[tech]['drought_qualification'].min(), data_eu_country_resolution[tech]['drought_qualification'].min())

# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

# Define tick text and tick values
tick_text = [*[round(0.1 * i, 1) for i in range(1, 11)], 'no drought']
tickvals = [round(0.1 * i, 1) for i in range(1, 12)]

# Create subplots with custom titles
fig = make_subplots(rows=1, cols=2, 
                    subplot_titles=("island scenario", "European copperplate scenario"),
                    specs=[[{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}]])

# Add initial traces
initial_date = data[tech]['date'].unique()[0]
df1 = data[tech][data[tech]['date'] == initial_date]
df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date'] == initial_date]

trace1 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df1['region'],
    z=df1['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace2 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df2['region'],
    z=df2['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

fig.add_trace(trace1, row=1, col=1)
fig.add_trace(trace2, row=1, col=2)

def clean_date_string(date_str):
    # Remove the trailing fractional seconds part if it exists
    if '.' in date_str:
        date_str = date_str.split('.')[0]
    # Return the cleaned string
    return date_str

def format_date_only(date_str):
    try:
        # Clean the date string before parsing
        cleaned_date_str = clean_date_string(date_str)
        # Try to parse the datetime with 'T' separator and no fractional seconds
        date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%dT%H:%M:%S")
    except ValueError:
        try:
            # If only the date is present
            date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%d")
        except ValueError:
            raise ValueError(f"Time data '{date_str}' does not match the expected format.")
    # Return the formatted string: day month year
    return date_obj.strftime("%d %b '%y")

def format_date_with_time(date_str):
    try:
        # Clean the date string before parsing
        cleaned_date_str = clean_date_string(date_str)
        # Try to parse the datetime with 'T' separator and no fractional seconds
        date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%dT%H:%M:%S")
    except ValueError:
        try:
            # If only the date is present, parse the date and set the time to 00:00
            date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%d")
            date_obj = date_obj.replace(hour=0, minute=0)
        except ValueError:
            raise ValueError(f"Time data '{date_str}' does not match the expected format.")
    # Return the formatted string: day month year hour:minute
    return date_obj.strftime("%d %b '%y %H:%M")


# Create frames
frames = []
for date in data[tech]['date'].unique():
    df1 = data[tech][data[tech]['date'] == date]
    df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date'] == date]
    
    frames.append(go.Frame(data=[
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_ID',
            locations=df1['region'],
            z=df1['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox1"
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_ID',
            locations=df2['region'],
            z=df2['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox2"
        )
    ], name=str(date)))

# Update layout for slider and displayed text
fig.update_layout(
    mapbox1=dict(
        domain=dict(x=[0, 0.5], y=[0, 1]),
        style="white-bg",
        zoom=3.1,
        center={"lat": 56.9, "lon": 10}
    ),
    mapbox2=dict(
        domain=dict(x=[0.5, 1], y=[0, 1]),
        style="white-bg",
        zoom=3.1,
        center={"lat": 56.9, "lon": 10}
    ),
    height=950,
    width=1300,
    margin=dict(l=0, r=0, t=40, b=0),
    coloraxis=dict(
        colorscale=custom_colors,
        cmin=min_val,
        cmax=max_val,
        colorbar=dict(
            title=dict(
                text="minimum drought threshold",
                side="right"
            ),
            titlefont=dict(size=18),
            tickvals=tickvals,
            ticktext=tick_text,
            tickfont=dict(size=14),
            outlinewidth=0.1
        )
    ),
    sliders=[{
        'x': 0.1,
        'len': 0.9,
        'currentvalue': {
            'prefix': 'displayed time frame: ',
            'visible': True,
            'xanchor': 'right',
            'font': {'size': 17, 'color': '#666'}
        },
        'steps': [{
            'args': [
                [str(date)],
                {'frame': {'duration': 300, 'redraw': True}, 'mode': 'immediate'}
            ],
            'label': format_date_only(str(date)),  # Slider label with date only
            'method': 'animate'
        } for date in data[tech]['date'].unique()],
        'transition': {'duration': 100},
    }],
    updatemenus=[{
        'buttons': [
            {
                'args': [None, {'frame': {'duration': 300, 'redraw': True}, 'fromcurrent': True, 'mode': 'immediate'}],
                'label': '⏵',
                'method': 'animate'
            },
            {
                'args': [[None], {'frame': {'duration': 0, 'redraw': True}, 'mode': 'immediate'}],
                'label': '⏸',
                'method': 'animate'
            }
        ],
        'direction': 'left',
        'pad': {'r': 10, 't': 70},
        'showactive': False,
        'type': 'buttons',
        'x': 0.02,
        'xanchor': 'left',
        'y': 0.05,
        'yanchor': 'top'
    }]
)

# Add frames to the figure
fig.frames = frames

# Show the figure
fig.show()

# %% date + time for text and slider

import pandas as pd
import json
from datetime import datetime
import plotly.graph_objects as go
from plotly.subplots import make_subplots

# Assuming data_eu_country_resolution and data have already been populated
tech = 'portfolio'
data_eu_country_resolution = {}
data = {}
data_eu_country_resolution[tech] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_eu_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_12.csv")
data[tech] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_12.csv")

path = "T:\\mk\\vreda\\input\\ref-countries-2020-20M\\CNTR_RG_20M_2020_4326.geojson"
with open(path, 'r', encoding='utf-8') as file:
    geojson_data = json.load(file)

# Replace 'GR' with 'EL' in the data
data_eu_country_resolution[tech] = data_eu_country_resolution[tech].replace({'GR': 'EL'})
data[tech] = data[tech].replace({'GR': 'EL'})

# Determine the range for the color scale
max_val = max(data[tech]['drought_qualification'].max(), data_eu_country_resolution[tech]['drought_qualification'].max())
min_val = min(data[tech]['drought_qualification'].min(), data_eu_country_resolution[tech]['drought_qualification'].min())

# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

# Define tick text and tick values
tick_text = [*[round(0.1 * i, 1) for i in range(1, 11)], 'no drought']
tickvals = [round(0.1 * i, 1) for i in range(1, 12)]

# Create subplots with custom titles
fig = make_subplots(rows=1, cols=2, 
                    subplot_titles=("island scenario", "European copperplate scenario"),
                    specs=[[{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}]])

# Add initial traces
initial_date = data[tech]['date'].unique()[0]
df1 = data[tech][data[tech]['date'] == initial_date]
df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date'] == initial_date]

trace1 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df1['region'],
    z=df1['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace2 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df2['region'],
    z=df2['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

fig.add_trace(trace1, row=1, col=1)
fig.add_trace(trace2, row=1, col=2)

# Helper function to clean the date string before parsing
def clean_date_string(date_str):
    if '.' in date_str:
        date_str = date_str.split('.')[0]  # Remove fractional seconds
    return date_str

def format_date_with_time(date_str):
    try:
        cleaned_date_str = clean_date_string(date_str)
        # Try to parse datetime with 'T' separator
        date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%dT%H:%M:%S")
    except ValueError:
        try:
            # If the first format fails, try parsing without the 'T' separator
            date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            try:
                # If only the date is present, parse the date and set the time to 00:00
                date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%d")
                date_obj = date_obj.replace(hour=0, minute=0)
            except ValueError:
                raise ValueError(f"Time data '{date_str}' does not match the expected format.")
    # Return the formatted string: day month year hour:minute
    return date_obj.strftime("%d %b '%y %H:%M")


# Create frames
frames = []
for date in data[tech]['date'].unique():
    df1 = data[tech][data[tech]['date'] == date]
    df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date'] == date]
    
    frames.append(go.Frame(data=[
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_ID',
            locations=df1['region'],
            z=df1['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox1"
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_ID',
            locations=df2['region'],
            z=df2['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox2"
        )
    ], name=str(date)))

# Update layout for slider and displayed text
fig.update_layout(
    mapbox1=dict(
        domain=dict(x=[0, 0.5], y=[0, 1]),
        style="white-bg",
        zoom=3.1,
        center={"lat": 56.9, "lon": 10}
    ),
    mapbox2=dict(
        domain=dict(x=[0.5, 1], y=[0, 1]),
        style="white-bg",
        zoom=3.1,
        center={"lat": 56.9, "lon": 10}
    ),
    height=950,
    width=1300,
    margin=dict(l=0, r=0, t=40, b=0),
    coloraxis=dict(
        colorscale=custom_colors,
        cmin=min_val,
        cmax=max_val,
        colorbar=dict(
            title=dict(
                text="minimum drought threshold",
                side="right"
            ),
            titlefont=dict(size=18),
            tickvals=tickvals,
            ticktext=tick_text,
            tickfont=dict(size=14),
            outlinewidth=0.1
        )
    ),
    sliders=[{
        'x': 0.1,
        'len': 0.9,
        'currentvalue': {
            'prefix': 'displayed time frame: ',
            'visible': True,
            'xanchor': 'right',
            'font': {'size': 17, 'color': '#666'}
        },
        'steps': [{
            'args': [
                [str(date)],
                {'frame': {'duration': 200, 'redraw': True}, 'mode': 'immediate'}
            ],
            'label': format_date_with_time(str(date)),  # Slider label with date and time
            'method': 'animate'
        } for date in data[tech]['date'].unique()],
        'transition': {'duration': 30},
    }],
    updatemenus=[{
        'buttons': [
            {
                'args': [None, {'frame': {'duration': 150, 'redraw': True}, 'fromcurrent': True, 'mode': 'immediate'}],
                'label': '⏵',
                'method': 'animate'
            },
            {
                'args': [[None], {'frame': {'duration': 0, 'redraw': True}, 'mode': 'immediate'}],
                'label': '⏸',
                'method': 'animate'
            }
        ],
        'direction': 'left',
        'pad': {'r': 10, 't': 70},
        'showactive': False,
        'type': 'buttons',
        'x': 0.02,
        'xanchor': 'left',
        'y': 0.05,
        'yanchor': 'top'
    }]
)

# Add frames to the figure
fig.frames = frames

# Show the figure
fig.show()

# =============================================================================
# # Directory to save images
# path = 'T:\\mk\\vreda\\result_plots\\'
# path = os.path.join(path, 'threshold_maps', 'frames')
# Path(path).mkdir(parents=True, exist_ok=True)
# 
# def sanitize_filename(name):
#     # Remove fractional seconds and replace ':' with '-' to make it a valid filename
#     sanitized = name.split('.')[0]  # Remove the fractional seconds part
#     sanitized = sanitized.replace(':', '-')  # Replace colons with hyphens
#     return sanitized
# 
# # Save frames as images
# for frame in fig.frames:
#     fig.update(data=frame.data)
#     sanitized_name = sanitize_filename(frame.name)
#     fig.write_image(f"{path}/{sanitized_name}.png", height=950, width=1300, scale=5)
# 
# # Create video from images
# image_files = sorted([f"{path}/{file}" for file in os.listdir(path) if file.endswith('.png')])
# frame = cv2.imread(image_files[0])
# height, width, layers = frame.shape
# 
# video = cv2.VideoWriter('animation_video.mp4', cv2.VideoWriter_fourcc(*'mp4v'), 10, (width, height))
# 
# for image in image_files:
#     video.write(cv2.imread(image))
# 
# cv2.destroyAllWindows()
# video.release()
# =============================================================================

# %% two animated maps side by side, no slider, video

import pandas as pd
import json
from datetime import datetime
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pathlib import Path
import cv2
import os

# Assuming data_eu_country_resolution and data have already been populated
tech = 'portfolio'
data_eu_country_resolution = {}
data = {}
data_eu_country_resolution[tech] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_eu_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_4.csv")
data[tech] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_4.csv")

path = "T:\\mk\\vreda\\input\\ref-countries-2020-20M\\CNTR_RG_20M_2020_4326.geojson"
with open(path, 'r', encoding='utf-8') as file:
    geojson_data = json.load(file)

# Replace 'GR' with 'EL' in the data
data_eu_country_resolution[tech] = data_eu_country_resolution[tech].replace({'GR': 'EL'})
data[tech] = data[tech].replace({'GR': 'EL'})

# Determine the range for the color scale
max_val = max(data[tech]['drought_qualification'].max(), data_eu_country_resolution[tech]['drought_qualification'].max())
min_val = min(data[tech]['drought_qualification'].min(), data_eu_country_resolution[tech]['drought_qualification'].min())

# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

# Define tick text and tick values
tick_text = [*[round(0.1 * i, 1) for i in range(1, 11)], 'no drought']
# tickvals = [round(0.1 * i, 1) for i in range(1, 12)]

# Create subplots with custom titles
fig = make_subplots(
    rows=1, cols=2, 
    #subplot_titles=("Island Scenario", "European Copperplate Scenario"),
    specs=[[{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}]],
    horizontal_spacing=0.1
)

# Add initial traces
initial_date = data[tech]['date'].unique()[0]
df1 = data[tech][data[tech]['date'] == initial_date]
df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date'] == initial_date]

trace1 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df1['region'],
    z=df1['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace2 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df2['region'],
    z=df2['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

fig.add_trace(trace1, row=1, col=1)
fig.add_trace(trace2, row=1, col=2)

# Helper function to clean the date string before parsing
def clean_date_string(date_str):
    if '.' in date_str:
        date_str = date_str.split('.')[0]  # Remove fractional seconds
    return date_str

# Function to format date with time for displayed text
def format_date_with_time(date_str):
    try:
        cleaned_date_str = clean_date_string(date_str)
        # Try to parse datetime with 'T' separator
        date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%dT%H:%M:%S")
    except ValueError:
        try:
            # If the first format fails, try parsing without the 'T' separator
            date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            try:
                # If only the date is present, parse the date and set the time to 00:00
                date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%d")
                date_obj = date_obj.replace(hour=0, minute=0)
            except ValueError:
                raise ValueError(f"Time data '{date_str}' does not match the expected format.")
    # Return the formatted string: day month year hour:minute
    return date_obj.strftime("%d %b '%y %H:%M")

# Create frames and save images
path = 'T:\\mk\\vreda\\result_plots\\'
path = os.path.join(path, 'threshold_maps', 'frames')
Path(path).mkdir(parents=True, exist_ok=True)

def sanitize_filename(name):
    # Remove fractional seconds and replace ':' with '-' to make it a valid filename
    sanitized = name.split('.')[0]  # Remove the fractional seconds part
    sanitized = sanitized.replace(':', '-')  # Replace colons with hyphens
    return sanitized

frames = data[tech]['date'].unique()

for date in frames:
    df1 = data[tech][data[tech]['date'] == date]
    df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date'] == date]
    
    # Update the traces with new data for each frame
    fig.data[0].locations = df1['region']
    fig.data[0].z = df1['drought_qualification']
    fig.data[1].locations = df2['region']
    fig.data[1].z = df2['drought_qualification']
    
    # Update the layout with the current date and time
    fig.update_layout(
        mapbox1=dict(
            style="white-bg",  # Use white-bg style for a clean background
            domain=dict(x=[0, 0.5], y=[0, 1]),
            zoom=3.5,
            center={"lat": 57.1, "lon": 10},
        ),
        mapbox2=dict(
            style="white-bg",  # Use white-bg style for a clean background
            domain=dict(x=[0.5, 1], y=[0, 1]),
            zoom=3.5,
            center={"lat": 57.1, "lon": 10},
        ),
        coloraxis=dict(
            colorscale=custom_colors,
            cmin=min_val,
            cmax=max_val,
            colorbar=dict(
                title=dict(
                    text="minimum drought threshold",
                    side="right"
                ),
                titlefont=dict(size=18),
                tickvals=[round(0.1 * i, 1) for i in range(1, 12)],  # [round(x, 1) for x in [min_val, (min_val + max_val) / 2, max_val]],
                ticktext=tick_text,
                tickfont=dict(size=14),
                outlinewidth=0.1
            )
        ),
        margin=dict(l=0, r=0, t=10, b=0),  # Add margin at the bottom for the text
        annotations=[
            # Add title for first subplot
            dict(
                text="Island Scenario",
                x=0.35, y=0.95,  # Position above the first subplot
                xref='paper', yref='paper',
                showarrow=False,
                xanchor='right',
                yanchor='bottom',
                font=dict(size=24)
            ),
            # Add title for second subplot
            dict(
                text="European Copperplate Scenario",
                x=0.9, y=0.95,  # Position above the second subplot
                xref='paper', yref='paper',
                showarrow=False,
                xanchor='right',
                yanchor='bottom',
                font=dict(size=24)
            ),
            # Add annotation for the displayed time frame
            dict(
                text=f"displayed time frame: {format_date_with_time(date)}",
                x=0.95, y=0,  # Position in the lower right corner
                xref='paper', yref='paper',
                showarrow=False,
                xanchor='right',
                yanchor='bottom',
                font=dict(size=25)
            ),
        ],
    )
    
    # Save the frame as an image with higher resolution
    sanitized_name = sanitize_filename(date)
    fig.write_image(f"{path}/{sanitized_name}.png", height=1200, width=1600, scale=3)

# Get sorted list of image files
image_files = sorted([f"{path}/{file}" for file in os.listdir(path) if file.endswith('.png')])

os.chdir(path)

# Read the first image to determine the height and width
frame = cv2.imread(image_files[0])
height, width, layers = frame.shape

# Increase frame duration by setting a lower FPS
fps = 6  # 5 frames per second, adjust to increase/decrease duration

# Use a suitable codec
fourcc = cv2.VideoWriter_fourcc(*'XVID')

# Create the video writer object
video = cv2.VideoWriter('animation_video.avi', fourcc, fps, (width, height))

# Write each image to the video file
for image in image_files:
    img = cv2.imread(image)
    video.write(img)

# Release the video writer object
cv2.destroyAllWindows()
video.release()

print("Video has been successfully created.")

# %% four animated maps 2x2: all techs -> video

import pandas as pd
import json
from datetime import datetime
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pathlib import Path
import cv2
import os

# Assuming data_eu_country_resolution and data have already been populated
tech1 = 'wind_onshore'
tech2 = 'wind_offshore'
tech3 = 'pv'
tech4 = 'portfolio'

data_eu_country_resolution = {}
data = {}
data_eu_country_resolution[tech] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_eu_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_4.csv")
data[tech1] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_4.csv")
data[tech2] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_4.csv")
data[tech3] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_4.csv")
data[tech4] = pd.read_csv("T:\\mk\\vreda\\result_data\\data_country_resolution_portfolio_1996-12-01_1997-01-31_nthhour_4.csv")

path = "T:\\mk\\vreda\\input\\ref-countries-2020-20M\\CNTR_RG_20M_2020_4326.geojson"
with open(path, 'r', encoding='utf-8') as file:
    geojson_data = json.load(file)

# Replace 'GR' with 'EL' in the data
data[tech1] = data[tech1].replace({'GR': 'EL'})
data[tech2] = data[tech2].replace({'GR': 'EL'})
data[tech3] = data[tech3].replace({'GR': 'EL'})
data[tech4] = data[tech4].replace({'GR': 'EL'})
data_eu_country_resolution[tech4] = data_eu_country_resolution[tech4].replace({'GR': 'EL'})

# Determine the range for the color scale
max_val = max(data[tech1]['drought_qualification'].max(), data[tech2]['drought_qualification'].max(), 
              data[tech4]['drought_qualification'].max(), data[tech4]['drought_qualification'].max())
min_val = min(data[tech1]['drought_qualification'].min(), data[tech2]['drought_qualification'].min(), 
              data[tech3]['drought_qualification'].min(), data[tech4]['drought_qualification'].min())


# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

# Define tick text and tick values
tick_text = [*[round(0.1 * i, 1) for i in range(1, 11)], 'no drought']
# tickvals = [round(0.1 * i, 1) for i in range(1, 12)]

# Create subplots with custom titles
fig = make_subplots(
    rows=1, cols=2, 
    #subplot_titles=("Island Scenario", "European Copperplate Scenario"),
    specs=[[{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}]],
    horizontal_spacing=0.1
)

# Add initial traces
initial_date = data[tech]['date'].unique()[0]
df1 = data[tech][data[tech]['date'] == initial_date]
df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date'] == initial_date]

trace1 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df1['region'],
    z=df1['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace2 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_ID',
    locations=df2['region'],
    z=df2['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

fig.add_trace(trace1, row=1, col=1)
fig.add_trace(trace2, row=1, col=2)

# Helper function to clean the date string before parsing
def clean_date_string(date_str):
    if '.' in date_str:
        date_str = date_str.split('.')[0]  # Remove fractional seconds
    return date_str

# Function to format date with time for displayed text
def format_date_with_time(date_str):
    try:
        cleaned_date_str = clean_date_string(date_str)
        # Try to parse datetime with 'T' separator
        date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%dT%H:%M:%S")
    except ValueError:
        try:
            # If the first format fails, try parsing without the 'T' separator
            date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            try:
                # If only the date is present, parse the date and set the time to 00:00
                date_obj = datetime.strptime(cleaned_date_str, "%Y-%m-%d")
                date_obj = date_obj.replace(hour=0, minute=0)
            except ValueError:
                raise ValueError(f"Time data '{date_str}' does not match the expected format.")
    # Return the formatted string: day month year hour:minute
    return date_obj.strftime("%d %b '%y %H:%M")

# Create frames and save images
path = 'T:\\mk\\vreda\\result_plots\\'
path = os.path.join(path, 'threshold_maps', 'frames')
Path(path).mkdir(parents=True, exist_ok=True)

def sanitize_filename(name):
    # Remove fractional seconds and replace ':' with '-' to make it a valid filename
    sanitized = name.split('.')[0]  # Remove the fractional seconds part
    sanitized = sanitized.replace(':', '-')  # Replace colons with hyphens
    return sanitized

frames = data[tech]['date'].unique()

for date in frames:
    df1 = data[tech][data[tech]['date'] == date]
    df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date'] == date]
    
    # Update the traces with new data for each frame
    fig.data[0].locations = df1['region']
    fig.data[0].z = df1['drought_qualification']
    fig.data[1].locations = df2['region']
    fig.data[1].z = df2['drought_qualification']
    
    # Update the layout with the current date and time
    fig.update_layout(
        mapbox1=dict(
            style="white-bg",  # Use white-bg style for a clean background
            domain=dict(x=[0, 0.5], y=[0, 1]),
            zoom=3.5,
            center={"lat": 57.1, "lon": 10},
        ),
        mapbox2=dict(
            style="white-bg",  # Use white-bg style for a clean background
            domain=dict(x=[0.5, 1], y=[0, 1]),
            zoom=3.5,
            center={"lat": 57.1, "lon": 10},
        ),
        coloraxis=dict(
            colorscale=custom_colors,
            cmin=min_val,
            cmax=max_val,
            colorbar=dict(
                title=dict(
                    text="minimum drought threshold",
                    side="right"
                ),
                titlefont=dict(size=18),
                tickvals=[round(0.1 * i, 1) for i in range(1, 12)],  # [round(x, 1) for x in [min_val, (min_val + max_val) / 2, max_val]],
                ticktext=tick_text,
                tickfont=dict(size=14),
                outlinewidth=0.1
            )
        ),
        margin=dict(l=0, r=0, t=10, b=0),  # Add margin at the bottom for the text
        annotations=[
            # Add title for first subplot
            dict(
                text="Island Scenario",
                x=0.35, y=0.95,  # Position above the first subplot
                xref='paper', yref='paper',
                showarrow=False,
                xanchor='right',
                yanchor='bottom',
                font=dict(size=24)
            ),
            # Add title for second subplot
            dict(
                text="European Copperplate Scenario",
                x=0.9, y=0.95,  # Position above the second subplot
                xref='paper', yref='paper',
                showarrow=False,
                xanchor='right',
                yanchor='bottom',
                font=dict(size=24)
            ),
            # Add annotation for the displayed time frame
            dict(
                text=f"displayed time frame: {format_date_with_time(date)}",
                x=0.95, y=0,  # Position in the lower right corner
                xref='paper', yref='paper',
                showarrow=False,
                xanchor='right',
                yanchor='bottom',
                font=dict(size=25)
            ),
        ],
    )
    
    # Save the frame as an image with higher resolution
    sanitized_name = sanitize_filename(date)
    fig.write_image(f"{path}/{sanitized_name}.png", height=1200, width=1600, scale=3)

# Get sorted list of image files
image_files = sorted([f"{path}/{file}" for file in os.listdir(path) if file.endswith('.png')])

os.chdir(path)

# Read the first image to determine the height and width
frame = cv2.imread(image_files[0])
height, width, layers = frame.shape

# Increase frame duration by setting a lower FPS
fps = 6  # 5 frames per second, adjust to increase/decrease duration

# Use a suitable codec
fourcc = cv2.VideoWriter_fourcc(*'XVID')

# Create the video writer object
video = cv2.VideoWriter('animation_video.avi', fourcc, fps, (width, height))

# Write each image to the video file
for image in image_files:
    img = cv2.imread(image)
    video.write(img)

# Release the video writer object
cv2.destroyAllWindows()
video.release()

print("Video has been successfully created.")


# %% two animated maps side by side -> video

import os
from pathlib import Path
import json
from urllib.request import urlopen
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from datetime import datetime
import imageio
import cv2

tech = 'portfolio'

# Fetch the geojson data
# =============================================================================
# url = 'https://gisco-services.ec.europa.eu/distribution/v2/nuts/geojson/NUTS_RG_60M_2021_4326_LEVL_0.geojson'
# with urlopen(url) as response:
#     geojson_data = json.load(response)
# =============================================================================

path = "T:\\mk\\vreda\\input\\NUTS_RG_20M_2021_4326.geojson"
with open(file_path, 'r', encoding='utf-8') as file:
    geojson_data = json.load(file)

# Replace 'GR' with 'EL' in the data
data_eu_country_resolution[tech] = data_eu_country_resolution[tech].replace({'GR': 'EL'})
data[tech] = data[tech].replace({'GR': 'EL'})

# Determine the range for the color scale
max_val = max(data[tech]['drought_qualification'].max(), data_eu_country_resolution[tech]['drought_qualification'].max())
min_val = min(data[tech]['drought_qualification'].min(), data_eu_country_resolution[tech]['drought_qualification'].min())

# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

# Define tick text and tick values
tick_text = [*[round(0.1 * i, 1) for i in range(1, 11)], 'no drought']
tickvals = [round(0.1 * i, 1) for i in range(1, 12)]

# Create subplots with custom titles
fig = make_subplots(rows=1, cols=2, 
                    subplot_titles=("island scenario", "European copperplate scenario"),
                    specs=[[{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}]])

# Add initial traces
initial_date = data[tech]['date_only'].unique()[0]
df1 = data[tech][data[tech]['date_only'] == initial_date]
df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date_only'] == initial_date]

trace1 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df1['region'],
    z=df1['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace2 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df2['region'],
    z=df2['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

fig.add_trace(trace1, row=1, col=1)
fig.add_trace(trace2, row=1, col=2)

# Create frames
frames = []
for date in data[tech]['date_only'].unique():
    df1 = data[tech][data[tech]['date_only'] == date]
    df2 = data_eu_country_resolution[tech][data_eu_country_resolution[tech]['date_only'] == date]
    
    frames.append(go.Frame(data=[
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df1['region'],
            z=df1['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox1"  # Link to the first map
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df2['region'],
            z=df2['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox2"  # Link to the second map
        )
    ], name=str(date)))

# Format date for the slider
def format_date(date_str):
    date_obj = datetime.strptime(date_str, "%Y-%m-%d")
    return date_obj.strftime("%d %b '%y")

# Add frames to the figure
fig.frames = frames

# Update layout
fig.update_layout(
    mapbox1=dict(
        domain=dict(x=[0, 0.5], y=[0, 1]),
        style="white-bg",
        zoom=3.1,
        center={"lat": 56.49, "lon": 10}
    ),
    mapbox2=dict(
        domain=dict(x=[0.5, 1], y=[0, 1]),
        style="white-bg",
        zoom=3.1,
        center={"lat": 56.49, "lon": 10}
    ),
    height=950,
    width=1300,  # Adjust width to fit both maps side by side
    margin=dict(l=0, r=0, t=40, b=30),  # Adjust margins to control white space
    coloraxis=dict(
        colorscale=custom_colors,
        cmin=min_val,
        cmax=max_val,
        colorbar=dict(
            title=dict(
                text="minimum drought threshold",
                side="right"
            ),
            titlefont=dict(size=18),
            tickvals=tickvals,
            ticktext=tick_text,
            tickfont=dict(size=14),
            outlinewidth=0.1  # Remove the frame around the colorbar
        )
    ),
    sliders=[{
        'x': 0.1,  # Move slider slightly to the right
        'len': 0.9,  # Shrink the slider
        'currentvalue': {
            'prefix': 'displayed time frame: ',
            'visible': True,
            'xanchor': 'right',
            'font': {'size': 17, 'color': '#666'}
        },
        'steps': [{
            'args': [
                [str(date)],
                {'frame': {'duration': 150, 'redraw': True}, 'mode': 'immediate'}
            ],
            'label': format_date(str(date)),  # Format date for the slider label
            'method': 'animate'
        } for date in data[tech]['date_only'].unique()],
        'transition': {'duration': 30},
    }],
    updatemenus=[{
        'buttons': [
            {
                'args': [None, {'frame': {'duration': 150, 'redraw': True}, 'fromcurrent': True, 'mode': 'immediate'}],
                'label': '⏵',  # Unicode symbol for play
                'method': 'animate'
            },
            {
                'args': [[None], {'frame': {'duration': 0, 'redraw': True}, 'mode': 'immediate'}],
                'label': '⏸',  # Unicode symbol for pause
                'method': 'animate'
            }
        ],
        'direction': 'left',
        'pad': {'r': 10, 't': 70},
        'showactive': False,
        'type': 'buttons',
        'x': 0.02,  # Position the buttons in the new empty space
        'xanchor': 'left',
        'y': 0.05,  # Adjust y position to move below the slider
        'yanchor': 'top'
    }]
)

fig.update_geos(visible=False, resolution=50)

# Directory to save images
path = 'T:\\mk\\vreda\\result_plots\\'
path = os.path.join(path, 'threshold_maps', 'frames')
Path(path).mkdir(parents=True, exist_ok=True)

# Save frames as images
for frame in fig.frames:
    fig.update(data=frame.data)
    fig.write_image(f"{path}/{frame.name}.png", height=950, width=1300,scale=3)

# Create video from images
image_files = sorted([f"{path}/{file}" for file in os.listdir(path) if file.endswith('.png')])
frame = cv2.imread(image_files[0])
height, width, layers = frame.shape

video = cv2.VideoWriter('animation_video.mp4', cv2.VideoWriter_fourcc(*'mp4v'), 10, (width, height))

for image in image_files:
    video.write(cv2.imread(image))

cv2.destroyAllWindows()
video.release()





# %% four animated maps 2x2: all techs: TODO

import os
from pathlib import Path
import json
from urllib.request import urlopen
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from datetime import datetime

tech1 = 'wind_onshore'
tech2 = 'wind_offshore'
tech3 = 'pv'
tech4 = 'portfolio'


# Fetch the geojson data
url = 'https://gisco-services.ec.europa.eu/distribution/v2/nuts/geojson/NUTS_RG_60M_2021_4326_LEVL_0.geojson'
with urlopen(url) as response:
    geojson_data = json.load(response)

# Replace 'GR' with 'EL' in the data
data[tech1] = data[tech1].replace({'GR': 'EL'})
data[tech2] = data[tech2].replace({'GR': 'EL'})
data[tech3] = data[tech3].replace({'GR': 'EL'})
data[tech4] = data[tech4].replace({'GR': 'EL'})
data_eu_country_resolution[tech4] = data_eu_country_resolution[tech4].replace({'GR': 'EL'})

# Determine the range for the color scale
max_val = max(data[tech1]['drought_qualification'].max(), data[tech2]['drought_qualification'].max(), 
              data[tech4]['drought_qualification'].max(), data[tech4]['drought_qualification'].max())
min_val = min(data[tech1]['drought_qualification'].min(), data[tech2]['drought_qualification'].min(), 
              data[tech3]['drought_qualification'].min(), data[tech4]['drought_qualification'].min())

# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

# Define tick text and tick values
tick_text = [*[round(0.1 * i, 1) for i in range(1, 11)], 'no drought']
tickvals = [round(0.1 * i, 1) for i in range(1, 12)]

# Create subplots with custom titles
fig = make_subplots(
    rows=2, cols=2, 
    subplot_titles=(f"{tech1} - island scenario", f"{tech2} - island scenario",
                    f"{tech3} - island scenario", f"{tech4} - island scenario"),
    specs=[[{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}],
           [{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}]]
)

# Add initial traces
initial_date = data[tech1]['date_only'].unique()[0]
df1 = data[tech1][data[tech1]['date_only'] == initial_date]
df2 = data[tech2][data[tech2]['date_only'] == initial_date]
df3 = data[tech3][data[tech3]['date_only'] == initial_date]
df4 = data[tech4][data[tech4]['date_only'] == initial_date]

trace1 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df1['region'],
    z=df1['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace2 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df2['region'],
    z=df2['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace3 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df3['region'],
    z=df3['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace4 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df4['region'],
    z=df4['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

fig.add_trace(trace1, row=1, col=1)
fig.add_trace(trace2, row=1, col=2)
fig.add_trace(trace3, row=2, col=1)
fig.add_trace(trace4, row=2, col=2)

# Create frames
frames = []
for date in data[tech1]['date_only'].unique():
    df1 = data[tech1][data[tech1]['date_only'] == date]
    df2 = data[tech2][data[tech2]['date_only'] == date]
    df3 = data[tech3][data[tech3]['date_only'] == date]
    df4 = data[tech4][data[tech4]['date_only'] == date]
    
    frames.append(go.Frame(data=[
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df1['region'],
            z=df1['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox1"
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df2['region'],
            z=df2['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox2"
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df3['region'],
            z=df3['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox3"
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df4['region'],
            z=df4['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox4"
        )
    ], name=str(date)))

# Format date for the slider
def format_date(date_str):
    date_obj = datetime.strptime(date_str, "%Y-%m-%d")
    return date_obj.strftime("%d %b '%y, %H:%M")

# Add frames to the figure
fig.frames = frames

# Update layout
fig.update_layout(
    mapbox1=dict(
        domain=dict(x=[0, 0.5], y=[0.505, 1]),
        style="white-bg",
        zoom=2.1,
        center={"lat": 56.49, "lon": 10}
    ),
    mapbox2=dict(
        domain=dict(x=[0.5, 1], y=[0.505, 1]),
        style="white-bg",
        zoom=2.1,
        center={"lat": 56.49, "lon": 10}
    ),
    mapbox3=dict(
        domain=dict(x=[0, 0.5], y=[0, 0.495]),
        style="white-bg",
        zoom=2.1,
        center={"lat": 56.49, "lon": 10}
    ),
    mapbox4=dict(
        domain=dict(x=[0.5, 1], y=[0, 0.495]),
        style="white-bg",
        zoom=2.1,
        center={"lat": 56.49, "lon": 10}
    ),
    height=950,
    width=950,
    margin=dict(l=0, r=0, t=40, b=0),
    coloraxis=dict(
        colorscale=custom_colors,
        cmin=min_val,
        cmax=max_val,
        colorbar=dict(
            title=dict(
                text="minimum drought threshold",
                side="right"
            ),
            titlefont=dict(size=18),
            tickvals=tickvals,
            ticktext=tick_text,
            tickfont=dict(size=14),
            outlinewidth=0.1
        )
    ),
    sliders=[{
        'x': 0.1,
        'len': 0.9,
        'currentvalue': {
            'prefix': 'displayed time frame: ',
            'visible': True,
            'xanchor': 'right',
            'font': {'size': 17, 'color': '#666'}
        },
        'steps': [{
            'args': [
                [str(date)],
                {'frame': {'duration': 150, 'redraw': True}, 'mode': 'immediate'}
            ],
            'label': format_date(str(date)),
            'method': 'animate'
        } for date in data[tech1]['date_only'].unique()],
        'transition': {'duration': 30},
    }],
    updatemenus=[{
        'buttons': [
            {
                'args': [None, {'frame': {'duration': 150, 'redraw': True}, 'fromcurrent': True, 'mode': 'immediate'}],
                'label': '⏵',
                'method': 'animate'
            },
            {
                'args': [[None], {'frame': {'duration': 0, 'redraw': True}, 'mode': 'immediate'}],
                'label': '⏸',
                'method': 'animate'
            }
        ],
        'direction': 'left',
        'pad': {'r': 10, 't': 70},
        'showactive': False,
        'type': 'buttons',
        'x': 0.02,
        'xanchor': 'left',
        'y': 0.05,
        'yanchor': 'top'
    }]
)

fig.update_geos(visible=False, resolution=50)

# Show the figure
fig.show()

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'threshold_maps')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'threshold_map_island_2x2_{tech1}_{tech2}_{tech3}_{tech4}_{year_selection[0]}_{year_selection[-1]}.html')
fig.write_html(path_file)

#%% animated maps 2x2 + 1

import os
from pathlib import Path
import json
from urllib.request import urlopen
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from datetime import datetime

tech1 = 'wind_onshore'
tech2 = 'wind_offshore'
tech3 = 'pv'
tech4 = 'portfolio'

# Fetch the geojson data
url = 'https://gisco-services.ec.europa.eu/distribution/v2/nuts/geojson/NUTS_RG_60M_2021_4326_LEVL_0.geojson'
with urlopen(url) as response:
    geojson_data = json.load(response)

# Replace 'GR' with 'EL' in the data
data[tech1] = data[tech1].replace({'GR': 'EL'})
data[tech2] = data[tech2].replace({'GR': 'EL'})
data[tech3] = data[tech3].replace({'GR': 'EL'})
data[tech4] = data[tech4].replace({'GR': 'EL'})
data_eu_country_resolution[tech4] = data_eu_country_resolution[tech4].replace({'GR': 'EL'})

# Determine the range for the color scale
max_val = max(data[tech1]['drought_qualification'].max(), data[tech2]['drought_qualification'].max(), 
              data[tech3]['drought_qualification'].max(), data[tech4]['drought_qualification'].max(),
              data_eu_country_resolution[tech4]['drought_qualification'].max())
min_val = min(data[tech1]['drought_qualification'].min(), data[tech2]['drought_qualification'].min(), 
              data[tech3]['drought_qualification'].min(), data[tech4]['drought_qualification'].min(),
              data_eu_country_resolution[tech4]['drought_qualification'].min())

# Define a custom color scale based on "Burgyl_r"
custom_colors = [
    (0, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color
    (0.95, 'rgb(231, 212, 207)'),    # Brightest end of "Burgyl_r" for 1
    (1, 'rgb(255, 255, 255)'),   # White for 0: no droughts
]

# Define tick text and tick values
tick_text = [*[round(0.1 * i, 1) for i in range(1, 11)], 'no drought']
tickvals = [round(0.1 * i, 1) for i in range(1, 12)]

# Create subplots with custom titles
fig = make_subplots(
    rows=2, cols=3, 
    subplot_titles=(f"{tech1} - island scenario", f"{tech2} - island scenario",
                    f"{tech4} - European copperplate scenario", f"{tech3} - island scenario",
                    f"{tech4} - island scenario",
                    ),
    specs=[[{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}, {"type": "choroplethmapbox", "rowspan": 2}],
           [{"type": "choroplethmapbox"}, {"type": "choroplethmapbox"}, None]]
)

# Add initial traces
initial_date = data[tech1]['date_only'].unique()[0]
df1 = data[tech1][data[tech1]['date_only'] == initial_date]
df2 = data[tech2][data[tech2]['date_only'] == initial_date]
df3 = data_eu_country_resolution[tech4][data_eu_country_resolution[tech4]['date_only'] == initial_date]
df3 = data[tech3][data[tech3]['date_only'] == initial_date]
df5 = data[tech4][data[tech4]['date_only'] == initial_date]

trace1 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df1['region'],
    z=df1['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace2 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df2['region'],
    z=df2['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace3 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df3['region'],
    z=df3['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace4 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df4['region'],
    z=df4['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

trace5 = go.Choroplethmapbox(
    geojson=geojson_data,
    featureidkey='properties.CNTR_CODE',
    locations=df5['region'],
    z=df5['drought_qualification'],
    colorscale=custom_colors,
    zmin=min_val,
    zmax=max_val,
    coloraxis="coloraxis"
)

fig.add_trace(trace1, row=1, col=1)
fig.add_trace(trace2, row=1, col=2)
fig.add_trace(trace3, row=2, col=1)
fig.add_trace(trace4, row=2, col=2)
fig.add_trace(trace5, row=1, col=3)

# Create frames
frames = []
for date in data[tech1]['date_only'].unique():
    df1 = data[tech1][data[tech1]['date_only'] == date]
    df2 = data[tech2][data[tech2]['date_only'] == date]
    df3 = data[tech3][data[tech3]['date_only'] == date]
    df4 = data[tech4][data[tech4]['date_only'] == date]
    df5 = data_eu_country_resolution[tech4][data_eu_country_resolution[tech4]['date_only'] == date]
    
    frames.append(go.Frame(data=[
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df1['region'],
            z=df1['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox1"
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df2['region'],
            z=df2['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox2"
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df3['region'],
            z=df3['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox3"
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df4['region'],
            z=df4['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox4"
        ),
        go.Choroplethmapbox(
            geojson=geojson_data,
            featureidkey='properties.CNTR_CODE',
            locations=df5['region'],
            z=df5['drought_qualification'],
            colorscale=custom_colors,
            zmin=min_val,
            zmax=max_val,
            coloraxis="coloraxis",
            subplot="mapbox5"
        )
    ], name=str(date)))

# Format date for the slider
def format_date(date_str):
    date_obj = datetime.strptime(date_str, "%Y-%m-%d")
    return date_obj.strftime("%d %b '%y, %H:%M")

# Add frames to the figure
fig.frames = frames

# Update layout
fig.update_layout(
    mapbox1=dict(
        domain=dict(x=[0, 0.29], y=[0.505, 1]),
        style="white-bg",
        zoom=2.1,
        center={"lat": 56.49, "lon": 10}
    ),
    mapbox2=dict(
        domain=dict(x=[0.29, 0.58], y=[0.505, 1]),
        style="white-bg",
        zoom=2.1,
        center={"lat": 56.49, "lon": 10}
    ),
    mapbox3=dict(
        domain=dict(x=[0, 0.29], y=[0, 0.495]),
        style="white-bg",
        zoom=2.1,
        center={"lat": 56.49, "lon": 10}
    ),
    mapbox4=dict(
        domain=dict(x=[0.29, 0.58], y=[0, 0.495]),
        style="white-bg",
        zoom=2.1,
        center={"lat": 56.49, "lon": 10}
    ),
    mapbox5=dict(
        domain=dict(x=[0.58, 1], y=[0, 1]),
        style="white-bg",
        zoom=3.1,
        center={"lat": 56.49, "lon": 10}
    ),
    height=950,
    width=1400,
    margin=dict(l=0, r=0, t=40, b=0),
    coloraxis=dict(
        colorscale=custom_colors,
        cmin=min_val,
        cmax=max_val,
        colorbar=dict(
            title=dict(
                text="minimum drought threshold",
                side="right"
            ),
            titlefont=dict(size=18),
            tickvals=tickvals,
            ticktext=tick_text,
            tickfont=dict(size=14),
            outlinewidth=0.1
        )
    ),
    sliders=[{
        'x': 0.1,
        'len': 0.9,
        'currentvalue': {
            'prefix': 'displayed time frame: ',
            'visible': True,
            'xanchor': 'right',
            'font': {'size': 17, 'color': '#666'}
        },
        'steps': [{
            'args': [
                [str(date)],
                {'frame': {'duration': 150, 'redraw': True}, 'mode': 'immediate'}
            ],
            'label': format_date(str(date)),
            'method': 'animate'
        } for date in data[tech1]['date_only'].unique()],
        'transition': {'duration': 30},
    }],
    updatemenus=[{
        'buttons': [
            {
                'args': [None, {'frame': {'duration': 150, 'redraw': True}, 'fromcurrent': True, 'mode': 'immediate'}],
                'label': '⏵',
                'method': 'animate'
            },
            {
                'args': [[None], {'frame': {'duration': 0, 'redraw': True}, 'mode': 'immediate'}],
                'label': '⏸',
                'method': 'animate'
            }
        ],
        'direction': 'left',
        'pad': {'r': 10, 't': 70},
        'showactive': False,
        'type': 'buttons',
        'x': 0.02,
        'xanchor': 'left',
        'y': 0.05,
        'yanchor': 'top'
    }]
)

fig.update_geos(visible=False, resolution=50)

# Show the figure
fig.show()

path = 'T:\\mk\\vreda\\result_plots\\'
path_output = os.path.join(path, 'threshold_maps')
Path(path_output).mkdir(parents=True, exist_ok=True)
path_file = os.path.join(path_output, f'threshold_map_island_2x2_{tech1}_{tech2}_{tech3}_{tech4}_copperplate_{tech4}_{year_selection[0]}_{year_selection[-1]}.html')
fig.write_html(path_file)




# %% get most extreme droughts in DE

ts = get_time_series(a)


de = {}
de_ts = {}
for tech in config['technologies']:
    if tech == 'wind_offshore':
        de[tech] = a.region_results[tech][1]
    else:
        de[tech] = a.region_results[tech][4]

de_extremes_yearly_long = {}
for tech, ra_tech in de.items():
    df = ra_tech.extremes_yearly.copy()
    df = df[df.index != 'Maximum']
    df = df.rename(columns={'f-mean-cf=0.2': 0.2, 'f-mean-cf=0.3': 0.3,
                   'f-mean-cf=0.5': 0.5, 'f-mean-cf=0.7': 0.7, 'f-mean-cf=0.8': 0.8})
    df = df.reset_index(names='year')
    df = pd.melt(df, id_vars='year', value_vars=[0.2, 0.3, 0.5, 0.7, 0.8],
                 var_name='f-mean-cf', value_name='max_duration')
    df['technology'] = tech
    de_extremes_yearly_long[tech] = df

de_extremes_yearly_long = pd.concat(de_extremes_yearly_long.values())

de_extremes_yearly = {}
for tech, ra_tech in de.items():
    df = ra_tech.extremes_yearly.add_prefix(f'{tech}_')
    de_extremes_yearly[tech] = df

de_extremes_yearly = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True, how='outer'),
                            de_extremes_yearly.values())


region = 'DE'
min_duration = 336
threshold_name = 'f-mean-cf=0.7'


def get_drought_time_series(config, analyzer, min_duration, threshold):

    drought_ts = {}

    for region in configanalyzer.region_results.keys():
        drought_ts[tech] = {}

        for ra in analyzer.region_results[tech]:
            df = a.drought_ts_region[ra.region][min_duration][threshold_name].copy()
            df = df.columns.difference(['year'])

    de_drought_ts = a.drought_ts_region[region][min_duration][threshold_name].copy()
    de_techs = de_drought_ts.columns.difference(['year'])
    de_drought_ts = de_drought_ts.add_suffix('_drought')
    de_drought_ts = de_drought_ts.rename(columns={'year_drought': 'year'})
    de_drought_ts = de_drought_ts.reset_index()

    de_duration_max_period = {tech: de_extremes_yearly.loc[1997, f'{tech}_{threshold_name}'] for tech in de_techs}

    # generate new first year
    de_orig_1981_all_techs = pd.DataFrame(index=range(0, 8760), columns=['year'], data=[1981 for _ in range(0, 8760)])
    de_orig_1981_all_techs.index.name = 'hour'

    for i, tech in enumerate(de_techs):
        # get complete ts
        de_orig_tech = de_ts[tech]
        if i == 0:
            de_orig = de_orig_tech[['hour', 'year', 'capacity_factor']]
            de_orig = de_orig.rename(columns={'capacity_factor': tech})
        else:
            de_orig[tech] = de_orig_tech['capacity_factor'].values
        last_year = de_orig_tech.year.unique()[-1]
        orig_1981 = de_orig_tech[de_orig_tech['year'] == last_year]
        de_orig_1981_all_techs[tech] = orig_1981['capacity_factor'].values
    de_orig = pd.concat([de_orig_1981_all_techs, de_orig])

    # add original time series
    de_drought_ts = de_drought_ts.merge(de_orig, on=['hour', 'year'])
    de_drought_ts = de_drought_ts.set_index('hour')

    for tech in de_techs:
        col = f'{tech}_ma{de_duration_max_period[tech]}'
        de_drought_ts[col] = de_drought_ts[tech].rolling(window=de_duration_max_period[tech]).mean()

    thresholds_run = {tech: ra.drought_counters[threshold].threshold_run for tech, ra in de.items()}

    for tech, thres in thresholds_run.items():
        de_drought_ts[f'{tech}_threshold'] = thres

    de_techs_drought = {tech: f'{tech}_drought' for tech in de_techs}
    de_techs_tresholds = {tech: f'{tech}_threshold' for tech in de_techs}
    de_techs_ma = {tech: f'{tech}_ma{de_duration_max_period[tech]}' for tech in de_techs}

    de_drought_ts.reset_index(names='hour', inplace=True)
    # drought_ts_de_long = pd.melt(de_drought_ts, id_vars=['hour', 'year'], \
    #                        value_vars=de_drought_ts.columns.difference(['hour', 'year']), var_name='technology', value_name='cf')
    #drought_ts_de_long['date'] = drought_ts_de_long['hour'].astype(str).str.cat(drought_ts_de_long['year'].astype(str), '_')
    de_drought_ts['date'] = de_drought_ts['hour'].astype(str).str.cat(de_drought_ts['year'].astype(str), '_')

# %% get most extreme droughts in FR


fr = {}
fr_ts = {}
for tech in config['technologies']:
    if tech == 'wind_offshore':
        fr[tech] = a.region_results[tech][5]
        fr_ts[tech] = a.region_results[tech][5].input_extractor.ts_long
    else:
        fr[tech] = a.region_results[tech][8]
        fr_ts[tech] = a.region_results[tech][8].input_extractor.ts_long

fr_extremes_yearly_long = {}
for tech, ra_tech in fr.items():
    df = ra_tech.extremes_yearly.copy()
    df = df[df.index != 'Maximum']
    df = df.rename(columns={'f-mean-cf=0.2': 0.2, 'f-mean-cf=0.3': 0.3,
                   'f-mean-cf=0.5': 0.5, 'f-mean-cf=0.7': 0.7, 'f-mean-cf=0.8': 0.8})
    df = df.reset_index(names='year')
    df = pd.melt(df, id_vars='year', value_vars=[0.2, 0.3, 0.5, 0.7, 0.8],
                 var_name='f-mean-cf', value_name='max_duration')
    df['technology'] = tech
    fr_extremes_yearly_long[tech] = df

fr_extremes_yearly_long = pd.concat(fr_extremes_yearly_long.values())

fr_extremes_yearly = {}
for tech, ra_tech in fr.items():
    df = ra_tech.extremes_yearly.add_prefix(f'{tech}_')
    fr_extremes_yearly[tech] = df

fr_extremes_yearly = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True, how='outer'),
                            fr_extremes_yearly.values())

# %% get time series drought data (most extreme periods time series)

region = 'FR'
min_duration = 336
threshold_name = 'f-mean-cf=0.7'
thresholds_run = {tech: ra.drought_counters[threshold_name].threshold_run for tech, ra in fr.items()}

fr_drought_ts = a.drought_ts_region[region][min_duration][threshold_name].copy()
fr_techs = fr_drought_ts.columns.difference(['year'])
fr_drought_ts = fr_drought_ts.add_suffix('_drought')
fr_drought_ts = fr_drought_ts.rename(columns={'year_drought': 'year'})
fr_drought_ts = fr_drought_ts.reset_index()

fr_duration_max_period = {tech: fr_extremes_yearly.loc[1997, f'{tech}_{threshold_name}'] for tech in fr_techs}

# generate new first year
fr_orig_1981_all_techs = pd.DataFrame(index=range(0, 8760), columns=['year'], data=[1981 for _ in range(0, 8760)])
fr_orig_1981_all_techs.index.name = 'hour'

for i, tech in enumerate(fr_techs):
    # get complete ts
    fr_orig_tech = fr_ts[tech]
    if i == 0:
        fr_orig = fr_orig_tech[['hour', 'year', 'capacity_factor']]
        fr_orig = fr_orig.rename(columns={'capacity_factor': tech})
    else:
        fr_orig[tech] = fr_orig_tech['capacity_factor'].values
    last_year = fr_orig_tech.year.unique()[-1]
    orig_1981 = fr_orig_tech[fr_orig_tech['year'] == last_year]
    fr_orig_1981_all_techs[tech] = orig_1981['capacity_factor'].values
fr_orig = pd.concat([fr_orig_1981_all_techs, fr_orig])

# add original time series
fr_drought_ts = fr_drought_ts.merge(fr_orig, on=['hour', 'year'])
fr_drought_ts = fr_drought_ts.set_index('hour')

for tech in fr_techs:
    col = f'{tech}_ma{fr_duration_max_period[tech]}'
    fr_drought_ts[col] = fr_drought_ts[tech].rolling(window=fr_duration_max_period[tech]).mean()

for tech, thres in thresholds_run.items():
    fr_drought_ts[f'{tech}_threshold'] = thres

fr_techs_drought = {tech: f'{tech}_drought' for tech in fr_techs}
fr_techs_tresholds = {tech: f'{tech}_threshold' for tech in fr_techs}
fr_techs_ma = {tech: f'{tech}_ma{fr_duration_max_period[tech]}' for tech in fr_techs}

fr_drought_ts.reset_index(names='hour', inplace=True)
# drought_ts_fr_long = pd.melt(fr_drought_ts, id_vars=['hour', 'year'], \
#                        value_vars=fr_drought_ts.columns.difference(['hour', 'year']), var_name='technology', value_name='cf')
#drought_ts_fr_long['date'] = drought_ts_fr_long['hour'].astype(str).str.cat(drought_ts_fr_long['year'].astype(str), '_')
fr_drought_ts['date'] = fr_drought_ts['hour'].astype(str).str.cat(fr_drought_ts['year'].astype(str), '_')

# %% quick plot with long data

#data_figure_de = de_drought_ts
#data_figure_de = drought_ts_de_long.loc[(drought_ts_de_long['year'] == 1996) | (drought_ts_de_long['year'] == 1997)].copy()
#fig = px.line(data_figure_de, x='date', y='cf', color='technology')
# fig.update_layout(font_size=10, xaxis_title=None, title='168 drought periods') #template = "simple_white"
# fig.update_xaxes(nticks=len_years*2)
# fig.show()

# %% plot yearly correlation matrix

data_figure_de = de_drought_ts.loc[(de_drought_ts['year'] >= 1996) & (de_drought_ts['year'] <= 1997)].copy()
data_figure_de = data_figure_de.iloc[4368:13127, :]
data_figure_fr = fr_drought_ts.loc[(fr_drought_ts['year'] >= 1996) & (fr_drought_ts['year'] <= 1997)].copy()
data_figure_fr = data_figure_fr.iloc[4368:13127, :]

sto = pd.read_csv("D:\\git\\vre_droughts_power_sector\\result_data\\h2_sto_l_1996-1997.csv")
sto_noflow = sto[sto['custom_flow'] == 0]['value'].values
sto_flow = sto[sto['custom_flow'] == 1]['value'].values

cu = pd.read_csv("D:\\git\\vre_droughts_power_sector\\result_data\\cu_1996-1997.csv")
cu_won_noflow = cu[(cu['tech'] == 'wind_on') & (cu['custom_flow'] == 0)]['value'].values
cu_won_flow = cu[(cu['tech'] == 'wind_on') & (cu['custom_flow'] == 1)]['value'].values
cu_woff_noflow = cu[(cu['tech'] == 'wind_off') & (cu['custom_flow'] == 0)]['value'].values
cu_woff_flow = cu[(cu['tech'] == 'wind_off') & (cu['custom_flow'] == 1)]['value'].values
cu_pv_noflow = cu[(cu['tech'] == 'pv') & (cu['custom_flow'] == 0)]['value'].values
cu_pv_flow = cu[(cu['tech'] == 'pv') & (cu['custom_flow'] == 1)]['value'].values

ntc = pd.read_csv("D:\\git\\vre_droughts_power_sector\\result_data\\ntc_flow_1996-1997.csv")
ntc_de_fr = ntc[(ntc['l'] == 'DE_FR')]['value'].values


# %%

colors = {'portfolio': '#aee571', 'wind_onshore': '#518696', 'wind_offshore': '#6ff9ff', 'pv': '#fffb4e'}
colors_droughts = {'portfolio': '#7cb342', 'wind_onshore': '#215968', 'wind_offshore': '#26c6da', 'pv': '#ffeb3b'}
colors_thresholds = {'portfolio': '#880000', 'wind_onshore': '#c62200', 'wind_offshore': '#f95827', 'pv': '#ff814b'}
colors_ma = {'portfolio': '#5f0000', 'wind_onshore': '#99002b', 'wind_offshore': '#c42d4a', 'pv': '#ff7485'}

fig = go.Figure()

for tech in de_techs:

    # DE
    fig.add_trace(go.Scatter(x=data_figure_de['date'], y=data_figure_de[tech], mode='lines',
                  name=f'DE {tech}', line=dict(color=colors[tech], width=1.5), yaxis='y1'))
    fig.add_trace(go.Scatter(x=data_figure_de['date'], y=data_figure_de[de_techs_drought[tech]], mode='lines',
                  name=f'DE {de_techs_drought[tech]}', line=dict(color=colors_droughts[tech], width=2), yaxis='y1'))
    fig.add_trace(go.Scatter(x=data_figure_de['date'], y=data_figure_de[de_techs_tresholds[tech]], mode='lines',
                  name=f'DE {de_techs_tresholds[tech]}', line=dict(color=colors_thresholds[tech], width=2), yaxis='y1'))
    fig.add_trace(go.Scatter(x=data_figure_de['date'], y=data_figure_de[de_techs_ma[tech]], mode='lines',
                  name=f'DE {de_techs_ma[tech]}', line=dict(color=colors_ma[tech], width=2), yaxis='y1'))

    # FR
    fig.add_trace(go.Scatter(x=data_figure_fr['date'], y=data_figure_fr[tech], mode='lines',
                  name=f'FR {tech}', line=dict(width=1.5), yaxis='y1'))  # color=colors[tech]
    fig.add_trace(go.Scatter(x=data_figure_fr['date'], y=data_figure_fr[fr_techs_drought[tech]], mode='lines',
                  name=f'FR {fr_techs_drought[tech]}', line=dict(width=2), yaxis='y1'))  # color=colors_droughts[tech],
    fig.add_trace(go.Scatter(x=data_figure_fr['date'], y=data_figure_fr[fr_techs_tresholds[tech]], mode='lines',
                  name=f'FR {fr_techs_tresholds[tech]}', line=dict(width=2), yaxis='y1'))  # color=colors_thresholds[tech]
    fig.add_trace(go.Scatter(x=data_figure_fr['date'], y=data_figure_fr[fr_techs_ma[tech]], mode='lines',
                  name=f'FR {fr_techs_ma[tech]}', line=dict(width=2), yaxis='y1'))  # color=colors_ma[tech],

fig.add_trace(go.Scatter(x=data_figure_de['date'], y=sto_noflow, mode='lines',
              name='DE storage level (island)', line=dict(color='#9526b7', width=2), yaxis='y2'))
fig.add_trace(go.Scatter(x=data_figure_de['date'], y=sto_flow, mode='lines',
              name='DE storage level (interconnection)', line=dict(color='#620086', width=2), yaxis='y2'))

# fig.add_trace(go.Scatter(x=data_figure_de['date'], y=cu_won_noflow, mode='lines', name='DE cu won (island)', line=dict(color='#c62200', width=2), yaxis='y3'))
# fig.add_trace(go.Scatter(x=data_figure_de['date'], y=cu_won_flow, mode='lines', name='DE cu won (interconnection)', line=dict(color='#c62200', width=2), yaxis='y3'))
# fig.add_trace(go.Scatter(x=data_figure_de['date'], y=cu_woff_noflow, mode='lines', name='DE cu woff (island)', line=dict(color='#f95827', width=2), yaxis='y3'))
# fig.add_trace(go.Scatter(x=data_figure_de['date'], y=cu_woff_flow, mode='lines', name='DE cu woff (interconnection)', line=dict(color='#f95827', width=2), yaxis='y3'))
# fig.add_trace(go.Scatter(x=data_figure_de['date'], y=cu_pv_noflow, mode='lines', name='DE cu pv (island)', line=dict(color='#ff7485', width=2), yaxis='y3'))
# fig.add_trace(go.Scatter(x=data_figure_de['date'], y=cu_pv_flow, mode='lines', name='DE cu pv (interconnection)', line=dict(color='#ff7485', width=2), yaxis='y3'))

parameter_text = f'region: DE, minimum drought duration={min_duration/24} days, threshold={threshold_name[-3:]} of mean capacity factor'

fig.update_layout(
    title_text=f'Droughts and long-term storage use in 1996/97 ({parameter_text})',
    template='none',
    # xaxis=dict(domain=[0, 0.9]),
    yaxis=dict(title='capacity factor', titlefont=dict(color="#1f77b4"), tickfont=dict(color="#1f77b4")),
    # titlefont=dict(color="#ff7f0e"), tickfont=dict(color="#ff7f0e"),
    yaxis2=dict(title='storage level [MWh]', anchor="x", overlaying="y", side="right"),
    yaxis3=dict(title="curtailment [MWh]", anchor="free", overlaying="y", side="right",
                position=0.95)  # titlefont=dict(color="#d62728"), tickfont=dict(color="#d62728"),
)

fig.show()
