# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 22:56:54 2023

@author: mkittel
"""

# TODO: check which imports are needed
import pandas as pd
import numpy as np
from collections import defaultdict


class DroughtCounter():
    '''
    Counts droughts for a single region.

    RegionAnalyzer instance removes all attributes not required for subsequent analysis
    before storing DroughtCounter instance as attribute.

    Attributes kept for subsequent analyses are:
    - self.count_yearly
    - self.start
    - self.end
    - self.idx
    - self.leap_years
    - self.years
    - self.cf_idx
    - self.e_idx
    '''

    def __init__(
            self,
            drought_type,
            counting_algorithm,
            threshold_run,
            threshold_label,
            start,
            end,
            technology,
            region,
            time_series_mean,
            years_complete,
            years=None,
            leap_years=None
            ):
        self.drought_type = drought_type
        self.counting_algorithm = counting_algorithm
        self.threshold_run = threshold_run
        self.threshold_label = threshold_label
        self.start = start
        self.end = end
        self.technology = technology
        self.region = region
        self.time_series_mean = time_series_mean
        self.years_complete = sorted(years_complete)
        self.years = sorted(years_complete) if not years else sorted(years)
        self.leap_years = leap_years
        self.periods = None
        self.data = None
        self.temp_idx = None
        self.temp_idx_mid = None
        self.temp_cf = None
        self.temp_cf_idx = None
        self.temp_e_idx_cum = None
        self.count_monthly = None
        self.count_quarterly = None
        self.idx = None
        # self.cf = None
        self.cf_idx = None
        self.e_idx = None
        self.e_idx_cum = None
        self.energy_deficit_cum = None
        self.monthly = None
        self.monthly_mid = None
        self.quarterly = None
        self.count_yearly = None
        self.mean_yearly_singular = None
        self.mean_monthly_singular = None
        self.mean_monthly_mid_singular = None
        self.mean_quarterly_singular = None
        self.mean_yearly_cum = None
        # self.mean_monthly_cum = None
        self.mean_quarterly_cum = None
        self.extremes_monthly = None
        self.extremes_monthly_mid = None
        self.extremes_yearly = None
        self.return_period_all = None
        self.return_period_q1 = None
        self.return_period_q2 = None
        self.return_period_q3 = None
        self.return_period_q4 = None
        self.return_period_q14 = None
        self.return_period_q23 = None
        self.return_period_all_standard = None
        self.return_period_q1_standard = None
        self.return_period_q2_standard = None
        self.return_period_q3_standard = None
        self.return_period_q4_standard = None
        self.return_period_q14_standard = None
        self.return_period_q23_standard = None

        if drought_type == 'mbt':
            self.sma_array = None
            self.temp_cf = None
            # self.cf = None
        elif drought_type == 'cbt':
            self.cbt_array = None
        # elif drought_type == 'spa':
            # self.sma_array = None
            # self.temp_cf = None
            # self.cf = None
            # self.cbt_array = None

    def __str__(self):
        return (
            f"{type(self).__name__} object with following parameters:\n"
            f"Drought type: {self.drought_type}\n"
            f"Counting algorithm: {self.counting_algorithm}\n"
            f"Threshold={self.threshold_run}\n"
            f"Range start={self.start}, range end={self.end}\n"
            f"Technology: {self.technology}\n"
            f"Region: {self.region}\n"
            f"Years: {self.years}"
        )

    def define_period_range(self):
        '''
        Returns range for counting algorithm in either ascending (window) or
        descending order (event).
        '''
        if self.counting_algorithm == 'window':
            self.periods = range(self.start, self.end+1)
        elif self.counting_algorithm == 'event':
            self.periods = range(self.start, self.end-1, -1)
        else:
            print('Your input sucks')

    def extract_data(self, ts_long, y, period):
        '''
        Slices relevant data out of full set of timeseries. Includes last p-1
        hours from previous year. For first year, p-1 hours from last year are
        included.
        '''
        # copy for analysis to be modified while counting energy droughts
        ts = ts_long.copy()
        data_main = ts.loc[ts['year'] == y].copy()

        # add hours from previous year depending on period size, for first year, use last year
        if y == self.years_complete[0]:
            data_pre = ts.loc[ts['year'] == self.years_complete[-1]].copy()
        else:
            data_pre = ts.loc[ts['year'] == y-1].copy()
        data_pre = data_pre.iloc[len(data_pre)-period+1:len(data_pre), :]
        data = pd.concat([data_pre, data_main])
        self.data = data

    def simple_moving_average(self, row, period):
        '''
        Determines simple moving average on row "row" in data set "data" for
        period "period" in new row "SMA". SMA in hour 'h' refers to SMA of rolling
        window [h-period+1, h]. Resulting array is saved to sma_array without NaNs
        corresponding to p-1 hours of datapoints from previous year when computing
        a simple moving average.
        '''
        self.data['SMA'] = self.data.iloc[:, row].rolling(window=period).mean()
        sma_array = self.data['SMA'].to_numpy()
        self.sma_array = sma_array[~np.isnan(sma_array)]

        # TODO: increase speed
        # https://stackoverflow.com/questions/13728392/moving-average-or-running-mean/43200476#43200476

    def rolling_cbt_test(self, col, period):
        '''
        Tests whether all values within rolling window with length 'period'
        on column 'col' in data 'data' are below threshold 'threshold'. A value
        of '0' in hour h indicates that there are values above the threshold
        'threshold' in rolling window [h-period+1, h]. A value of '1' shows that all
        values are below 'threshold'. Result array is saved to data in new
        row 'CBT'. 'data' constains NaNs corresponding to period-1 hours of
        datapoints from previous year when for rolling window of first hours
        of the year. 'cbt_array' has no NaNs and starts in first hours or the year.
        '''
        self.data['CBT'] = self.data.iloc[:, col].rolling(window=period).apply(
            lambda x: np.all(x < self.threshold_run), raw=True
            )
        cbt_array = self.data['CBT'].to_numpy()
        self.cbt_array = cbt_array[~np.isnan(cbt_array)]

    def find_mbt_windows(self, period):
        '''
        Counts MBT windows. Starts with first simple moving average below threshold
        in time series. Then excludes underlying periods and calculates average
        again. Continues with next average below threshold until no minimum below
        threshold exists.
        
        TODO: Take last (?) hour of identified period as idx. Is it really last one
        when searching for min of sma_array with windows?
        '''
        # temporary data containers
        temp_cf = []
        temp_idx = []
        temp_idx_mid = []
        temp_cf_idx = {}
        temp_e_idx_cum = {}

        # search first occurence of moving average below threshold
        while np.min(self.sma_array) < self.threshold_run:
            idx = np.where(self.sma_array < self.threshold_run)[0][0]
            idx_mid = idx - round(period / 2)  # rounds up
            temp_cf.append(self.sma_array[idx])
            temp_idx.append(idx)
            temp_idx_mid.append(idx_mid)

            # check whether window contains excluded values
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            if np.any(self.data.iloc[idx:period+idx, 3].to_numpy() == 1e5):
                print(f"Error for hour {idx} and period duration {period} hours"
                      f" - overlapping with previously excluded MBT hours.")
                break

            # save time series slice
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            temp_series = self.data.iloc[idx:period+idx, 3].copy()

            # accumulate the energy deficit, reference is mean capacity factor
            temp_e_idx_cum[int(idx)] = self.time_series_mean * period - temp_series.sum()
            # temp_e_idx_cum[int(idx)] = self.threshold_run * period - temp_series.sum()

            # assign new index based on index within generic year [0:8759], np.arange(idx-period+1, idx+1) =
            # np.arange(idx-period+1(=inclusive start), idx+1(=exclusive end))
            temp_series = temp_series.set_axis(list(np.arange(idx-period+1, idx+1)), copy=False)
            temp_cf_idx[int(idx)] = temp_series

            # to exclude this window set all window values to 1e5
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            self.data.iloc[idx:period+idx, 3] = 1e5

            # determine windows with moving average below threshold with window [t-p+1, t]
            self.simple_moving_average(3, period)

        self.temp_cf = temp_cf
        self.temp_idx = temp_idx
        self.temp_idx_mid = temp_idx_mid
        self.temp_cf_idx = temp_cf_idx
        self.temp_e_idx_cum = temp_e_idx_cum

    def find_cbt_windows(self, period):
        '''
        Counts BBT windows. Starts with first occurence of rolling window only
        containing values below threshold. Then excludes underlying period. Repeat
        until no qualified rolling window is found.

        TODO: Take last instead of first hour of identified period as idx. Is it really last one
        when searching for min of sma_array with windows?
        '''
        # temporary data containers
        temp_idx = []
        temp_idx_mid = []
        temp_cf_idx = {}
        temp_e_idx_cum = {}

        # search first occurence of constantly below threshold windows
        while 1 in self.cbt_array:
            idx = np.where(self.cbt_array == 1)[0][0]
            idx_mid = idx - round(period / 2)  # rounds up
            temp_idx.append(idx)
            temp_idx_mid.append(idx_mid)

            # check whether window overlaps with previous CBT windows
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            if np.any(self.data.iloc[idx:period+idx, 3].to_numpy() == 1e5):
                print(f"error at hour {idx} and period duration {period} hours"
                      f" - overlapping with previously excluded CBT windows")
                break
            # ALternative: 1 in CBT_array!

            # exclude the CBT period and all subsequent hours [idx, idx+period(=exclusive end)] from drought counting
            end = idx+period if idx+period <= len(self.cbt_array) else len(self.cbt_array)
            self.cbt_array[np.arange(idx, end)] = 0

            # save time series slice
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            temp_series = self.data.iloc[idx:period+idx, 3].copy()

            # accumulate the energy deficit, reference is mean capacity factor
            temp_e_idx_cum[int(idx)] = self.time_series_mean * period - temp_series.sum()
            # temp_e_idx_cum[int(idx)] = self.threshold_run * period - temp_series.sum()

            # assign new index based on index within generic year [0:8759], np.arange(idx-period+1, idx+1) =
            # np.arange(idx-period+1(=inclusive start), idx+1(=exclusive end))
            temp_series = temp_series.set_axis(list(np.arange(idx-period+1, idx+1)), copy=False)
            temp_cf_idx[int(idx)] = temp_series

            # to exclude this period set all period values to 1e5
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            self.data.iloc[idx:period+idx, 3] = 1e5

        self.temp_idx = temp_idx
        self.temp_idx_mid = temp_idx_mid
        self.temp_cf_idx = temp_cf_idx
        self.temp_e_idx_cum = temp_e_idx_cum

    def find_mbt_events(self, period):
        '''
        Counts MBT events. Starts with minimum simple moving average in time
        series. Then excludes the underlying period and calculates average again.
        Continues with next minimum until no minimum below the threshold exists.
        '''
        # temporary data containers
        temp_cf = []
        temp_idx = []
        temp_idx_mid = []
        temp_cf_idx = {}
        temp_e_idx_cum = {}

        # search minimum of moving average below threshold
        while np.min(self.sma_array) < self.threshold_run:
            idx = np.argmin(self.sma_array)
            idx_mid = idx - round(period / 2)  # rounds up
            temp_cf.append(self.sma_array[idx])
            temp_idx.append(idx)
            temp_idx_mid.append(idx_mid)

            # check whether window contains excluded values from same period class
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            if np.any(self.data.iloc[idx:period+idx, 3].to_numpy() == 1e5):
                print(f"Error at hour {idx} and period duration {period} hours"
                      f" - previously excluded MBT event.")
                break

            # save time series slice
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            temp_series = self.data.iloc[idx:period+idx, 3].copy()

            # accumulate the energy deficit, reference is mean capacity factor
            temp_e_idx_cum[int(idx)] = self.time_series_mean * period - temp_series.sum()
            # temp_e_idx_cum[int(idx)] = self.threshold_run * period - temp_series.sum()

            # assign new index based on index within generic year [0:8759], np.arange(idx-period+1, idx+1) =
            # np.arange(idx-period+1(=inclusive start), idx+1(=exclusive end))
            temp_series = temp_series.set_axis(list(np.arange(idx-period+1, idx+1)), copy=False)
            temp_cf_idx[int(idx)] = temp_series

            # to exclude this period set all period values to 1e5
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            self.data.iloc[idx:period+idx, 3] = 1e5

            # determine periods with moving average below threshold with period [t-period+1, t]
            self.simple_moving_average(3, period)

        self.temp_cf = temp_cf
        self.temp_idx = temp_idx
        self.temp_idx_mid = temp_idx_mid
        self.temp_cf_idx = temp_cf_idx
        self.temp_e_idx_cum = temp_e_idx_cum

    def find_cbt_events(self, period):
        '''
        Counts CBT events. Starts with first occurence of rolling window only
        containing values below threshold. Then excludes underlying period. Repeat
        until no qualified rolling window is found.
        '''
        # temporary data container
        temp_idx = []
        temp_idx_mid = []
        temp_cf_idx = {}
        temp_e_idx_cum = {}

        # search first occurence of moving average below threshold
        while 1 in self.cbt_array:
            idx = np.where(self.cbt_array == 1)[0][0]
            idx_mid = idx - round(period / 2)  # rounds up
            temp_idx.append(idx)
            temp_idx_mid.append(idx_mid)

            # check whether window overlaps with previous CBT events
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            if np.any(self.data.iloc[idx:period+idx, 3].to_numpy() == 1e5):
                print(f"Error at hour {idx} and period duration {period} hours"
                      f" - overlapping with previously excluded CBT events")
                break
            # ALternative: 1 in CBT_array!

            # exclude the CBT period and all subsequent hours [idx, idx+period(=exclusive end)] from drought counting
            end = idx+period if idx+period <= len(self.cbt_array) else len(self.cbt_array)
            self.cbt_array[np.arange(idx, end)] = 0

            # save time series slice
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            temp_series = self.data.iloc[idx:period+idx, 3].copy()

            # accumulate the energy deficit, reference is mean capacity factor
            temp_e_idx_cum[int(idx)] = self.time_series_mean * period - temp_series.sum()
            # temp_e_idx_cum[int(idx)] = self.threshold_run * period - temp_series.sum()

            # assign new index based on index within generic year [0:8759], np.arange(idx-period+1, idx+1) =
            # np.arange(idx-period+1(=inclusive start), idx+1(=exclusive end))
            temp_series = temp_series.set_axis(list(np.arange(idx-period+1, idx+1)), copy=False)
            temp_cf_idx[int(idx)] = temp_series

            # to exclude this period set all period values to 1e5
            # data includes data point from previous year: iloc[idx:period+idx] =
            # = iloc[period-1(=prev yr) + idx-period+1(=inclusive start):period-1(=prev yr) + idx+1(=exclusive end)]
            self.data.iloc[idx:period+idx, 3] = 1e5
            # =============================================================================
            #         # Alternative: find steaks
            #         df = pd.DataFrame(cbt_array.copy(), columns=['v'])
            #         df['idx'] = df.index
            #         df['cs'] = (df['v'].diff(1) != 0).cumsum()
            #         result = pd.DataFrame({'BeginIdx' : df.groupby('cs').idx.first(),
            #                       'EndIdx' : df.groupby('cs').idx.last(),
            #                       'Consecutive' : df.groupby('cs').size(),
            #                       'Value' : df.groupby('cs').v.first()}).reset_index(drop=True)
            #         result = result[result.Value != 0]
            #         result = result[result.Consecutive == 1]
            #         temp_idx.extend(result.EndIdx.values)
            #         for idx in result.EndIdx.values:
            #             # to exclude this window set all window values to 1e5 (data includes previous year)
            #             temp_data.iloc[np.arange(idx, idx+w), 3] = 1e5
            # =============================================================================

        self.temp_idx = temp_idx
        self.temp_idx_mid = temp_idx_mid
        self.temp_cf_idx = temp_cf_idx
        self.temp_e_idx_cum = temp_e_idx_cum

    def aggregate_to_monthly_count(self, y):
        '''
        Distributes hourly indices of critical periods across all months
        for both leap and ordinary years.
        '''
        count_monthly = pd.Series(np.zeros(12), range(1, 13), dtype='int64')
        index_mid = np.concatenate((np.arange(-1, -13, -1), np.arange(1, 13)))
        count_monthly_mid = pd.Series(np.zeros(24, dtype='int64'), index=index_mid)

        # idx based on last hour of drought, counting for non-leap years        
        if y not in self.leap_years:
            for idx in self.temp_idx:
                if 0 <= idx < 744:
                    count_monthly[1] += 1
                elif 744 <= idx < 1416:
                    count_monthly[2] += 1
                elif 1416 <= idx < 2160:
                    count_monthly[3] += 1
                elif 2160 <= idx < 2880:
                    count_monthly[4] += 1
                elif 2880 <= idx < 3624:
                    count_monthly[5] += 1
                elif 3624 <= idx < 4344:
                    count_monthly[6] += 1
                elif 4344 <= idx < 5088:
                    count_monthly[7] += 1
                elif 5088 <= idx < 5832:
                    count_monthly[8] += 1
                elif 5832 <= idx < 6552:
                    count_monthly[9] += 1
                elif 6552 <= idx < 7296:
                    count_monthly[10] += 1
                elif 7296 <= idx < 8016:
                    count_monthly[11] += 1
                elif 8016 <= idx <= 8759:
                    count_monthly[12] += 1

        # # idx based on last hour of drought, counting for leap years
        elif y in self.leap_years:
            for idx in self.temp_idx:
                if 0 <= idx < 744:
                    count_monthly[1] += 1
                elif 744 <= idx < 1440:
                    count_monthly[2] += 1
                elif 1440 <= idx < 2184:
                    count_monthly[3] += 1
                elif 2184 <= idx < 2904:
                    count_monthly[4] += 1
                elif 2904 <= idx < 3648:
                    count_monthly[5] += 1
                elif 3648 <= idx < 4368:
                    count_monthly[6] += 1
                elif 4368 <= idx < 5112:
                    count_monthly[7] += 1
                elif 5112 <= idx < 5856:
                    count_monthly[8] += 1
                elif 5856 <= idx < 6576:
                    count_monthly[9] += 1
                elif 6576 <= idx < 7320:
                    count_monthly[10] += 1
                elif 7320 <= idx < 8040:
                    count_monthly[11] += 1
                elif 8040 <= idx <= 8783:
                    count_monthly[12] += 1

        self.count_monthly = count_monthly

        # idx based on mid hour of drought, counting for non-leap years
        if y not in self.leap_years:
            for idx in self.temp_idx_mid:
                if -8759 <= idx < -8016:
                    count_monthly_mid[-1] += 1
                elif -8016 <= idx < -7344:
                    count_monthly_mid[-2] += 1
                elif -7344 <= idx < -6600:
                    count_monthly_mid[-3] += 1
                elif -6600 <= idx < -5880:
                    count_monthly_mid[-4] += 1
                elif -5880 <= idx < -5136:
                    count_monthly_mid[-5] += 1
                elif -5136 <= idx < -4416:
                    count_monthly_mid[-6] += 1
                elif -4416 <= idx < -3672:
                    count_monthly_mid[-7] += 1
                elif -3672 <= idx < -2928:
                    count_monthly_mid[-8] += 1
                elif -2928 <= idx < -2208:
                    count_monthly_mid[-9] += 1
                elif -2208 <= idx < -1464:
                    count_monthly_mid[-10] += 1
                elif -1464 <= idx < -744:
                    count_monthly_mid[-11] += 1
                elif -744 <= idx <= -1:
                    count_monthly_mid[-12] += 1
                elif 0 <= idx < 744:
                    count_monthly_mid[1] += 1
                elif 744 <= idx < 1416:
                    count_monthly_mid[2] += 1
                elif 1416 <= idx < 2160:
                    count_monthly_mid[3] += 1
                elif 2160 <= idx < 2880:
                    count_monthly_mid[4] += 1
                elif 2880 <= idx < 3624:
                    count_monthly_mid[5] += 1
                elif 3624 <= idx < 4344:
                    count_monthly_mid[6] += 1
                elif 4344 <= idx < 5088:
                    count_monthly_mid[7] += 1
                elif 5088 <= idx < 5832:
                    count_monthly_mid[8] += 1
                elif 5832 <= idx < 6552:
                    count_monthly_mid[9] += 1
                elif 6552 <= idx < 7296:
                    count_monthly_mid[10] += 1
                elif 7296 <= idx < 8016:
                    count_monthly_mid[11] += 1
                elif 8016 <= idx <= 8759:
                    count_monthly_mid[12] += 1

        # idx based on mid hour of drought, counting for leap years
        elif y in self.leap_years:
            for idx in self.temp_idx:
                if -8783 <= idx < -8040:
                    count_monthly_mid[-1] += 1
                elif -8040 <= idx < -7344:
                    count_monthly_mid[-2] += 1
                elif -7344 <= idx < -6600:
                    count_monthly_mid[-3] += 1
                elif -6600 <= idx < -5880:
                    count_monthly_mid[-4] += 1
                elif -5880 <= idx < -5136:
                    count_monthly_mid[-5] += 1
                elif -5136 <= idx < -4416:
                    count_monthly_mid[-6] += 1
                elif -4416 <= idx < -3672:
                    count_monthly_mid[-7] += 1
                elif -3672 <= idx < -2928:
                    count_monthly_mid[-8] += 1
                elif -2928 <= idx < -2208:
                    count_monthly_mid[-9] += 1
                elif -2208 <= idx < -1464:
                    count_monthly_mid[-10] += 1
                elif -1464 <= idx < -744:
                    count_monthly_mid[-11] += 1
                elif -744 <= idx <= -1:
                    count_monthly_mid[-12] += 1
                if 0 <= idx < 744:
                    count_monthly_mid[1] += 1
                elif 744 <= idx < 1440:
                    count_monthly_mid[2] += 1
                elif 1440 <= idx < 2184:
                    count_monthly_mid[3] += 1
                elif 2184 <= idx < 2904:
                    count_monthly_mid[4] += 1
                elif 2904 <= idx < 3648:
                    count_monthly_mid[5] += 1
                elif 3648 <= idx < 4368:
                    count_monthly_mid[6] += 1
                elif 4368 <= idx < 5112:
                    count_monthly_mid[7] += 1
                elif 5112 <= idx < 5856:
                    count_monthly_mid[8] += 1
                elif 5856 <= idx < 6576:
                    count_monthly_mid[9] += 1
                elif 6576 <= idx < 7320:
                    count_monthly_mid[10] += 1
                elif 7320 <= idx < 8040:
                    count_monthly_mid[11] += 1
                elif 8040 <= idx <= 8783:
                    count_monthly_mid[12] += 1

        self.count_monthly_mid = count_monthly_mid

    def aggregate_to_quarterly_count(self, y):
        '''
        Distributes hourly indices of critical periods across all quarters
        for both leap and ordinary years.
        '''
        count_quarterly = pd.Series(np.zeros(4), range(1, 5), dtype='int64')

        # TODO: assign to previous quarter if half of drought period in previous quarter (compare idx and period p)

        # counting for non-leap-years
        if y not in self.leap_years:
            for idx in self.temp_idx:
                if 0 <= idx < 2184:
                    count_quarterly[1] += 1
                elif 2184 <= idx < 4368:
                    count_quarterly[2] += 1
                elif 4368 <= idx < 6576:
                    count_quarterly[3] += 1
                elif 6576 <= idx < 8783:
                    count_quarterly[4] += 1

        # counting for non-leap-years
        elif y in self.leap_years:
            for idx in self.temp_idx:
                if 0 <= idx < 2160:
                    count_quarterly[1] += 1
                elif 2160 <= idx < 4344:
                    count_quarterly[2] += 1
                elif 4344 <= idx < 6552:
                    count_quarterly[3] += 1
                elif 6552 <= idx < 8759:
                    count_quarterly[4] += 1

        self.count_quarterly = count_quarterly

    def mean_below_threshold(self, ts_long):
        '''
        TODO: idea: compute moving average ts and apply same algorithm as CBT

        Counts windows or events. When counting events, make sure that first
        considered period duration has no match. Otherwise, there may be
        longer-lasting events not counted.
        '''
        # data container
        cf = {}
        idx = {}
        cf_idx = {}
        e_idx_cum = {}
        monthly = {}
        monthly_mid = {}
        monthly_mid_prev_year = {}
        quarterly = {}

        # progress documentation
        print(
            f"Search MBT {self.counting_algorithm}s -> "
            f"technology: {self.technology}, threshold: {self.threshold_label}, "
            f"region: {self.region}."
        )

        # iterate overall years
        for idx_y, y in enumerate(self.years):

            # data container per year
            cf[y] = {}
            idx[y] = {}
            cf_idx[y] = {}
            e_idx_cum[y] = {}
            monthly[y] = {}
            monthly_mid[y] = {}
            monthly_mid_prev_year[y] = {}
            quarterly[y] = {}

            # define period range
            self.define_period_range()

            # iterate overall periods
            for p_count, period in enumerate(self.periods):

                if self.counting_algorithm == 'window':

                    # extract data from original timeseries
                    self.extract_data(ts_long, y, period)

                elif self.counting_algorithm == 'event':

                    # fetch data for first iteration, smaller windows use modified data
                    if p_count == 0:

                        # extract data from original timeseries
                        self.extract_data(ts_long, y, period)

                    elif p_count > 0:

                        # drop first data entry
                        self.data = self.data.iloc[1:, :]

                # determine windows with moving average below threshold with window [t-period+1, t]
                self.simple_moving_average(3, period)

# =============================================================================
#                 # progress documentation
#                 print(
#                     f"Search MBT {self.counting_algorithm}s -> "
#                     f"technology: {self.technology}, threshold: {self.threshold_run}, "
#                     f"region: {self.region}, year: {y}, duration: {period}.",
#                     end=''
#                 )
# =============================================================================

                # count mbt windows or events
                if self.counting_algorithm == 'window':

                    # windows count agorithm
                    self.find_mbt_windows(period)

                elif self.counting_algorithm == 'event':

                    # events count agorithm
                    self.find_mbt_events(period)

                # check whether at least one window or event is found
                if self.temp_idx:

                    # count occurences per month
                    self.aggregate_to_monthly_count(y)

                    # count occurences per quarter
                    self.aggregate_to_quarterly_count(y)

                    # data containers per window size
                    cf[y][period] = self.temp_cf
                    idx[y][period] = self.temp_idx
                    cf_idx[y][period] = self.temp_cf_idx
                    e_idx_cum[y][period] = self.temp_e_idx_cum
                    monthly[y][period] = self.count_monthly
                    monthly_mid[y][period] = self.count_monthly_mid.loc[1:12]
                    monthly_mid_prev_year[y][period] = self.count_monthly_mid.loc[-1:-12]
                    quarterly[y][period] = self.count_quarterly

# =============================================================================
#                     # new line in stdout
#                     if len(self.temp_idx) == 1:
#                         print(f" -> {len(self.temp_idx)} {self.counting_algorithm} found")
#                     elif len(self.temp_idx) > 1:
#                         print(f" -> {len(self.temp_idx)} {self.counting_algorithm}s found")
# =============================================================================

                # break inner for loop over years if no energy drougts determined
                elif self.counting_algorithm == 'window':
                    # print('\nNo MBT energy drought windows longer than {} found.'.format(period-1))
                    break

                # continue inner for loop over periods if no energy drougts determined
                elif self.counting_algorithm == 'event':
                    # check whether energy droughts have been found at all
                    # if not idx[y]:
                    #     print(' -> none found')
                    # else:
                    #     print(' -> no additional events found')
                    continue

        # sort in ascending order of y
        monthly_mid_prev_year = {y: data_period for y, data_period in sorted(monthly_mid_prev_year.items())}

        # allocate droughts into previous year  (only for median hours)
        for idx_y, (y, data_period) in enumerate(monthly_mid_prev_year.items()):
            if idx_y > 0:
                for period, count_monthly_mid_prev_year in data_period.items():
                    count_monthly_mid_prev_year.index = np.arange(1, 13)
                    if period in monthly_mid[y-1]:
                        monthly_mid[y-1][period] += count_monthly_mid_prev_year
                    else:
                        monthly_mid[y-1][period] = count_monthly_mid_prev_year

        # self.cf = cf
        self.idx = idx
        self.cf_idx = cf_idx
        self.e_idx_cum = e_idx_cum
        self.monthly = monthly
        self.monthly_mid = monthly_mid
        self.quarterly = quarterly

    def constantly_below_threshold(self, ts_long):
        '''
        Counts windows or events. When counting events, make sure that first
        considered period duration has no match. Otherwise, there may be
        longer-lasting events not counted.

        Parameters
        ----------
        years : list
            List of years to be considered in the analysis.
        ts_long : DataFrame
            DataFrame with timeseries in long format.
        Returns
        -------
        None.

        '''
        # data container
        idx = {}
        cf_idx = {}
        e_idx_cum = {}
        monthly = {}
        monthly_mid = {}
        monthly_mid_prev_year = {}
        quarterly = {}

        # progress documentation
        print(
            f"Search CBT {self.counting_algorithm}s -> "
            f"technology: {self.technology}, threshold: {self.threshold_label}, "
            f"region: {self.region}."
        )

        # iterate overall years
        for idx_y, y in enumerate(self.years):

            # data container per year
            idx[y] = {}
            cf_idx[y] = {}
            e_idx_cum[y] = {}
            monthly[y] = {}
            monthly_mid[y] = {}
            monthly_mid_prev_year[y] = {}
            quarterly[y] = {}

            # define period range
            self.define_period_range()

            # iterate overall periods
            for p_count, period in enumerate(self.periods):

                if self.counting_algorithm == 'window':

                    # extract data from original timeseries
                    self.extract_data(ts_long, y, period)

                elif self.counting_algorithm == 'event':

                    # fetch data for first iteration, smaller windows use modified data
                    if p_count == 0:

                        # extract data from original timeseries
                        self.extract_data(ts_long, y, period)

                    elif p_count > 0:

                        # drop first data entry
                        self.data = self.data.iloc[1:, :]

                # test for rolling windows with all values below threshold
                self.rolling_cbt_test(3, period)

# =============================================================================
#                 # progress documentation
#                 print(
#                     f"Search CBT {self.counting_algorithm}s -> "
#                     f"technology: {self.technology}, threshold: {self.threshold_run}, "
#                     f"region: {self.region}, year: {y}, duration: {period}."#, end=''
#                 )
# =============================================================================

                # count cbt windows or events
                if self.counting_algorithm == 'window':

                    # windows count agorithm
                    self.find_cbt_windows(period)

                elif self.counting_algorithm == 'event':

                    # events count agorithm
                    self.find_cbt_events(period)

                # check whether at least one window or event is found
                if self.temp_idx:

                    # count occurences per month
                    self.aggregate_to_monthly_count(y)

                    # count occurences per quarter
                    self.aggregate_to_quarterly_count(y)

                    # data containers per window size
                    idx[y][period] = self.temp_idx
                    cf_idx[y][period] = self.temp_cf_idx
                    e_idx_cum[y][period] = self.temp_e_idx_cum
                    monthly[y][period] = self.count_monthly
                    monthly_mid[y][period] = self.count_monthly_mid.loc[1:12]
                    if idx_y > 0:
                        monthly_mid_prev_year[y][period] = self.count_monthly_mid.loc[-1:-12]
                    quarterly[y][period] = self.count_quarterly

# =============================================================================
#                     # new line in stdout
#                     if len(self.temp_idx) == 1:
#                         print(f" -> {len(self.temp_idx)} {self.counting_algorithm} found")
#                     elif len(self.temp_idx) > 1:
#                         print(f" -> {len(self.temp_idx)} {self.counting_algorithm}s found")
# =============================================================================

                # break inner for loop over years if no energy drougts determined
                elif self.counting_algorithm == 'window':
                    # print('')
                    # print(f"No CBT energy drought {self.counting_algorithm} longer than {period-1} found.")
                    break

                # continue inner for loop over periods if no energy drougts determined
                elif self.counting_algorithm == 'event':
                    # check whether energy droughts have been found at all
                    # if not idx[y]:
                    #     print(' -> none found')
                    # else:
                    #     print(' -> no additional events found')
                    continue

        # sort in ascending order of y
        monthly_mid_prev_year = {y: data_period for y, data_period in sorted(monthly_mid_prev_year.items())}

        # allocate droughts into previous year (only for median hours)
        for idx_y, (y, data_period) in enumerate(monthly_mid_prev_year.items()):
            if idx_y > 0:
                for period, count_monthly_mid_prev_year in data_period.items():
                    count_monthly_mid_prev_year.index = np.arange(1, 13)
                    if period in monthly_mid[y-1]:
                        monthly_mid[y-1][period] += count_monthly_mid_prev_year
                    else:
                        monthly_mid[y-1][period] = count_monthly_mid_prev_year

        self.idx = idx
        self.cf_idx = cf_idx
        self.e_idx_cum = e_idx_cum
        self.monthly = monthly
        self.monthly_mid = monthly_mid
        self.quarterly = quarterly

    def sequent_peak_algorithm(self, ts_long):

        # data container
        idx = {}
        cf_idx = {}
        e_idx = {}
        e_idx_cum = {}
        monthly = {}
        monthly_mid = {}
        monthly_mid_prev_year = {}
        quarterly = {}
        
        # define period range
        self.define_period_range()

        # progress documentation
        print(
            f"Search SPA {self.counting_algorithm}s -> "
            f"technology: {self.technology}, threshold: {self.threshold_label}, "
            f"region: {self.region}."
        )

        # iterate overall years
        for idx_y, y in enumerate(self.years):

            # data container per year
            idx[y] = {}
            cf_idx[y] = {}
            e_idx[y] = {}
            e_idx_cum[y] = {}
            monthly[y] = {}
            monthly_mid[y] = {}
            monthly_mid_prev_year[y] = {}
            quarterly[y] = {}

            # extract data from original timeseries plus full previous year - 1h
            period = 8760
            self.extract_data(ts_long, y, period)

            # Initialize the energy_deficit column with zeros
            self.data.reset_index(drop=True, inplace=True)
            self.data['energy_deficit'] = 0.0

            # sequent peak algorithm: compute cumulative energy deficit
            for i, row in self.data.iterrows():
                if i == 0:  # First row
                    energy_deficit = round(self.threshold_run - row['capacity_factor'], 10)
                    self.data.at[i, 'energy_deficit'] = max(0, energy_deficit)
                else:
                    previous_deficit = self.data.at[i-1, 'energy_deficit']
                    energy_deficit = round(previous_deficit + self.threshold_run - row['capacity_factor'], 10)
                    self.data.at[i, 'energy_deficit'] = max(0, energy_deficit)

            # find consective hours that form an event incl. time steps beyond max
            non_zero = self.data['energy_deficit'] > 0
            change = non_zero != non_zero.shift(1)

            # if first two rows in energy_deficit are both negative, then avoid misdetection of end
            if non_zero.iloc[0] == False:
                change.iloc[0] = False

            # find start and end indices of non-zero slices (ends are deliberately first index of zero-slices)
            starts = np.where(change & non_zero)[0]
            ends = np.where(change & ~non_zero)[0]

            # if the last value is non-zero, add the end index
            if non_zero.iloc[-1]:
                ends = np.append(ends, len(self.data))

            # slice identified events until max, drop the time steps after
            for start, end in zip(starts, ends):
                slice_df = self.data.iloc[start:end]
                max_idx = slice_df['energy_deficit'].idxmax()
                max_idx_pos = slice_df.index.get_loc(max_idx)
                self.data.loc[start:start+max_idx_pos, 'energy_deficit_until_max'] = self.data.loc[start:start+max_idx_pos, 'energy_deficit']
                self.data.loc[start+max_idx_pos+1:end, 'energy_deficit_until_max'] = 0
            self.data['energy_deficit_until_max'] = self.data['energy_deficit_until_max'].fillna(0)

            # check for event spanning across turn of the previous year and the one under investigation
            if self.data.loc[8758, 'energy_deficit_until_max'] > 0 and self.data.loc[8759, 'energy_deficit_until_max'] > 0:
                # find the start of this non-zero period
                running_index = 8758
                while running_index >= 0 and self.data.loc[running_index, 'energy_deficit_until_max'] > 0:
                    running_index -= 1
                drop_index = running_index + 1
            else:
                drop_index = 8759
            self.data = self.data.iloc[drop_index:].reset_index(drop=True)
            no_time_steps_from_prev_year = 8759 - drop_index

            # find consective hours that form an event after time steps beyond max were removed
            non_zero = self.data['energy_deficit_until_max'] > 0
            change = non_zero != non_zero.shift(1)

            # if first row in energy_deficit is negative, then avoid misdetection of end
            if non_zero.iloc[0] == False:
                change.iloc[0] = False

            starts = np.where(change & non_zero)[0]
            ends = np.where(change & ~non_zero)[0]

            # If the last value is non-zero, add the end index
            if non_zero.iloc[-1]:
                ends = np.append(ends, len(self.data))

            # determine event characteristics (duration, index last time step & median time step, energy deficit, etc.)
            events = []
            for start, end in zip(starts, ends):
                duration = end - start
                # correct time step index for previous year -> index in current year exl. prev year
                last_index_hour = end - 1 - no_time_steps_from_prev_year
                median_index = last_index_hour - round(duration / 2)
                max_e_deficit = self.data.iloc[end - 1]['energy_deficit_until_max']
                sequence_cf = self.data['capacity_factor'].iloc[start:end]
                sequence_cf.index = range(last_index_hour-duration+1, last_index_hour+1)
                sequence_e_deficit = self.data['energy_deficit_until_max'].iloc[start:end]
                sequence_e_deficit.index = range(last_index_hour-duration+1, last_index_hour+1)
                event_info = (duration, last_index_hour, median_index, max_e_deficit, sequence_cf, sequence_e_deficit)
                events.append(event_info)

            # convert DataFrame for sorting
            events = pd.DataFrame(events, columns=['duration', 'last_index', 'median_index', 'cum_energy_deficit',
                                                   'sequence_cf', 'sequence_e_deficit'])
            events = events.sort_values(by='duration', ascending=False).reset_index(drop=True)

            # allocate results in results container
            for duration, group_events in events.groupby('duration'):
                idx[y][duration] = list(group_events['last_index'])
                cf_idx[y][duration] = group_events.set_index('last_index')['sequence_cf'].to_dict()
                e_idx[y][duration] = group_events.set_index('last_index')['sequence_e_deficit'].to_dict()
                e_idx_cum[y][duration] = group_events.set_index('last_index')['cum_energy_deficit'].to_dict()

                # count occurences per month
                self.temp_idx = list(group_events['last_index'])
                self.temp_idx_mid = list(group_events['median_index'])
                self.aggregate_to_monthly_count(y)
                monthly[y][duration] = self.count_monthly
                monthly_mid[y][duration] = self.count_monthly_mid.loc[1:12]
                if idx_y > 0:
                    monthly_mid_prev_year[y][duration] = self.count_monthly_mid.loc[-1:-12]

                # count occurences per quarter
                self.aggregate_to_quarterly_count(y)
                quarterly[y][duration] = self.count_quarterly
                
        # sort in ascending order of y
        monthly_mid_prev_year = {y: data_period for y, data_period in sorted(monthly_mid_prev_year.items())}

        # allocate droughts into previous year (only for median hours)
        for idx_y, (y, data_period) in enumerate(monthly_mid_prev_year.items()):
            if idx_y > 0:
                for period, count_monthly_mid_prev_year in data_period.items():
                    count_monthly_mid_prev_year.index = np.arange(1, 13)
                    if period in monthly_mid[y-1]:
                        monthly_mid[y-1][period] += count_monthly_mid_prev_year
                    else:
                        monthly_mid[y-1][period] = count_monthly_mid_prev_year

        self.idx = idx
        self.cf_idx = cf_idx
        self.e_idx = e_idx
        self.e_idx_cum = e_idx_cum
        self.monthly = monthly
        self.monthly_mid = monthly_mid
        self.quarterly = quarterly

    def transform_results_format(self):
        '''
        Flipps dict hierarchy and converts to series or dataframes.

        TODO: This is only correct for the event perspective. The windows
        perspective requires an additional averging overall duration-specific,
        islotaed time series scans that contribute to the frequency in the end.
        Idea: Divide over number of periods in self.idx[y][period].
        '''
        count_yearly = defaultdict(dict)
        count_monthly = defaultdict(dict)
        count_monthly_mid = defaultdict(dict)
        count_quarterly = defaultdict(dict)
        energy_deficit_cum = defaultdict(dict)
        energy_deficit_cum_max = {}

        # flip idx dict hierarchy
        for key, val in self.idx.items():
            for subkey, subval in val.items():
                count_yearly[subkey][key] = len(subval)

        # convert dict values to series
        for k, v in count_yearly.items():
            count_yearly[k] = pd.Series(v)

        # flip monthly dict hierarchy
        for key, val in self.monthly.items():
            for subkey, subval in val.items():
                count_monthly[subkey][key] = subval

        # flip monthly_mid dict hierarchy
        for key, val in self.monthly_mid.items():
            for subkey, subval in val.items():
                count_monthly_mid[subkey][key] = subval

        # convert dict values to dataframe
        for k, v in count_monthly.items():
            count_monthly[k] = pd.DataFrame(v, columns=self.years).fillna(0)
        for k, v in count_monthly_mid.items():
            count_monthly_mid[k] = pd.DataFrame(v, columns=self.years).fillna(0)

        # flip quarterly dict hierarchy
        for key, val in self.quarterly.items():
            for subkey, subval in val.items():
                count_quarterly[subkey][key] = subval

        # convert dict values to dataframe
        for k, v in count_quarterly.items():
            count_quarterly[k] = pd.DataFrame(v, columns=self.years).fillna(0)

        # flip energy dict hierarchy
        for key, val in self.e_idx_cum.items():
            for subkey, subval in val.items():
                energy_deficit_cum[subkey][key] = subval

        # find maximum cumulative energy deficit across all years for each period duration
        for period, data in energy_deficit_cum.items():
            energy_deficit_cum_max[period] = {}
            for y, subdata in data.items():
                energy_deficit_cum_max[period][y] = max(subdata.values())
            energy_deficit_cum_max[period] = max(energy_deficit_cum_max[period].values())
        energy_deficit_cum_max = pd.Series(energy_deficit_cum_max, dtype='float', name=self.threshold_run)
        energy_deficit_cum_max.sort_values(axis=0, inplace=True)

        # TODO. find average energy deficit across all years

        self.count_yearly = count_yearly
        self.count_monthly = count_monthly
        self.count_monthly_mid = count_monthly_mid
        self.count_quarterly = count_quarterly
        self.energy_deficit_cum = energy_deficit_cum
        self.energy_deficit_cum_max = energy_deficit_cum_max

    def mean_occurence_singular(self):
        '''
        Averages yearly results of monthly and quarterly count. Computes mean for
        each period for yearly, monthly, and quarterly count.
        '''
        mean_monthly_singular = {}
        mean_monthly_mid_singular = {}
        mean_quarterly_singular = {}

        # determine mean for each period for monthly and quarterly counts across all years
        for period in self.count_monthly:
            mean_monthly_singular[period] = self.count_monthly[period].mean(axis=1)
            mean_monthly_mid_singular[period] = self.count_monthly_mid[period].mean(axis=1)
            mean_quarterly_singular[period] = self.count_quarterly[period].mean(axis=1)

        # convert dict to df and name index
        mean_yearly_singular = pd.DataFrame(self.count_yearly)
        mean_monthly_singular = pd.DataFrame(mean_monthly_singular)
        mean_monthly_mid_singular = pd.DataFrame(mean_monthly_mid_singular)
        mean_quarterly_singular = pd.DataFrame(mean_quarterly_singular)

        # compute mean for each period across all years, months, and quarters
        mean_yearly_singular = mean_yearly_singular.fillna(0)
        mean_yearly_singular.loc['Average'] = mean_yearly_singular.mean(axis=0)
        mean_monthly_singular.loc['Average'] = mean_monthly_singular.mean(axis=0)
        mean_monthly_mid_singular.loc['Average'] = mean_monthly_mid_singular.mean(axis=0)

        # rename index
        months = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
                  'August', 'September', 'October', 'November', 'December', 'Average']
        mean_monthly_singular['month'] = months
        mean_monthly_mid_singular['month'] = months
        mean_monthly_singular.set_index('month', inplace=True)
        mean_monthly_mid_singular.set_index('month', inplace=True)

        quarters = ['Q1', 'Q2', 'Q3', 'Q4']
        mean_quarterly_singular['quarter'] = quarters
        mean_quarterly_singular.set_index('quarter', inplace=True)
        mean_quarterly_singular.loc['Q1+Q4'] = mean_quarterly_singular.loc['Q1'] + mean_quarterly_singular.loc['Q4']
        mean_quarterly_singular.loc['Q2+Q3'] = mean_quarterly_singular.loc['Q2'] + mean_quarterly_singular.loc['Q3']

        # reorder columns in ascending order
        mean_yearly_singular = mean_yearly_singular.reindex(sorted(mean_yearly_singular.columns), axis=1)
        mean_monthly_singular = mean_monthly_singular.reindex(sorted(mean_monthly_singular.columns), axis=1)
        mean_monthly_mid_singular = mean_monthly_mid_singular.reindex(sorted(mean_monthly_mid_singular.columns), axis=1)
        mean_quarterly_singular = mean_quarterly_singular.reindex(sorted(mean_quarterly_singular.columns), axis=1)

        self.mean_yearly_singular = mean_yearly_singular
        self.mean_monthly_singular = mean_monthly_singular
        self.mean_monthly_mid_singular = mean_monthly_mid_singular
        self.mean_quarterly_singular = mean_quarterly_singular

    def mean_occurence_cumulative(self):
        '''
        Computes cumulative sum of minimum drought occurrences. Interpretation: How many
        drought periods with a duration of at least so many hours. Number of hours are
        in index of returned dataframes.
        '''
        # temporary objects to get complete range of investigated durations
        # periods = define_period_range(self.counting_algorithm, self.start, self.end)
        temp = pd.DataFrame(columns=list(self.periods))

        # compute cumsum for yearly counts starting from longest to shortest duration
        mean_yearly_cum = pd.merge(
            self.mean_yearly_singular.T, temp.T, how='outer', left_index=True, right_index=True
            ).T
        mean_yearly_cum = mean_yearly_cum.fillna(0)
        mean_yearly_cum = mean_yearly_cum.iloc[:, ::-1].cumsum(axis=1).iloc[:, ::-1]
        mean_yearly_cum = mean_yearly_cum.replace(0, np.nan).dropna(axis=1, how="all")
        mean_yearly_cum = mean_yearly_cum.fillna(0)

        # compute cumsum for monthly counts starting from longest to shortest duration
        # mean_monthly_cum = pd.merge(
        #     self.mean_monthly_singular.T, temp.T, how='outer', left_index=True, right_index=True
        #    ).T
        # mean_monthly_cum = mean_monthly_cum.fillna(0)
        # mean_monthly_cum = mean_monthly_cum.iloc[:, ::-1].cumsum(axis=1).iloc[:, ::-1]
        # mean_monthly_cum = mean_monthly_cum.fillna(0)

        # compute cumsum for quarterly counts starting from longest to shortest duration
        mean_quarterly_cum = pd.merge(
            self.mean_quarterly_singular.T, temp.T, how='outer', left_index=True, right_index=True
            ).T
        mean_quarterly_cum = mean_quarterly_cum.fillna(0)
        mean_quarterly_cum = mean_quarterly_cum.iloc[:, ::-1].cumsum(axis=1).iloc[:, ::-1]
        mean_quarterly_cum = mean_quarterly_cum.fillna(0)

        self.mean_yearly_cum = mean_yearly_cum
        # self.mean_monthly_cum = mean_monthly_cum
        self.mean_quarterly_cum = mean_quarterly_cum

    @staticmethod
    def find_most_extreme_period(df):
        '''
        For each row in df, the function finds index of last cell with value
        greater than zero. Applied to monthly, quarterly, and yearly aggregated
        results, this is the longest period duration in hours.
        '''
        most_extreme = {}
        for row_idx, row in df.iterrows():
            most_extreme[row_idx] = row.index[row > 0][-1] if len(row.index[row > 0]) > 0 else 0
            most_extreme = pd.Series(most_extreme)
            most_extreme.rename({'Average': 'Maximum'}, inplace=True)
        return most_extreme

    def return_period(self, df, row):
        '''
        Computes return period as reciprocal of average occurence. Only return periods
        longer than half a year are returned. For each unique return period, the longest
        drought period is selected.
        '''
        # compute return period for all periods
        temp = 1 / df.loc[row]
        temp = temp.round(2)

        # find unique return periods
        unique_values = temp.unique()
        return_period = pd.Series(dtype='float32')
        for v in unique_values:
            return_period[v] = max(temp.index[temp == v])

        # rename index and series
        return_period = return_period.rename_axis('return_period')
        return_period.name = self.threshold_run

        return return_period

    def standardized_return_period(self, return_period):
        '''
        Returns standardized return period for full the range of investigated years.
        Not for every duration there is an event, i.e., not for every event duration
        there is a return period. Hence, the duration for a given standardized return
        period is extraploated, always using the longest event duration found that has
        at least the given return period. Once there is a duration whose frequency
        reciprocal is at least the standardized return period, it is assigned as
        maximum duration. This duration then is assigned for longer return periods
        until again there is a longer duration whose whose frequency reciprocal is
        at least the standardized return period. This is implemented using 'ffill'.
        If there is no return period for a specific year, then the year is marked
        with NaN.
        '''
        years = range(1, len(self.years)+1)
        return_period_standard = pd.Series(dtype='object')
        for y in years:
            return_period_standard.at[y] = return_period.iloc[return_period.index.get_indexer([y], 'ffill')[0]]  # 'nearest'
        return_period_standard = return_period_standard.rename_axis('return_period')
        return return_period_standard

    def count_droughts(self, ts_long):
        '''
        Initiates the counting algorithm. Tranforms result in suitable format.
        Computes relevant results.
        '''
        # determine MBT or CBT results
        if self.drought_type == 'mbt':
            self.mean_below_threshold(ts_long)
        elif self.drought_type == 'cbt':
            self.constantly_below_threshold(ts_long)
        elif self.drought_type == 'spa':
            self.sequent_peak_algorithm(ts_long)

        # transform results
        self.transform_results_format()

        # compute mean occurence singular count
        self.mean_occurence_singular()

        # compute mean occurence cumulative count
        self.mean_occurence_cumulative()

        # compute return period for specified row
        self.return_period_all = self.return_period(self.mean_yearly_cum, 'Average')
        self.return_period_q1 = self.return_period(self.mean_quarterly_cum, 'Q1')
        self.return_period_q2 = self.return_period(self.mean_quarterly_cum, 'Q2')
        self.return_period_q3 = self.return_period(self.mean_quarterly_cum, 'Q3')
        self.return_period_q4 = self.return_period(self.mean_quarterly_cum, 'Q4')
        self.return_period_q14 = self.return_period(self.mean_quarterly_cum, 'Q1+Q4')
        self.return_period_q23 = self.return_period(self.mean_quarterly_cum, 'Q2+Q3')

        # compute standardized return periods
        self.return_period_all_standard = self.standardized_return_period(self.return_period_all)
        self.return_period_q1_standard = self.standardized_return_period(self.return_period_q1)
        self.return_period_q2_standard = self.standardized_return_period(self.return_period_q2)
        self.return_period_q3_standard = self.standardized_return_period(self.return_period_q3)
        self.return_period_q4_standard = self.standardized_return_period(self.return_period_q4)
        self.return_period_q14_standard = self.standardized_return_period(self.return_period_q14)
        self.return_period_q23_standard = self.standardized_return_period(self.return_period_q23)

        # most extreme periods
        self.extremes_monthly = self.find_most_extreme_period(self.mean_monthly_singular)
        self.extremes_monthly_mid = self.find_most_extreme_period(self.mean_monthly_mid_singular)
        self.extremes_yearly = self.find_most_extreme_period(self.mean_yearly_singular)
