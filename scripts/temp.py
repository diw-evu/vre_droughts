# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 16:16:48 2023

@author: mkittel
"""

import seaborn as sns
import matplotlib.pyplot as plt

p89="D:\\git\\vre_droughts\\output\\2023-07-26_10-08-58_mbt_event\\portfolio\\pearson_correlation_per_technology_all_regions\\correlation_threshold_168\\f-mean-cf=0.4\\pearson_correlation_drought_threshold_f-mean-cf=0.4_1989.csv"
p91="D:\\git\\vre_droughts\\output\\2023-07-26_10-08-58_mbt_event\\portfolio\\pearson_correlation_per_technology_all_regions\\correlation_threshold_168\\f-mean-cf=0.4\\pearson_correlation_drought_threshold_f-mean-cf=0.4_1991.csv"

df = pd.read_csv(p89)
df = pd.read_csv(p91)

df.index = df.iloc[:,0]
df = df.iloc[:,1:]

# upper triagle to be hidden in heatmap
upper = np.triu(df.to_numpy())

cbar_label='Pearson correlation'
font_scale=0.9
figsize=(9, 7)
cmap='flare'

# adjust mask and df
upper_trunc = upper[1:, :-1]
corr_trunc = df.iloc[1:, :-1].copy()

# plot heat map
sns.set(font_scale=font_scale, font='serif')
fig = plt.figure()#figsize=figsize)
sns.heatmap(corr_trunc, cmap=cmap, cbar_kws={'label': cbar_label}, \
            mask=upper_trunc, vmax=1, vmin=-0.25)
fig.show()