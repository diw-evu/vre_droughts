# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 17:13:45 2024

@author: mkittel
"""

import os
os.chdir('D:\\git\\vre_droughts\\scripts')
import pickle
import gzip
import pandas as pd
import plotly.express as px
from analyzer import Analyzer
import pathlib
from pathlib import Path
temp = pathlib.PosixPath
pathlib.PosixPath = pathlib.WindowsPath


def load_object(path, file):
    '''
    For debugging: Loads object from pickle.
    '''
    cwd = os.getcwd()
    os.chdir(path)
    with gzip.open(file, 'rb') as file:
        obj = pickle.load(file)
    os.chdir(cwd)
    return obj

# import results
path_cbt_w = 'T:\\mk\\vreda\\output\\2024-07-19_13-46-18_cbt_window_DE_won_case_study'
path_cbt_e = 'T:\\mk\\vreda\\output\\2024-07-22_19-43-54_cbt_event_DE_won_case_study'
path_vmbt_w = 'T:\\mk\\vreda\\output\\2024-07-19_11-57-06_mbt_window_DE_won_case_study'
path_vmbt_e = 'T:\\mk\\vreda\\output\\2024-07-23_10-59-13_mbt_event_DE_won_case_study'

a_cbt_w = load_object(path_cbt_w,'analyzer.gz')
a_cbt_e = load_object(path_cbt_e,'analyzer.gz')
a_vmbt_w = load_object(path_vmbt_w,'analyzer.gz')
a_vmbt_e = load_object(path_vmbt_e,'analyzer.gz')
a_objects = [a_cbt_e, a_vmbt_e]
#a_objects = [a_cbt_w, a_cbt_e, a_vmbt_w, a_vmbt_e]
#a_objects_names = ['windows-CBT', 'events-CBT', 'windows-VMBT', 'events-VMBT']
a_objects_names = ['CBT','VMBT']

# get drought time series
droughts_ts_cbt_w = load_object(path_cbt_w,'drought_ts_tech.gz')['wind_onshore'][48]['a=0.1']
droughts_ts_cbt_e = load_object(path_cbt_e,'drought_ts_tech.gz')['wind_onshore'][1]['a=0.1']
droughts_ts_vmbt_w = load_object(path_vmbt_w,'drought_ts_tech.gz')['wind_onshore'][48]['a=0.1']
droughts_ts_vmbt_e = load_object(path_vmbt_e,'drought_ts_tech.gz')['wind_onshore'][1]['a=0.1']

# prepare data
droughts_ts_vmbt_w['method'] = 'windows-VMBT'
droughts_ts_cbt_e['method'] = 'CBT'
droughts_ts_vmbt_e['method'] = 'VMBT'
drought_ts_e = pd.concat([droughts_ts_cbt_e, droughts_ts_vmbt_e])
drought_ts_e.reset_index(names='hour', inplace=True)
drought_ts_e = drought_ts_e.query('year in [1990, 1991, 1992, 1993, 1994, 1995]')
drought_ts_e = drought_ts_e.rename(columns={'DE': 'availability'})

droughts_ts_vmbt_w_max = droughts_ts_vmbt_w.query('duration == @droughts_ts_vmbt_w["duration"].max()')
droughts_ts_vmbt_w_max['method'] = f'windows-VMBT-{droughts_ts_vmbt_w["duration"].max()}h'
droughts_ts_vmbt = pd.concat([droughts_ts_vmbt_w_max, droughts_ts_vmbt_e])
droughts_ts_vmbt.reset_index(names='hour', inplace=True)

# plot drought time series events
fig = px.line(drought_ts_e, x='hour', y="availability", color='method', facet_row='year')
# Update layout
fig.update_layout(
    template='simple_white',
    margin=dict(l=0, r=0, t=0, b=0),
    height=600, width=1000,
    legend=dict(
        title='',
        orientation='h',  # Horizontal orientation
        x=0.5,  # Center the legend horizontally
        y=-0.1,  # Position the legend below the plot area
        xanchor='center',  # Anchor the legend's horizontal center to x position
        yanchor='top',  # Anchor the legend's top to the y position
        itemsizing='constant'
    ),
)
fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))
# Set opacity for each trace
for trace in fig.data:
    if trace.name == 'VMBT':
        trace.line.dash = 'dot'
fig.update_xaxes(range=[4000, 4500])
fig.update_yaxes(range=[0, 0.35])
fig.show()

path = 'T:\\mk\\vreda\\result_plots'
path_output = os.path.join(path, 'method_paper_case_study')
Path(path_output).mkdir(parents=True, exist_ok=True)
name = 'ts_events_min_duration_1'
path_output = os.path.join(path_output, name)
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')
fig.write_image(f'{path_output}.png', scale=3)

# plot vmbt drought time series
fig = px.line(droughts_ts_vmbt.query('year == 1994'), x='hour', y="DE", color='method')
fig.update_layout(
    template='simple_white',
    margin=dict(l=0, r=0, t=0, b=0),
    legend={'itemsizing': 'constant'},
    height=300, width=1000
    )
fig.update_layout(
    legend=dict(
        orientation='h',  # Horizontal orientation
        x=0.5,  # Center the legend horizontally
        y=-0.2,  # Position the legend below the plot area
        xanchor='center',  # Anchor the legend's horizontal center to x position
        yanchor='top'  # Anchor the legend's top to the y position
    )
)
fig.update_layout(legend_title_text='')
fig.show()

path = 'T:\\mk\\vreda\\result_plots'
path_output = os.path.join(path, 'method_paper_case_study')
Path(path_output).mkdir(parents=True, exist_ok=True)
name = 'ts_vmbt_min_duration_48'
path_output = os.path.join(path_output, name)
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')
fig.write_image(f'{path_output}.png', scale=3)


# prepare frequency-duration data
mean_frequency_yearly = []
for idx, a in enumerate(a_objects):
    df = a.mean_frequency_yearly['wind_onshore']
    df['method'] = a_objects_names[idx]
    mean_frequency_yearly.append(df)
mean_frequency_yearly = pd.concat(mean_frequency_yearly)
mean_frequency_yearly = mean_frequency_yearly.rename(columns={'frequency': 'frequency [h]', 'duration': 'duration [h]'})

# frequency-duration plots
fig = px.line(mean_frequency_yearly, x="duration [h]", y="frequency [h]", color='method')
fig.update_layout(
    template='simple_white',
    margin=dict(l=0, r=0, t=0, b=0),
    legend={'itemsizing': 'constant'},
    height=300, width=1000)
fig.update_layout(
    legend=dict(
        orientation='h',  # Horizontal orientation
        x=0.5,  # Center the legend horizontally
        y=-0.25,  # Position the legend below the plot area
        xanchor='center',  # Anchor the legend's horizontal center to x position
        yanchor='top'  # Anchor the legend's top to the y position
    )
)
fig.update_layout(legend_title_text='')
fig.update_xaxes(range=[5, 1200])

fig.show()

path = 'T:\\mk\\vreda\\result_plots'
path_output = os.path.join(path, 'method_paper_case_study')
Path(path_output).mkdir(parents=True, exist_ok=True)
name = 'frequency'
path_output = os.path.join(path_output, name)
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')
fig.write_image(f'{path_output}.png', scale=3)

# prepare max_yearly data
extremes_yearly_long = []
for idx, a in enumerate(a_objects):
    df = a.extremes_yearly_long['wind_onshore']
    df['method'] = a_objects_names[idx]
    extremes_yearly_long.append(df)
extremes_yearly_long = pd.concat(extremes_yearly_long)
extremes_yearly_long = extremes_yearly_long.query('years != "Maximum"')
extremes_yearly_long = extremes_yearly_long.rename(columns={'extremes_yearly': 'maximum duration [h]'})

# max year plot
fig = px.bar(extremes_yearly_long, x="years", y="maximum duration [h]", color='method', barmode='group')
fig.update_layout(
    template='simple_white',
    margin=dict(l=0, r=0, t=0, b=0),
    legend={'itemsizing': 'constant'},
    height=300, width=1000)
fig.update_layout(
    legend=dict(
        orientation='h',  # Horizontal orientation
        x=0.5,  # Center the legend horizontally
        y=-0.1,  # Position the legend below the plot area
        xanchor='center',  # Anchor the legend's horizontal center to x position
        yanchor='top'  # Anchor the legend's top to the y position
    )
)
fig.update_xaxes(title_text='')
fig.update_layout(legend_title_text='')
fig.show()

path = 'T:\\mk\\vreda\\result_plots'
path_output = os.path.join(path, 'method_paper_case_study')
Path(path_output).mkdir(parents=True, exist_ok=True)
name = 'max_yearly'
path_output = os.path.join(path_output, name)
fig.write_html(f'{path_output}.html')
fig.write_image(f'{path_output}.pdf')
fig.write_image(f'{path_output}.png', scale=3)
