# -*- coding: utf-8 -*-
"""
Created on Sat Jul 20 14:23:44 2024

@author: mkittel
"""

import os
import multiprocess as mp
import pandas as pd
import numpy as np
import pickle
import psutil
import gzip
from pathlib import Path
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.io as pio
import datetime
import webbrowser

os_plotting = 'local'

if os_plotting == 'local':
    firefox_path = 'C:\\Program Files\\Mozilla Firefox\\firefox.exe'
else:
    firefox_path = '/bin/firefox'
webbrowser.register('firefox', None, webbrowser.BackgroundBrowser(firefox_path))
pio.renderers.default = 'firefox'  # 'browser'


def load_object(path, file):
    '''
    For debugging: Loads object from pickle.
    '''
    cwd = os.getcwd()
    os.chdir(path)
    with gzip.open(file, 'rb') as file:
        obj = pickle.load(file)
    os.chdir(cwd)
    return obj


# configuration countries
sel = ['DE', 'ES', 'FR', 'IT', 'GR', 'SE', 'PL', 'UK']

# import storage level results
print('Importing storage level data.')
if os_plotting == 'local':
    path = 'S:\\projekte_paper\\vre-drought-mass\\result_data\\h2_sto_l.csv'
else:
    path = '/home/mkittel/tsla-tde/mk/vreda/input/h2_sto_l.csv'
sto_l = pd.read_csv(path)
scenarios = {'island-e': 'island', 'TYNDP-e': 'TYNDP', 'copperplate-e': 'copperplate'}
sto_l = sto_l.replace(scenarios)
sto_l_cp = sto_l.query('custom_flow == "copperplate"')
sto_l_cp = sto_l_cp[['n', 'h', 'value', 'custom_year']]
sto_l_cp = sto_l_cp.groupby(['h', 'custom_year'])['value'].sum().reset_index()
sto_l = sto_l.query('n in @sel and custom_flow != "copperplate"')
sel.insert(0, 'CP')

# import drought mass results
print('Importing drought mass data.')
if os_plotting == 'local':
    path = 'T:\\mk\\vreda\\output\\2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete\\drought_mass_marginal_max'
else:
    path = '/home/mkittel/tsla-tde/mk/vreda/output/2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete/drought_mass_marginal_max'
items = os.listdir(path)
weighting_funcs = [item for item in items if os.path.isdir(os.path.join(path, item))]
mass_single_events_max_avg_mass_per_time_frame_all = {}
for weighting_func in weighting_funcs:
    path_import = os.path.join(path, weighting_func)
    file = 'mass_single_events_max_avg_mass_per_time_frame.gz'
    mass_single_events_max_avg_mass_per_time_frame = load_object(path_import, file)
    mass_single_events_max_avg_mass_per_time_frame = mass_single_events_max_avg_mass_per_time_frame.query('region in @sel')
    mass_single_events_max_avg_mass_per_time_frame_all[weighting_func] = mass_single_events_max_avg_mass_per_time_frame

# import drought occurrences
print('Importing drought occurrence data.')
if os_plotting == 'local':
    path = 'T:\\mk\\vreda\\output\\2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete'
else:
    path = '/home/mkittel/tsla-tde/mk/vreda/output/2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete/'
drought_occurence_portfolio = load_object(path, 'drought_occurence_portfolio_1.gz')

drought_occurence_portfolio = drought_occurence_portfolio.query('region in @sel')
drought_occurence_portfolio['drought_indicator'] = drought_occurence_portfolio['drought_indicator'].replace(0, np.nan)
drought_occurence_portfolio['threshold'] = drought_occurence_portfolio['threshold'].apply(lambda x: "{:.2f}".format(float(x.split('=')[1])))
excluded = ['1.00']
drought_occurence_portfolio = drought_occurence_portfolio.query('threshold not in @excluded')

# define years and colors
year_pairs = [[i, i+1] for i in range(1992, 1998)]

# customize colors
custom_colors_extended = [
    '#4a006e',  # Dark Violet
    '#611785',  # Between Dark Violet and Violet
    '#78209b',  # Violet
    #'#7f104c',  # Transition between Violet and Dark Red
    '#840826',  # New Color (Enhanced Violet to Dark Red Transition)
    '#880000',  # Dark Red
    '#a41100',  # Between Dark Red and Light Red
    '#c62200',  # Light Red
    '#e84013',  # Between Light Red and Dark Orange
    '#f95927',  # Dark Orange
    '#fc6f20',  # New Color (Enhanced Red to Orange Transition)
    '#ff851a',  # Between Dark Orange and Light Orange
    '#ffa72d',  # Light Orange
    '#ffcc2a',  # Between Light Orange and Yellow
    '#ffdf2d',  # Yellow
    '#d0d830',  # New Color (Enhanced Yellow to Green Transition)
    '#a0d034',  # Between Yellow and Light Green
    '#7cb342',  # Light Green
    '#05723c',  # Dark Green
    '#022915',  # New Color (Enhanced Light Green to Dark Green Transition)
]

for weighting_func, mass_single_events_max_avg_mass_per_time_frame in mass_single_events_max_avg_mass_per_time_frame_all.items():
    
    print(f'Plotting started for {weighting_func}.')

    for yy in year_pairs:
        #print(yy)
        
        print(f'Plotting started for {yy[0]}-{yy[1]}.')
    
        # sel = ['AT', 'BE', 'CH', 'CZ', 'DE', 'DK', 'EE', 'ES', 'IE', 'LT', 'LV', 'NO', 'PT']
        #sel = ['DE', 'ES', 'FR', 'IT','UK'] #, 'NL', 'PL', 'SE', 'RO', 'UK']
        sel_no = len(sel)
    
        drought_occurrence_sel = drought_occurence_portfolio.query('year in @yy')
    
        # get number of thresholds
        thresholds = sorted(list(drought_occurrence_sel.threshold.unique()))
    
        no_col = len(custom_colors_extended) - len(thresholds)
        custom_colors_extended_sel = custom_colors_extended[no_col:]
    
        row_heights = [
            # 0.04, 0.07,  # for 9 regions
            #0.04, 0.06,  # for 10 regions
            0.04, 0.07, 0.008,
            0.04, 0.07, 0.008,
            0.04, 0.07, 0.008,
            0.04, 0.07, 0.008,
            0.04, 0.07, 0.008,
            0.04, 0.07, 0.008,
            0.04, 0.07, 0.008,
            0.04, 0.07, 0.008,
            0.04, 0.07
            # 0.02, 0.03,  # for 20 regions
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            # 0.02, 0.03,
            ]
    
        row_no = len(row_heights)

        fig = make_subplots(rows=row_no, cols=1, row_heights=row_heights, shared_xaxes=True, vertical_spacing=0.01)
        # fig.print_grid()
    
        # plot drought ts
        regions = []
        row_counter = 1
    
        for region in sel:
    
            group_drought_region = drought_occurrence_sel.query('region == @region')
    
            regions.append(region)
            regions.append(region)
            regions.append(region)
    
            for idx, (threshold, group_drought_threshold) in enumerate(group_drought_region.groupby('threshold')):
    
                # with legend
                if row_counter == 1:
                    fig.add_trace(
                        go.Scatter(
                            x=group_drought_threshold.index,
                            y=group_drought_threshold.drought_indicator,
                            marker=dict(size=1.4, color=custom_colors_extended[idx]),
                            name=threshold,
                            mode='markers',
                            legend='legend',
                            #legendgrouptitle=dict(text='relative thresholds')
                        ),
                        row=row_counter, col=1,
                    )
    
                # without legend
                else:
                    fig.add_trace(
                        go.Scatter(
                            x=group_drought_threshold.index,
                            y=group_drought_threshold.drought_indicator,
                            marker=dict(size=1.4, color=custom_colors_extended[idx]),
                            name=threshold,
                            mode='markers',
                            showlegend=False
                        ),
                        row=row_counter, col=1,
                    )
            row_counter += 3
    
        # plot identified max period
        time_frame = f'{yy[0]}-{yy[1]}'
        row_counter = 1
    
        for region in sel:
    
            start = mass_single_events_max_avg_mass_per_time_frame.query(
                'region == @region and time_frame == @time_frame')['start'].iloc[0][0] - datetime.timedelta(hours=30)
            end = mass_single_events_max_avg_mass_per_time_frame.query(
                'region == @region and time_frame == @time_frame')['end'].iloc[0][0] + datetime.timedelta(hours=30)
    
    # =============================================================================
    #         start = event_max_mass_region_all_years.query(
    #             'region == @region and time_frame == @time_frame')['start'].iloc[0][0] - datetime.timedelta(hours=30)
    #         end = event_max_mass_region_all_years.query(
    #             'region == @region and time_frame == @time_frame')['end'].iloc[0][0] + datetime.timedelta(hours=30)
    # =============================================================================
    
            fig.add_vrect(x0=start, x1=end, row=row_counter, col=1,
                          # annotation_text="decline", annotation_position="top left",
                          fillcolor='gray', opacity=0.3, line_width=0)
    
            row_counter += 3
    
        # add storage SOC CP
        sto_l_sel = sto_l_cp.query('custom_year in [@yy[0]]')
        
        fig.add_trace(
            go.Scatter(
                x=group_drought_threshold.index,
                y=sto_l_sel.value,
                line=dict(color='darkred', width=2),
                mode='lines',
                name='copperplate',
                legend='legend',
                #legendgrouptitle=dict(text='storage level scenarios')
            ),
            row=2, col=1,
        )
    
        # add storage SOC countries
        scens = ['island', 'TYNDP']
        colors_scen = ['black', 'gray']
        
        sel.remove('CP')
        
        for scen_idx, scen in enumerate(scens):
            
            row_counter = 5
            
            sto_l_sel = sto_l.query('custom_year in [@yy[0]] and custom_flow == @scen')
        
            for idx, region in enumerate(sel):
        
                group_sto = sto_l_sel.query('n == @region')
        
                # group_sto['MA'] = group_sto['value'].rolling(window=ts_len).mean()
                # group_sto = group_sto.iloc[::ts_len]
        
                # with legend
                if idx == 0:
                    fig.add_trace(
                        go.Scatter(
                            x=group_drought_threshold.index,
                            y=group_sto.value,
                            line=dict(color=colors_scen[scen_idx], width=2),
                            mode='lines',
                            name=scen,
                            legend='legend',
                            #legendgrouptitle=dict(text='storage level scenarios')
                        ),
                        row=row_counter, col=1,
                    )
                    row_counter += 3
        
                # without legend
                else:
                    fig.add_trace(
                        go.Scatter(
                            x=group_drought_threshold.index,
                            y=group_sto.value,
                            line=dict(color=colors_scen[scen_idx], width=2),
                            mode='lines',
                            #name='',
                            showlegend=False
                        ),
                        row=row_counter, col=1,
                    )
                    row_counter += 3
    
        sel.insert(0, 'CP')
    
        # Update xaxis properties
        start_year = yy[0]
        end_year = yy[-1]
    
        for r in range(row_no):
            if r < row_no-1:
                fig.update_xaxes(
                    title_text='',
                    range=[datetime.datetime(start_year-1, 12, 31), datetime.datetime(end_year+1, 1, 1)],
                    mirror=True,
                    dtick="M1",
                    row=r+1, col=1)
    
        fig.update_xaxes(
            title_text='',
            range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
            mirror=True,
            row=row_no, col=1,
            dtick="M1",
            tickformat="%b<br>'%y",
            tickfont=dict(size=10)
            )
    
        # Update yaxis properties
        for r in range(1, row_no+1, 3):
            fig.update_yaxes(title=dict(text=regions[r], font=dict(size=20), standoff=35), range=[0, 0.95],
                             ticks='', showticklabels=False, mirror=True, row=r, col=1)
            
        fig.update_layout(yaxis2=dict(title=dict(text='TWh', font=dict(size=15)), mirror=True),
                          yaxis5=dict(title='TWh', mirror=True),
                          yaxis8=dict(title='TWh', mirror=True),
                          yaxis11=dict(title='TWh', mirror=True),
                          yaxis14=dict(title='TWh', mirror=True),
                          yaxis17=dict(title='TWh', mirror=True),
                          yaxis20=dict(title='TWh', mirror=True),
                          yaxis23=dict(title='TWh', mirror=True),
                          yaxis26=dict(title='TWh', mirror=True),
                          )
    
        fig.update_layout(template='simple_white',
                          margin=dict(l=0, r=0, t=0, b=30),
                          height=1200, width=1100
                          )
        
        fig.update_layout(
            legend=dict(
                title='Relative thresholds (dots) and storage level of modeled scenarios (lines):',
                font=dict(size=10),
                title_side='top',
                orientation='h',  # Horizontal orientation
                x=0.52,  # Center the legend horizontally
                y=-0.04,  # Move the legend further down
                xanchor='center',  # Anchor the legend's horizontal center to x position
                yanchor='top',  # Anchor the legend's top to the y position
                #itemsizing='constant',
                traceorder='normal',
                entrywidthmode='fraction',
                entrywidth=0.11
                ),
# =============================================================================
#             legend2=dict(
#                 title='Storage level of the modeled scenarios:',
#                 font=dict(size=10),
#                 title_side='top',
#                 orientation='h',  # Horizontal orientation
#                 x=0.1,  #14 Center the legend horizontally
#                 y=-0.1,  # Move the legend further down
#                 xanchor='center',  # Anchor the legend's horizontal center to x position
#                 yanchor='top',  # Anchor the legend's top to the y position
#                 #itemsizing='constant',
#                 traceorder='normal',
#                 entrywidthmode='fraction',
#                 #entrywidth=0.152
#                 )
# =============================================================================
            )    
    
        # fig.show()
        
        #print(f'Printing figure started for {yy[0]}-{yy[1]}.')
        
        if os_plotting == 'local':
            path = 'T:\\mk\\vreda\\result_plots\\'
        else:
            path = '/home/mkittel/tsla-tde/mk/vreda/result_plots/'
        path_output = os.path.join(path, 'drought_time_series_STO_L', 'year_pair_all_regions_sto_l',
                                   'margical_max', f'{weighting_func}')
        Path(path_output).mkdir(parents=True, exist_ok=True)
        name = f'drought_ts_sto_l_{start_year}_{end_year}'
        path_output = os.path.join(path_output, name)
        
        #fig.write_html(f'{path_output}.html')
        
# =============================================================================
#         print('Writing svg image.')
#         fig.write_html(f'{path_output}.svg', engine="kaleido")
#         
#         print('Writing pdf image.')
#         fig.write_image(f'{path_output}.pdf', engine="kaleido")
# =============================================================================
        
        print('Writing png image.')
        fig.write_image(f'{path_output}.png', scale=3)
        
        del fig
        # print(f'Png written.')
    
# =============================================================================
#     fig.update_layout(
#         xaxis_range=[datetime.datetime(start_year-1, 12, 30), datetime.datetime(end_year+1, 1, 1)],
#         yaxis_range=[0.05, 0.95],
#         template='simple_white',
#         margin=dict(l=0, r=0, t=30, b=0),
#         legend={'itemsizing': 'constant'},
#         height=1400, width=1240
#     )
#     fig.update_xaxes(dtick="M1", tickformat="%b%y")
#     fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))
#     fig.update_yaxes(visible=False, showticklabels=False)
#     fig.update_xaxes(title='')
# =============================================================================