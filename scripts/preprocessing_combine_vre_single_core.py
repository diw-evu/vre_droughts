# -*- coding: utf-8 -*-
"""
Created on Sun May 22 14:09:23 2022

@author: mkittel
"""

import pandas as pd
import os
import yaml
import time
from pathlib import Path
import ipyparallel as ipp


def get_config(path):
    # initial directory
    os.chdir(path)
    # import settings
    with open('config.yaml', 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    return config


def get_capacity_data(config):
    '''
    Reads capacity data from ENTSO-e MAF 2020 report. Returns dataframe(index=technology,
    columns=region codes).
    '''
    path_cap = os.path.join(config['paths']['input'], config['paths']['capacity'])
    cap = pd.read_excel(path_cap, sheet_name=config['paths']['capacity_sheet'], skiprows=1, index_col=0)
    cap = cap.loc[list(config['technologies']['combined'].values()), :]
    cap.loc['cap_total'] = cap.sum(axis=0)
    return cap


def combined_vre_availability(energy, region, cols):
    # combine to one df
    df = sum(list(energy.values())) / cap.loc['cap_total', region]
    df.insert(0, cols.columns[1], cols.Hour)
    df.insert(0, cols.columns[0], cols.Date)
    return df


def get_vre_availability(region):
    energy = {}
    for tech, tech_code in config['technologies']['combined'].items():
        path = os.path.join(config['paths']['input'], config['paths'][tech], f'{region}.csv')
        ts_wide = pd.read_csv(path)
        vre_energy = ts_wide.iloc[:, 2:] * cap.loc[tech_code, region]
        energy[tech] = vre_energy
    cols = ts_wide.iloc[:, :2]
    df = combined_vre_availability(energy, region, cols)
    return df


def export(region, df):
    path_new_folder = os.path.join(config['paths']['input'], config['paths']['combined'])
    Path(path_new_folder).mkdir(parents=True, exist_ok=True)
    path_file = os.path.join(path_new_folder, f'{region}.csv')
    df.to_csv(path_file, index=True, header=True)


# time the execution
start_time_total = time.time()

config = get_config('C:\\git\\vre_droughts\\scripts')
cap = get_capacity_data(config)


# get vre availabilty data
for r_name, r_code in config['regions'].items():
    df = get_vre_availability(r_code)
    export(r_code, df)

duration = time.time() - start_time_total
print(f"Duration computation: {round(duration, 2)} seconds")
