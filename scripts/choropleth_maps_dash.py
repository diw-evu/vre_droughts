# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 20:09:46 2023

@author: mkittel
"""

import json
from urllib.request import urlopen
import plotly.express as px
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
import pandas as pd
import os



# import data
path_data_eu = 'D:\\git\\vre_droughts\\output\\2023-09-12_13-44-28_mbt_event\\additional_plots\\threshold_maps'
path_data_eu = os.path.join(path_data_eu, 'data_eu_country_resolution.csv')
data_eu_country_resolution = pd.read_csv(path_data_eu)

path_data = 'D:\\git\\vre_droughts\\output\\2023-09-12_13-44-28_mbt_event\\additional_plots\\threshold_maps'
path_data = os.path.join(path_data, 'data_country_resolution.csv')
data= pd.read_csv(path_data)

# map option 2
url = 'https://gisco-services.ec.europa.eu/distribution/v2/nuts/geojson/NUTS_RG_60M_2021_4326_LEVL_0.geojson'

with urlopen(url) as response:
    ccaa = json.load(response)

# Create a Dash app
app = dash.Dash(__name__)

max_val = data['drought_qualification'].max()
min_val = data['drought_qualification'].min()

# define a custom color scale
custom_colors = [
    (0, 'rgb(255, 255, 255)'),   # White for 0: no droughts
    (0.1, 'rgb(60, 9, 17)'),       # Reddish and darkest end near 0
    (0.5, 'rgb(186, 74, 47)'),    # Intermediate darkest color 
    (1, 'rgb(231, 212, 207)')    # Brightest end of "Burgyl_r" for 1
]

# define a custom color bar tick text
tick_text = ['no drought', *[round(0.1*i,1) for i in range(1,10)]]

# generate figure with country resolution
fig_countries = px.choropleth_mapbox(
    data,
    geojson=ccaa,
    featureidkey='properties.CNTR_CODE',
    locations='region',
    color='drought_qualification',
    color_continuous_scale=custom_colors,#[(0, "rgb(255, 255, 255)"), (0.1, 'rgb(60, 9, 17)'), (0.5, "rgb(186, 74, 47)"), (1, "rgb(231, 212, 207)")],
    animation_frame="date_only",
    range_color=[min_val, max_val],
    height=950,
    width=850,
    #opacity=0.5,
    labels={'drought_qualification':'Minimum drought threshold'},
)

fig_countries.update_layout(
    mapbox_style="white-bg",
    mapbox_zoom=3,
    mapbox_center={"lat": 57.5, "lon": 10},
    #title_text=f'Maximum {tech} drought duration for thresholds based on the {fraction} fraction of the mean capacity factor',
    coloraxis_colorbar=dict(
        title="Minimum drought threshold",
        tickvals=[0.1*i for i in range(10)],
        ticktext=tick_text,
        lenmode="pixels",
        #len=200,
        )
)
fig_countries.update_geos(visible=False, resolution=50) # , projection_type="equirectangular", fitbounds="locations",

# increase animation speed
fig_countries.layout.updatemenus[0].buttons[0].args[1]['frame']['duration'] = 100
fig_countries.layout.updatemenus[0].buttons[0].args[1]['transition']['duration'] = 50

# generate figure with EU resolution
fig_eu = px.choropleth_mapbox(
    data_eu_country_resolution,
    geojson=ccaa,
    featureidkey='properties.CNTR_CODE',
    locations='region',
    color='drought_qualification',
    color_continuous_scale=custom_colors,#[(0, "rgb(255, 255, 255)"), (0.1, 'rgb(60, 9, 17)'), (0.5, "rgb(186, 74, 47)"), (1, "rgb(231, 212, 207)")],
    animation_frame="date_only",
    range_color=[min_val, max_val],
    height=950,
    width=850,
    #opacity=0.5,
    labels={'drought_qualification':'Minimum drought threshold'},
)

fig_eu.update_layout(
    mapbox_style="white-bg",
    mapbox_zoom=3,
    mapbox_center={"lat": 57.5, "lon": 10},
    #title_text=f'Maximum {tech} drought duration for thresholds based on the {fraction} fraction of the mean capacity factor',
    coloraxis_colorbar=dict(
        title="Minimum drought threshold",
        tickvals=[0.1*i for i in range(10)],
        ticktext=tick_text,
        lenmode="pixels",
        #len=200,
        )
)
fig_eu.update_geos(visible=False, resolution=50) # , projection_type="equirectangular", fitbounds="locations",

# increase animation speed
fig_eu.layout.updatemenus[0].buttons[0].args[1]['frame']['duration'] = 100
fig_eu.layout.updatemenus[0].buttons[0].args[1]['transition']['duration'] = 50

# Create a Dash app
app = dash.Dash(__name__)

# Define Dash layout
app.layout = html.Div([
    dcc.Graph(id='fig_countries', figure=fig_countries, style={'display': 'inline-block', 'width': '50%'}),
    dcc.Graph(id='fig_eu', figure=fig_eu, style={'display': 'inline-block', 'width': '50%'}),
    html.Button("Play Animation", id="play-button", n_clicks=0),
])

# Define callback to trigger animation play
@app.callback(
    [Output("fig_countries", "relayoutData"), Output("fig_eu", "relayoutData")],
    Input("play-button", "n_clicks"),
    State("fig_countries", "relayoutData"),
    State("fig_eu", "relayoutData"),
)
def play_animation(n_clicks, relayoutData_countries, relayoutData_eu):
    if n_clicks % 2 == 1:  # Toggle play on odd clicks
        # Update the relayoutData to trigger animation play
        relayoutData_countries["mapbox.play"] = 1
        relayoutData_eu["mapbox.play"] = 1
    else:
        # Pause the animation on even clicks
        relayoutData_countries["mapbox.pause"] = 1
        relayoutData_eu["mapbox.pause"] = 1

    return relayoutData_countries, relayoutData_eu

if __name__ == '__main__':
    app.run_server(debug=True)

