# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 17:35:15 2023

@author: mkittel
"""

import pandas as pd
import os
import numpy as np
from functools import reduce

# import parquet data
path = 'D:/git/vre_droughts_power_sector/data/eraa2021'
pv = pd.read_parquet(os.path.join(path, 'PECD-2021.3-country-LFSolarPV-2030.parquet'))
won = pd.read_parquet(os.path.join(path, 'PECD-2021.3-country-Onshore-2030.parquet'))
woff = pd.read_parquet(os.path.join(path, 'PECD-2021.3-country-Offshore-2030.parquet'))

# convert to wide
pv_c = {c: {} for c in pv.country.unique()}
years = [np.int_(y) for y in pv.year.unique()]
for (c, y), group in pv.groupby(['country', 'year']):
    pv_c[c][np.int_(y)] = group['cf'].reset_index(drop=True)
    date = group['day'].astype(int).astype(str).str.zfill(2) + \
        '.' + group['month'].astype(int).astype(str).str.zfill(2) + '.'
date.reset_index(drop=True, inplace=True)

won_c = {c: {} for c in won.country.unique()}
for (c, y), group in won.groupby(['country', 'year']):
    won_c[c][np.int_(y)] = group['cf'].reset_index(drop=True)

woff_c = {c: {} for c in woff.country.unique()}
for (c, y), group in woff.groupby(['country', 'year']):
    woff_c[c][np.int_(y)] = group['cf'].reset_index(drop=True)

# merge early time series to one df
for c, data in pv_c.items():
    df = pd.DataFrame(list(data.values()), index=list(data.keys())).T.reset_index(names='hour')
    df.hour += 1
    df['date'] = date
    order = ['date', 'hour'] + years
    df = df.loc[:, order]
    pv_c[c] = df

for c, data in won_c.items():
    df = pd.DataFrame(list(data.values()), index=list(data.keys())).T.reset_index(names='hour')
    df.hour += 1
    df['date'] = date
    order = ['date', 'hour'] + years
    df = df.loc[:, order]
    won_c[c] = df

for c, data in woff_c.items():
    df = pd.DataFrame(list(data.values()), index=list(data.keys())).T.reset_index(names='hour')
    df.hour += 1
    df['date'] = date
    order = ['date', 'hour'] + years
    df = df.loc[:, order]
    woff_c[c] = df

# export to csv
path_pv = 'D:/git/vre_droughts/input/PECD_2021.3_pv'
for c, data in pv_c.items():
    data.to_csv(os.path.join(path_pv, f'{c}.csv'), index=False)

path_won = 'D:/git/vre_droughts/input/PECD_2021.3_onshore'
for c, data in won_c.items():
    data.to_csv(os.path.join(path_won, f'{c}.csv'), index=False)

path_woff = 'D:/git/vre_droughts/input/PECD_2021.3_offshore'
for c, data in woff_c.items():
    data.to_csv(os.path.join(path_woff, f'{c}.csv'), index=False)