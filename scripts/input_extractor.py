# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 22:59:47 2023

@author: mkittel
"""
# TODO: check which import are needed
import os
import pandas as pd

class InputExtractor():

    # TODO: transform to data class: https://realpython.com/python-data-classes/

    def __init__(self, config, years=None):
        self.years = years
        self.years_complete = None
        self.leap_years = []
        self.ts_long = None

    def get_timeseries_data(self, path, region, skip_rows=None):
        '''
        Reads time series input data from csv files. Input data has to be in wide format
        in the following structure:
            Column 1: 'date' in format '%d.%m.'
            Column 2: 'hour' as integer -> TODO: change to time step
            Column 3 and following: year as integer and capacity factor data as integer
        If leap days are considered, leave cells in rows of corresponding non-leap-years
        empty. Selects relevant years according to user's year selection. If no years
        are pre-selected, all years extracted. If first year of the original dataset is
        selected, then the last year of the original dataset is also extracted.
        Data is returned in long format. Leap days are kept in leap years and erased
        in non-leap-years.
        '''
        # read data
        # ts_wide = pd.read_excel(path, sheet_name=sheet_name, skiprows=skip_rows)
        path_csv = os.path.join(path, f'{region}.csv')
        ts_wide = pd.read_csv(path_csv, skiprows=skip_rows)

        # complete range of years
        years_complete = ts_wide.drop(columns=['date', 'hour']).columns.tolist()

        # check sanity of years_complete
        if not all(isinstance(x, int) for x in years_complete):

            # convert to int
            years_complete = [int(float(i)) for i in years_complete]
            if not all(isinstance(x, int) for x in years_complete):
                raise InputError(
                    f"Expected list of integers in column headers that represent years."
                    f"Data provided: {years_complete}."
                    )

            # reassign correct headers
            headers = ['date', 'hour'] + years_complete
            ts_wide.columns = headers

        # drop unnecessary years if user enters selection of years
        if self.years:

            # Option 1: first year selected, include also data from last year
            if self.years[0] == years_complete[0]:
                years_selection = self.years.copy()
                years_selection.append(years_complete[-1])

            # Option 2: first year not selected, include also previous year
            else:
                years_selection = [self.years[0]-1]
                years_selection.extend(self.years)

            years_selection.append('date')
            years_selection.append('hour')

            # reduce data to only relevant years
            ts_wide.drop(ts_wide.columns.difference(years_selection), axis=1, inplace=True)

        # if no user-specified years, then assign years based on input data set
        elif not self.years:
            self.years = years_complete

        # check for leap years and other nan
        ts_wide_nan = ts_wide[ts_wide.isna().any(axis=1)]
        nan_dates = ts_wide_nan['date'].unique()

        # identify leap years
        if (len(nan_dates) == 1 and nan_dates[0] == '29.02.'):

            no_leap_years = [i for i in ts_wide_nan.columns if ts_wide_nan[i].isnull().any()]
            no_leap_years_set = set(no_leap_years)
            self.leap_years = [x for x in self.years if x not in no_leap_years_set]

        # if there nan other than during leap days
        elif len(nan_dates) > 1:
            raise InputError(
                f"There are NaNs in csv {region} during . Please insert complete data."
                )

        # transform data in long format
        ts_long = pd.melt(
            ts_wide,
            id_vars=['date', 'hour'],
            var_name='year',
            value_name='capacity_factor'
            )

        # drop leap day rows in non-leap-years
        ts_long = ts_long.dropna()

        self.ts_long = ts_long
        self.years_complete = years_complete

    def generate_combined_vre_availability():
        # TODO
        pass