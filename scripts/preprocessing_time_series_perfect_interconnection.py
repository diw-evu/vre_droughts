# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 17:20:26 2023

@author: mkittel
"""

import pandas as pd
import os
import yaml
from pathlib import Path
from functools import reduce

# initial directory
path = 'T:\\mk\\vreda\\config'
os.chdir(path)
with open('config.yaml', 'r') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

# capacity
path_cap = os.path.join(config['paths']['input'], config['paths']['capacity'])
cap = pd.read_csv(path_cap, index_col=0)
cap = cap.loc[list(config['technologies_portfolio'].values())]
cap = cap.loc[:, list(config['regions'])]
cap.loc['portfolio'] = cap.sum(axis=0)

# weights
tech_sum = cap.sum(axis=1)
weights = cap.div(tech_sum, axis=0)

# copperplate availability factors for each tech
path_vre = config['paths']['input']
energy = {}
for tech, tech_code in config['technologies'].items():
    energy[tech] = {}

    for r in list(config['regions']):

        # get data only if technology applies to region
        if tech in config['excluded'] and r in config['excluded'][tech]:
            continue

        path_vre_data = os.path.join(path_vre, config['paths'][tech], f'{r}.csv')
        ts_wide = pd.read_csv(path_vre_data)

        # weight capacity factors
        vre_energy = ts_wide.iloc[:, 2:] * weights.loc[tech_code, r]
        energy[tech][r] = vre_energy

    # sum over all regions
    df = reduce(lambda a, b: a.add(b, fill_value=0), list(energy[tech].values()))

    # add first two columns
    cols = ts_wide.iloc[:, :2]
    df.insert(0, cols.columns[1], cols.hour)
    df.insert(0, cols.columns[0], cols.date)

    # export
    path_export = os.path.join(path_vre, config['paths'][tech], 'CP.csv')
    df.to_csv(path_export, index=False)
