# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 12:43:16 2021

@author: mkittel
"""
import os
import yaml
import time
import multiprocess as mp
import psutil
import gzip
import pickle

# import own modules
from config_check import ConfigCheck
from analyzer import Analyzer
from region_analyzer import RegionAnalyzer

# TODO:
# correlation analysis: https://doi.org/10.1016/j.solener.2019.11.087
# instead of computing each year separately, do everything once for all years!
# use a number of data sets representing many possible weather realisations
# - inputs as subclassed from immutable Built-in types to check whether there are hourly or daily values
#   - https://realpython.com/lessons/returning-instances-of-different-class/
# - Analyzer class as Singleton class
#   - https://realpython.com/lessons/allowing-only-single-instance-in-class/
# - cleaner code with namedtuples
#   - https://realpython.com/python-namedtuple/
# - convert code to package to ensure all scripts are at the right spot -> install it to current environment
# - proper logging
#   - https://realpython.com/lessons/logging-python-introduction/
# better writing of code (10min): https://www.youtube.com/watch?v=C-gEQdGVXbk
# multiprocessing intro: https://www.youtube.com/watch?v=fKl2JW_qrso, https://zetcode.com/python/multiprocessing/


def parallelize(function, values, technology):
    '''
    The unbound function is mapped to all components of values. The mapping pairs
    are automatically parallelized.
    '''
    # print id of parent process
    print(f'------------------------------------------------\n'
          f'Parent process {os.getpid()} initializes parallelization for technology {technology}.\n'
          f'------------------------------------------------\n')

    # number of physical cores
    no_cpu = psutil.cpu_count(logical=True)

    # iniatialize parllelization
    with mp.Pool(no_cpu) as pool:
        results = pool.starmap(function, values)
    return results

# %% main


if __name__ == "__main__":

    # time the execution
    start_time_total = time.perf_counter()

    print("##################################################")
    print("VREDA run started")
    print("##################################################")

    # config directory
    os.chdir('../config')

    # import settings
    with open('config.yaml', 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    os.chdir('..')
    cwd = os.getcwd()

    # config sanity check
    ic = ConfigCheck(config)

    # instantiate analyzer object
    a = Analyzer(config, cwd)

    # run analysis for each technology
    for technology in config['technologies']:

        # results container
        region_results = {}

        # generate mutiprocessing args
        values = tuple([(a.path_run, a.path_project, config, technology, region)
                        for region in config['regions']
                        if not (technology in config['excluded'] and region in config['excluded'][technology])])

        # parallelized region analysis
        region_results = parallelize(RegionAnalyzer.parallelly_analyze_region, values, technology)

        # analysis for all regions
        a.analyze_all_regions(technology, region_results)

        # pickle and remove RegionAnalyzer instances
        cwd = os.getcwd()
        os.chdir(os.path.join(a.path_run, technology))
        file_name = f'region_results_{technology}.gz'
        with gzip.open(file_name, 'wb') as file:
            pickle.dump(region_results, file, protocol=pickle.HIGHEST_PROTOCOL)
        del region_results

    # pickle and remove tech-ordered drought time series
    os.chdir(a.path_run)
    file_name = 'drought_ts_tech.gz'
    with gzip.open(file_name, 'wb') as file:
        pickle.dump(a.drought_ts_tech, file, protocol=pickle.HIGHEST_PROTOCOL)
    del a.drought_ts_tech
    os.chdir(cwd)

    # plot correlation matrix of droughts (one technology, all regions)
    if len(config['regions']) > 1:
        print('a.parallel_plot_corr_matrix_tech started')
        a.parallel_plot_corr_matrix_tech()
        print('a.parallel_plot_corr_matrix_tech ended')

    # correlation matrix of droughts (one region, all technologies)
    if len(config['technologies']) > 1:
        print('a.corr_matrix_pearson_region started')
        a.corr_matrix_pearson_region()
        print('a.corr_matrix_pearson_region ended')

    # merge and export mean cf
    a.export_mean_capacity_factor()

    # export & plot thresholds
    if config['drought_thresholds']['fractions_mean_capacity_factor']:

        min_thres, max_thres = a.export_drought_thresholds()

        if config['plots']['mean_capacity_factors']:
            a.plotter.plot_drought_thresholds(a.drought_thresholds_all, min_thres, max_thres, a.path_run)

    # pickle results
    a.save_analyzer_object()

    # std output for log
    print(f"Computation for {technology} finished.")

    # display execution time total
    duration = time.perf_counter() - start_time_total
    print("##################################################")
    print(f"Duration in total: {round(duration/60, 2)} minutes")
    print("##################################################")
