# -*- coding: utf-8 -*-
"""
Created on Mon Jul 22 18:08:15 2024

@author: mkittel
"""

import plotly.express as px
import os
from pathlib import Path

# Sample figure for demonstration
fig = px.scatter(x=[1, 2, 3], y=[4, 5, 6])

# Path to save the image
#path = 'T:\\mk\\vreda\\result_plots\\'
path = '/home/mkittel/tsla-tde/mk/vreda/result_plots/'
path_output = os.path.join(path, 'drought_time_series_STO_L', 'year_pair_all_regions_sto_l', 'drought_mass_weighting_func2_1.5')
Path(path_output).mkdir(parents=True, exist_ok=True)
name = 'test.png'
path_output = os.path.join(path_output, name)

try:
    fig.write_image(path_output, scale=3)
    print("Image saved successfully.")
except Exception as e:
    print(f"An error occurred: {e}")
