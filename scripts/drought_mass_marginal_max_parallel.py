# -*- coding: utf-8 -*-
"""
Created on Fri Jun 28 12:03:19 2024

@author: mkittel
"""

import os
import multiprocess as mp
import pandas as pd
import numpy as np
import pickle
import psutil
import gzip
from pathlib import Path

#from pympler import asizeof
#import yaml
#from functools import reduce
#from urllib.request import urlopen
#import json
#import datetime
#import math

# import own modules
#from analyzer import Analyzer
#from region_analyzer import RegionAnalyzer
#from input_extractor import InputExtractor
#from drought_counter import DroughtCounter

#temp = pathlib.PosixPath
#pathlib.PosixPath = pathlib.WindowsPath

def load_object(path, file):
    '''
    For debugging: Loads object from pickle.
    '''
    cwd = os.getcwd()
    os.chdir(path)
    with gzip.open(file, 'rb') as file:
        obj = pickle.load(file)
    os.chdir(cwd)
    return obj


def save_object(path, obj, name):
    '''
    For debugging: Stores objects as pickle in path directory.
    '''
    cwd = os.getcwd()
    os.chdir(path)
    file_name = f"{name}.gz"
    with gzip.open(file_name, 'wb') as file:
        pickle.dump(obj, file, protocol=pickle.HIGHEST_PROTOCOL)
    os.chdir(cwd)


def parallelize(function, values):
    '''
    The unbound function is mapped to all components of values. The mapping pairs
    are automatically parallelized.
    '''
    # print id of parent process
    print(f'--------------------------------------------------------------------------------------------------\n'
          f'Parent process {os.getpid()} initializes parallelization.\n'
          f'--------------------------------------------------------------------------------------------------\n')

    # number of physical cores
    no_cpu = psutil.cpu_count(logical=True)  # False)

    # iniatialize parllelization
    with mp.Pool(no_cpu) as pool:  # no_cpu
        results = pool.starmap(function, values)
    return results


def weighting_func(x, a):
    '''
    Creates a weight for a discrete threshold.

    Parameters
    ----------
    x : float
        Threshold.
    a : int
        Factor for weighting function.

    Returns
    -------
    float
        Weighting factor.

    '''
    return 1/(x**(a))  # a * x**(-a)  # - a*0.9**(-a)


def weighting_func_doubling(thresholds, largest_weight):
    """
    Assign weights to floats in ascending order. Each float gets half the weight of the next larger float.
    
    :param floats: A list of floats.
    :param largest_weight: The weight for the largest float.
    :return: A dictionary with floats as keys and their weights as values.
    """
    # Sort the list of floats in ascending order
    sorted_thresholds = sorted(thresholds)
    
    # Initialize an empty dictionary to hold the float-weight pairs
    weights = {}
    
    # Calculate and assign weights starting from the largest
    current_weight = largest_weight
    for float_number in reversed(sorted_thresholds):
        weights[round(float_number, 2)] = current_weight
        current_weight *= 2  # Halve the weight for the next smaller float
    
    return weights


def get_x_weighting_func(y, a):
    '''
    Retrieves the corresponding x value of the weighting_func given a parameter a.

    Parameters
    ----------
    y : float
        Function value of weighting_func.
    a : int or float
        Parameter of weighting_func.

    Returns
    -------
    float
        Corresponding x value of y.

    '''
    return (a / y)**(1/a)  # (a / (y + a * 0.9**(-a)))**(1/a)


def compute_drought_mass(
        region,
        drought_occurence,
        year_pairs,
        weights,
        exclude_thresholds_drought_mass
        ):
    '''
    Parameters
    ----------
    drought_occurence : TYPE
        DESCRIPTION.
    weights : TYPE
        DESCRIPTION.
    exclude_thresholds_drought_mass : TYPE
        DESCRIPTION.

    Returns
    -------
    mass_all_events : TYPE
        DESCRIPTION.
    mass_all_events_max_total_mass : TYPE
        DESCRIPTION.
    mass_all_events_max_avg_mass : TYPE
        DESCRIPTION.
    mass_single_events : TYPE
        DESCRIPTION.
    mass_single_events_max_total_mass : TYPE
        DESCRIPTION.
    mass_single_events_max_avg_mass : TYPE
        DESCRIPTION.
    mass_single_events_max_avg_mass_per_time_frame : TYPE
        DESCRIPTION.
    '''
    
    # print process id
    print(f'--------------------------------------------------------------------\n'
          f'Process {os.getpid()} runs for region {region}.\n'
          f'--------------------------------------------------------------------\n')
    
    mass_all_events = {}
    mass_all_events_max_total_mass = {}
    mass_all_events_max_avg_mass = {}
    mass_single_events = {}
    mass_single_events_max_total_mass = {}
    mass_single_events_max_total_mass_per_time_frame = {}
    mass_single_events_max_avg_mass = {}
    mass_single_events_max_avg_mass_per_time_frame = {}

    # iterate over year pairs
    for yy in year_pairs:

        df = drought_occurence.query('year in @yy').copy()

        # prepare data in wide format
        df = df.reset_index()
        df['threshold'] = df['threshold'].apply(lambda x: float(x.split('=')[1]))

        df = df.pivot(index=['date', 'hour', 'year'], columns=['threshold'], values='drought_indicator')
        df = df.replace(weights)

        # drop threshold columns that are excluded from drought mass
        df = df.drop(exclude_thresholds_drought_mass, axis=1)

        # slice df into single dfs, each includes one more threshold
        thres_dfs = {}
        for idx, thres in enumerate(df):
            df_thres = df.iloc[:, :idx+1].copy()
            thres_dfs[thres] = df_thres
            
        # metric 1: drought mass across all hours of the corresponding time frame (here: two years)
        mass_total = {}
        mass_avg_all_hours = {}
        mass_avg_all_hours_thres = {}
        # mass_num_drought_hours = {}
        mass_avg_drought_hours = {}
        mass_avg_drought_hours_thres = {}
        
        for thres, df_thres in thres_dfs.items():
            
            if df_thres.sum().sum() == 0:
                continue
            
            # total drought mass
            mass_total[thres] = df_thres.sum().sum()
            
            # average drought mass for each hour
            mass_avg_all_hours[thres] = mass_total[thres] / df_thres.shape[0]

            # average threshold across all time steps
            mass_avg_all_hours_thres[thres] = get_x_weighting_func(mass_avg_all_hours[thres], 4)
            
            # average drought mass for each hour with drought
            mass_avg_drought_hours[thres] = mass_total[thres] / (df_thres.ne(0).any(axis=1)).sum()   
            
            # average threshold across time steps that qualify as drought
            mass_avg_drought_hours_thres[thres] = get_x_weighting_func(mass_avg_drought_hours[thres], 4)

        # dict to df
        mass_total = pd.DataFrame.from_dict(mass_total, orient='index', columns=['mass_total'])
        mass_avg_all_hours = pd.DataFrame.from_dict(mass_avg_all_hours, orient='index', columns=['mass_avg_all_hours'])
        mass_avg_all_hours_thres = pd.DataFrame.from_dict(mass_avg_all_hours_thres, orient='index', columns=['mass_avg_all_hours_thres'])
        mass_avg_drought_hours = pd.DataFrame.from_dict(mass_avg_drought_hours, orient='index', columns=['mass_avg_drought_hours'])
        mass_avg_drought_hours_thres = pd.DataFrame.from_dict(mass_avg_drought_hours_thres, orient='index', columns=['mass_avg_drought_hours_thres'])
        
        # merge to one df
        mass_all_events_r_yy = pd.concat([mass_total, mass_avg_all_hours, mass_avg_all_hours_thres, mass_avg_drought_hours, mass_avg_drought_hours_thres], axis=1)
        mass_all_events_r_yy['region'] = region
        mass_all_events_r_yy['time_frame'] = f'{yy[0]}-{yy[1]}'
        
        # get threshold specific results with highest total drought mass
        _mass_total = mass_all_events_r_yy.loc[mass_all_events_r_yy['mass_total'].idxmax()]
        _thres = pd.Series([_mass_total.name], ['threshold'])
        mass_all_events_max_total_mass[f'{yy[0]}-{yy[1]}'] = pd.concat([_mass_total, _thres])
        
        # get threshold specific results with highest average drought mass
        _mass_avg = mass_all_events_r_yy.loc[mass_all_events_r_yy['mass_avg_drought_hours'].idxmax()]
        _thres = pd.Series([_mass_avg.name], ['threshold'])
        mass_all_events_max_avg_mass[f'{yy[0]}-{yy[1]}'] = pd.concat([_mass_avg, _thres])
        
        # save results across all thresholds
        mass_all_events[f'{yy[0]}-{yy[1]}'] = mass_all_events_r_yy.reset_index(names=['threshold'])

        # metric 2: drought mass of single events for each time frame
        mass_single_events[f'{yy[0]}-{yy[1]}'] = {}
        mass_single_events_max_total_mass[f'{yy[0]}-{yy[1]}'] = {}
        mass_single_events_max_total_mass_per_time_frame[f'{yy[0]}-{yy[1]}'] = {}
        mass_single_events_max_avg_mass[f'{yy[0]}-{yy[1]}'] = {}
        mass_single_events_max_avg_mass_per_time_frame[f'{yy[0]}-{yy[1]}'] = {}
        
        for thres, df_thres in thres_dfs.items():
            
            # compute drought mass of each hour
            df_thres['mass_hour'] = df_thres.sum(axis=1)
            
            if df_thres['mass_hour'].sum() == 0:
                continue
            
            # find drought hours
            event_mask = (df_thres['mass_hour'] > 0).astype(int)

            # find single events and their drought mass
            events = {}
    
            # group data in droughts and no droughts, compute cumulative drought mass
            for idx, event in event_mask.rename('event_indicator').astype(int).groupby(event_mask.diff().ne(0).cumsum()):
                event = pd.merge(event, df_thres['mass_hour'], on=['date', 'hour', 'year'], how='left')
                event = event['mass_hour'].cumsum().rename('cumulative_mass')
                events[idx] = {'mass': event[-1], 'duration': event.shape[0], 'avg_mass': event[-1] /event.shape[0],
                                    'start': event.index[0], 'end': event.index[-1], 'region': region,
                                    'threshold': thres, 'time_frame': f'{yy[0]}-{yy[1]}'}
            events = pd.DataFrame.from_dict(events, orient='index')
            events = events[events['mass'] != 0]

            # save all single events
            mass_single_events[f'{yy[0]}-{yy[1]}'][thres] = events

            # retrieve event with max total drought mass
            mass_single_events_max_total_mass[f'{yy[0]}-{yy[1]}'][thres] = \
                events.loc[events['mass'].idxmax()].rename(region)
            
            # retrieve event with max average drought mass
            mass_single_events_max_avg_mass[f'{yy[0]}-{yy[1]}'][thres] = \
                events.loc[events['avg_mass'].idxmax()].rename(region)

        # concat threshold specific dfs 
        mass_single_events[f'{yy[0]}-{yy[1]}'] = pd.concat(mass_single_events[f'{yy[0]}-{yy[1]}'].values())
        mass_single_events[f'{yy[0]}-{yy[1]}'].reset_index(drop=True, inplace=True)
        
        # create dfs from series in dict
        mass_single_events_max_total_mass[f'{yy[0]}-{yy[1]}'] = \
            pd.DataFrame.from_dict(mass_single_events_max_total_mass[f'{yy[0]}-{yy[1]}'], orient='index')
        mass_single_events_max_total_mass[f'{yy[0]}-{yy[1]}'].reset_index(drop=True, inplace=True)
        
        mass_single_events_max_avg_mass[f'{yy[0]}-{yy[1]}'] = \
            pd.DataFrame.from_dict(mass_single_events_max_avg_mass[f'{yy[0]}-{yy[1]}'], orient='index')
        mass_single_events_max_avg_mass[f'{yy[0]}-{yy[1]}'].reset_index(drop=True, inplace=True)
        
        # select event with highest drought mass and highest drought average mass
        _mass_total = mass_single_events_max_total_mass[f'{yy[0]}-{yy[1]}']
        mass_single_events_max_total_mass_per_time_frame[f'{yy[0]}-{yy[1]}'] = _mass_total.loc[_mass_total['mass'].idxmax()]
        
        _mass_avg = mass_single_events_max_avg_mass[f'{yy[0]}-{yy[1]}']
        mass_single_events_max_avg_mass_per_time_frame[f'{yy[0]}-{yy[1]}'] = _mass_avg.loc[_mass_avg['avg_mass'].idxmax()]

    # concat year specific dfs across all events
    mass_all_events = pd.concat(mass_all_events.values()).reset_index(drop=True)
    mass_all_events_max_total_mass = pd.DataFrame.from_dict(mass_all_events_max_total_mass, orient='index').reset_index(drop=True) 
    mass_all_events_max_avg_mass = pd.DataFrame.from_dict(mass_all_events_max_avg_mass, orient='index').reset_index(drop=True) 
    
    # concat year specific dfs across single events
    mass_single_events = pd.concat(mass_single_events.values()).reset_index(drop=True)
    mass_single_events_max_total_mass = pd.concat(mass_single_events_max_total_mass.values()).reset_index(drop=True)
    mass_single_events_max_total_mass_per_time_frame = pd.DataFrame.from_dict(mass_single_events_max_total_mass_per_time_frame, orient='index').reset_index(drop=True)
    mass_single_events_max_avg_mass = pd.concat(mass_single_events_max_avg_mass.values()).reset_index(drop=True)
    mass_single_events_max_avg_mass_per_time_frame = pd.DataFrame.from_dict(mass_single_events_max_avg_mass_per_time_frame, orient='index').reset_index(drop=True)
    
    return dict(region=region,
                mass_all_events=mass_all_events,
                mass_all_events_max_total_mass=mass_all_events_max_total_mass,
                mass_all_events_max_avg_mass=mass_all_events_max_avg_mass,
                mass_single_events=mass_single_events,
                mass_single_events_max_total_mass=mass_single_events_max_total_mass,
                mass_single_events_max_total_mass_per_time_frame=mass_single_events_max_total_mass_per_time_frame,
                mass_single_events_max_avg_mass=mass_single_events_max_avg_mass,
                mass_single_events_max_avg_mass_per_time_frame=mass_single_events_max_avg_mass_per_time_frame)

#%%
        
# Main entry point
if __name__ == '__main__':
    
    # import drought occurrences
    print(f'Importing drought occurrence data.')
    path_results = '/home/mkittel/tsla-tde/mk/vreda/output/2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete'
    #path_results = 'T:\\mk\\vreda\\output\\2024-07-26_19-32-18_mbt_event_all_ts_filter_1_incomplete'
    drought_occurence_portfolio = load_object(path_results, 'drought_occurence_portfolio_1.gz')

    year_pairs = [[i, i+1] for i in range(1982, 2019)]

    # import weights
    print(f'Importing weighting data.')
    path_weights = '/home/mkittel/tsla-tde/mk/vreda/input/weights_drought_mass.xlsx'
    #path_weights = "T:\\mk\\vreda\\input\\weights_drought_mass.xlsx"
    weights_all = pd.read_excel(path_weights, index_col='Threshold', skiprows=1)

    # select which thresholds are excluded from drought mass
    exclude_thresholds_drought_mass = [1.0]

    for weighting_function in weights_all.columns:
        
        print(f'Parallel computation for {weighting_function} started.')
        
        weights_run = weights_all[weighting_function].to_dict()

        # generate mutiprocessing args
        values = tuple([(region, drought_occurence, year_pairs, weights_run, exclude_thresholds_drought_mass) for region, drought_occurence in drought_occurence_portfolio.groupby('region')])
    
        # parallelized region analysis
        try:
            drought_mass_region_results = parallelize(compute_drought_mass, values)
        except NameError as e:
            print(f"Error: {e}")
        
        # restructure in dictionary
        drought_mass = {}
        for result_dict in drought_mass_region_results:
            region = result_dict['region']
            del result_dict['region']
            drought_mass[region] = result_dict
        del drought_mass_region_results
        
        # result container
        mass_all_events = {}
        mass_all_events_max_total_mass = {}
        mass_all_events_max_avg_mass = {}
        mass_single_events = {}
        mass_single_events_max_total_mass = {}
        mass_single_events_max_total_mass_per_time_frame = {}
        mass_single_events_max_avg_mass = {}
        mass_single_events_max_avg_mass_per_time_frame = {}
        
        # reorganize results in dictionaries
        for region, regional_results in drought_mass.items():
            mass_all_events[region] = regional_results['mass_all_events']
            mass_all_events_max_total_mass[region] = regional_results['mass_all_events_max_total_mass']
            mass_all_events_max_avg_mass[region] = regional_results['mass_all_events_max_avg_mass']
            mass_single_events[region] = regional_results['mass_single_events']
            mass_single_events_max_total_mass[region] = regional_results['mass_single_events_max_total_mass']
            mass_single_events_max_total_mass_per_time_frame[region] = regional_results['mass_single_events_max_total_mass_per_time_frame']
            mass_single_events_max_avg_mass[region] = regional_results['mass_single_events_max_avg_mass']
            mass_single_events_max_avg_mass_per_time_frame[region] = regional_results['mass_single_events_max_avg_mass_per_time_frame']
            
        # concat regional dfs
        mass_all_events = pd.concat(mass_all_events.values()).reset_index(drop=True)
        mass_all_events_max_total_mass = pd.concat(mass_all_events_max_total_mass.values()).reset_index(drop=True)
        mass_all_events_max_avg_mass = pd.concat(mass_all_events_max_avg_mass.values()).reset_index(drop=True)
        mass_single_events = pd.concat(mass_single_events.values()).reset_index(drop=True)
        mass_single_events_max_total_mass = pd.concat(mass_single_events_max_total_mass.values()).reset_index(drop=True)
        mass_single_events_max_total_mass_per_time_frame = pd.concat(mass_single_events_max_total_mass_per_time_frame.values()).reset_index(drop=True)
        mass_single_events_max_avg_mass = pd.concat(mass_single_events_max_avg_mass.values()).reset_index(drop=True)
        mass_single_events_max_avg_mass_per_time_frame = pd.concat(mass_single_events_max_avg_mass_per_time_frame.values()).reset_index(drop=True)
        
        # pickle results
        path_output = os.path.join(path_results, 'drought_mass_marginal_max', f'{weighting_function}')
        Path(path_output).mkdir(parents=True, exist_ok=True)
        save_object(path_output, mass_all_events, 'mass_all_events')
        save_object(path_output, mass_all_events_max_total_mass, 'mass_all_events_max_total_mass')
        save_object(path_output, mass_all_events_max_avg_mass, 'mass_all_events_max_avg_mass')
        save_object(path_output, mass_single_events, 'mass_single_events')
        save_object(path_output, mass_single_events_max_total_mass, 'mass_single_events_max_total_mass')
        save_object(path_output, mass_single_events_max_total_mass_per_time_frame, 'mass_single_events_max_total_mass_per_time_frame')
        save_object(path_output, mass_single_events_max_avg_mass, 'mass_single_events_max_avg_mass')
        save_object(path_output, mass_single_events_max_avg_mass_per_time_frame, 'mass_single_events_max_avg_mass_per_time_frame')
        
        print(f'Parallel computation for {weighting_function} completed.')
    
