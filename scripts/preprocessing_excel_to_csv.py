# -*- coding: utf-8 -*-
"""
Script converts excel file with multiple sheets into csv files.

To significantly speed up processing time, convert excel file with one spread
sheet per region into multiple csv files, one for each region.

Specify path of excel file. The csv files will be stored in new folder named
as the excel file.

This may take a while at the beginning, but it is worth the time.
"""

import pandas as pd
import os
from pathlib import Path

excel_path = "D:\\Git\\vre_droughts\\input\\PECD_Onshore_2030_edition 2021.3.xlsx"  # specify path
folder_path = os.path.splitext(excel_path)[0]
Path(folder_path).mkdir(parents=True, exist_ok=True)

data = pd.read_excel(excel_path, sheet_name=None)
for key, df in data.items():
    if '10' in key:
        pass
    else:
        key = key.replace('0', '')
    csv_path = os.path.join(folder_path, f'{key}.csv')
    # drop first rows at beginning
    df = df.iloc[9:, :]  # adjust if necessary
    df.to_csv(csv_path, header=False, index=False)
