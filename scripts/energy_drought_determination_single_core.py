# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 12:43:16 2021

@author: mkittel
"""
import yaml
import pandas as pd
import numpy as np
# import datetime
import time
# import plotly.graph_objects as go
import os
from pathlib import Path
from datetime import datetime
from functools import reduce
import pickle
import gzip
from collections import defaultdict
import multiprocessing
import multiprocess

# import own modules
from config_check import InputError, ConfigCheck
from plotter import Plotter

os.chdir('C:\\git\\vre_droughts\\scripts')
# TODO:
# - inputs as subclassed from immutable Built-in types to check whether there are hourly or daily values
#   - https://realpython.com/lessons/returning-instances-of-different-class/
# - Analyzer class as Singleton class
#   - https://realpython.com/lessons/allowing-only-single-instance-in-class/
# - cleaner code with namedtuples
#   - https://realpython.com/python-namedtuple/
# - convert code to package to ensure all scripts are at the right spot -> install it to current environment


class Analyzer():

    def __init__(self, config):
        self.path_project = config['paths']['project']
        self.load_drought_counters = config['pickle']['load_drought_counters']
        self.load_region_analyzer = config['pickle']['load_region_analyzer']
        self.pickle_time_stamp = config['pickle']['time_stamp']
        if 'combined' in config['technologies']:
            self.tech_combined = config['technologies']['combined']
            self.vre_avail = {}
        self.regions = config['regions']

        # create result directory
        self.create_result_directory()

    def create_result_directory(self):
        '''
        Creates result directory, either for previous run or for new run.
        '''
        if not (self.load_drought_counters or self.load_region_analyzer):
            time_stamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        else:
            time_stamp = self.pickle_time_stamp

        # create output folder based on timestamp
        path_new_folder = os.path.join(self.path_project, '03_results', time_stamp)
        Path(path_new_folder).mkdir(parents=True, exist_ok=True)
        self.path_run = path_new_folder


class RegionAnalyzer():
    '''
    Initiates drought counting for different regions, stores results, and initiates plotting.
    '''

    def __init__(
            self,
            path_run,
            config,
            technology
            ):
        self.drought_type = config['drought_type']
        self.counting_algorithm = config['counting_algorithm']
        self.start = config['start']
        self.end = config['end']
        self.thresholds = config['thresholds']
        self.technology = technology
        self.regions = config['regions']
        self.excluded = config['excluded']
        self.load_drought_counters = config['pickle']['load_drought_counters']
        self.load_region_analyzer = config['pickle']['load_region_analyzer']
        self.pickle_time_stamp = config['pickle']['time_stamp']
        self.to_pickle = config['pickle']['to_pickle']
        self.path_input = config['paths']['input']

        # create directories
        if not self.load_region_analyzer:
            self.create_directories(path_run, technology)

        # instantiate input extractor
        if not self.load_drought_counters:
            self.input_extractor = InputExtractor(config['years'])

        # container for drought_counter objects
        self.drought_counters = {}

        # aggregated results
        if not self.load_region_analyzer:
            self.mean_yearly_cum = {}
            self.mean_yearly_cum_all_t = {}
            self.mean_yearly_cum_all_t_long = {}
            self.mean_season_cum = {}
            self.extremes_monthly = {}
            self.extremes_monthly_long = {}
            self.extremes_yearly = {}
            self.extremes_yearly_long = {}
            self.return_period_all = {}
            self.return_period_winter = {}
            self.return_period_all_long = {}
            self.return_period_winter_long = {}

        # instantiate plotter
        self.plotter = Plotter(
            self.drought_type,
            self.counting_algorithm,
            self.start,
            self.end,
            config['plots']['interactive_single_plots'],
            config['plots']['interactive_aggregated_plots'],
            save=True
            )

    @staticmethod
    def series_in_dict_to_dataframe(dictionary):
        '''
        Returns a dataframe with columns that are stored as values in dictionary. Column
        names equal dictionary keys.
        '''
        return pd.DataFrame(list(dictionary.values()), index=list(dictionary.keys())).T

    def create_directories(self, path_run, technology):
        '''
        Creates output path. Hierarchy: tech -> region -> file format. format is png and pdf.
        '''
        # TODO: move from OC to self-sustained directory structure, replace folder name with 'output'

        if not self.load_drought_counters:

            # create region result directories
            for r in self.regions:
                if not (technology in self.excluded.keys() and r in self.excluded[technology]):
                    formats = ['png', 'pdf']
                    for f in formats:
                        path_new_folder = os.path.join(path_run, technology, r, f)
                        Path(path_new_folder).mkdir(parents=True, exist_ok=True)

            # return trimmed output path
            path_output = Path(path_new_folder)
            path_output = path_output.parent.parent

            # create aggregated result directories
            formats = ['html', 'png', 'pdf']
            for f in formats:
                path_new_folder = os.path.join(path_output, 'aggregated', f)
                Path(path_new_folder).mkdir(parents=True, exist_ok=True)

            self.path_output = path_output

            # change current working directory
            os.chdir(path_output)

        else:
            self.path_output = os.path.join(path_run, technology)

            # change current working directory
            os.chdir(self.path_output)

    def wide_to_long(self, wide, col_name, value_name, region):
        '''
        Transforms data in wide into long format. Name of value column is derived from name.
        col_name is index name of wide.
        '''
        # name_col = str(name.split('_')[1][:-2] + 's') if 'extremes' in name else name
        # value_name = name if 'extremes' in name else f"max_{name.split('_')[1]}"
        long = wide.reset_index().rename(columns={'index': col_name})
        long = pd.melt(
            long,
            id_vars=col_name,
            value_vars=self.thresholds,
            var_name='threshold',
            value_name=value_name
            )
        long['region'] = region
        return long

    @staticmethod
    def dict_to_df(dictionary):
        '''
        Returns one dataframe that contains all single dataframes from dictionary values.
        '''
        return pd.concat(dictionary.values(), ignore_index=True)

    @staticmethod
    def dataframes_in_dict_to_dataframe(dictionary):
        '''
        Returns dataframe that merges dataframes contained in dictionary.
        '''
        dfs = list(dictionary.values())
        df = reduce(lambda left, right: pd.merge(left, right, on=['index'], how='outer'), dfs).fillna(0)
        df.set_index('index', inplace=True)
        return df

    def save_object(self, obj=None):
        '''
        Stores objects as pickle in current working directory.
        '''
        if obj:
            file_name = f"{obj.technology}_{obj.region}_{obj.threshold}.gz"
            with gzip.open(file_name, 'wb') as file:
                pickle.dump(obj, file, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            os.chdir(self.path_output)
            file_name = f"{self.technology}.gz"
            with gzip.open(file_name, 'wb') as file:
                pickle.dump(self, file, protocol=pickle.HIGHEST_PROTOCOL)

    def load_drought_counter(self, region, threshold):
        '''
        Loads pickled object from current working directory.
        '''
        file_name = f"{self.technology}_{region}_{threshold}.gz"
        with gzip.open(file_name, 'rb') as file:
            return pickle.load(file)

    @classmethod
    def load_region_analyzer(cls, config, technology):
        '''
        Loads region analyzer from pickle.
        '''
        path_results = config['paths']['results']
        time_stamp = config['pickle']['time_stamp']
        os.chdir(os.path.join(path_results, time_stamp, technology))
        file_name = f"{technology}.gz"
        with gzip.open(file_name, 'rb') as file:
            return pickle.load(file)

    def analyze_region(self, region, region_code, technology):
        '''
        Manages analysis on region level. Calls DroughtCounter class to run drought
        counting and stores results from DroughtCounter instance. Calls Plotter to plot
        region-specifics figures.
        '''
        # print process id
        print(f'Process {os.getpid()} working region {region}.')

        # working directory
        wd = os.path.join(self.path_output, region)

        # if region is excluded for technology exit
        if (technology in self.excluded.keys() and region in self.excluded[technology]):
            return

        # time the execution for each region
        start_time_region = time.perf_counter()

        # settings
        self.plotter.region = region
        self.plotter.path_output_region = wd
        os.chdir(wd)

        # result container
        self.mean_yearly_cum[region] = {}
        self.mean_yearly_cum_all_t[region] = {}
        self.mean_season_cum[region] = {}
        self.extremes_monthly[region] = {}
        self.extremes_yearly[region] = {}
        self.return_period_all[region] = {}
        self.return_period_winter[region] = {}
        self.drought_counters[region] = {}

        # import input data
        if not self.load_drought_counters:
            path_ts = os.path.join(self.path_input, config['paths'][technology])
            self.input_extractor.get_timeseries_data(path_ts, region_code)

        # count droughts for different thresholds
        for threshold_idx, thres in enumerate(self.thresholds):

            # count droughts
            if not self.load_drought_counters:
                dc = DroughtCounter(
                    self.drought_type,
                    self.counting_algorithm,
                    thres,
                    self.start,
                    self.end,
                    technology,
                    region,
                    self.input_extractor.years_complete,
                    self.input_extractor.years
                )

                # execute counting algorithm
                dc.count_droughts(
                    self.input_extractor.years,
                    self.input_extractor.years_complete,
                    self.input_extractor.ts_long
                    )

            # load pickled data
            elif self.load_drought_counters:
                # TODO: from ra instead of dc
                dc = self.load_drought_counter(region, thres)

            # save results
            self.mean_yearly_cum[region][thres] = dc.mean_yearly_cum
            self.mean_yearly_cum_all_t[region][thres] = dc.mean_yearly_cum.iloc[-1].reset_index()
            self.mean_yearly_cum_all_t[region][thres].rename(columns={'Average': thres}, inplace=True)
            self.mean_season_cum[region][thres] = dc.mean_season_cum
            self.extremes_monthly[region][thres] = dc.extremes_monthly
            self.extremes_yearly[region][thres] = dc.extremes_yearly
            self.return_period_all[region][thres] = dc.return_period_all_standard
            self.return_period_winter[region][thres] = dc.return_period_winter_standard

# =============================================================================
#             # pickle results
#             if self.to_pickle:
#                 self.save_object(dc)
# =============================================================================

            # plot frequency for single threshold
            self.plotter.threshold = thres
            mean_season_cum_temp = dc.mean_season_cum
            mean_season_cum_temp = mean_season_cum_temp.loc[:, (mean_season_cum_temp != 0).any(axis=0)]
            self.plotter.lines_single(mean_season_cum_temp, 'average frequency per season')
            self.plotter.lines_single(mean_season_cum_temp, 'average frequency per season', truncate=True)

            # safe country results
            self.drought_counters[region][thres] = dc

        # concat average values for all thresholds
        self.mean_yearly_cum_all_t[region] = self.dataframes_in_dict_to_dataframe(self.mean_yearly_cum_all_t[region])

        # plot average yearly frequency for all thresholds
        self.plotter.lines_single(self.mean_yearly_cum_all_t[region].T, 'average frequency across all years')

        # transform series in dict to dataframe
        self.extremes_yearly[region] = self.series_in_dict_to_dataframe(self.extremes_yearly[region])
        self.extremes_monthly[region] = self.series_in_dict_to_dataframe(self.extremes_monthly[region])
        self.return_period_all[region] = self.series_in_dict_to_dataframe(self.return_period_all[region])
        self.return_period_winter[region] = self.series_in_dict_to_dataframe(self.return_period_winter[region])

        # transform data in long format
        self.mean_yearly_cum_all_t_long[region] = self.wide_to_long(
            self.mean_yearly_cum_all_t[region], 'duration', 'frequency', region
            )
        self.extremes_yearly_long[region] = self.wide_to_long(
            self.extremes_yearly[region], 'years', 'extremes_yearly', region
            )
        self.extremes_monthly_long[region] = self.wide_to_long(
            self.extremes_monthly[region], 'months', 'extremes_monthly', region
            )
        self.return_period_all_long[region] = self.wide_to_long(
            self.return_period_all[region], 'return_period', 'max_period', region
            )
        self.return_period_winter_long[region] = self.wide_to_long(
            self.return_period_winter[region], 'return_period', 'max_period', region
            )

        # plot extremes single
        self.plotter.bars_single(self.extremes_yearly[region]/24, 'extremes_yearly')
        self.plotter.bars_single(self.extremes_monthly[region]/24, 'extremes_monthly')
        self.plotter.lines_single(self.return_period_all[region].T, 'duration any season [h]', 'return period')
        self.plotter.lines_single(self.return_period_winter[region].T, 'duration in winter [h]', 'return period')

        # display execution time
        duration = time.perf_counter() - start_time_region
        print(f"Duration for region {region}: {round(duration, 2)} seconds")

        # print process complettion statement
        print(f'Process {os.getpid()} done processing region {region}.')

    def analyze_all_regions(self):
        '''
        Transforms region-specific results in one dataframe that has long format. Uses
        instance of Plotter for aggregated figures. Saves figures and pickles results
        for later use.
        '''
        # transform data
        self.mean_yearly_cum_all_t_long = self.dict_to_df(self.mean_yearly_cum_all_t_long)
        self.extremes_yearly_long = self.dict_to_df(self.extremes_yearly_long)
        self.extremes_monthly_long = self.dict_to_df(self.extremes_monthly_long)
        self.return_period_all_long = self.dict_to_df(self.return_period_all_long)
        self.return_period_winter_long = self.dict_to_df(self.return_period_winter_long)

        # plot most extremes periods per year and month for all regions
        self.plotter.bars_all(
            self.path_output, self.extremes_yearly_long, x='years', y='extremes_yearly', days=True, rotate=True)
        self.plotter.bars_all(
            self.path_output, self.extremes_monthly_long, x='months', y='extremes_monthly', days=True)

        # plot requency & return periods of all regions
        self.plotter.lines_all(
            self.path_output,
            self.mean_yearly_cum_all_t_long,
            'duration',
            'frequency',
            'threshold',
            'region',
            'Average frequency of MBT events'
            )
        self.plotter.lines_all(
            self.path_output,
            self.mean_yearly_cum_all_t_long,
            'duration',
            'frequency',
            'region',
            'threshold',
            'Average frequency of MBT events'
            )
        self.plotter.lines_all(
            self.path_output,
            self.return_period_all_long,
            'return_period',
            'max_period',
            'threshold',
            'region',
            'any_season',
            {'return_period': 'return period', 'max_period': 'days'},
            'Return periods of MBT events from any season'
            )
        self.plotter.lines_all(
            self.path_output,
            self.return_period_all_long,
            'return_period',
            'max_period',
            'region',
            'threshold',
            'any_season',
            {'return_period': 'return period', 'max_period': 'days'},
            'Return periods of MBT events from any season'
            )
        self.plotter.lines_all(
            self.path_output,
            self.return_period_winter_long,
            'return_period',
            'max_period',
            'threshold',
            'region',
            'winter',
            {'return_period': 'return period', 'max_period': 'days'},
            'Return periods of MBT events in winter'
            )
        self.plotter.lines_all(
            self.path_output,
            self.return_period_winter_long,
            'return_period',
            'max_period',
            'region',
            'threshold',
            'winter',
            {'return_period': 'return period', 'max_period': 'days'},
            'Return periods of MBT events in winter'
            )

        # pickle results
        if self.to_pickle:
            self.save_object()


class InputExtractor():

    # TODO: transform to data class: https://realpython.com/python-data-classes/

    def __init__(self, years=None):
        self.years = years
        self.years_complete = None
        self.ts_long = None

    def get_timeseries_data(self, path, region_code, skip_rows=None, return_data=False):
        '''
        Reads time series input data from excel. Datat has to be in wide form acc. to
        ENTSO-e's PECD format. Selects relevant years according to year selection of
        user. If no years are specified, than all years extracted. If first year of
        the original dataset is selected, then the last year of the original dataset
        is also extracted. Data is returned in long format.
        '''
        # read data
        # ts_wide = pd.read_excel(path, sheet_name=sheet_name, skiprows=skip_rows)
        path_csv = os.path.join(path, f'{region_code}.csv')
        ts_wide = pd.read_csv(path_csv, skiprows=skip_rows)

        # complete range of years
        years_complete = ts_wide.drop(columns=['Date', 'Hour']).columns.tolist()

        # check sanity of years_complete
        if not all(isinstance(x, int) for x in years_complete):

            # convert to int
            years_complete = [int(float(i)) for i in years_complete]
            if not all(isinstance(x, int) for x in years_complete):
                raise InputError(
                    f"Expected list of integers in column headers that represent years."
                    f"Data provided: {years_complete}."
                    )

            # reassign correct headers
            headers = ['Date', 'Hour'] + years_complete
            ts_wide.columns = headers

        # drop unnecessary years if user enters selection of years
        if self.years:

            # Option 1: first year selected, include also data from last year
            if self.years[0] == years_complete[0]:
                years_selection = self.years.copy()
                years_selection.append(years_complete[-1])

            # Option 2: first year not selected, include also previous year
            else:
                years_selection = [self.years[0]-1]
                years_selection.extend(self.years)

            years_selection.append('Date')
            years_selection.append('Hour')

            # reduce data to only relevant years
            ts_wide.drop(ts_wide.columns.difference(years_selection), axis=1, inplace=True)

        # if no user-specified years, then assign years based on input data set
        elif not self.years:
            self.years = years_complete

        # check data quality
        check_nan = ts_wide.isna().sum()
        if any(check_nan.values):
            raise InputError(
                f"There are NaNs in csv {region_code}. Please insert complete data."
                )

        # transform data in long format
        self.ts_long = pd.melt(
            ts_wide,
            id_vars=['Date', 'Hour'],
            var_name='year',
            value_name='capacity_factor'
            )

        self.years_complete = years_complete

        # return data if required
        if return_data:
            return self.ts_long, self.years_complete

    def get_vre_capacity(self, path, sheet_name, technology, skip_rows=False):
        '''
        Extracts static capacity data. Data has to be in the wide format acc. to
        ENTSO-e's MAF2020 Dataset file. ''technology' refers to name in input file.
        '''
        # read and filter data
        cap = pd.read_excel(path, sheet_name=sheet_name, skiprows=skip_rows, index_col=0)
        self.cap = cap.loc[technology, :]
        
    # TODO
    def generate_combined_vre_availability():
        pass


class DroughtCounter():
    '''
    Counts droughts for a single region.
    '''

    def __init__(
            self,
            drought_type,
            counting_algorithm,
            threshold,
            start,
            end,
            technology,
            region,
            years_complete,
            years=None
            ):
        self.drought_type = drought_type
        self.counting_algorithm = counting_algorithm
        self.threshold = threshold
        self.start = start
        self.end = end
        self.technology = technology
        self.region = region
        self.years_complete = years_complete
        self.years = years_complete if not years else years
        self.periods = None
        self.data = None
        self.temp_idx = None
        self.count_monthly = None
        self.count_season = None
        self.idx = None
        self.monthly = None
        self.season = None
        self.count_yearly = None
        self.mean_yearly_singular = None
        self.mean_montly_singular = None
        self.mean_season_singular = None
        self.mean_yearly_cum = None
        self.mean_monthly_cum = None
        self.mean_season_cum = None
        self.extremes_monthly = None
        self.extremes_yearly = None
        self.return_period_all = None
        self.return_period_winter = None
        self.return_period_all_standard = None
        self.return_period_winter_standard = None
        if counting_algorithm == 'mbt':
            self.sma_array = None
            self.temp_cf = None
            self.cf = None
        elif counting_algorithm == 'cbt':
            self.cbt_array = None

    def __str__(self):
        description = (
            f"{type(self).__name__} object with following parameters:\n"
            f"Drought type: {self.drought_type}\n"
            f"Counting algorithm: {self.counting_algorithm}\n"
            f"Threshold={self.threshold}\n"
            f"Range start={self.start}, range end={self.end}\n"
            f"Technology: {self.technology}\n"
            f"Region: {self.region}\n"
            f"Years: {self.years}"
        )
        return description

    def define_period_range(self):
        '''
        Returns range for counting algorithm in either ascending (window) or
        descending order (event).
        '''
        if self.counting_algorithm == 'window':
            self.periods = range(self.start, self.end+1)
        elif self.counting_algorithm == 'event':
            self.periods = range(self.start, self.end-1, -1)
        else:
            print('Your input sucks')

    def extract_data(self, ts_long, complete_years, y, period):
        '''
        Slices relevant data out of full set of timeseries. Includes last p-1
        hours from previous year. For first year, p-1 hours from last year are
        included.
        '''
        # copy for analysis to be modified while counting energy droughts
        ts = ts_long.copy()
        data_main = ts.loc[ts['year'] == y].copy()

        # add hours from previous year depending on period size, for first year, use last year
        if y == complete_years[0]:
            data_pre = ts.loc[ts['year'] == complete_years[-1]].copy()
        else:
            data_pre = ts.loc[ts['year'] == y-1].copy()
        data_pre = data_pre.iloc[len(data_pre)-period+1:len(data_pre), :]
        data = pd.concat([data_pre, data_main])
        self.data = data

    def simple_moving_average(self, row, period):
        '''
        Determines simple moving average on row "row" in data set "data" for
        period "p" in new row "SMA". SMA in hour 'h' refers to SMA of rolling
        window [h-period+1, h]. Resulting array is saved to sma_array without NaNs
        corresponding to p-1 hours of datapoints from previous year when computing
        a simple moving average.
        '''
        self.data['SMA'] = self.data.iloc[:, row].rolling(window=period).mean()
        sma_array = self.data['SMA'].to_numpy()
        self.sma_array = sma_array[~np.isnan(sma_array)]

        # TODO: increase speed
        # https://stackoverflow.com/questions/13728392/moving-average-or-running-mean/43200476#43200476

    def rolling_cbt_test(self, row, period):
        '''
        Tests whether all values within rolling window with length 'period'
        on row 'row' in data 'data' are below threshold 'threshold'. A value
        of '0' in hour h indicates that there are values above the threshold
        'threshold' in rolling window [h-period+1, h]. A value of '1' shows that all
        values are below 'threshold'. Result array is saved to data in new
        row 'CBT'. 'data' constains NaNs corresponding to period-1 hours of
        datapoints from previous year when for rolling window of first hours
        of the year. 'cbt_array' has no NaNs and starts in first hours or the year.
        '''
        self.data['CBT'] = self.data.iloc[:, row].rolling(window=period).apply(
            lambda x: np.all(x < self.threshold), raw=True
            )
        cbt_array = self.data['CBT'].to_numpy()
        self.cbt_array = cbt_array[~np.isnan(cbt_array)]

    def find_mbt_windows(self, period):
        '''
        Counts MBT windows. Starts with first simple moving average below threshold
        in time series. Then excludes underlying periods and calculates average
        again. Continues with next avergae below threshold until no minimum below
        threshold exists.
        '''
        # temporary data containers
        temp_cf = []
        temp_idx = []

        # search first occurence of moving average below threshold
        while np.min(self.sma_array) < self.threshold:
            idx = np.where(self.sma_array < self.threshold)[0][0]
            temp_cf.append(self.sma_array[idx])
            temp_idx.append(idx)

            # check whether window contains excluded values (temp_data includes previous year)
            if np.any(self.data.iloc[np.arange(idx, idx+period), 3].to_numpy() == 1e5):
                print(f"Error for hour: {idx} - overlapping with previously excluded MBT hours.")
                break

            # to exclude this window set all window values to 1e5 (data includes previous year)
            self.data.iloc[np.arange(idx, idx+period), 3] = 1e5

            # determine windows with moving average below threshold with window [t-p+1, t]
            self.simple_moving_average(3, period)

            self.temp_cf = temp_cf
            self.temp_idx = temp_idx

    def find_cbt_windows(self, period):
        '''
        Counts BBT windows. Starts with first occurence of rolling window only
        containing values below threshold. Then excludes underlying period. Repeat
        until no qualified rolling window is found.
        '''
        # temporary data containers
        temp_idx = []

        # search first occurence of constantly below threshold windows
        while 1 in self.cbt_array:
            idx = np.where(self.cbt_array == 1)[0][0]
            temp_idx.append(idx)

            # exclude the CBT period [idx, idx+period] plus all [idx+period, idx+period+period-1]
            # subsequent hours that not eligble anymore due to the exlcusion of the CBT period
            end = idx+period if idx+period <= len(self.cbt_array) else len(self.cbt_array)
            self.cbt_array[np.arange(idx, end)] = 0

        self.temp_idx = temp_idx

    def find_mbt_events(self, period):
        '''
        Counts MBT events. Starts with minimum simple moving average in time
        series. Then excludes the underlying period and calculates average again.
        Continues with next minimum until no minimum below the threshold exists.
        '''
        # temporary data containers
        temp_cf = []
        temp_idx = []

        # search minimum of moving average below threshold
        while np.min(self.sma_array) < self.threshold:
            idx = np.argmin(self.sma_array)
            temp_cf.append(self.sma_array[idx])
            temp_idx.append(idx)

            # check whether window contains excluded values from same period class (data includes previous year)
            if np.any(self.data.iloc[np.arange(idx, idx+period), 3].to_numpy() == 1e5):
                print(f"Error at hour: {idx} - previously excluded MBT event.")
                break

            # to exclude this period set all period values to 1e5 (data includes previous year)
            self.data.iloc[np.arange(idx, idx+period), 3] = 1e5

            # determine periods with moving average below threshold with period [t-period+1, t]
            self.simple_moving_average(3, period)

        self.temp_cf = temp_cf
        self.temp_idx = temp_idx

    def find_cbt_events(self, period):
        '''
        Counts CBT events. Starts with first occurence of rolling window only
        containing values below threshold. Then excludes underlying period. Repeat
        until no qualified rolling window is found.
        '''
        # temporary data container
        temp_idx = []

        # search first occurence of moving average below threshold
        while 1 in self.cbt_array:
            idx = np.where(self.cbt_array == 1)[0][0]
            temp_idx.append(idx)

            # check whether window overlaps with previous CBT windows (data includes previous year)
            if np.any(self.data.iloc[np.arange(idx, idx+period), 3].to_numpy() == 1):
                print('error at hour:', idx, '- overlapping with previously excluded CBT windows')
                break

            # exclude the CBT period [idx, idx+period] plus all [idx+period, idx+period+period-1]
            # subsequent hours that not eligble anymore due to the exlcusion of the CBT period
            end = idx+period if idx+period <= len(self.cbt_array) else len(self.cbt_array)
            self.cbt_array[np.arange(idx, end)] = 0

            # to exclude this period set all period values to 1e5 (data includes previous year)
            self.data.iloc[np.arange(idx, idx+period), 3] = 1e5
            # =============================================================================
            #         # Alternative: find steaks
            #         df = pd.DataFrame(cbt_array.copy(), columns=['v'])
            #         df['idx'] = df.index
            #         df['cs'] = (df['v'].diff(1) != 0).cumsum()
            #         result = pd.DataFrame({'BeginIdx' : df.groupby('cs').idx.first(),
            #                       'EndIdx' : df.groupby('cs').idx.last(),
            #                       'Consecutive' : df.groupby('cs').size(),
            #                       'Value' : df.groupby('cs').v.first()}).reset_index(drop=True)
            #         result = result[result.Value != 0]
            #         result = result[result.Consecutive == 1]
            #         temp_idx.extend(result.EndIdx.values)
            #         for idx in result.EndIdx.values:
            #             # to exclude this window set all window values to 1e5 (data includes previous year)
            #             temp_data.iloc[np.arange(idx, idx+w), 3] = 1e5
            # =============================================================================

        self.temp_idx = temp_idx

    def aggregate_to_monthly_count(self):
        '''
        Distributes hourly indices of critical periods across all months.
        '''
        count_monthly = pd.Series(np.zeros(12), range(1, 13), dtype='int64')
        for idx in self.temp_idx:
            if 0 <= idx < 744:
                count_monthly[1] += 1
            elif 744 <= idx < 1416:
                count_monthly[2] += 1
            elif 1416 <= idx < 2160:
                count_monthly[3] += 1
            elif 2160 <= idx < 2880:
                count_monthly[4] += 1
            elif 2880 <= idx < 3624:
                count_monthly[5] += 1
            elif 3624 <= idx < 4344:
                count_monthly[6] += 1
            elif 4344 <= idx < 5088:
                count_monthly[7] += 1
            elif 5088 <= idx < 5832:
                count_monthly[8] += 1
            elif 5832 <= idx < 6552:
                count_monthly[9] += 1
            elif 6552 <= idx < 7296:
                count_monthly[10] += 1
            elif 7296 <= idx < 8016:
                count_monthly[11] += 1
            elif 8016 <= idx <= 8759:
                count_monthly[12] += 1

        self.count_monthly = count_monthly

    def aggregate_to_season_count(self):
        '''
        Distributes hourly indices of critical periods across all seasons.
        '''
        count_season = pd.Series(np.zeros(4), range(1, 5), dtype='int64')
        for idx in self.temp_idx:
            if 0 <= idx < 2160:
                count_season[1] += 1
            elif 2160 <= idx < 4344:
                count_season[2] += 1
            elif 4344 <= idx < 6552:
                count_season[3] += 1
            elif 6552 <= idx < 8759:
                count_season[4] += 1

        self.count_season = count_season

    def mean_below_threshold(self, years, complete_years, ts_long):
        '''
        Counts windows or events. When counting events, make sure that first
        considered period duration has no match. Otherwise, there may be
        longer-lasting events not counted.

        Parameters
        ----------
        years : list
            List of years to be considered in the analysis.
        ts_long : DataFrame
            DataFrame with timeseries in long format.

        Returns
        -------
        None.
        '''
        # data container
        cf = {}
        idx = {}
        monthly = {}
        season = {}

        # iterate overall years
        for y in years:

            # progress documentation
            print(
                f"Search MBT {self.counting_algorithm}s -> "
                f"technology: {self.technology}, threshold: {self.threshold}, "
                f"region: {self.region}, year: {y}."
            )

            # data container per year
            cf[y] = {}
            idx[y] = {}
            monthly[y] = {}
            season[y] = {}

            # define period range
            self.define_period_range()

            # iterate overall periods
            for p_count, period in enumerate(self.periods):

                if self.counting_algorithm == 'window':

                    # extract data from original timeseries
                    self.extract_data(ts_long, complete_years, y, period)

                elif self.counting_algorithm == 'event':

                    # fetch data for first iteration, smaller windows use modified data
                    if p_count == 0:

                        # extract data from original timeseries
                        self.extract_data(ts_long, complete_years, y, period)

                    elif p_count > 0:

                        # drop first data entry
                        self.data = self.data.iloc[1:, :]

                # determine windows with moving average below threshold with window [t-period+1, t]
                self.simple_moving_average(3, period)

# =============================================================================
#                 # progress documentation
#                 print(
#                     f"Search MBT {self.counting_algorithm}s -> "
#                     f"technology: {self.technology}, threshold: {self.threshold}, "
#                     f"region: {self.region}, year: {y}, duration: {period}.",
#                     end=''
#                 )
# =============================================================================

                # count mbt windows or events
                if self.counting_algorithm == 'window':

                    # windows count agorithm
                    self.find_mbt_windows(period)

                elif self.counting_algorithm == 'event':

                    # events count agorithm
                    self.find_mbt_events(period)

                # check whether at least one window or event is found
                if self.temp_idx:

                    # count occurences per month
                    self.aggregate_to_monthly_count()

                    # count occurences per season
                    self.aggregate_to_season_count()

                    # data containers per window size
                    cf[y][period] = self.temp_cf
                    idx[y][period] = self.temp_idx
                    monthly[y][period] = self.count_monthly
                    season[y][period] = self.count_season

# =============================================================================
#                     # new line in stdout
#                     if len(self.temp_idx) == 1:
#                         print(f" -> {len(self.temp_idx)} {self.counting_algorithm} found")
#                     elif len(self.temp_idx) > 1:
#                         print(f" -> {len(self.temp_idx)} {self.counting_algorithm}s found")
# =============================================================================

                # break inner for loop over years if no energy drougts determined
                elif self.counting_algorithm == 'window':
                    # print('\nNo MBT energy drought windows longer than {} found.'.format(period-1))
                    break

                # continue inner for loop over periods if no energy drougts determined
                elif self.counting_algorithm == 'event':
                    # check whether energy droughts have been found at all
# =============================================================================
#                     if not idx[y]:
#                         print(' -> none found')
#                     else:
#                         print(' -> no additional events found')
# =============================================================================
                    continue

        self.cf = cf
        self.idx = idx
        self.monthly = monthly
        self.season = season

    def constantly_below_threshold(self, years, complete_years, ts_long):
        '''
        Counts windows or events. When counting events, make sure that first
        considered period duration has no match. Otherwise, there may be
        longer-lasting events not counted.

        Parameters
        ----------
        years : list
            List of years to be considered in the analysis.
        ts_long : DataFrame
            DataFrame with timeseries in long format.
        Returns
        -------
        None.

        '''
        # data container
        idx = {}
        monthly = {}
        season = {}

        # iterate overall years
        for y in years:

            # data container per year
            idx[y] = {}
            monthly[y] = {}
            season[y] = {}

            # define period range
            self.define_period_range()

            # iterate overall periods
            for p_count, period in enumerate(self.periods):

                if self.counting_algorithm == 'window':

                    # extract data from original timeseries
                    self.extract_data(ts_long, complete_years, y, period)

                elif self.counting_algorithm == 'event':

                    # fetch data for first iteration, smaller windows use modified data
                    if p_count == 0:

                        # extract data from original timeseries
                        self.extract_data(ts_long, complete_years, y, period)

                    elif p_count > 0:

                        # drop first data entry
                        self.data = self.data.iloc[1:, :]

                # test for rolling windows with all values below threshold
                self.rolling_cbt_test(3, period)

                # progress documentation
                step_description = (
                    f"Search CBT {self.counting_algorithm}s -> "
                    f"technology: {self.technology}, threshold: {self.threshold}, "
                    f"region: {self.region}, year: {y}, duration: {period}."
                )
                print(step_description, end='')

                # count cbt windows or events
                if self.counting_algorithm == 'window':

                    # windows count agorithm
                    self.find_cbt_windows(period)

                elif self.counting_algorithm == 'event':

                    # events count agorithm
                    self.find_cbt_events(period)

                # check whether at least one window or event is found
                if self.temp_idx:

                    # count occurences per month
                    self.aggregate_to_monthly_count()

                    # count occurences per season
                    self.aggregate_to_season_count()

                    # data containers per window size
                    idx[y][period] = self.temp_idx
                    monthly[y][period] = self.count_monthly
                    season[y][period] = self.count_season

                    # new line in stdout
                    if len(self.temp_idx) == 1:
                        print(f" -> {len(self.temp_idx)} {self.counting_algorithm} found")
                    elif len(self.temp_idx) > 1:
                        print(f" -> {len(self.temp_idx)} {self.counting_algorithm}s found")

                # break inner for loop over years if no energy drougts determined
                elif self.counting_algorithm == 'window':
                    print('')
                    print(f"No CBT energy drought {self.counting_algorithm} longer than {period-1} found.")
                    break

                # continue inner for loop over periods if no energy drougts determined
                elif self.counting_algorithm == 'event':
                    # check whether energy droughts have been found at all
                    if not idx[y]:
                        print(' -> none found')
                    else:
                        print(' -> no additional events found')
                    continue

        self.idx = idx
        self.monthly = monthly
        self.season = season

    def transform_results_format(self, years):
        '''
        Flipps dict hierarchy and converts to series or dataframes.
        '''
# =============================================================================
#         # period in ascending order
#         if self.counting_algorithm == 'event':
#             periods = [period for period in reversed(self.periods)]
#         elif self.counting_algorithm == 'window':
#             periods = self.periods
# =============================================================================

        # flipp dict hierarchy
        count_yearly = defaultdict(dict)
        for key, val in self.idx.items():
            for subkey, subval in val.items():
                count_yearly[subkey][key] = len(subval)
        # convert dict values to series
        for k, v in count_yearly.items():
            count_yearly[k] = pd.Series(v)

        # flipp dict hierarchy
        count_monthly = defaultdict(dict)
        for key, val in self.monthly.items():
            for subkey, subval in val.items():
                count_monthly[subkey][key] = subval
        # convert dict values to dataframe
        for k, v in count_monthly.items():
            count_monthly[k] = pd.DataFrame(v, columns=years).fillna(0)

        # flipp dict hierarchy
        count_season = defaultdict(dict)
        for key, val in self.season.items():
            for subkey, subval in val.items():
                count_season[subkey][key] = subval
        # convert dict values to dataframe
        for k, v in count_season.items():
            count_season[k] = pd.DataFrame(v, columns=years).fillna(0)

# =============================================================================
#         # data container
#         count_yearly = {p: pd.Series(index=years, dtype='int64') for p in periods}
#         count_monthly = {p: pd.DataFrame(index=range(1, 13), columns=years, dtype='int64') for p in periods}
#         count_season = {p: pd.DataFrame(index=range(1, 5), columns=years, dtype='int64') for p in periods}
#         
#         # count occurences of windows
#         for y in years:
#             for period in periods:
#                 if period in idx[y]:
#                     count_yearly[period][y] = len(idx[y][period])
#                     count_monthly[period][y] = monthly[y][period]
#                     count_season[period][y] = season[y][period]
# 
#         # replace nan with zero
#         for period in count_monthly.keys():
#             count_monthly[period] = count_monthly[period].fillna(0)
#             count_season[period] = count_season[period].fillna(0)
# 
#         # erase empty entries
#         for period in list(count_yearly.keys()):
#             if (count_yearly[period] == 0).all():
#                 del count_yearly[period]
#                 del count_monthly[period]
#                 del count_season[period]
#         
#         breakpoint()
# =============================================================================

        self.count_yearly = count_yearly
        self.count_monthly = count_monthly
        self.count_season = count_season

    def mean_occurence_singular(self):
        '''
        Averages yearly results of monthly and seasonal count. Computes mean for
        each period for yearly, monthly, and seasonal count.
        '''
        # determine mean for each period for monthly and seasonal counts across all years
        mean_monthly_singular = {}
        mean_season_singular = {}
        for period in self.count_monthly:
            mean_monthly_singular[period] = self.count_monthly[period].mean(axis=1)
            mean_season_singular[period] = self.count_season[period].mean(axis=1)

        # convert dict to df and name index
        mean_yearly_singular = pd.DataFrame(self.count_yearly)
        mean_monthly_singular = pd.DataFrame(mean_monthly_singular)
        mean_season_singular = pd.DataFrame(mean_season_singular)

        # compute mean for each period across all years, months, and seasons
        mean_yearly_singular.loc['Average'] = mean_yearly_singular.mean(axis=0)
        mean_monthly_singular.loc['Average'] = mean_monthly_singular.mean(axis=0)
        mean_season_singular.loc['Average'] = mean_season_singular.mean(axis=0)

        # rename index
        months = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
                  'August', 'September', 'October', 'November', 'December', 'Average']
        mean_monthly_singular['month'] = months
        mean_monthly_singular.set_index('month', inplace=True)

        seasons = ['Winter', 'Spring', 'Summer', 'Autumn', 'Average']
        mean_season_singular['season'] = seasons
        mean_season_singular.set_index('season', inplace=True)

        self.mean_yearly_singular = mean_yearly_singular
        self.mean_monthly_singular = mean_monthly_singular
        self.mean_season_singular = mean_season_singular

    def mean_occurence_cumulative(self):
        '''
        Computes cumulative sum of minimum drought occurrences. Interpretation: How many
        drought periods with a duration of at least so many hours. Number of hours are
        in index of returned dataframes.
        '''
        # temporary objects to get complete range of investigated durations
        # periods = define_period_range(self.counting_algorithm, self.start, self.end)
        temp = pd.DataFrame(columns=list(self.periods))

        # compute cumsum for yearly counts starting from longest to shortest duration
        mean_yearly_cum = pd.merge(
            self.mean_yearly_singular.T, temp.T, how='outer', left_index=True, right_index=True
            ).T
        mean_yearly_cum = mean_yearly_cum.fillna(0)
        mean_yearly_cum = mean_yearly_cum.iloc[:, ::-1].cumsum(axis=1).iloc[:, ::-1]
        mean_yearly_cum = mean_yearly_cum.replace(0, np.nan).dropna(axis=1, how="all")
        mean_yearly_cum = mean_yearly_cum.fillna(0)

        # compute cumsum for yearly counts starting from longest to shortest duration
        mean_monthly_cum = pd.merge(
            self.mean_monthly_singular.T, temp.T, how='outer', left_index=True, right_index=True
            ).T
        mean_monthly_cum = mean_monthly_cum.fillna(0)
        mean_monthly_cum = mean_monthly_cum.iloc[:, ::-1].cumsum(axis=1).iloc[:, ::-1]
        mean_monthly_cum = mean_monthly_cum.fillna(0)

        # compute cumsum for yearly counts starting from longest to shortest duration
        mean_season_cum = pd.merge(
            self.mean_season_singular.T, temp.T, how='outer', left_index=True, right_index=True
            ).T
        mean_season_cum = mean_season_cum.fillna(0)
        mean_season_cum = mean_season_cum.iloc[:, ::-1].cumsum(axis=1).iloc[:, ::-1]
        mean_season_cum = mean_season_cum.fillna(0)

        self.mean_yearly_cum = mean_yearly_cum
        self.mean_monthly_cum = mean_monthly_cum
        self.mean_season_cum = mean_season_cum

    @staticmethod
    def find_most_extreme_period(df):
        '''
        For each row in df, the function finds index of last cell with value
        greater than zero. Applied to monthly, seasonal, and yearly aggregated
        results, this is the longest period duration in hours.
        '''
        most_extreme = {}
        for row_idx, row in df.iterrows():
            most_extreme[row_idx] = row.index[row > 0][-1] if len(row.index[row > 0]) > 0 else 0
            most_extreme = pd.Series(most_extreme)
            most_extreme.rename({'Average': 'Maximum'}, inplace=True)
        return most_extreme

    def return_period(self, df, row):
        '''
        Computes return period as reciprocal of average occurence. Only return periods
        longer than half a year are returned. For each unique return period, the longest
        drought period is selected.
        '''
        # compute return period for all periods
        temp = 1 / df.loc[row]
        temp = temp.round(2)

        # find unique return periods
        unique_values = temp.unique()
        return_period = pd.Series(dtype='float32')
        for v in unique_values:
            return_period[v] = max(temp.index[temp == v])

        # rename index and series
        return_period = return_period.rename_axis('return_period')
        return_period.name = self.threshold

        return return_period

    def standardized_return_period(self, return_period):
        '''
        Returns standardized return period for full the range of investiagted years.
        The standardized return period is determined using the nearest neighbor,
        i.e. there is a maximum deviation of half a year. If there is no return
        period for a specific year, then the year is marked with NaN.
        '''
        years = range(1, len(self.years)+1)
        return_period_standard = pd.Series(dtype='object')
        for y in years:
            return_period_standard.at[y] = return_period.iloc[return_period.index.get_loc(y, 'nearest')]
        return_period_standard = return_period_standard.rename_axis('return_period')
        return return_period_standard

    def count_droughts(self, years, years_complete, ts_long):
        '''
        Initiates the counting algorithm. Tranforms result in suitable format.
        Computes relevant results.
        '''
        # determine MBT or CBT results
        if self.drought_type == 'mbt':

            self.mean_below_threshold(years, years_complete, ts_long)

        elif self.drought_type == 'cbt':

            self.constantly_below_threshold(years, years_complete, ts_long)

        # transform results
        self.transform_results_format(years)

        # compute mean occurence singular count
        self.mean_occurence_singular()

        # compute mean occurence cumulative count
        self.mean_occurence_cumulative()

        # compute return period for specified row
        self.return_period_all = self.return_period(self.mean_yearly_cum, 'Average')
        self.return_period_winter = self.return_period(self.mean_season_cum, 'Winter')

        # compute standardized return periods
        self.return_period_all_standard = self.standardized_return_period(self.return_period_all)
        self.return_period_winter_standard = self.standardized_return_period(self.return_period_winter)

        # most extreme periods
        self.extremes_monthly = self.find_most_extreme_period(self.mean_monthly_singular)
        self.extremes_yearly = self.find_most_extreme_period(self.mean_yearly_singular)


# %% main


if __name__ == "__main__":

    # time the execution
    start_time_total = time.perf_counter()

    # initial directory
    os.chdir('C:\\git\\vre_droughts\\scripts')

    # import settings
    with open('config.yaml', 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    # config sanity check
    ic = ConfigCheck(config)

    # instantiate analyzer object
    a = Analyzer(config)

    # start analysis for each technology
    for technology in config['technologies']:

        # run analysis
        if not config['pickle']['load_region_analyzer']:

            # instantiate new RegionAnalyzer object
            ra = RegionAnalyzer(a.path_run, config, technology)

            for region, region_code in config['regions'].items():
                ra.analyze_region(region, region_code, technology)

            # analysis for all regions
            ra.analyze_all_regions()

        else:

            # load from pickle
            ra = RegionAnalyzer.load_region_analyzer(config, technology)

            ra.path_output = os.path.join(
                config['paths']['results'],
                config['pickle']['time_stamp'],
                technology
                )

            ra.plotter = Plotter(
                ra.drought_type,
                ra.counting_algorithm,
                ra.start,
                ra.end,
                interactive_single_plots=False,
                interactive_aggregated_plots=True,
                save=True
                )

        # std output for log
        print(f"Computation for {technology} finished.")

    # display execution time total
    duration = time.perf_counter() - start_time_total
    print(f"Duration in total: {duration} seconds")

# %% playground

# =============================================================================
# ra.plotter = Plotter(
#     ra.drought_type,
#     ra.counting_algorithm,
#     ra.start,
#     ra.end,
#     False,
#     True,
#     True)
# 
# 
# df=ra.extremes_yearly_long
# df = df[df.years != 'Maximum']
# df = df[df['threshold'] == 0.05]
# regions = ['CZ', 'DE', 'ITN', 'NOM']
# df= df[df.region.isin(regions)]
# 
# ra.plotter.bars_all(path=ra.path_output,
#                     df=df,
#                     x='years',
#                     y='extremes_yearly',
#                     color=None,
#                     facet_row='region',
#                     barmode='stack',
#                     days=True,
#                     rotate=True)
# 
# 
# 
# df=ra.return_period_all_long
# df = df[df['threshold'] == 0.1]
# 
# ra.plotter.lines_all(path=ra.path_output,
#                      df=df,
#                      x='return_period',
#                      y='max_period',
#                      color='region',
#                      facet_row=None,
#                      additional_name='any_season',
#                      labels={'return_period': 'return period', 'max_period': 'days'},
#                      title='Return periods of MBT events from any season')
# 
# # =============================================================================
# # ra.plotter.lines_all(path=ra.path_output,
# #                      df=ra.return_period_all_long,
# #                      x='return_period',
# #                      y='max_period',
# #                      color='threshold',
# #                      facet_row='region',
# #                      additional_name='any_season',
# #                      labels={'return_period': 'return period', 'max_period': 'days'},
# #                      title='Return periods of MBT events from any season')
# # =============================================================================
# 
# ra.plotter.lines_all(path=ra.path_output,
#                      df=ra.return_period_winter_long,
#                      x='return_period',
#                      y='max_period',
#                      color='region',
#                      facet_row='threshold',
#                      additional_name='winter',
#                      labels={'return_period': 'return period', 'max_period': 'days'},
#                      title='Return periods of MBT events in winter')
# 
# # =============================================================================
# # ra.plotter.lines_all(path=ra.path_output,
# #                      df=ra.return_period_winter_long,
# #                      x='return_period',
# #                      y='max_period',
# #                      color='threshold',
# #                      facet_row='region',
# #                      additional_name='winter',
# #                      labels={'return_period': 'return period', 'max_period': 'days'},
# #                      title='Return periods of MBT events in winter')
# # =============================================================================
# 
# ra.plotter.lines_all(path=ra.path_output,
#                      df=ra.return_period_winter_long,
#                      x='return_period',
#                      y='max_period',
#                      color='region',
#                      facet_row='threshold',
#                      additional_name='winter',
#                      labels={'return_period': 'return period', 'max_period': 'days'},
#                      title='Return periods of MBT events in winter')
# 
# 
# ra.plotter.lines_all(path=ra.path_output,
#                      df=ra.return_period_winter_long,
#                      x='return_period',
#                      y='max_period',
#                      color='threshold',
#                      facet_row='region',
#                      additional_name='winter',
#                      labels={'return_period': 'return period', 'max_period': 'days'},
#                      title='Return periods of MBT events in winter')
# 
# df=ra.mean_yearly_cum_all_t_long
# df = df[df['threshold'] == 0.1]
# 
# ra.plotter.lines_all(ra.path_output,
#                     df,
#                     'duration',
#                     'frequency',
#                     None,
#                     'region',
#                     'Average frequency of MBT events'
#                     )
# ra.plotter.lines_all(ra.path_output,
#                      ra.mean_yearly_cum_all_t_long,
#                      'duration',
#                      'frequency',
#                      'region',
#                      'threshold',
#                      'Average frequency of MBT events'
#                      )
# 
# 
# #%%
# 
# data = ra.drought_counters['DE'].mean_yearly_singular
# 
# data = data.iloc[:-1, :].reset_index().rename(columns={'index': 'years'})
# 
# data_wide = pd.melt(data, id_vars='years', var_name='period_duration', value_name='frequency')
# 
# fig = px.box(data_wide, x='period_duration', y='frequency', hover_name='period_duration')
# #fig.update_traces(quartilemethod="exclusive")
# #fig.for_each_annotation(lambda x: x.update(text=x.text.split("=")[-1]))
# fig.show()
# 
# #%%
# import plotly.express as px
#  
# df = px.data.iris()
#  
# fig = px.box(df, x="sepal_width", y="sepal_length")
#  
# fig.show()
# #%% full load hours for seeing weak/strong wind/pv years for all countries -> see differences
# 
# plt.figure(figsize=[15,10])
# plt.grid(True)
# plt.plot(df['capacity_factor'],label='data')
# plt.plot(df['SMA_3'],label='SMA 3 Months')
# plt.plot(df['pandas_SMA_4'],label='SMA 4 Months')
# plt.legend(loc=2)
# 
# 
# #%%
# 
# def dummyfunction(input_matrix, output_matrix, start_year, final_year):
#     for t in tqdm(range(start_year,final_year+1)):
#         for i in range(0,input_matrix.shape[0]):
#             if i == 0:
#                 output_matrix[t].iloc[i] = 0
#             if i > 0:
#                 if np.isnan(input_matrix[t].iloc[i]) == True:
#                     input_matrix[t].iloc[i] = 0
#                 elif (input_matrix[t].iloc[i] == -1):
#                     output_matrix[t].iloc[i] = -1                            
#                 elif (input_matrix[t].iloc[i] == -1 and input_matrix[t].iloc[i-1] > -1 and input_matrix[t].iloc[i+1] > -1):
#                     output_matrix[t].iloc[i] = output_matrix[t].iloc[i-1] + 1      
#                 elif (input_matrix[t].iloc[i] == 0 and input_matrix[t].iloc[i-1] == -1):
#                     output_matrix[t].iloc[i] = 0
#                 elif (input_matrix[t].iloc[i] == 0 and output_matrix[t].iloc[i-1] != -1):
#                     output_matrix[t].iloc[i] = output_matrix[t].iloc[i-1] + 1  
#                 else:
#                     input_matrix[t].iloc[i] = 0
#                     
#                     
# #Definition for mean_below_threshold average
# def running_mean(x, N):
#     cumsum = np.cumsum(np.insert(x, 0, 0)) 
#     return (cumsum[N:] - cumsum[:-N]) / N 
# 
# 
# 
# 
# ts = ts.T
# ts = ts.stack()
# # Threshold value in percent of poweroutput
# threshold = [2, 5, 10]
# 
# # Time lenght in hours, six options for both analysistypes and three differnt threshold values for higher efficiency
# timelength2constantly_below_threshold = np.arange(5, 25, 1)
# timelength5constantly_below_threshold = np.arange(5, 70, 1)
# timelength10constantly_below_threshold = np.arange(5, 130, 1)
# timelength2mean_below_threshold = np.arange(5, 70, 1)
# timelength5mean_below_threshold = np.arange(5, 140, 1)
# timelength10mean_below_threshold = np.arange(5, 260, 1)
# 
# #Included years
# start = 1982
# end = 2016
# year = np.arange(start, end+1)  
# yearamount = np.arange(0, year.size)
# datayears = year.size
# 
# ts_cum = np.cumsum(ts)
# 
# =============================================================================


#%% Steinbruch - old MBT structure with window and event separated
# =============================================================================
#             # iterate overall periods - old
#             for p_count, period in enumerate(periods):
#                 
#                 if counting_algorithm == 'window':
#                     
#                     # extract data from original timeseries
#                     data = extract_data(ts_long, years, y, period)
#                     
#                     # determine windows with moving average below threshold with window [t-period+1, t]
#                     data, sma_array = simple_moving_average(data, 3, period)
#                     
#                     # temporary data containers
#                     temp_cf = []
#                     temp_idx = []
#                     
#                     # progress documentation
#                     step_description = f"Start search for MBT drought windows --> threshold: {threshold}, year: {y}, window size: {period}."
#                     print(step_description, end='')
#                     
#                     # count mbt windows
#                     find_mbt_windows(sma_array, data, period, threshold, temp_cf, temp_idx)
#                     
#                     # check whether at least one window is found
#                     if temp_cf:
#                         
#                         # count occurences per month
#                         count_monthly = aggregate_to_monthly_count(temp_idx)
#                         
#                         # count occurences per season
#                         count_season = aggregate_to_season_count(temp_idx)
#                         
#                         # count diurnal occurences
#                         #aggregate_to_diurnal_count()
#                         
#                         # data containers per window size
#                         cf[y][period] = temp_cf
#                         idx[y][period] = temp_idx
#                         monthly[y][period] = count_monthly
#                         season[y][period] = count_season
#                         
#                         # new line in stdout
#                         if len(temp_idx) == 1:
#                             print(' -> {} window found'.format(len(temp_idx)))
#                         elif len(temp_idx) > 1:
#                             print(' -> {} windows found'.format(len(temp_idx)))
#                         
#                     # break inner for loop over years if no energy drougts determined
#                     else:
#                         print('')
#                         print('No MBT energy drought windows longer than {} found.'.format(period-1))
#                         break
#                         
#                 elif counting_algorithm == 'event':
#                     
#                     # fetch data for first iteration, smaller windows use modified data
#                     if p_count == 0:
#                         
#                         # extract data from original timeseries
#                         data = extract_data(ts_long, years, y, period)
#                                                 
#                     elif p_count > 0:
#                         data = data.iloc[1:, :]
#                     
#                     # determine windows with moving average below threshold with window [t-period+1, t]
#                     data, sma_array = simple_moving_average(data, 3, period)
#                     
#                     # temporary data containers
#                     temp_cf = []
#                     temp_idx = []
#                     
#                     # progress documentation
#                     step_description = f"Start search for MBT drought event --> threshold: {threshold}, year: {y}, event size: {period}."
#                     print(step_description, end = '')
#                     
#                     # count mbt events
#                     data, temp_cf, temp_idx = find_mbt_events(sma_array, data, period, threshold, temp_cf, temp_idx)
#                     
#                     # check whether at least one event was found               
#                     if temp_idx:
#                         
#                         # count occurences per month
#                         count_monthly = aggregate_to_monthly_count(temp_idx)
#                         
#                         # count occurences per season
#                         count_season = aggregate_to_season_count(temp_idx)
#                         
#                         # data containers per window size
#                         cf[y][period] = temp_cf
#                         idx[y][period] = temp_idx
#                         monthly[y][period] = count_monthly
#                         season[y][period] = count_season
#                         
#                         # new line in stdout
#                         if len(temp_idx) == 1:
#                             print(' -> {} event found'.format(len(temp_idx)))
#                         elif len(temp_idx) > 1:
#                             print(' -> {} events found'.format(len(temp_idx)))
#                             
#                     # continue inner for loop over periods if no energy drougts determined
#                     else:
#                         # check whether energy droughts have been found at all
#                         if not idx[y]:
#                             print(' -> none found')
#                         else:
#                             print(' -> no additional events found')
#                         continue
# =============================================================================

# %%

# =============================================================================
# #%% Steinbruch - MBT windows separated
# 
# # set minimum and maximum window size in hours (1416 equals 2 months)
# windows = range(10, 1416)
# years = [1982]
# 
# # data container
# mbt_cf = {}
# mbt_idx = {}
# mbt_monthly = {}
# mbt_season = {}
# 
# # find numbers below threshold for all years, windows
# for y in years: 
#     
#     # data container per year
#     mbt_cf[y] = {}
#     mbt_idx[y] = {}
#     mbt_monthly[y] = {}
#     mbt_season[y] = {}
# 
#     for w in windows:
#         
#         # copy for analysis to be modified while counting energy droughts
#         ts = ts_long.copy()
#         temp_data_main = ts.loc[ts['year'] == y].copy()
#         
#         # add hours from previous year depending on window size, for first year, use last year
#         if y == years[0]:
#             temp_data_pre = ts.loc[ts['year'] == years[-1]].copy()
#         else:
#             temp_data_pre = ts.loc[ts['year'] == y-1].copy()
#         temp_data_pre = temp_data_pre.iloc[len(temp_data_pre)-w+1:len(temp_data_pre), :]
#         temp_data = pd.concat([temp_data_pre, temp_data_main])
#         
#         # determine windows with moving average below threshold with window [t-w+1, t]
#         temp_data['SMA'] = temp_data.iloc[:,3].rolling(window=w).mean()
#         #temp_data = temp_data.dropna(axis=0, how='any')
#         temp_array = temp_data['SMA'].to_numpy()
#         temp_array = temp_array[~np.isnan(temp_array)]
#         
#         # temporary data containers
#         temp_cf = []
#         temp_idx = []
#         #temp_df = []
#         
#         # progress documentation
#         step_description = 'Start search for MBT drought windows --> threshold: {}, year: {}, window size: {}.'.format(threshold, y, w)
#         print(step_description, end='')
#         
#         # search first occurence of moving average below threshold         
#         while np.min(temp_array) < threshold:
#             idx = np.where(temp_array < threshold)[0][0]
#             temp_cf.append(temp_array[idx])
#             temp_idx.append(idx)
#             #temp_df.append(temp_data.loc[np.arange(idx, idx+w), :])
#             
#             # check whether window contains excluded values (temp_data includes previous year)
#             if np.any(temp_data.iloc[np.arange(idx, idx+w), 3].to_numpy() == 1e5):
#                 print('error at hour:', idx, '- overlapping with previously excluded MBT hours')
#                 break
#             
#             # to exclude this window set all window values to 1e5 (data includes previous year)
#             temp_data.iloc[np.arange(idx, idx+w), 3] = 1e5
#             temp_data['SMA'] = temp_data.iloc[:,3].rolling(window=w).mean()
#             temp_array = temp_data['SMA'].to_numpy()
#             temp_array = temp_array[~np.isnan(temp_array)]
#             
#         # check whether at least one window is found
#         if temp_cf:
#             
#             # count occurences per month
#             temp_monthly = pd.Series(np.zeros(12), range(1,13), dtype='int64')
#             for idx in temp_idx:
#                 if 0 <= idx < 744:   
#                     temp_monthly[1] += 1
#                 elif 744 <= idx < 1416:
#                     temp_monthly[2] += 1
#                 elif 1416 <= idx < 2160:
#                     temp_monthly[3] += 1
#                 elif 2160 <= idx < 2880:
#                     temp_monthly[4] += 1
#                 elif 2880 <= idx < 3624:
#                     temp_monthly[5] += 1
#                 elif 3624 <= idx < 4344:
#                     temp_monthly[6] += 1
#                 elif 4344 <= idx < 5088:
#                     temp_monthly[7] += 1
#                 elif 5088 <= idx < 5832:
#                     temp_monthly[8] += 1
#                 elif 5832 <= idx < 6552:
#                     temp_monthly[9] += 1
#                 elif 6552 <= idx < 7296:
#                     temp_monthly[10] += 1
#                 elif 7296 <= idx < 8016:
#                     temp_monthly[11] += 1
#                 elif 8016 <= idx <= 8759:
#                     temp_monthly[12] += 1   
#             
#             # count occurences per season
#             temp_season = pd.Series(np.zeros(4), range(1,5), dtype='int64')
#             for idx in temp_idx:
#                 if 0 <= idx < 2160:   
#                     temp_season[1] += 1
#                 elif 2160 <= idx < 4344:
#                     temp_season[2] += 1
#                 elif 4344 <= idx < 6552:
#                     temp_season[3] += 1
#                 elif 6552 <= idx < 8759:
#                     temp_season[4] += 1
#             
#             # data containers per window size
#             mbt_cf[y][w] = temp_cf
#             mbt_idx[y][w] = temp_idx
#             #mbt_df[y][w] = temp_df
#             mbt_monthly[y][w] = temp_monthly
#             mbt_season[y][w] = temp_season
#             
#             # new line in stdout
#             if len(temp_idx) == 1:
#                 print(' -> {} window found'.format(len(temp_idx)))
#             elif len(temp_idx) > 1:
#                 print(' -> {} windows found'.format(len(temp_idx)))
#             
#         # break inner for loop over years if no energy drougts determined
#         else:
#             print('')
#             print('No MBT energy drought windows longer than {} found.'.format(w-1))
#             break
# 
# # data container
# mbt_count_yearly = {}
# mbt_count_monthly = {}
# mbt_count_season = {}
# for w in windows:
#     mbt_count_yearly[w] = pd.Series(index=years, dtype='int64')
#     mbt_count_monthly[w] = pd.DataFrame(index=range(1,13), columns=years, dtype='int64')
#     mbt_count_season[w] = pd.DataFrame(index=range(1,5), columns=years, dtype='int64')
# 
# # count occurences of windows
# for y in years:
#     for w in windows:
#         if w in mbt_idx[y]:
#             mbt_count_yearly[w][y] = len(mbt_idx[y][w])
#             mbt_count_monthly[w][y] = mbt_monthly[y][w]
#             mbt_count_season[w][y] = mbt_season[y][w]
#         else:
#             break
# 
# # replace nan with zero
# for w in mbt_count_monthly:
#     mbt_count_monthly[w] = mbt_count_monthly[w].fillna(0)
#     mbt_count_season[w] = mbt_count_season[w].fillna(0)
# 
# # erase empty entries        
# for w in list(mbt_count_yearly.keys()):
#     if (mbt_count_yearly[w] == 0).all():
#         del mbt_count_yearly[w]
#         del mbt_count_monthly[w]
#         del mbt_count_season[w]
# 
# # determine mean for monthly and seasonal results
# avg_monthly = {}
# avg_season = {}
# for w in mbt_count_monthly.keys():
#     avg_monthly[w] = mbt_count_monthly[w].mean(axis=1)
#     avg_season[w] = mbt_count_season[w].mean(axis=1)
# 
# # convert dict to df and name index     
# result_yearly = pd.DataFrame(mbt_count_yearly)
# result_avg_monthly = pd.DataFrame(avg_monthly)
# result_avg_season = pd.DataFrame(avg_season)
# result_avg_season.loc['Average'] = result_avg_season.mean(axis=0)
# 
# months = ['Janury', 'February', 'March', 'April', 'May', 'June', 'July', 
#           'August', 'September', 'October', 'November', 'December']
# result_avg_monthly['month'] = months
# result_avg_monthly.set_index('month', inplace=True)
# 
# seasons = ['Winter', 'Spring', 'Summer', 'Autumn', 'Average']
# result_avg_season['season'] = seasons
# result_avg_season.set_index('season', inplace=True)
# 
# 
# =============================================================================

# %% Steinbruch - MBT events separated

# =============================================================================
# # set minimum and maximum window size in hours (1416 equal 2 months)
# windows = range(1416, 9, -1)
# years = [1982]
# 
# # data container
# mbt_cf = {}
# mbt_idx = {}
# mbt_count = {}
# mbt_monthly = {}
# mbt_season = {}
# 
# # find numbers below threshold for all years, windows
# for y in years: 
#     
#     # data container
#     mbt_cf[y] = {}
#     mbt_idx[y] = {}
#     mbt_monthly[y] = {}
#     mbt_season[y] = {}
# 
#     for w_count, w in enumerate(windows):
#         
#         # fetch data for first iteration, smaller windows use modified data
#         if w_count == 0:
#             
#             # copy for analysis to be modified while counting energy droughts
#             ts = ts_long.copy()
#             temp_data_main = ts.loc[ts['year'] == y].copy()
#             
#             # add hours from previous year depending on window size, for first year, use last year
#             if y == years[0]:
#                 temp_data_pre = ts.loc[ts['year'] == years[-1]].copy()
#             else:
#                 temp_data_pre = ts.loc[ts['year'] == y-1].copy()
#             temp_data_pre = temp_data_pre.iloc[len(temp_data_pre)-w+1:len(temp_data_pre), :]
#             temp_data = pd.concat([temp_data_pre, temp_data_main])
#             
#         elif w_count > 0:
#             temp_data = temp_data.iloc[1:, :]
#             
#         # determine windows with moving average below threshold with window [t-w+1, t]
#         temp_data['SMA'] = temp_data.iloc[:, 3].rolling(window=w).mean()
#         temp_array = temp_data['SMA'].to_numpy()
#         temp_array = temp_array[~np.isnan(temp_array)]
#         
#         # temporary data containers
#         temp_cf = []
#         temp_idx = []
#         #temp_df = []
#         
#         # progress documentation
#         step_description = 'Start search for MBT drought event --> threshold: {}, year: {}, window size: {}'.format(threshold, y, w)
#         print(step_description, end = '')
#         
#         # search minimum of moving average below threshold         
#         while np.min(temp_array) < threshold:
#             idx = np.argmin(temp_array)
#             temp_cf.append(temp_array[idx])
#             temp_idx.append(idx)
#             #temp_df.append(temp_data.loc[idx-w+1:idx, :])
#             
#             # check whether window contains excluded values from same window class (data includes previous year)
#             if np.any(temp_data.iloc[np.arange(idx, idx+w), 3].to_numpy() == 1e5):
#                 print('error at hour:', idx, '- previously excluded MBT event')
#                 break
#             
#             # to exclude this window set all window values to 1e5 (data includes previous year)
#             temp_data.iloc[np.arange(idx, idx+w), 3] = 1e5
#             temp_data['SMA'] = temp_data.iloc[:,3].rolling(window=w).mean()
#             temp_array = temp_data['SMA'].to_numpy()
#             temp_array = temp_array[~np.isnan(temp_array)]
#         
#         # check whether energy droughts are found               
#         if temp_idx:
#             
#             # count occurences per month
#             temp_monthly = pd.Series(np.zeros(12), range(1,13), dtype='int64')
#             for idx in temp_idx:
#                 if 0 <= idx < 744:   
#                     temp_monthly[1] += 1
#                 elif 744 <= idx < 1416:
#                     temp_monthly[2] += 1
#                 elif 1416 <= idx < 2160:
#                     temp_monthly[3] += 1
#                 elif 2160 <= idx < 2880:
#                     temp_monthly[4] += 1
#                 elif 2880 <= idx < 3624:
#                     temp_monthly[5] += 1
#                 elif 3624 <= idx < 4344:
#                     temp_monthly[6] += 1
#                 elif 4344 <= idx < 5088:
#                     temp_monthly[7] += 1
#                 elif 5088 <= idx < 5832:
#                     temp_monthly[8] += 1
#                 elif 5832 <= idx < 6552:
#                     temp_monthly[9] += 1
#                 elif 6552 <= idx < 7296:
#                     temp_monthly[10] += 1
#                 elif 7296 <= idx < 8016:
#                     temp_monthly[11] += 1
#                 elif 8016 <= idx <= 8759:
#                     temp_monthly[12] += 1   
#                     
#             # count occurences per season
#             temp_season = pd.Series(np.zeros(4), range(1,5), dtype='int64')
#             for idx in temp_idx:
#                 if 0 <= idx < 2160:   
#                     temp_season[1] += 1
#                 elif 2160 <= idx < 4344:
#                     temp_season[2] += 1
#                 elif 4344 <= idx < 6552:
#                     temp_season[3] += 1
#                 elif 6552 <= idx < 8759:
#                     temp_season[4] += 1
#                     
#             # safe results
#             mbt_cf[y][w] = temp_cf
#             mbt_idx[y][w] = temp_idx
#             #mbt_df[y][w] = temp_df
#             mbt_monthly[y][w] = temp_monthly
#             mbt_season[y][w] = temp_season
#             
#             # new line in stdout
#             if len(temp_idx) == 1:
#                 print(' -> {} event found'.format(len(temp_idx)))
#             elif len(temp_idx) > 1:
#                 print(' -> {} events found'.format(len(temp_idx)))
#             
#         # continue inner for loop over windows if no energy drougts determined
#         else:
#             # check whether energy droughts have been found at all
#             if not mbt_idx[y]:
#                 print(' -> none found')
#             else:
#                 print(' -> no additional events found')
#             continue
# 
# # data container
# mbt_count_yearly = {}
# mbt_count_monthly = {}
# mbt_count_season = {}
# for w in windows:
#     mbt_count_yearly[w] = pd.Series(index=years, dtype='int64')
#     mbt_count_monthly[w] = pd.DataFrame(index=range(1,13), columns=years, dtype='int64')
#     mbt_count_season[w] = pd.DataFrame(index=range(1,5), columns=years, dtype='int64')
# 
# # count occurences of windows
# for y in years:
#     for w in windows:
#         if w in mbt_idx[y]:
#             mbt_count_yearly[w][y] = len(mbt_idx[y][w])
#             mbt_count_monthly[w][y] = mbt_monthly[y][w]
#             mbt_count_season[w][y] = mbt_season[y][w]
# 
# # replace nan with zero
# for w in mbt_count_monthly.keys():
#     mbt_count_monthly[w] = mbt_count_monthly[w].fillna(0)
#     mbt_count_season[w] = mbt_count_season[w].fillna(0)
# 
# # erase empty entries        
# for w in list(mbt_count_yearly.keys()):
#     if (mbt_count_yearly[w] == 0).all():
#         del mbt_count_yearly[w]
#         del mbt_count_monthly[w]
#         del mbt_count_season[w]
# 
# # determine mean for monthly and seasonal results
# avg_monthly = {}
# avg_season = {}
# for w in mbt_count_monthly.keys():
#     avg_monthly[w] = mbt_count_monthly[w].mean(axis=1)
#     avg_season[w] = mbt_count_season[w].mean(axis=1)
# 
# # convert dict to df and name index     
# result_yearly = pd.DataFrame(mbt_count_yearly)
# result_avg_monthly = pd.DataFrame(avg_monthly)
# result_avg_season = pd.DataFrame(avg_season)
# #result_avg_season.loc['Average'] = result_avg_season.mean(axis=0)
# 
# months = ['Janury', 'February', 'March', 'April', 'May', 'June', 'July', 
#           'August', 'September', 'October', 'November', 'December']
# result_avg_monthly['month'] = months
# result_avg_monthly.set_index('month', inplace=True)
# 
# seasons = ['Winter', 'Spring', 'Summer', 'Autumn']#, 'Average']
# result_avg_season['season'] = seasons
# result_avg_season.set_index('season', inplace=True)
# #result_avg_season = result_avg_season.iloc[:, ::-1]
# 
# =============================================================================
# %% Steinbruch: CBT windows separated
# =============================================================================
# #%% --- window perspective: consecutive hours constant below threshold (cbt) --
# 
# start_time = time.perf_counter()
# 
# # set minimum and maximum window size in hours (1416 equal 2 months)
# windows = range(5, 15)
# years = [1982]
# threshold = 0.1
# 
# # data container
# cbt_cf = {}
# cbt_idx = {}
# cbt_df = {}
# cbt_monthly = {}
# cbt_season = {}
# 
# # find numbers below threshold for all years, windows
# for y in years: 
#     # data container
#     cbt_cf[y] = {}
#     cbt_idx[y] = {}
#     cbt_df[y] = {}
#     cbt_monthly[y] = {}
#     cbt_season[y] = {}
# 
#     for w in windows:
#         # copy for analysis to be modified while counting energy droughts
#         ts = ts_long.copy()
#         temp_data_main = ts.loc[ts['year'] == y].copy()
#         
#         # add hours from previous year depending on window size, for first year, use last year
#         if y == years[0]:
#             temp_data_pre = ts.loc[ts['year'] == years[-1]].copy()
#         else:
#             temp_data_pre = ts.loc[ts['year'] == y-1].copy()
#         temp_data_pre = temp_data_pre.iloc[len(temp_data_pre)-w+1:len(temp_data_pre), :]
#         temp_data = pd.concat([temp_data_pre, temp_data_main])
#         
#         # determine windows with moving average below threshold with window [t-w+1, t]
#         temp_data['CBT'] = temp_data.iloc[:,3].rolling(window=w).apply(lambda x: np.all(x < threshold), raw=True)
#         temp_array = temp_data['CBT'].to_numpy()
#         temp_array = temp_array[~np.isnan(temp_array)]
#         
#         # temporary data containers
#         temp_idx = []
#         #temp_df = []
#         
#         # progress documentation
#         step_description = 'Start search for CBT drought windows --> threshold: {}, year: {}, window size: {}.'.format(threshold, y, w)
#         print(step_description, end = '')
#         
#         # search first occurence of moving average below threshold         
#         while 1 in temp_array:
#             idx = np.where(temp_array == 1)[0][0]
#             temp_idx.append(idx)
#             #temp_df.append(temp_data.loc[idx-w+1:idx, :])
#                   
#             # check whether window overlaps with previous CBT windows (data includes previous year)
#             if np.any(temp_data.iloc[np.arange(idx, idx+w), 3].to_numpy() == 1):
#                 print('error at hour:', idx, '- overlapping with previously excluded CBT windows')
#                 break
#             
#             # exclude the CBT period [idx, idx+w] plus all [idx+w, idx+w+w-1] subsequent hours that not eligble 
#             # anymore due to the exlcusion of the CBT period
#             if idx+w <= len(temp_array):
#                 end = idx+w
#             else:
#                 end = len(temp_array)
#             temp_array[np.arange(idx, end)] = 0
# # =============================================================================
# #             # to exclude this window set all window values to 1e5 (data includes previous year)
# #             temp_data.iloc[np.arange(idx, idx+w), 3] = 1e5
# #             temp_data['CBT'] = temp_data.iloc[:,3].rolling(window=w).apply(lambda x: np.all(x < threshold), raw=True)
# #             temp_array = temp_data['CBT'].to_numpy()
# #             temp_array = temp_array[~np.isnan(temp_array)]
# # =============================================================================
#             
#         # count occurences per month
#         temp_monthly = pd.Series(np.zeros(12), range(1,13), dtype='int64')
#         for idx in temp_idx:
#             if 0 <= idx < 744:   
#                 temp_monthly[1] += 1
#             elif 744 <= idx < 1416:
#                 temp_monthly[2] += 1
#             elif 1416 <= idx < 2160:
#                 temp_monthly[3] += 1
#             elif 2160 <= idx < 2880:
#                 temp_monthly[4] += 1
#             elif 2880 <= idx < 3624:
#                 temp_monthly[5] += 1
#             elif 3624 <= idx < 4344:
#                 temp_monthly[6] += 1
#             elif 4344 <= idx < 5088:
#                 temp_monthly[7] += 1
#             elif 5088 <= idx < 5832:
#                 temp_monthly[8] += 1
#             elif 5832 <= idx < 6552:
#                 temp_monthly[9] += 1
#             elif 6552 <= idx < 7296:
#                 temp_monthly[10] += 1
#             elif 7296 <= idx < 8016:
#                 temp_monthly[11] += 1
#             elif 8016 <= idx <= 8759:
#                 temp_monthly[12] += 1   
#                 
#         # count occurences per season
#         temp_season = pd.Series(np.zeros(4), range(1,5), dtype='int64')
#         for idx in temp_idx:
#             if 0 <= idx < 2160:   
#                 temp_season[1] += 1
#             elif 2160 <= idx < 4344:
#                 temp_season[2] += 1
#             elif 4344 <= idx < 6552:
#                 temp_season[3] += 1
#             elif 6552 <= idx < 8759:
#                 temp_season[4] += 1
#         
#         if temp_idx:
#             cbt_idx[y][w] = temp_idx
#             #cbt_df[y][w] = temp_df
#             cbt_monthly[y][w] = temp_monthly
#             cbt_season[y][w] = temp_season
#             
#             # new line in stdout
#             if len(temp_idx) == 1:
#                 print(' -> {} window found'.format(len(temp_idx)))
#             elif len(temp_idx) > 1:
#                 print(' -> {} windows found'.format(len(temp_idx)))
#             
#         # break inner for loop over years if no energy drougts determined
#         else:
#             print('No CBT energy drought windows longer than {} found.'.format(w-1))
#             break
# =============================================================================

# %% Steinbruch: CBT events separated
# =============================================================================
# #%% -- event perspective: consecutive hours constantly below threshold (cbt) --
# 
# option = 'option2'
# 
# start_time = time.perf_counter()
# 
# # set minimum and maximum window size in hours (1416 equal 2 months)
# windows = range(500, 9, -1)
# years = [1982]
# threshold = 0.1
# 
# # data container
# cbt_cf = {}
# cbt_idx = {}
# cbt_monthly = {}
# cbt_season = {}
# 
# # find numbers below threshold for all years, windows
# for y in years: 
#     # data container
#     cbt_cf[y] = {}
#     cbt_idx[y] = {}
#     cbt_monthly[y] = {}
#     cbt_season[y] = {}
# 
#     for w_count, w in enumerate(windows):
#         
#         # fetch data for first iteration, smaller windows use modified data
#         if w_count == 0:
#             # copy for analysis to be modified while counting energy droughts
#             ts = ts_long.copy()
#             temp_data_main = ts.loc[ts['year'] == y].copy()
#             
#             # add hours from previous year depending on window size, for first year, use last year
#             if y == years[0]:
#                 temp_data_pre = ts.loc[ts['year'] == years[-1]].copy()
#             else:
#                 temp_data_pre = ts.loc[ts['year'] == y-1].copy()
#             temp_data_pre = temp_data_pre.iloc[len(temp_data_pre)-w+1:len(temp_data_pre), :]
#             temp_data = pd.concat([temp_data_pre, temp_data_main])
#         
#         elif w_count > 0:
#             # use data including exluded hours and drop first instance
#             temp_data = temp_data.iloc[1:, :]
#             
#         # determine windows with moving average below threshold with window [t-w+1, t]
#         temp_data['CBT'] = temp_data.iloc[:,3].rolling(window=w).apply(lambda x: np.all(x < threshold), raw=True)
#         temp_array = temp_data['CBT'].to_numpy()
#         temp_array = temp_array[~np.isnan(temp_array)]
#         
#         # temporary data containers
#         temp_idx = []
#         #temp_df = []
#         
#         # progress documentation
#         step_description = 'Start search for CBT drought event --> threshold: {}, year: {}, period size: {}.'.format(threshold, y, w)
#         print(step_description, end = '')
#         
# # =============================================================================
# #         # Option1: find steaks
# #         df = pd.DataFrame(temp_array.copy(), columns=['v'])
# #         df['idx'] = df.index
# #         df['cs'] = (df['v'].diff(1) != 0).cumsum()
# #         result = pd.DataFrame({'BeginIdx' : df.groupby('cs').idx.first(), 
# #                       'EndIdx' : df.groupby('cs').idx.last(),
# #                       'Consecutive' : df.groupby('cs').size(),
# #                       'Value' : df.groupby('cs').v.first()}).reset_index(drop=True)
# #         result = result[result.Value != 0]
# #         result = result[result.Consecutive == 1]
# #         temp_idx.extend(result.EndIdx.values)
# #         for idx in result.EndIdx.values:
# #             # to exclude this window set all window values to 1e5 (data includes previous year)
# #             temp_data.iloc[np.arange(idx, idx+w), 3] = 1e5
# # =============================================================================
#         
#         # search first occurence of moving average below threshold         
#         while 1 in temp_array:
#             idx = np.where(temp_array == 1)[0][0]
#             temp_idx.append(idx)
#             #temp_df.append(temp_data.loc[idx-w+1:idx, :])
#                   
#             # check whether window overlaps with previous CBT windows (data includes previous year)
#             if np.any(temp_data.iloc[np.arange(idx, idx+w), 3].to_numpy() == 1):
#                 print('error at hour:', idx, '- overlapping with previously excluded CBT windows')
#                 break
#             
#             # exclude the CBT period [idx, idx+w] plus all [idx+w, idx+w+w-1] subsequent hours that not eligble 
#             # anymore due to the exlcusion of the CBT period
#             if idx+w <= len(temp_array):
#                 end = idx+w
#             else:
#                 end = len(temp_array)
#             temp_array[np.arange(idx, end)] = 0
#             
#             # to exclude this window set all window values to 1e5 (data includes previous year)
#             temp_data.iloc[np.arange(idx, idx+w), 3] = 1e5
#         
#         # check whether energy droughts are found               
#         if temp_idx:
#             # count occurences per month
#             temp_monthly = pd.Series(np.zeros(12), range(1,13), dtype='int64')
#             for idx in temp_idx:
#                 if 0 <= idx < 744:   
#                     temp_monthly[1] += 1
#                 elif 744 <= idx < 1416:
#                     temp_monthly[2] += 1
#                 elif 1416 <= idx < 2160:
#                     temp_monthly[3] += 1
#                 elif 2160 <= idx < 2880:
#                     temp_monthly[4] += 1
#                 elif 2880 <= idx < 3624:
#                     temp_monthly[5] += 1
#                 elif 3624 <= idx < 4344:
#                     temp_monthly[6] += 1
#                 elif 4344 <= idx < 5088:
#                     temp_monthly[7] += 1
#                 elif 5088 <= idx < 5832:
#                     temp_monthly[8] += 1
#                 elif 5832 <= idx < 6552:
#                     temp_monthly[9] += 1
#                 elif 6552 <= idx < 7296:
#                     temp_monthly[10] += 1
#                 elif 7296 <= idx < 8016:
#                     temp_monthly[11] += 1
#                 elif 8016 <= idx <= 8759:
#                     temp_monthly[12] += 1   
#                     
#             # count occurences per season
#             temp_season = pd.Series(np.zeros(4), range(1,5), dtype='int64')
#             for idx in temp_idx:
#                 if 0 <= idx < 2160:   
#                     temp_season[1] += 1
#                 elif 2160 <= idx < 4344:
#                     temp_season[2] += 1
#                 elif 4344 <= idx < 6552:
#                     temp_season[3] += 1
#                 elif 6552 <= idx < 8759:
#                     temp_season[4] += 1
#                     
#             # safe results
#             #cbt_cf[y][w] = temp_cf
#             cbt_idx[y][w] = temp_idx
#             #cbt_df[y][w] = temp_df
#             cbt_monthly[y][w] = temp_monthly
#             cbt_season[y][w] = temp_season
#             
#             # new line in stdout
#             if len(temp_idx) == 1:
#                 print(' -> {} event found'.format(len(temp_idx)))
#             elif len(temp_idx) > 1:
#                 print(' -> {} events found'.format(len(temp_idx)))
#             
#         # continue inner for loop over windows if no energy drougts determined
#         else:
#             # check whether energy droughts have been found at all
#             if not cbt_idx[y]:
#                 print(' -> none found')
#             else:
#                 print(' -> no additional events found')
#             continue
# 
# duration = time.perf_counter() - start_time
# print(f"Duration {duration} seconds")
# 
#  
# # data container
# cbt_count_yearly = {}
# cbt_count_monthly = {}
# cbt_count_season = {}
# for w in windows:
#     cbt_count_yearly[w] = pd.Series(index=years, dtype='int64')
#     cbt_count_monthly[w] = pd.DataFrame(index=range(1,13), columns=years, dtype='int64')
#     cbt_count_season[w] = pd.DataFrame(index=range(1,5), columns=years, dtype='int64')
# 
# # count occurences of windows
# for y in years:
#     for w in windows:
#         if w in cbt_idx[y]:
#             cbt_count_yearly[w][y] = len(cbt_idx[y][w])
#             cbt_count_monthly[w][y] = cbt_monthly[y][w]
#             cbt_count_season[w][y] = cbt_season[y][w]
# 
# # replace nan with zero
# for w in cbt_count_monthly.keys():
#     cbt_count_monthly[w] = cbt_count_monthly[w].fillna(0)
#     cbt_count_season[w] = cbt_count_season[w].fillna(0)
# 
# # erase empty entries        
# for w in list(cbt_count_yearly.keys()):
#     if (cbt_count_yearly[w] == 0).all():
#         del cbt_count_yearly[w]
#         del cbt_count_monthly[w]
#         del cbt_count_season[w]
# 
# # determine mean for monthly and seasonal results
# avg_monthly = {}
# avg_season = {}
# for w in cbt_count_monthly.keys():
#     avg_monthly[w] = cbt_count_monthly[w].mean(axis=1)
#     avg_season[w] = cbt_count_season[w].mean(axis=1)
# 
# # convert dict to df and name index     
# result_yearly = pd.DataFrame(cbt_count_yearly)
# result_avg_monthly = pd.DataFrame(avg_monthly)
# result_avg_season = pd.DataFrame(avg_season)
# result_avg_season.loc['Average'] = result_avg_season.mean(axis=0)
# 
# months = ['Janury', 'February', 'March', 'April', 'May', 'June', 'July', 
#           'August', 'September', 'October', 'November', 'December']
# result_avg_monthly['month'] = months
# result_avg_monthly.set_index('month', inplace=True)
# 
# seasons = ['Winter', 'Spring', 'Summer', 'Autumn', 'Average']
# result_avg_season['season'] = seasons
# result_avg_season.set_index('season', inplace=True)
# result_avg_season = result_avg_season.iloc[:, ::-1]
# =============================================================================
