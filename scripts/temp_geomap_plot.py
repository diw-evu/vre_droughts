# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 18:19:29 2023

@author: mkittel
"""
import matplotlib.pyplot as plt
import pandas as pd
import geopandas as gpd
# import contextily as ctx
# from shapely.geometry import Polygon
import pandas as pd
import matplotlib.pyplot as plt
import webbrowser
firefox_path = 'C:\\Program Files\\Mozilla Firefox\\firefox.exe'
webbrowser.register('firefox', None, webbrowser.BackgroundBrowser(firefox_path))


# %% F Neumann Kurs: https://fneum.github.io/data-science-for-esm/04-workshop-geopandas.html

fn = "https://raw.githubusercontent.com/PyPSA/powerplantmatching/master/powerplants.csv"
output = "C:\\owncloud_mkittel\\04_Projekte\\01_VRE_droughts\\03_results\\"
ppl = pd.read_csv(fn, index_col=0)
ppl.plot.scatter('lon', 'lat', s=ppl.Capacity/1e3)

geometry = gpd.points_from_xy(ppl['lon'], ppl['lat'])
gdf = gpd.GeoDataFrame(ppl, geometry=geometry, crs=4326)

plot = gdf.plot(column='Fueltype', markersize=gdf.Capacity/1e2)
my_map = gdf.explore(column='Fueltype')
my_map.save(f"{output}_map.html")
webbrowser.open('file://' + os.path.realpath(f"{output}_map.html"))

# ax1.set_extent([5, 16, 47, 55])  # DE
gdf.plot(
    ax=ax1,
    column='Fueltype',
    markersize=gdf.Capacity/1e2,
)

fig2 = plt.figure(figsize=(7,7))
ax2 = plt.axes(projection=ccrs.Mollweide())
ax2.stock_img()
gdf.plot(
    ax=ax2,
    column='Fueltype',
    markersize=gdf.Capacity/1e2,
)


fig = plt.figure(figsize=(7,7))
crs = ccrs.AlbersEqualArea()
ax = plt.axes(projection=crs)
gdf.to_crs(crs.proj4_init).plot(
    ax=ax,
    column='Fueltype',
    markersize=gdf.Capacity/1e2,
)
ax.coastlines()

#%%

url = "https://tubcloud.tu-berlin.de/s/RHZJrN8Dnfn26nr/download/NUTS_RG_10M_2021_4326.geojson"
nuts = gpd.read_file(url)
nuts = nuts.set_index('id')
nuts.geometry
nuts.crs
nuts.total_bounds

nuts0 = nuts.query("LEVL_CODE == 0")
map_nuts0 = nuts0.explore()
map_nuts0.save(f"{output}_map_nuts0.html")
webbrowser.open('file://' + os.path.realpath(f"{output}_map_nuts0.html"))
nuts0.to_file(f"{output}_nuts0.geojson")

nuts1 = nuts.query("LEVL_CODE == 1")
map_nuts1 = nuts1.explore()
map_nuts1.save(f"{output}_map_nuts1.html")
webbrowser.open('file://' + os.path.realpath(f"{output}_map_nuts1.html"))

nuts1.to_file(f"{output}_nuts1.geojson")

nuts0 = nuts0.to_crs(3035)
area = nuts0.area / 1e6
map_nuts0_area = nuts0.explore(column=area, vmax=1e5)
map_nuts0_area.save(f"{output}map_nuts0_area.html")
webbrowser.open('file://' + os.path.realpath(f"{output}map_nuts0_area.html"))


fig = plt.figure(figsize=(7,7))
ax = plt.axes(projection=ccrs.epsg(3035))
nuts1.plot(
    ax=ax,
    edgecolor='black',
    facecolor='lightgrey'
)
gdf.to_crs(3035).plot(
    ax=ax,
    column='Fueltype',
    markersize=gdf.Capacity/20,
    legend=True
)
ax.set_extent([5, 19, 47, 55])

joined = gdf.sjoin(nuts1)
cap = joined.groupby("NUTS_ID").Capacity.sum() / 1000 # GW
nuts1.index.difference(cap.index)
cap = cap.reindex(nuts1.index)

nuts1.plot(figsize=(7,7), column=cap, legend=True)

# %%

# Download a shapefile of the European Union countries
data_dir = "C:\\owncloud_mkittel\\04_Projekte\\01_VRE_droughts\\01_data\\europe\\eurostat\\ref-nuts-2021-01m.json\\"
path_rg = data_dir + "NUTS_RG_01M_2021_3035_LEVL_0.json"
eu_map_rg = gpd.read_file(path_rg)

# Create a map of the European Union member states
eu_map_rg.plot()

# Create a data frame with the data for the grouped bar chart
data = {'Country': ['Austria', 'Belgium', 'Bulgaria', 'Croatia', 'Cyprus', 'Czechia', 'Denmark', 'Estonia', 'Finland', 'France', 'Germany', 'Greece', 'Hungary', 'Ireland', 'Italy', 'Latvia', 'Lithuania', 'Luxembourg', 'Malta', 'Netherlands', 'Poland', 'Portugal', 'Romania', 'Slovakia', 'Slovenia', 'Spain', 'Sweden'],
        'Group': ['Group 1', 'Group 1', 'Group 1', 'Group 1', 'Group 1', 'Group 2', 'Group 2', 'Group 2', 'Group 2', 'Group 2', 'Group 3', 'Group 3', 'Group 3', 'Group 3', 'Group 3', 'Group 4', 'Group 4', 'Group 4', 'Group 4', 'Group 4', 'Group 5', 'Group 5', 'Group 5', 'Group 6', 'Group 6', 'Group 6', 'Group 6'],
        'Value': [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270]}
df = pd.DataFrame(data)

# Merge the data frame with the geopandas data frame
merged = eu_map_rg.merge(df, left_on='NUTS_NAME', right_on='Country')

# Group the merged data frame by country and the grouping variable
grouped = merged.groupby(['Country', 'Group']).sum()['Value']

# Create a grouped bar chart for each country
for country in merged['Country']:
    temp = grouped[country]
    temp.plot(kind='bar', title=country)
plt.show()

# %%

# load data

data_dir = "C:\\owncloud_mkittel\\04_Projekte\\01_VRE_droughts\\01_data\\europe\\eurostat\\ref-nuts-2021-01m.json\\"

path_rg = data_dir + "NUTS_RG_01M_2021_3035_LEVL_0.json"
gdf_rg = gpd.read_file(path_rg)

path_bn = data_dir + "NUTS_BN_01M_2021_3035_LEVL_0.json"
gdf_bn = gpd.read_file(path_bn)

path_lb = data_dir + "NUTS_LB_2021_3035_LEVL_0.json"
gdf_lb = gpd.read_file(path_lb)

# plot maps

ax = gdf_rg.plot(figsize=(20,15), color="gray")

plt.show()

gdf_bn.plot(figsize=(20,15), ax=ax, color="red")
gdf_lb.plot(figsize=(20,15), ax=ax, color="yellow")


# remove France from the dataset to make the picture more concise
#   (due to the France's overseas territories) - sorry, France :-/
gdf0 = gdf_rg[gdf_rg.CNTR_CODE != "FR"]

# the TopoJSON file does not contain information about the projection,
#  but we know from the file name that this is 3035
gdf0.crs = "EPSG:3035"

# reproject the GeoDataFrame into 4326 and save it as a copy
gdf1 = gdf0.to_crs("EPSG:4326")

# reproject the GeoDataFrame into 4326 and save it as a copy
gdf2 = gdf0.to_crs("EPSG:3857")

# plot both projections side by side
fig, axes = plt.subplots(1, 3, figsize=(20,8), dpi=150)
gdf0.plot(ax=axes[0])
axes[0].set_title(gdf0.crs, fontsize="20")
gdf1.plot(ax=axes[1])
axes[1].set_title(gdf1.crs, fontsize="20")
gdf2.plot(ax=axes[2])
axes[2].set_title(gdf2.crs, fontsize="20")

# hide ticks
for i in range(0, 3):

    axes[i].get_xaxis().set_ticks([])
    axes[i].get_yaxis().set_ticks([])


# Germany

path = data_dir + "NUTS_RG_01M_2021_3035_LEVL_1.json"
gdf = gpd.read_file(path)

# specify CRS (see the filename)
gdf.crs = "EPSG:3035"
gdf_de = gdf[gdf.CNTR_CODE == "DE"]

# we need to transform the data into the 3857 CRS because of the Contextily base map
gdf_de = gdf_de.to_crs("EPSG:3857")

# plot the German federated states (Bundesländer)
ax = gdf_de.plot(figsize=(20,15), color="lightgray")

# plot borders between the states green
gdf_de.boundary.plot(color="darkgreen", ax=ax)

# add background map by OpenStreetMap
# ctx.add_basemap(ax, source=ctx.providers.OpenStreetMap.HOT)


#%%

world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
europe = world[world.continent=='Europe']
europe.plot()
plt.show()

# Create a custom polygon
polygon = Polygon([(-25,35), (40,35), (40,75),(-25,75)])
poly_gdf = geopandas.GeoDataFrame([1], geometry=[polygon], crs=world.crs)
fig, ax = plt.subplots()
ax = europe.plot(ax=ax)
poly_gdf.plot(edgecolor='red', ax=ax, alpha=0.1)
plt.show()


#%%

from mpl_toolkits.basemap import Basemap
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches

# prep values for map extents and more
llcrnrlat = 35
llcrnrlon = -10
urcrnrlat = 70
urcrnrlon = 55
mid_lon = (urcrnrlon+llcrnrlon)/2.0
hr_lon = (urcrnrlon-llcrnrlon)/2.0
mid_lat = (urcrnrlat+llcrnrlat)/2.0
hr_lat = (urcrnrlat-llcrnrlat)/2.0

fig, ax = plt.subplots(figsize=(10, 9))  # bigger is better

bm = Basemap(llcrnrlat=llcrnrlat,
             llcrnrlon=llcrnrlon,
             urcrnrlat=urcrnrlat,
             urcrnrlon=urcrnrlon,
             ax=ax,
             resolution='i',
             projection='tmerc',
             lon_0=-2,
             lat_0=49)

bm.fillcontinents(color='lightyellow', zorder=0)
bm.drawcoastlines(color='gray', linewidth=0.3, zorder=2)


# ======================
# make-up some locations
# ----------------------
n = 50   # you may use 121 here
lon1s = mid_lon + hr_lon*(np.random.random_sample(n)-0.5)
lat1s = mid_lat + hr_lat*(np.random.random_sample(n)-0.5)

# make-up list of 3-values data for the locations above
# -----------------------------------------------------
bar_data = np.random.randint(1,5,[n,3])  # list of 3 items lists

# create a barchart at each location in (lon1s,lat1s)
# ---------------------------------------------------

# function to create inset axes and plot bar chart on it
# this is good for 3 items bar chart
def build_bar(mapx, mapy, ax, width, xvals=['a','b','c'], yvals=[1,4,2], fcolors=['r','y','b']):
    ax_h = inset_axes(ax, width=width, \
                    height=width, \
                    loc=3, \
                    bbox_to_anchor=(mapx, mapy), \
                    bbox_transform=ax.transData, \
                    borderpad=0, \
                    axes_kwargs={'alpha': 0.35, 'visible': True})
    for x,y,c in zip(xvals, yvals, fcolors):
        ax_h.bar(x, y, label=str(x), fc=c)
    #ax.xticks(range(len(xvals)), xvals, fontsize=10, rotation=30)
    ax_h.axis('off')
    return ax_h
bar_width = 0.1  # inch
colors = ['r','y','b']
for ix, lon1, lat1 in zip(list(range(n)), lon1s, lat1s):
    x1, y1 = bm(lon1, lat1)   # get data coordinates for plotting
    bax = build_bar(x1, y1, ax, 0.2, xvals=['a','b','c'], \
              yvals=bar_data[ix], \
              fcolors=colors)

# create legend (of the 3 classes)
patch0 = mpatches.Patch(color=colors[0], label='The 0-data')
patch1 = mpatches.Patch(color=colors[1], label='The 1-data')
patch2 = mpatches.Patch(color=colors[2], label='The 2-data')
ax.legend(handles=[patch0,patch1,patch2], loc=1)

plt.show()

# %% manual positioning

import geopandas as gpd
import matplotlib.pyplot as plt

import cartopy
import cartopy.crs as ccrs

world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
europe = world[world['continent'] == 'Europe']
europe = europe[(europe['name'] != 'Russia') & (europe['name'] != 'Moldava') & (europe['name'] != 'Iceland')]
europe['color'] = 'grey'
europe.set_index('name', inplace=True)
europe.loc[['Germany', 'Spain'], 'color'] = 'blue'
fig, ax = plt.subplots(figsize=(12, 12))
# ax_map = fig.add_axes([0, 0, 1, 1])
xlim = ([-12, 34])
ylim = ([35.7, 72])
ax.set_xlim(xlim)
ax.set_ylim(ylim)
europe.plot(ax=ax, color=europe.color)

ax_de = fig.add_axes([0.46, 0.43, 0.05, 0.05])
ax_de.bar([1, 2, 3], [1, 2, 3], color=['C1', 'C2', 'C3'])
ax_de.set_axis_off()

ax_es = fig.add_axes([0.2, 0.14, 0.05, 0.05])
ax_es.bar([1, 2, 3], [1, 2, 3], color=['C1', 'C2', 'C3'])
ax_es.set_axis_off()

plt.tight_layout()
plt.show()

# %% automatic positioning
import cartopy
import cartopy.crs as ccrs

ax = plt.axes(projection=ccrs.AlbersEqualArea()) # PlateCarree
ax.coastlines()
ax.add_feature(cartopy.feature.BORDERS, color='grey', linewidth=0.7)
ax.add_feature(cartopy.feature.OCEAN, color='azure')
ax.add_feature(cartopy.feature.LAND, color='cornsilk')
ax.set_extent([-12, 34, 35, 72])  # Europe

lat_long = {
    #'AT': (47.516231, 14.550072),
    'DE': (51.165691, 10.451526),
    'ES': (40.416775, -3.703790)
    }

# ax1.bar()
geometry = gpd.points_from_xy(ppl['lon'], ppl['lat'])
gdf = gpd.GeoDataFrame(ppl, geometry=geometry, crs=4326)

fig.plot(ax=ax_map)
lat, lon = 19.432608, -99.133208
ax_bar = fig.add_axes([0.5*(1+lon/180) , 0.5*(1+lat/90) , 0.05, 0.05])
ax_bar.bar([1, 2, 3], [1, 2, 3], color=['C1', 'C2', 'C3'])
ax_bar.set_axis_off()
plt.show()

# %%

import cartopy
import cartopy.crs as ccrs
import geopandas as gpd
import matplotlib.colors as colors

nuts_path = "C:\\owncloud_mkittel\\04_Projekte\\01_VRE_droughts\\01_data\\europe\\eurostat\\NUTS_RG_20M_2021_3857.geojson"
gdf = gpd.read_file(nuts_path)
gdf = gdf[gdf['LEVL_CODE'] == 0]
gdf = gdf[['CNTR_CODE', 'geometry']]
gdf = gdf.rename(columns={'CNTR_CODE': 'region'})

nuts_points_path = "C:\\owncloud_mkittel\\04_Projekte\\01_VRE_droughts\\01_data\\europe\\eurostat\\NUTS_LB_2021_3857.geojson"
gdf_points = gpd.read_file(nuts_points_path)
gdf_points = gdf_points.query("LEVL_CODE == 0")
gdf_points = gdf_points[['CNTR_CODE', 'geometry']]
gdf_points = gdf_points.rename(columns={'CNTR_CODE': 'region'})

gdf = pd.merge(df, gdf, on='region', how='outer')
gdf['weight'].fillna(0, inplace=True)
gdf = gpd.GeoDataFrame(gdf, geometry='geometry', crs=3857)

# %%

fig = plt.figure(figsize=(9, 10))
ax = plt.axes(projection=ccrs.epsg(3857))  # ax = plt.axes([0, 0, 1, 1], projection=ccrs.LambertCylindrical())

lat_eu_min = 35
lat_eu_max = 71
lon_eu_min = -12
lon_eu_max = 34

ax.set_extent([lon_eu_min, lon_eu_max, lat_eu_min, lat_eu_max])

# plot countries
gdf.plot(
    ax=ax,
    column='weight',
    edgecolor='gray',
    facecolor='lightgrey',
    legend=False
)

# add colorbar
norm = colors.Normalize(vmin=gdf.weight.min(), vmax=gdf.weight.max())
cbar = plt.cm.ScalarMappable(norm=norm)
ax_cbar = fig.colorbar(cbar, ax=ax)
ax_cbar.set_label('capacity-based weighting factor')
plt.tight_layout()

# Add bar plot DE
longitude, latitute = 0.38, 0.33
bar_ax = fig.add_axes([longitude, latitute, 0.05, 0.05])
bar_ax.bar([1, 2, 3], [1, 2, 3], color=['C1', 'C2', 'C3'])
bar_ax.set_axis_off()

# add bar plot ES
bar_ax = fig.add_axes([0.15, 0.1, 0.05, 0.05])
bar_ax.bar([1, 2, 3], [1, 2, 3], color=['C1', 'C2', 'C3'])
bar_ax.set_axis_off()

# other settings
plt.show()

# %%

import matplotlib.pyplot as plt
import numpy as np

thresholds = df.query('region == "DE"')['threshold'].to_numpy()
hours_weighted_region = df.query('region == "DE"')['hours*_region'].to_numpy()
hours_max_region = df.query('region == "DE"')['hours_max'].to_numpy()
year_weighted = df.query('region == "DE"')['year*_all'].to_numpy()
year_region = df.query('region == "DE"')['year_max'].to_numpy()

n_groups = len(thresholds)

fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.4

bar_weighted = plt.bar(index - bar_width/2, hours_weighted_region, bar_width, color='#2c7fb8', label='hours_weighted_region')
bar_maxmax = plt.bar(index + bar_width/2, hours_max_region, bar_width, color='#7fcdbb', label='hours_max_region')

# list of ticks: combine the ticks from both groups
# followed by the list of corresponding labels
# note that matplotlib takes care of sorting both lists while maintaining the correspondence
plt.xticks(np.append(index - bar_width/2, index + bar_width/2),  # tick positions
           [*year_weighted, *year_region],  # label corresponding to each tick
           rotation=90)

# put threshold names at right position below x_ticks

plt.ylabel('hours')
plt.legend()
plt.tight_layout()
plt.show()

# %%

import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure(figsize=(9, 10))
ax = plt.axes(projection=ccrs.epsg(3857))  # ax = plt.axes([0, 0, 1, 1], projection=ccrs.LambertCylindrical())

lat_eu_min = 35
lat_eu_max = 71
lon_eu_min = -12
lon_eu_max = 34

ax.set_extent([lon_eu_min, lon_eu_max, lat_eu_min, lat_eu_max])

# plot countries
gdf.plot(
    ax=ax,
    column='weight',
    edgecolor='gray',
    facecolor='lightgrey',
    legend=False
)

# add colorbar
norm = colors.Normalize(vmin=gdf.weight.min(), vmax=gdf.weight.max())
cbar = plt.cm.ScalarMappable(norm=norm)
ax_cbar = fig.colorbar(cbar, ax=ax)
ax_cbar.set_label('capacity-based weighting factor')
plt.tight_layout()


# Add bar plot DE
longitude, latitute = 0.38, 0.33
bar_ax = fig.add_axes([longitude, latitute, 0.05, 0.05])
thresholds = df.query('region == "DE"')['threshold'].to_numpy()
thres0 = df[df['threshold'] == thresholds[0]].query('region == "DE"')[['hours*_region', 'hours_max']].to_numpy()[0]
thres1 = df[df['threshold'] == thresholds[1]].query('region == "DE"')[['hours*_region', 'hours_max']].to_numpy()[0]
thres2 = df[df['threshold'] == thresholds[2]].query('region == "DE"')[['hours*_region', 'hours_max']].to_numpy()[0]
thres3 = df[df['threshold'] == thresholds[3]].query('region == "DE"')[['hours*_region', 'hours_max']].to_numpy()[0]
hours_max_region = df.query('region == "DE"')['hours_max'].to_numpy()
year_weighted = df.query('region == "DE"')['year*_all'].to_numpy()
year_region = df.query('region == "DE"')['year_max'].to_numpy()

n_groups = 2

# fig, bar_ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.2
pos0 = index - bar_width/2
pos1 = index + bar_width/2
pos2 = index + bar_width*1.5
pos3 = index + bar_width*2.5

bar0 = plt.bar(pos0, thres0, bar_width, color='#2c7fb8', label=thresholds[0])
bar1 = plt.bar(pos1, thres1, bar_width, color='#7fcdbb', label=thresholds[1])
bar2 = plt.bar(pos2, thres2, bar_width, color='#9c7fb8', label=thresholds[2])
bar3 = plt.bar(pos3, thres3, bar_width, color='#7c73b8', label=thresholds[3])

bar_ax.bar_label(bar0, label_type='center', color='white', rotation=90, fontsize=8)
bar_ax.bar_label(bar1, label_type='center', color='white', rotation=90, fontsize=8)
bar_ax.bar_label(bar2, label_type='center', color='white', rotation=90, fontsize=8)
bar_ax.bar_label(bar3, label_type='center', color='white', rotation=90, fontsize=8)

bar_ax.spines['top'].set_visible(False)
bar_ax.spines['right'].set_visible(False)
#ax.spines['bottom'].set_visible(False)
bar_ax.spines['left'].set_visible(False)
bar_ax.get_yaxis().set_ticks([])
bar_ax.set_facecolor(None)
bar_ax.patch.set_facecolor(None)

# list of ticks: combine the ticks from both groups
# followed by the list of corresponding labels
# note that matplotlib takes care of sorting both lists while maintaining the correspondence
bar_ax.set_xticks([*pos0, *pos1, *pos2, *pos3])  # tick positions
bar_ax.set_xticklabels([*year_weighted, *year_region], rotation=90, fontsize=6)   # label corresponding to each tick

#ax.set_ylabel('hours')

# box = ax.get_position()
#ax.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
#ax.subplots_adjust(top=0.99,bottom=0.2,left=0.105,right=0.995,hspace=0.2,wspace=0.2)

#ax.text(0.25, 0.12, 'year_weighted_region', ha='center', va='center', fontsize=13)
#ax.text(0.75, 0.12, 'year_max_region', ha='center', va='center', fontsize=13)

#plt.legend()
#plt.tight_layout()
plt.show()


#%%




#%%

import cartopy.crs as ccrs
import geopandas as gpd

# import and reduce country polygon data
gdf = gpd.read_file(nuts_path)
gdf = gdf[gdf['LEVL_CODE'] == 0]
gdf = gdf[['CNTR_CODE', 'geometry']]
gdf = gdf.rename(columns={'CNTR_CODE': 'region'})
gdf = gpd.GeoDataFrame(gdf, geometry='geometry', crs=3857)

# import and reduce country point data
nuts_points_path = '.\NUTS_LB_2021_3857.geojson'
gdf_points = gpd.read_file(nuts_points_path)
gdf_points = gdf_points.query("LEVL_CODE == 0")
gdf_points = gdf_points[['CNTR_CODE', 'geometry']]
gdf_points = gdf_points.rename(columns={'CNTR_CODE': 'region'})

# plot
fig = plt.figure(figsize=(9, 10))
ax = plt.axes(projection=ccrs.epsg(3857))

# extent
lat_eu_min = 35
lat_eu_max = 71
lon_eu_min = -12
lon_eu_max = 34
ax.set_extent([lon_eu_min, lon_eu_max, lat_eu_min, lat_eu_max])

# plot countries
gdf.plot(
    ax=ax,
    edgecolor='black',
    facecolor='lightgrey',
)

# manual positing
longitude, latitute = 0.46, 0.33

# add bar plot
bar_ax = fig.add_axes([longitude, latitute, 0.05, 0.05])
bar_ax.bar([1, 2, 3], [1, 2, 3], color=['C1', 'C2', 'C3'])
bar_ax.set_axis_off()

plt.tight_layout()
plt.show()

# %%

# =============================================================================
# # plot country centroids
# gdf_points.plot(
#     ax=ax,
#     color="yellow",
# )
# 
# =============================================================================

lat_norm = (lat - lat_eu_min) / (lat_eu_max - lat_eu_min)
lon_norm = (lon - lon_eu_min) / (lon_eu_max - lon_eu_min)

ax_de = fig.add_axes([0.46, 0.43, 0.05, 0.05])
ax_de.bar([1, 2, 3], [1, 2, 3], color=['C1', 'C2', 'C3'])
ax_de.set_axis_off()

ax_es = fig.add_axes([0.2, 0.14, 0.05, 0.05])
ax_es.bar([1, 2, 3], [1, 2, 3], color=['C1', 'C2', 'C3'])
ax_es.set_axis_off()

plt.show()

# =============================================================================
# gdf.plot(
#     ax=ax,
#     column='Fueltype',
#     markersize=gdf.Capacity/20,
#     legend=True
# )
# =============================================================================



# %%
import matplotlib.pyplot as plt
import numpy as np
figu = plt.figure()  
r = figu.patch 
r.set_facecolor('lightslategray')

axes = figu.add_axes([0, 0.4, 0.1, 1])
axes = figu.add_axes([1, 1, 0.2, 0.3])
plt.show()
